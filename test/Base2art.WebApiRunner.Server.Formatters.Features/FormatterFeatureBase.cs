﻿namespace Base2art.WebApiRunner.Server.Formatters.Features
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using FluentAssertions;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Extensions.Primitives;

    public abstract class FormatterFeatureBase
    {
        private TextWriter CreateWriter(Stream stream, Encoding encoding, StringWriter sr) => sr;

        protected async void ShouldFormatItem<T>(TextOutputFormatter formatter, T input, string expected)
        {
            var mediaType = formatter.SupportedMediaTypes.First();

            var expectedContentType = new StringSegment(mediaType);

            using (var sr = new StringWriter(CultureInfo.InvariantCulture))
            {
                var item = input;
                var context = new OutputFormatterWriteContext(
                                                              new DefaultHttpContext(),
                                                              (d, x) => this.CreateWriter(d, x, sr),
                                                              typeof(T),
                                                              item);

                context.ContentType = new StringSegment(mediaType);

                if (formatter.CanWriteResult(context))
                {
                    // Act
                    await formatter.WriteResponseBodyAsync(context, Encoding.Default);
                    var actual = sr.GetStringBuilder()
                                   .ToString()
                                   .Trim()
                                   .Replace("\r", string.Empty);

                    actual.Should().Be(expected);
                }
                else
                {
                    "NO_WRITE".Should().Be(expected);
                }
            }
        }

        protected async void ShouldFormatItem(TextOutputFormatter formatter, object input, Type objectType, string expected)
        {
            var mediaType = formatter.SupportedMediaTypes.First();

            var expectedContentType = new StringSegment(mediaType);

            using (var sr = new StringWriter())
            {
                var item = input;
                var context = new OutputFormatterWriteContext(
                                                              new DefaultHttpContext(),
                                                              (d, x) => this.CreateWriter(d, x, sr),
                                                              objectType,
                                                              item);

                context.ContentType = new StringSegment(mediaType);

                if (formatter.CanWriteResult(context))
                {
                    // Act
                    await formatter.WriteResponseBodyAsync(context, Encoding.Default);
                    var actual = sr.GetStringBuilder().ToString().Trim();

                    actual.Should().Be(expected);
                }
                else
                {
                    "NO_WRITE".Should().Be(expected);
                }
            }
        }
    }
}

/*
 *
        protected abstract TextOutputFormatter CreateFormatter();


        protected abstract string GetExpectedSingleItem();
        protected abstract object GetSingleItem();
       
        public virtual async void ShouldFormatSingleItem()
        {
            var formatter = this.CreateFormatter();
            var expected = this.GetExpectedSingleItem();
            
            var mediaType = formatter.SupportedMediaTypes.First();
            
            var expectedContentType = new StringSegment(mediaType);

            using (var sr = new StringWriter())
            {
                var item = this.GetSingleItem();
                var context = new OutputFormatterWriteContext(
                                                              new DefaultHttpContext(),
                                                              (d, x) => this.CreateWriter(d, x, sr),
                                                              item.GetType(),
                                                              item);

                context.ContentType = new StringSegment(mediaType);

                // Act
                await formatter.WriteResponseBodyAsync(context, Encoding.Default);
                sr.GetStringBuilder().ToString().Trim().Should().Be(this.GetExpectedSingleItem());
            }
        }
 * 
 */