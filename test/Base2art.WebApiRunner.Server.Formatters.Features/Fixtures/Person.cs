﻿namespace Base2art.WebApiRunner.Server.Formatters.Features.Fixtures
{
    using System;

    public class Person
    {
        public string name { get; set; }

        public DateTime BirthDay { get; set; }
    }
}