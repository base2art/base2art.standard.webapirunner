﻿namespace Base2art.WebApiRunner.Server.Formatters.Features
{
    using System;
    using Fixtures;
    using Xunit;

    public class MarkdownTableFormatterFeature : FormatterFeatureBase
    {
        private readonly MarkdownOutputFormatter formatter = new MarkdownOutputFormatter();

        [Fact]
        public void ShouldFormatMultipleItems()
        {
            var expected = @"
name | BirthDay
---|---
Scott ""The Man"" Person | 1988-03-02T00:00:00
RedGrin Grumble ⎮ Imaginary ⎮ | 1970-01-01T00:00:00
".Replace("\r\n", "\n").Trim();

            var input = new object[]
                        {
                            new {name = "Scott \"The Man\" Person", BirthDay = new DateTime(1988, 03, 02)},
                            new {name = "RedGrin Grumble | Imaginary |", BirthDay = new DateTime(1970, 01, 01)}
                        };

            this.ShouldFormatItem(this.formatter, input, expected);
        }

        [Fact]
        public void ShouldFormatMultipleItemsTyped()
        {
            var expected = @"
name | BirthDay
---|---
Scott ""The Man"" Person | 1988-03-02T00:00:00
RedGrin Grumble ⎮ Imaginary ⎮ | 1970-01-01T00:00:00
".Replace("\r\n", "\n").Trim();

            var input = new[]
                        {
                            new Person {name = "Scott \"The Man\" Person", BirthDay = new DateTime(1988, 03, 02)},
                            new Person {name = "RedGrin Grumble | Imaginary |", BirthDay = new DateTime(1970, 01, 01)}
                        };

            this.ShouldFormatItem(this.formatter, input, expected);
        }

        [Fact]
        public void ShouldFormatSingleItem()
        {
            var input = new {name = "RedGrin Grumble", BirthDay = new DateTime(1970, 01, 01)};

            this.ShouldFormatItem(this.formatter, input, "NO_WRITE");
        }
    }
}