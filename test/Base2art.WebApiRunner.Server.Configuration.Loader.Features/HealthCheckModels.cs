namespace Base2art.WebApiRunner.Server.Configuration.Loader.Features
{
    using System.Collections.Generic;
    using FluentAssertions;
    using Routing.Models;
    using Xunit;

    public class HealthCheckModels
    {
        [Theory]
        [InlineData(null, null, null)]
        [InlineData(true, null, null)]
        [InlineData(false, null, false)]
        [InlineData(null, true, null)]
        [InlineData(null, false, false)]
        [InlineData(false, false, false)]
        [InlineData(true, false, false)]
        [InlineData(false, true, false)]
        [InlineData(true, true, true)]
        public void ShouldAggregateCorrectly(bool? input1, bool? input2, bool? expected)
        {
            var result = new HealthCheckResult();

            result.Items = new List<KeyValuePair<string, HealthCheckResultItem>>();
            result.Items.Add(new KeyValuePair<string, HealthCheckResultItem>("Item0", new HealthCheckResultItem {IsHealthy = true}));
            result.Items.Add(new KeyValuePair<string, HealthCheckResultItem>("Item1", new HealthCheckResultItem {IsHealthy = input1}));
            result.Items.Add(new KeyValuePair<string, HealthCheckResultItem>("Item2", new HealthCheckResultItem {IsHealthy = input2}));
            result.Items.Add(new KeyValuePair<string, HealthCheckResultItem>("Item3", new HealthCheckResultItem {IsHealthy = true}));

            result.IsHealthy.Should().Be(expected);
        }

        [Fact]
        public void ShouldAggregateCorrectlyOnNull()
        {
            var result = new HealthCheckResult();

            result.IsHealthy.Should().BeNull();
        }
    }
}