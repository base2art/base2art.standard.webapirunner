namespace Base2art.WebApiRunner.Server.Configuration.Loader.Features
{
    using System.Collections.Generic;
    using FluentAssertions;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Runners;
    using Xunit;

    public class ParameterMapperFeature
    {
        [Fact]
        public void ShouldLoad()
        {
            var input = new Dictionary<string, object>();

            var output = input.MapParametersFor(typeof(PersonService));
            output.Count.Should().Be(0);
        }
        
        [Fact]
        public void ShouldLoadAll()
        {
            var input = new Dictionary<string, object>();
            input["i"] = 1;
            input["lookup"] = null;

            var output = input.MapParametersFor(typeof(PersonService));
            output.Count.Should().Be(2);
        }
        
        [Theory]
        [InlineData("i", 0)]
        [InlineData("lookup", null)]
        public void ShouldLoadValue(string name, object value)
        {
            var input = new Dictionary<string, object>();
            input[name] = value;

            var output = input.MapParametersFor(typeof(PersonService));
            output.Count.Should().Be(1);
        }

        private class PersonService
        {
            public PersonService(IExceptionFilter filter)
            {
            }

            public PersonService(IExceptionFilter filter, int i)
            {
            }

            public PersonService(IExceptionFilter filter, Dictionary<string, string> lookup)
            {
            }

            public PersonService(IExceptionFilter filter, Dictionary<string, string> lookup, int i)
            {
            }
        }
    }
}