namespace Base2art.WebApiRunner.Server.Configuration.Loader.Features
{
    using System.Collections.Generic;
    using System.Reflection;
    using Reflection.Discovery;

    public class TestAssemblyFactory : IAssemblyFactory
    {
        public Assembly CreateFromPath(string path, bool forceRootLoadingLoader) => Assembly.LoadFile(path);

        public void RegisterUnmanaged(IReadOnlyList<IPackagedNativeDll> valueTupleUnmanaged)
        {
        }

        public bool Supports(string rid) => false;
    }
}