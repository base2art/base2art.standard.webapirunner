namespace Base2art.WebApiRunner.Server.Configuration.Loader.Features
{
    using System;
    using System.Collections;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using AssemblyLoading;
    using CommandLine;
    using FluentAssertions;
    using IO;
    using Reflection.Discovery;
    using Runners.Configuration;
    using Web.App.Configuration;
    using Xunit;

    public class ConfigInjectionFeature
    {
        private class WorkingDirectoryProvider : IWorkingDirectoryProvider
        {
            public WorkingDirectoryProvider(string cd) => this.WorkingDirectory = cd;
            public string WorkingDirectory { get; }
        }

        private class AspectLookup : IAspectLookup
        {
            public ITypeInstanceConfiguration GetByName(string name) => null;
        }

        [Fact]
        public void ShouldLoad()
        {
            var content = Assembly.GetExecutingAssembly().GetResourceAsText("Resources/sample-complex-injection.yaml");
            var tmpFile = Path.GetTempFileName();
            File.WriteAllText(tmpFile, content);

            var provider = new ConfigurationProvider<ConsoleServerConfiguration>(tmpFile);

            var configPrimitive = provider.Configuration ?? new ConsoleServerConfiguration();

            ObjectInitializer.SetDefaultValue(() => configPrimitive.ServerSettings);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.ServerSettings.Http);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.AdminServerSettings);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.AdminServerSettings.Http);

            ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies.Names);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies.SearchPaths);

            ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints.Aspects);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints.Urls);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.EndpointsConfigurationItems);

            ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks.Aspects);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks.Items);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.TasksConfigurationItems);

            ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks.Aspects);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks.Items);
            ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecksConfigurationItems);

            var cd = Environment.CurrentDirectory;
            var asmLoader = new AssemblyLoader(
                                               new TestAssemblyFactory(),
                                               new WorkingDirectoryProvider(cd),
                                               cd,
                                               provider.JointAssemblies()
                                                       .Select(x => AssemblyLookupDataMapper.Map(x.Names,
                                                                                                 x.SearchPaths,
                                                                                                 x.Packages,
                                                                                                 x.SystemAssemblies))
                                                       .ToArray(),
                                               FrameworkType.NetCore,
                                               new[] {"netstandard2.0"});
            IServerConfiguration config = new ServerConfigurationWrapper(asmLoader,
                                                                         new TypeLoader(asmLoader),
                                                                         new AspectLookup(),
                                                                         provider);

            config.Endpoints.Should().HaveCount(1);

            var parms = config.Endpoints[0].Parameters;
            parms.Should().HaveCount(1);
            var parmsArray = parms.ToArray();
            parmsArray[0].Key.Should().Be("items");
            parmsArray[0].Value.Should().NotBeNull();
            var val = parmsArray[0].Value as IEnumerable;

            val.Should().HaveCount(2);
            var enumerator = val.GetEnumerator();
            enumerator.MoveNext();
            var c = enumerator.Current;
            c.Should().Be(3);
            enumerator.MoveNext();
            c = enumerator.Current;
            c.Should().Be(2);
        }
    }
}