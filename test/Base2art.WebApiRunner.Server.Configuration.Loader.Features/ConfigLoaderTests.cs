namespace Base2art.WebApiRunner.Server.Configuration.Loader.Features
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using CommandLine;
    using FluentAssertions;
    using IO;
    using Xunit;

    public class ConfigLoaderTests
    {
        private string Setup(string resourceName)
        {
            var content = Assembly.GetExecutingAssembly().GetResourceAsText(resourceName);
            var tmpFile = Path.GetTempFileName();
            File.WriteAllText(tmpFile, content);
            return tmpFile;
        }

        private void Cleanup(string tmpFile)
        {
            File.Delete(tmpFile);
        }

        [Fact]
        public void Test1()
        {
            var tmpFile = this.Setup("Resources/sample1.yaml");

            var initer = new ConfigurationProvider<ServerConfiguration>(tmpFile);
            initer.Configuration.Should().NotBeNull();
            initer.Configuration.Assemblies.Names.Should().HaveCount(1)
                  .And.Subject.First().Should().Be("abc");

            this.Cleanup(tmpFile);
        }

        [Fact]
        public void Test2()
        {
            var tmpFile = this.Setup("Resources/sample2.yaml");

            var initer = new ConfigurationProvider<ConsoleServerConfiguration>(tmpFile);
            initer.Configuration.Should().NotBeNull();
            initer.Configuration.ServerSettings.Http[0].Host.Should().Be("localhost");
            initer.Configuration.ServerSettings.Http[0].Port.Should().Be(8080);
            initer.Configuration.AdminServerSettings.Http[0].Host.Should().Be("localhost");
            initer.Configuration.AdminServerSettings.Http[0].Port.Should().Be(8081);

            initer.Configuration.Cors.Items[0].RequestHost.Should().Be("base2art.com");

            initer.Configuration.Injection.Items
                  .First(x => x.RequestedType.Value == "Base2art.WebApiRunner.IMetricLogger")
                  .Parameters["basePath"]
                  .Should().Be("C:\\logs\\b2a\\tests\\");

            var userLookup = initer.Configuration.Injection.Items
                                   .First(x => x.RequestedType.Value == "Base2art.WebApiRunner.Security.IRoleLookup")
                                   .Parameters["userLookup"];

            var item = userLookup.As<Dictionary<object, object>>();
            item.Should().NotBeNull();

            item.Should().ContainKey("test@base2art.com");
            item["test@base2art.com"].Should().BeOfType<List<object>>();
            ((List<object>) item["test@base2art.com"])[0].Should().Be("user");
            ((List<object>) item["test@base2art.com"])[1].Should().Be("everyone");

            // Unfortunately auto conversion is not currently happening.
            var listOfRolesObject = item["test@base2art.com"];
            var listOfRoles = ((List<object>) listOfRolesObject).Select(x => x.ToString()).ToArray();
            listOfRoles.Should().HaveCount(2);
            this.Cleanup(tmpFile);
        }

        [Fact]
        public void Test3()
        {
            var content = Assembly.GetExecutingAssembly().GetResourceAsText("Resources/sample3.yaml");
            var tmpFile = Path.GetTempFileName();
            File.WriteAllText(tmpFile, content);

            content = Assembly.GetExecutingAssembly().GetResourceAsText("Resources/sample3.tasks.yaml");
            var tmpFile2 = Path.Combine(Path.GetDirectoryName(Path.GetTempFileName()), "sample3.tasks.yaml");
            File.WriteAllText(tmpFile2, content);

            var initer = new ConfigurationProvider<ConsoleServerConfiguration>(tmpFile);
            initer.Configuration.Should().NotBeNull();

            initer.JointEndpoints().SelectMany(x => x.Urls).Should().HaveCount(43);
            initer.JointTasks().SelectMany(x => x.Items).Should().HaveCount(2);

            initer.JointTasks().SelectMany(x => x.Items).ToArray()[0].Name.Should().Be("create-file-simple");
            initer.JointTasks().SelectMany(x => x.Items).ToArray()[1].Name.Should().Be("throw-exception");
            initer.JointTasks().SelectMany(x => x.Items).ToArray()[1].Type.Value.Should()
                  .Be("Base2art.SampleApi.Public.Tasks.TimedExceptionThrowingTask");
            initer.JointTasks().SelectMany(x => x.Items).ToArray()[1].Delay.TotalMilliseconds.Should().Be(200);
            initer.JointTasks().SelectMany(x => x.Items).ToArray()[1].Interval.TotalMinutes.Should().Be(1);

            /*
            type: Base2art.SampleApi.Public.Tasks.TimedExceptionThrowingTask
            delay: 
            interval: 00:01:00*/

            File.Delete(tmpFile);
            File.Delete(tmpFile2);
        }
    }
}