namespace Base2art.WebApiRunner.Server.Features.Integration
{
    using AssemblyLoading;

    public class WorkingDirectoryProvider : IWorkingDirectoryProvider
    {
        public WorkingDirectoryProvider(string currentDirectory) => this.WorkingDirectory = currentDirectory;

        public string WorkingDirectory { get; }
    }
}