namespace Base2art.WebApiRunner.Server.Features.Integration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using AssemblyLoading;
    using Fixtures;
    using FluentAssertions;
    using Logging;
    using Reflection.Discovery;
    using Runners.CommandLineInterface;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Testability.Configuration;
    using Web.App.Configuration;
    using Web.Server.Registration;
    using Xunit;
    using Xunit.Abstractions;

    public class SimpleEndpointFeature
    {
        public SimpleEndpointFeature(ITestOutputHelper testOutputHelper) => this.testOutputHelper = testOutputHelper;

        private readonly ITestOutputHelper testOutputHelper;

        private class TestRouteConfigurationRegistrarInternal : RegistrationBase
        {
            public override void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
            {
                base.RegisterRoutes(router, provider, config);

                router.MapRoute<WebSettingsController>(HttpMethod.Get, "/settings", x => x.GetSettings(), RouteType.Endpoint);
            }
        }

        [Fact]
        public async void JsonInputFeature()
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrarInternal()))
            {
                var client = fixture.Client;


                var response = await client.GetAsync("http://localhost/settings");

                var responseString = await response.Content.ReadAsStringAsync();
                testOutputHelper.WriteLine(responseString);
                response.EnsureSuccessStatusCode();

                // var responseString = await response.Content.ReadAsStringAsync();
                responseString.Trim().Should().StartWith("{\"ContentRoot\":".Replace("\r", string.Empty).Trim());
            }
        }

#if Windows_NT
        [Theory]
        [InlineData(true, true)]
        [InlineData(true, false)]
        [InlineData(false, true)]
        [InlineData(false, false)]
#endif
        public async void ShouldProcessEndpointActiveDirectory(bool useNames, bool usePackages)
        {
            var workingDir = new WorkingDirectoryProvider(Environment.CurrentDirectory);
            var assemblyLookupData = new AssemblyLookupData();

            if (useNames)
            {
                assemblyLookupData.Names.AddRange(new List<string>
                                                  {
                                                      "Base2art.Standard.WebApiRunner.Samples",
                                                      "System.DirectoryServices",
                                                      "System.DirectoryServices.AccountManagement",
                                                      "System.Security.Principal.Windows"
                                                  });
            }

            if (usePackages)
            {
                assemblyLookupData.Packages.Add("System.DirectoryServices.AccountManagement");
            }

            var assemblyLoader = new AssemblyLoader(
                                                    new AssemblyFactory(),
                                                    workingDir,
                                                    "sample-1.yaml",
                                                    new IAssemblyLookupData[] {assemblyLookupData},
                                                    FrameworkType.NetCore,
                                                    new[] {"netstandard2.0"});
            assemblyLoader.Assemblies();
            var serverConfiguration = ServerConfiguration.Create(assemblyLoader);

            serverConfiguration.Endpoints.Add(new EndpointConfiguration
                                              {
                                                  Verb = HttpVerb.Get,
                                                  Url = "",
                                                  Type = typeof(ActiveDirectoryLookupController),
                                                  Method = RouteManagers.MethodOf<ActiveDirectoryLookupController>(x => x.List())
                                              });

            using (var fixture = IntegrationTests.CreateFixture(serverConfiguration))
            {
                var client = fixture.Client;
                var request = new HttpRequestMessage
                              {
                                  RequestUri = new Uri("http://localhost/"),
                                  Method = HttpMethod.Get
                              };

                var response = await client.SendAsync(request);

//                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();

                bool In<T>(T item, IComparer<T> comp, params T[] values)
                    => (values ?? new T[0]).Any(x => comp.Compare(item, x) == 0);

                In(
                   responseString.Trim(),
                   StringComparer.OrdinalIgnoreCase,
                   "S-1-5-21-2041924414-608314587-1170935872-65151",
                   "S-1-5-21-22944269-532726824-3940224087-500")
                    .Should().BeTrue();
            }
        }

        private class AssemblyLookupData : IAssemblyLookupData
        {
            public List<string> Names { get; } = new List<string>();
            public List<string> SearchPaths { get; } = new List<string>();
            public List<string> Packages { get; } = new List<string>();
            public List<string> SystemAssemblies { get; } = new List<string>();
            IReadOnlyList<string> IAssemblyLookupData.Names => this.Names;
            IReadOnlyList<string> IAssemblyLookupData.SearchPaths => this.SearchPaths;
            IReadOnlyList<string> IAssemblyLookupData.Packages => this.Packages;
            IReadOnlyList<string> IAssemblyLookupData.SystemAssemblies => this.SystemAssemblies;
        }

        public class OutputWrapper : StringWriter
        {
            private readonly ITestOutputHelper testOutputHelper1;

            public OutputWrapper(ITestOutputHelper testOutputHelper1) => this.testOutputHelper1 = testOutputHelper1;

            public override void WriteLine(string value)
            {
                this.testOutputHelper1.WriteLine(value);
            }
        }

        [Fact]
        public async void ShouldProcessEndpointSqlite()
        {
            try
            {
                Logger.Writer = this.testOutputHelper.WriteLine;
                Logger.Level = LogLevel.Debug;
//            var path = Environment.GetEnvironmentVariable("PATH");

//            var basePath = @"C:\Program Files\dotnet\sdk\NuGetFallbackFolder\sqlitepclraw.lib.e_sqlite3.v110_xp\1.1.11\runtimes";
//            var basePath1 = @"C:\Program Files\dotnet\sdk\NuGetFallbackFolder\sqlitepclraw.lib.e_sqlite3.v110_xp\1.1.11\runtimes\win-arm\native";
//            var basePath2 = @"C:\Program Files\dotnet\sdk\NuGetFallbackFolder\sqlitepclraw.lib.e_sqlite3.v110_xp\1.1.11\runtimes\win-x64\native";
////// 
//            Environment.SetEnvironmentVariable(
//                                               "PATH",
//                                               $"{path}{Path.PathSeparator}{basePath1}{Path.PathSeparator}{basePath2}");

                var workingDir = Environment.CurrentDirectory;
                var assemblyLookupData = new AssemblyLookupData();

                assemblyLookupData.Packages.Add("Microsoft.Data.Sqlite.Core");
                assemblyLookupData.Packages.Add("sqlitepclraw.*");
//            assemblyLookupData.Packages.Add("sqlitepclraw.bundle_e_sqlite3");
//            assemblyLookupData.Packages.Add("sqlitepclraw.provider.e_sqlite3.netstandard11");
//            assemblyLookupData.Packages.Add("sqlitepclraw.lib.e_sqlite3.*");
//            assemblyLookupData.Packages.Add("sqlitepclraw.lib.e_sqlite3.linux");
//            assemblyLookupData.Packages.Add("sqlitepclraw.lib.e_sqlite3.v110_xp");

                assemblyLookupData.Names.AddRange(new List<string>
                                                  {
                                                      "Base2art.Standard.WebApiRunner.Samples"
                                                  });

                var assemblyLoader = new AssemblyLoader(
                                                        new AssemblyFactory(),
                                                        new WorkingDirectoryProvider(workingDir),
                                                        "sample-1.yaml",
                                                        new IAssemblyLookupData[] {assemblyLookupData},
                                                        FrameworkType.NetCore,
                                                        new[] {"netstandard2.0"});
                assemblyLoader.Assemblies();
                var serverConfiguration = ServerConfiguration.Create(assemblyLoader);

                serverConfiguration.Endpoints.Add(new EndpointConfiguration
                                                  {
                                                      Verb = HttpVerb.Get,
                                                      Url = "",
                                                      Type = typeof(SqliteReaderController),
                                                      Method = RouteManagers.MethodOf<SqliteReaderController>(x => x.List())
                                                  });

                using (var fixture = IntegrationTests.CreateFixture(serverConfiguration))
                {
                    var client = fixture.Client;
                    var request = new HttpRequestMessage
                                  {
                                      RequestUri = new Uri("http://localhost/"),
                                      Method = HttpMethod.Get
                                  };

                    var response = await client.SendAsync(request);

//                response.EnsureSuccessStatusCode();

                    var responseString = await response.Content.ReadAsStringAsync();
                    responseString.Trim().Should().Be(@"[""Scott"",""Matt""]".Replace("\r", string.Empty).Trim());
                }
            }
            finally
            {
                Logger.Writer = null;
            }
        }
    }
}