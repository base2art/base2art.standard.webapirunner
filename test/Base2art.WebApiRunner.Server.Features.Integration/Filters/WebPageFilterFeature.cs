﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Filters
{
    using System;
    using System.Net;
    using Fixtures;
    using FluentAssertions;
    using Testability;
    using Testability.Configuration;
    using Xunit;

    public class WebPageFilterFeature
    {
        [Theory]
        [InlineData("html", "text/html")]
        [InlineData("js", "application/javascript")]
        public async void GetFile(string extension, string expectedContentType)
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;

                var url = $"http://localhost/index.{extension}";
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be(HttpStatusCode.OK);
                response.Content.Headers.ContentType.MediaType.Should().Be(expectedContentType);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = DateTime.UtcNow.Date.ToString("yyyy-MM-dd").Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }
    }
}