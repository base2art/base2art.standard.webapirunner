﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Filters
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Security.Authentication;
    using Fixtures;
    using FluentAssertions;
    using Testability;
    using Testability.Configuration;
    using Web.Exceptions;
    using Xunit;

    public class ExceptionalFilterFeature
    {
        private const string Expected_409 =
            "{\"message\":\"Operation would result in an invalid state\",\"code\":\"CONFLICT\",\"fields\":[]}";

        private const string Expected_301 =
            "{\"message\":\"Resource or Endpoint is located in a new or canonical location\",\"code\":\"MOVED_PERMANENTLY\",\"fields\":[{\"message\":\"See Path\",\"code\":\"LOCATION\",\"path\":\"https://google.com/\"}]}";

        private const string Expected_308 =
            "{\"message\":\"Resource or Endpoint is located in a new or canonical location\",\"code\":\"PERMANENT_REDIRECT\",\"fields\":[{\"message\":\"See Path\",\"code\":\"LOCATION\",\"path\":\"https://google.com/\"}]}";

        private const string Expected_303 =
            "{\"message\":\"Resource or Endpoint is located in a new or canonical location\",\"code\":\"SEE_OTHER\",\"fields\":[{\"message\":\"See Path\",\"code\":\"LOCATION\",\"path\":\"https://google.com/\"}]}";

        private const string Expected_307 =
            "{\"message\":\"Resource or Endpoint is located in a new or canonical location\",\"code\":\"TEMPORARY_REDIRECT\",\"fields\":[{\"message\":\"See Path\",\"code\":\"LOCATION\",\"path\":\"https://google.com/\"}]}";

        private const string Expected_401 =
            "{\"message\":\"You are not authenticated. Or are using an incorrect authenication mechanism.\",\"code\":\"NOT_AUTHENTICATED\",\"fields\":[]}";

        private const string Expected_403 =
            "{\"message\":\"Incorrect authorization\",\"code\":\"NOT_AUTHORIZED\",\"fields\":[]}";

        private const string Expected_404 =
            "{\"message\":\"Resource or Endpoint not found\",\"code\":\"NOT_FOUND\",\"fields\":[]}";

        private const string Expected_422 =
            "{\"message\":\"Bad Request Data (See Fields)...\",\"code\":\"UNPROCESSABLE_ENTITY\",\"fields\":[{\"message\":\"UNKNOWN\",\"code\":\"UNKNOWN\",\"path\":\"UNKNOWN\"}]}";

        private const string Expected_501 =
            "{\"message\":\"Operation not implemented\",\"code\":\"NOT_IMPLEMENTED\",\"fields\":[]}";

        [Theory]
        [InlineData(typeof(ConflictException), HttpStatusCode.Conflict, Expected_409)]
        [InlineData(typeof(NotAuthenticatedException), HttpStatusCode.Unauthorized, Expected_401)]
        [InlineData(typeof(NotAuthorizedToResourceException), HttpStatusCode.Forbidden, Expected_403)]
        [InlineData(typeof(NotImplementedException), HttpStatusCode.NotImplemented, Expected_501)]
        [InlineData(typeof(UnprocessableEntityException), (HttpStatusCode) 422, Expected_422)]
        [InlineData(typeof(NotFoundException), HttpStatusCode.NotFound, Expected_404)]
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Redirections
        //

        // Permanant: POST CONVERTED TO GET
        [InlineData(typeof(RedirectException), HttpStatusCode.Moved, Expected_301, new[] {"https://google.com/", "true", "false"})]

        // Permanant: method and body not changed
        [InlineData(typeof(RedirectException), HttpStatusCode.PermanentRedirect, Expected_308, new[] {"https://google.com/", "true", "true"})]

        // TEMP: POST CONVERTED TO GET
        [InlineData(typeof(RedirectException), HttpStatusCode.SeeOther, Expected_303, new[] {"https://google.com/", "false", "false"})]
        // TEMP: method and body not changed
        [InlineData(typeof(RedirectException), HttpStatusCode.TemporaryRedirect, Expected_307, new[] {"https://google.com/", "false", "true"})]

        // Extensions
        [InlineData(typeof(UnauthorizedAccessException), (HttpStatusCode) 401, "SKIP")]
        [InlineData(typeof(AuthenticationException), (HttpStatusCode) 401, "SKIP")]
        [InlineData(typeof(AccessViolationException), HttpStatusCode.Forbidden, "SKIP")]
        [InlineData(typeof(InvalidOperationException), (HttpStatusCode) 409, "SKIP")]
        [InlineData(typeof(ArgumentException), (HttpStatusCode) 422, "SKIP")]
        [InlineData(typeof(InvalidDataException), (HttpStatusCode) 422, "SKIP")]
        [InlineData(typeof(FormatException), (HttpStatusCode) 422, "SKIP")]
        [InlineData(typeof(InvalidCastException), (HttpStatusCode) 500, "SKIP")]
        public async void ExceptionFilterFeature(Type type, HttpStatusCode statusCode, string expected, string[] additionalData = null)
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;

                var qs = additionalData == null || additionalData.Length == 0
                             ? string.Empty
                             : $"additionalData={string.Join(",", additionalData.Select((x, i) => $"{x}"))}";

                var url = $"http://localhost/throw-exception/{type}?{qs}";
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be(statusCode);

                if (expected != "SKIP")
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    expected = expected.Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
            }
        }

        [Fact]
        public async void UnprocessableEntityExceptionFilterFeature()
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;
                var url = "http://localhost/throw-exception/" + typeof(UnprocessableEntityException);
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be((HttpStatusCode) 422);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = Expected_422.Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }
    }
}