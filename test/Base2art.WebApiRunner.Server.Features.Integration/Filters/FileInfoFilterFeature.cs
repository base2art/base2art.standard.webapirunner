﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Filters
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using Fixtures;
    using FluentAssertions;
    using Testability;
    using Testability.Configuration;
    using Xunit;

    public class FileInfoFilterFeature
    {
        [Fact]
        public async void GetFile()
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;

                var path = Path.GetTempPath();
                var guid = Guid.NewGuid().ToString("N");

                path = Path.Combine(path, guid + ".py");

                File.WriteAllText(path, "print('Hello World')");

                try
                {
                    var url = "http://localhost/file-info/" + path.Replace("/", "_").Replace("\\", "_");
                    var response = await client.GetAsync(url);

                    response.StatusCode.Should().Be(HttpStatusCode.OK);

                    var responseString = await response.Content.ReadAsStringAsync();
                    var expected = "print('Hello World')".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
                finally
                {
                    File.Delete(path);
                }
            }
        }

        [Fact]
        public async void UploadFile()
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;

                var bytesContent = new ByteArrayContent(Encoding.Default.GetBytes("HelloWorld!"));
                bytesContent.Headers.ContentType = MediaTypeHeaderValue.Parse("text/plain");
                var formData = new MultipartFormDataContent();
                formData.Add(bytesContent, "file", "MyFile.Text");

                var url = "http://localhost/file-info";
                var response = await client.PostAsync(url, formData);

                var responseString = await response.Content.ReadAsStringAsync();
                response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                var expected = "\"file:text/plain:HelloWorld!\"".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }
    }
}