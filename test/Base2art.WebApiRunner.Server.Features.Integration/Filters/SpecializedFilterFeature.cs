﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Filters
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Runtime.InteropServices;
    using Fixtures;
    using FluentAssertions;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Server.Filters;
    using Standard.WebApiRunner.Samples.Filters;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Testability.Configuration;
    using Testability.Registrars;
    using Text;
    using Xunit;

//    using Server.Filters.Specialized;

    public class SpecializedFilterFeature
    {
        [Theory]
        [InlineData("012345678", 409)]
        [InlineData("0123456789", 410)]
        public async void ShouldSetStatus(string name, int httpStatusCode)
        {
            IActionFilter ServiceRegistar(IServiceProvider provider) =>
                new ResponseEditorFilter(new StatusSetter());

            using (var fixture = IntegrationTests.CreateFixture(
                                                          ServerConfiguration.Create(),
                                                          new CustomRouteRegistrar<RedirectController>("{id}", x => x.Get("")),
                                                          new DelegateFilterRegistar(ServiceRegistar)))
            {
                var client = fixture.Client;
                var url = "http://localhost/" + name;
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be((HttpStatusCode) httpStatusCode);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = "{\"name\":\"" + name + "\"}".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }

        [Theory]
        [InlineData("012345678")] // , 409
        [InlineData("0123456789")]
        // , 410
        public async void ShouldRedirect(string name)
        {
            IActionFilter ServiceRegistar(IServiceProvider provider) =>
                new ResponseEditorFilter(new RedirectSetter());

            using (var fixture = IntegrationTests.CreateFixture(
                                                          ServerConfiguration.Create(),
                                                          new CustomRouteRegistrar<RedirectController>("{id}", x => x.Get("")),
                                                          new DelegateFilterRegistar(ServiceRegistar)))
            {
                var client = fixture.Client;
                var url = "http://localhost/" + name;
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be(HttpStatusCode.MovedPermanently);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = "{\"name\":\"" + name + "\"}".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
                response.Headers.GetValues("Location").Join().Should().Be("http://localhost/person/" + name);
            }
        }

        [Theory]
        [InlineData("012345678")] // , 409
        [InlineData("0123456789")]
        // , 410
        public async void ShouldSetCookie(string name)
        {
            IActionFilter ServiceRegistar(IServiceProvider provider) =>
                new ResponseEditorFilter(new CookieSetter());

            using (var fixture = IntegrationTests.CreateFixture(
                                                          ServerConfiguration.Create(),
                                                          new CustomRouteRegistrar<RedirectController>("{id}", x => x.Get("")),
                                                          new DelegateFilterRegistar(ServiceRegistar)))
            {
                var client = fixture.Client;
                var url = "http://localhost/" + name;
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = "{\"name\":\"" + name + "\"}".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
                response.Headers.GetValues("set-Cookie").Join().Should()
                        .Be($"name={name}; expires=Mon, 01 Jan 0001 {this.Time()} GMT; path=/; samesite=lax");
            }
        }

        private string Time()
        {
            var isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

            return !isWindows ? "07:53:00" : "08:00:00";
        }

        [Theory]
        [InlineData("012345678")] // , 409
        [InlineData("0123456789")]
        // , 410
        public async void ShouldSetHeader(string name)
        {
            IActionFilter ServiceRegistar(IServiceProvider provider) =>
                new ResponseEditorFilter(new HeaderSetter());

            using (var fixture = IntegrationTests.CreateFixture(
                                                          ServerConfiguration.Create(),
                                                          new CustomRouteRegistrar<RedirectController>("{id}", x => x.Get("")),
                                                          new DelegateFilterRegistar(ServiceRegistar)))
            {
                var client = fixture.Client;
                var url = "http://localhost/" + name;
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = "{\"name\":\"" + name + "\"}".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
                response.Headers.GetValues("Authorization").Join().Should().Be("Bearer " + name);
            }
        }

        [Theory]
        [InlineData("012345678", 409)]
        [InlineData("0123456789", 410)]
        public async void ShouldEditResponse(string name, int httpStatusCode)
        {
            IActionFilter ServiceRegistar(IServiceProvider provider) =>
                new ResponseEditorFilter(new ResponseEditor());

            using (var fixture = IntegrationTests.CreateFixture(
                                                          ServerConfiguration.Create(),
                                                          new CustomRouteRegistrar<RedirectController>("{id}", x => x.Get("")),
                                                          new DelegateFilterRegistar(ServiceRegistar)))
            {
                var client = fixture.Client;
                var url = "http://localhost/" + name;

                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be((HttpStatusCode) httpStatusCode);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = "{\"name\":\"" + name + "\"}".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
                response.Headers.GetValues("nameHeader").Join().Should().Be(name);
                response.Headers.GetValues("Location").Join().Should().Be("/" + name);
                response.Headers.GetValues("Set-Cookie").Should().HaveCount(2).And.Subject.Skip(0).First().Should().Be("name=" + name + "; path=/");
                response.Headers.GetValues("Set-Cookie").Should().HaveCount(2).And.Subject.Skip(1).First().Should()
                        .Be(".Session=CADA2260359B4EF2BFD7527EBE938D56; path=/");
            }
        }
    }
}