﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Fixtures
{
    using System;
    using System.Net.Http;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class TestRouteConfigurationRegistrar : RegistrationBase
    {
        public override void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
        {
            base.RegisterRoutes(router, provider, config);

            router.MapRoute<CustomController>(HttpMethod.Get, "", x => x.List(), RouteType.Endpoint);
            router.MapRoute<CustomController>(HttpMethod.Get, "throw-exception/{exceptionType}", x => x.Exception(null, null), RouteType.Endpoint);
            router.MapRoute<CustomController>(HttpMethod.Post, "", x => x.Augment(null), RouteType.Endpoint);
            router.MapRoute<FileInfoController>(HttpMethod.Get, "file-info/{path}", x => x.GetFileInfo(null), RouteType.Endpoint);
            router.MapRoute<FileInfoController>(HttpMethod.Post, "file-info", x => x.UploadFileInfo(null), RouteType.Endpoint);
            router.MapRoute<WebPageController>(HttpMethod.Get, "index.html", x => x.GetWebPage(), RouteType.Endpoint);
            router.MapRoute<WebPageController>(HttpMethod.Get, "index.js", x => x.GetScript(), RouteType.Endpoint);
        }
    }
}