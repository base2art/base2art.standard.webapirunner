namespace Base2art.WebApiRunner.Server.Features.Integration.Fixtures
{
    using System.Threading.Tasks;
    using Server.TaskScheduling;
    using Web.App.LifeCycle;

    public class TestApplication : IApplication
    {
        public void Reload()
        {
        }

        public Task ExecuteJob(string name) => ProdTaskRegistrar.ExecuteJob(name);

        public Task ExecuteJobWithParameters(string name, object parameters) => ProdTaskRegistrar.ExecuteJob(name, parameters);
    }
}