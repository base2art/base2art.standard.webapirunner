﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Fixtures
{
    using System.Collections.Generic;
    using System.Linq;
    using ApiExploration;
    using ApiExploration.Swagger;
    using Registrations;
    using Runners.CommandLineInterface;
    using Server.Aspects;
    using Server.Bindings;
    using Server.Cors;
    using Server.Filters;
    using Server.Formatters;
    using Server.Injection;
    using Server.Routing;
    using Server.TaskScheduling;
    using Web.App.Configuration;
    using Web.Server.Registration;
    using Web.ServerEnvironment;

    public class Startup : StartupBase
    {
        private static readonly TestApplication Application = new TestApplication();
        private readonly IEnumerable<IRegistration> registrations;

        public Startup(
            IServerConfiguration configuration,
            IFrameworkShim shim,
            IBoundTypeRegistrar registrar,
            IWebServerSettings webServerSettings,
            params IRegistration[] registrations)
            : base(configuration, Application, new RouteManagerProvider(), shim, webServerSettings, registrar) =>
            this.registrations = registrations;

        protected override IRegistration[] Registrations => this.KnownRegistrations.Union(this.registrations).ToArray();

        private IRegistration[] KnownRegistrations => new IRegistration[]
                                                      {
                                                          new FormatRegistrar(),
                                                          new FilterRegistrar(),
                                                          new SystemServicesRegistrar(Application),
                                                          new RouteRegistrar(this.Shim),
                                                          new AdminRouteConfigurationRegistrar(),
                                                          new PublicRouteConfigurationRegistrar(),
                                                          new CorsRegistrar(),
                                                          new BindingsRegistrar(),
                                                          new InjectionRegistrar(),

                                                          new ApiExplorationRegistrar(),
                                                          new ApiExplorationSwaggerRegistrar(),
                                                          new ProdTaskRegistrar(),
                                                          new AspectsRegistrar()
                                                      };
    }
}