namespace Base2art.WebApiRunner.Server.Features.Integration.Formatters
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using Fixtures;
    using FluentAssertions;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Testability.Configuration;
    using Web.App.Configuration;
    using Web.Server.Registration;
    using Xunit;
    using Xunit.Abstractions;

    public class PlainTextFormatterFeature
    {
        public PlainTextFormatterFeature(ITestOutputHelper output) => this.output = output;

        private readonly ITestOutputHelper output;

        private const string YamlText = @"
- Item1
- Item2";

        private const string JsonText = @"[""Item1"",""Item2""]";
        private const string JsonPrettyText = "[\n  \"Item1\",\n  \"Item2\"\n]";

        private const string XmlText =
            "<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
            + "<string>Item1</string>"
            + "<string>Item2</string>"
            + "</ArrayOfString>";

        [Theory]
        [InlineData("http://localhost/", "", JsonText)]
        [InlineData("http://localhost/", "text/yaml", YamlText)]
        [InlineData("http://localhost/?format=yaml", "", YamlText)]
        [InlineData("http://localhost/", "application/pretty-json", JsonPrettyText)]
        [InlineData("http://localhost/?format=pretty-json", "", JsonPrettyText)]
        [InlineData("http://localhost/", "application/json", JsonText)]
        [InlineData("http://localhost/?format=json", "", JsonText)]
        [InlineData("http://localhost/", "application/xml", XmlText)]
        [InlineData("http://localhost/?format=xml", "", XmlText)]
        public async void Test1(string url, string accept, string expected)
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;
                var request = new HttpRequestMessage
                              {
                                  RequestUri = new Uri(url),
                                  Method = HttpMethod.Get
                              };

                if (!string.IsNullOrWhiteSpace(accept))
                {
                    request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(accept));
                }

                var response = await client.SendAsync(request);

                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                responseString.Replace("\r", string.Empty).Trim().Should().Be(expected.Replace("\r", string.Empty).Trim());
            }
        }

        private async Task Send(HttpClient client, string url, string input, string expected)
        {
//                var content = new StringContent("input[0]=a&input[1]=b", Encoding.Default, "application/x-www-form-urlencoded");
            var content = new StringContent(input, Encoding.Default, "application/x-www-form-urlencoded");

            var response = await client.PostAsync("http://localhost" + url, content);

            var responseString = await response.Content.ReadAsStringAsync();
            this.output.WriteLine(responseString);
            response.EnsureSuccessStatusCode();

//                var responseString = await response.Content.ReadAsStringAsync();
            responseString.Trim().Should().Be(expected.Replace("\r", string.Empty).Trim());
        }

        private async Task SendJson(HttpClient client, string url, string input, string expected)
        {
//                var content = new StringContent("input[0]=a&input[1]=b", Encoding.Default, "application/x-www-form-urlencoded");
            var content = new StringContent(input, Encoding.Default, "application/json");

            var response = await client.PostAsync("http://localhost" + url, content);

            var responseString = await response.Content.ReadAsStringAsync();
            this.output.WriteLine(responseString);
            response.EnsureSuccessStatusCode();

//                var responseString = await response.Content.ReadAsStringAsync();
            responseString.Trim().Should().Be(expected.Replace("\r", string.Empty).Trim());
        }

        private class TestRouteConfigurationRegistrarInternal : RegistrationBase
        {
            public override void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
            {
                base.RegisterRoutes(router, provider, config);

                router.MapRoute<CustomController>(HttpMethod.Post, "augment/string-arr", x => x.Augment(null), RouteType.Endpoint);
                router.MapRoute<CustomController>(HttpMethod.Post, "augment/int", x => x.AugmentInt(0), RouteType.Endpoint);
                router.MapRoute<CustomController>(HttpMethod.Post, "augment/person", x => x.AugmentPerson(null), RouteType.Endpoint);
                router.MapRoute<CustomController>(HttpMethod.Post, "augment/persons", x => x.AugmentPersons(null), RouteType.Endpoint);
                router.MapRoute<CustomController>(HttpMethod.Post, "augment/string", x => x.AugmentString(null), RouteType.Endpoint);
            }
        }

        [Fact]
        public async void JsonInputFeature()
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;

                var content = new StringContent("[\"a\", \"b\"]", Encoding.Default, "application/json");

                var response = await client.PostAsync("http://localhost", content);

                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                responseString.Trim().Should().Be("[\"a-0\",\"b-1\"]".Replace("\r", string.Empty).Trim());
            }
        }

        [Fact]
        public async void JsonInputFeatureUrlEncoded()
        {
            using (var fixture = IntegrationTests.CreateFixture(ServerConfiguration.Create(), new TestRouteConfigurationRegistrarInternal()))
            {
                var client = fixture.Client;

                var date = DateTime.Today.ToString("yyyy-MM-dd");

                await this.Send(client, "/augment/persons", "value[0].name=poop&value[1].name=crap",
                                $"[{{\"name\":\"poop-{date}\"}},{{\"name\":\"crap-{date}\"}}]");

                await this.Send(client, "/augment/person", "value.name=poop", $"{{\"name\":\"poop-{date}\"}}");

                await this.Send(client, "/augment/string-arr", "input=a&input=b", "[\"a-0\",\"b-1\"]");
                await this.Send(client, "/augment/int", "value=2", $"[\"2\",\"{date}\"]");
                await this.Send(client, "/augment/string", "value=poop", $"[\"poop\",\"{date}\"]");
            }
        }

        [Fact]
        public async void JsonInputFeatureUrlEncodedJson()
        {
            using (var fixture = ServerConfiguration.Create().CreateFixture(new TestRouteConfigurationRegistrarInternal()))
            {
                var client = fixture.Client;

                var date = DateTime.Today.ToString("yyyy-MM-dd");

                // value[0].name=poop&value[1].name=crap
                await this.SendJson(client, "/augment/persons", "[{'name':'poop'},{'name':'crap'}]",
                                    $"[{{\"name\":\"poop-{date}\"}},{{\"name\":\"crap-{date}\"}}]");

                await this.SendJson(client, "/augment/person", "{'name':'poop'}", $"{{\"name\":\"poop-{date}\"}}");

                await this.SendJson(client, "/augment/string-arr", "['a', 'b']", "[\"a-0\",\"b-1\"]");
                await this.SendJson(client, "/augment/int", "2", $"[\"2\",\"{date}\"]");
                await this.SendJson(client, "/augment/string", "'poop'", $"[\"poop\",\"{date}\"]");
            }
        }

        [Fact]
        public void Test0()
        {
            this.output.WriteLine("123");
        }
    }
}