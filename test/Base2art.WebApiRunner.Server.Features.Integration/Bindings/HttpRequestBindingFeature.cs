namespace Base2art.WebApiRunner.Server.Features.Integration.Bindings
{
    using System.Net;
    using FluentAssertions;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability.Configuration;
    using Web.App.Configuration;
    using Xunit;

    public class HttpRequestBindingFeature
    {
        
        [Fact]
        public async void GetRouteWithIntParameter()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "this-is/a/fake-path",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetPath(null, null)),
                                     Verb = HttpVerb.Get
                                 });

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "this-is/a/fake-path/of/{id}",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetPath(null,null)),
                                     Verb = HttpVerb.Get
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;
                {
                    var url = "http://localhost/this-is/a/fake-path?id=3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "\"3/this-is/a/fake-path\"";
                    responseString.Trim().Should().Be(expected);
                }
                {
                    var url = "http://localhost/this-is/a/fake-path/of/3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "\"3/this-is/a/fake-path/of/3\"";
                    responseString.Trim().Should().Be(expected);
                }
            }
        }
    }
}