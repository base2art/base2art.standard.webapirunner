﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Bindings
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Aspects;
    using Fixtures;
    using FluentAssertions;
    using Newtonsoft.Json;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Testability.Configuration;
    using Text;
    using ValueTypes;
    using Web.App.Configuration;
    using Web.App.Logging;
    using Web.App.Principals;
    using Web.App.Principals.Jwt;
    using Web.ValueTypes;
    using Xunit;

    public class SimpleBindingFeature
    {
        private static IServerConfiguration GetConfiguration(bool includeAspect = false)
        {
            var serverConfiguration = ServerConfiguration.Create();
            var modelBindingConfiguration = new ModelBindingConfiguration();

            modelBindingConfiguration.BinderType = typeof(ClaimsPrincipalBinder);
            modelBindingConfiguration.BoundType = typeof(ClaimsPrincipal);
            modelBindingConfiguration.BinderParameters.Add("cookieName", "auth_jwt");
            modelBindingConfiguration.BinderParameters.Add("ssoUrl", "https://sso.base2art.com/api/keys/{keyId}");
            modelBindingConfiguration.BinderParameters.Add("ssoUrlKeyIdReplacementString", "{keyId}");
            modelBindingConfiguration.BinderParameters.Add("issuer", "http://sso.base2art.com");
            modelBindingConfiguration.BinderParameters.Add("audiences", new[] {"http://localhost"});

            var modelBindingConfiguration1 = new ModelBindingConfiguration();
            modelBindingConfiguration1.BinderType = typeof(DomainNameBinder);
            modelBindingConfiguration1.BoundType = typeof(DomainName);

            serverConfiguration.Injection.AddRange(new[]
                                                   {
                                                       new InjectionItemConfiguration
                                                       {
                                                           RequestedType = typeof(IRoleLookup),
                                                           FulfillingType = typeof(Base2art.Web.App.Principals.NullRoleLookup),
                                                           IsSingleton = true
                                                       }
                                                   });

            serverConfiguration.Application.ModelBindings.Add(modelBindingConfiguration);
            serverConfiguration.Application.ModelBindings.Add(modelBindingConfiguration1);

            serverConfiguration.Endpoints.Add(new EndpointConfiguration
                                              {
                                                  Url = "api/employees",
                                                  Type = typeof(CustomController),
                                                  Method = RouteManagers.MethodOf<CustomController>(x => x.GetFiltered(null, null)),
                                                  Verb = HttpVerb.Get
                                              });

            serverConfiguration.Endpoints.Add(new EndpointConfiguration
                                              {
                                                  Url = "api/value-types/domain-name/{id}",
                                                  Type = typeof(ValueTypeController),
                                                  Method = RouteManagers.MethodOf<ValueTypeController>(x => x.GetDomainName(null, 0)),
                                                  Verb = HttpVerb.Get
                                              });

            if (includeAspect)
            {
                serverConfiguration.Endpoints.ForEach(x => x.Aspects.Add(
                                                                         new AspectConfiguration
                                                                         {
                                                                             Type = typeof(RequestLogger),
                                                                             Method = typeof(RequestLogger).GetMethod(nameof(RequestLogger.OnInvoke)),
                                                                             Parameters =
                                                                             {
                                                                                 {"siteName", "Test"},
                                                                                 {"basePath", ""}
                                                                             }
                                                                         }
                                                                        ));

                serverConfiguration.Endpoints.ForEach(x => x.Aspects.Add(
                                                                         new AspectConfiguration
                                                                         {
                                                                             Type = typeof(MultiplyingAspect),
                                                                             Method = typeof(MultiplyingAspect)
                                                                                 .GetMethod(nameof(MultiplyingAspect.OnInvoke))
                                                                         }
                                                                        ));
            }

            return serverConfiguration;
        }

        [Fact]
        public async void RequestPublicObject()
        {
            using (var fixture = IntegrationTests.CreateFixture(GetConfiguration(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;

                var request = new HttpRequestMessage(HttpMethod.Get, "/api/employees?paginationData.PageIndex=1&paginationData.PageSize=2");
                request.Headers.Add("Authorization", "Bearer abc1234");
                var response = await client.SendAsync(request);
                response.StatusCode.Should().Be(HttpStatusCode.OK, await response.Content.ReadAsStringAsync());
                response.Headers.GetValues("X-Authentication-Type").Join().Should().Be("JWT");
                response.Headers.GetValues("X-Authentication-Issuer").Join().Should().Be("http://sso.base2art.com");

                var item = await response.Content.ReadAsStringAsync();

                var rez = JsonConvert.DeserializeObject<List<string>>(item);
                rez.Should().HaveCount(2);
                rez[0].Should().Be("Item2");
                rez[1].Should().Be("Item3");
            }
        }

        [Fact]
        public async void RequestPublicObjectWithAspectedLogging()
        {
            using (var fixture = IntegrationTests.CreateFixture(GetConfiguration(true), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;

                var request = new HttpRequestMessage(HttpMethod.Get, "api/value-types/domain-name/123");
                request.Headers.Add("Authorization", "Bearer abc1234");
                var response = await client.SendAsync(request);
                response.StatusCode.Should().Be(HttpStatusCode.OK, await response.Content.ReadAsStringAsync());

                var item = await response.Content.ReadAsStringAsync();

                item.Should().Be("\"localhost-246\"");
            }
        }
    }

    internal class MultiplyingAspect : IAspect
    {
        public Task OnInvoke(IMethodInterceptionArgs args1, Func<Task> next)
        {
            var args = args1.Arguments;
            for (var i = 0; i < args.Count; i++)
            {
                var value = args[i];

                if (value is int j)
                {
                    args[i] = j * 2;
                }
            }

            return next();
        }
    }
}