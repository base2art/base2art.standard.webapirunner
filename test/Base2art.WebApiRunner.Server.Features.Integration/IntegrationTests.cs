namespace Base2art.WebApiRunner.Server.Features.Integration
{
    using System;
    using System.IO;
    using Fixtures;
    using Runners.CommandLineInterface;
    using Runners.Commands;
    using Testability;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public static class IntegrationTests
    {
        public static TestFixture<Startup> CreateFixture(this IServerConfiguration serverConfiguration, params IRegistration[] registrations)
            => new TestFixture<Startup>(serverConfiguration, new FrameworkShim(), CreateWebServerSettings(), registrations);

        private static WebServerSettings CreateWebServerSettings()
            => new WebServerSettings
               {
                   ContentRoot = Environment.CurrentDirectory,
                   CurrentDirectory = Environment.CurrentDirectory,
                   WebRoot = Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "wwwroot")).FullName
               };
    }
}