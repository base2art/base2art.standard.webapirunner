﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Injection
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using Fixtures;
    using FluentAssertions;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Standard.WebApiRunner.Samples.Repositories;
    using Testability;
    using Testability.Configuration;
    using Web.App.Configuration;
    using Xunit;
    using Xunit.Abstractions;

    public class SimpleInjectionFeature
    {
        public SimpleInjectionFeature(ITestOutputHelper output) => this.output = output;
        private readonly ITestOutputHelper output;

        // [InlineData(typeof(CustomControllerWithConcreteParameters), typeof(IListProvider), typeof(ListProvider))]
        // [InlineData(typeof(CustomControllerWithConcreteParameters), typeof(ListProvider), typeof(ListProvider))]

        [Theory]
        [InlineData(typeof(CustomControllerWithConcreteParameters), typeof(IListProvider), null, false, HttpStatusCode.FailedDependency)]
        [InlineData(typeof(CustomControllerWithConcreteParameters), typeof(ListProvider), null, false, HttpStatusCode.OK)]
        [InlineData(typeof(CustomControllerWithConcreteParameters), typeof(ListProvider), typeof(ListProvider), false, HttpStatusCode.OK)]
        [InlineData(typeof(CustomControllerWithInterfaceParameters), typeof(IListProvider), typeof(ListProvider), true, HttpStatusCode.OK)]
        public async void GetSimpleEndpointWithExplicitBinding(Type parent, Type requestedType, Type boundType, bool addPath, HttpStatusCode expectedCode)
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc",
                                     Type = parent,
                                     Method = parent.GetMethod("GetNames"),
                                     Verb = HttpVerb.Get
                                 });

            config.Injection.Add(new InjectionItemConfiguration
                                 {
                                     RequestedType = requestedType,
                                     FulfillingType = boundType
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                var url = "http://localhost/abc";
                var response = await client.GetAsync(url);

                var responseString = await response.Content.ReadAsStringAsync();
                this.output.WriteLine(responseString);
                response.StatusCode.Should().Be(expectedCode);

                if (expectedCode == HttpStatusCode.OK)
                {
                    var expected = "";
                    if (addPath)
                    {
                        expected = "[\"sdf\",\"123\",\"/abc\"]".Replace("\r", string.Empty).Trim();
                    }
                    else
                    {
                        expected = "[\"sdf\",\"123\"]".Replace("\r", string.Empty).Trim();
                    }

                    responseString.Trim().Should().Be(expected);
                }
            }
        }

        [Fact]
        public async void ShouldCovertParameters()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc",
                                     Type = typeof(ControllerWithInjectedListParameters),
                                     Method = typeof(ControllerWithInjectedListParameters)
                                         .GetMethod(nameof(ControllerWithInjectedListParameters.Get)),
                                     Verb = HttpVerb.Get,
                                     Parameters =
                                     {
                                         {"items", new List<int> {3, 2}}
                                     }
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                var url = "http://localhost/abc";
                var response = await client.GetAsync(url);

                var responseString = await response.Content.ReadAsStringAsync();
                this.output.WriteLine(responseString);
                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var expected = "[3,2]".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }
    }
}