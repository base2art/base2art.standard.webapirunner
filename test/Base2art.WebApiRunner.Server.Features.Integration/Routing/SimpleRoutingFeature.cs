﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Routing
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Fixtures;
    using FluentAssertions;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Server.Routing.Models;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Standard.WebApiRunner.Samples.Public.HealthChecks;
    using Standard.WebApiRunner.Samples.Public.Tasks;
    using Testability;
    using Testability.Configuration;
    using Web.App.Configuration;
    using Xunit;
    using Xunit.Abstractions;

    public class SimpleRoutingFeature
    {
        public SimpleRoutingFeature(ITestOutputHelper output) => this.output = output;

        private readonly ITestOutputHelper output;

        [Theory]
        [InlineData(typeof(CustomHealthCheck), nameof(CustomHealthCheck.Execute), null, "E4937DE0-8B37-45AF-9B5E-82C71D8403B2")]
        [InlineData(typeof(CustomHealthCheck), nameof(CustomHealthCheck.ExecuteAsync), null, "F4AEAEAC-5B1E-48BC-BC4C-07BB4F31DAE9")]
        [InlineData(typeof(CustomHealthCheck), nameof(CustomHealthCheck.ExecuteWithReturnHealthCheck),
            "6EF6CAF4-CA7A-4A90-897A-DC3655BF2A18", "7EF6CAF4-CA7A-4A90-897A-DC3655BF2A18")]
        [InlineData(typeof(CustomHealthCheck), nameof(CustomHealthCheck.ExecuteHealthyWithReturnHealthCheck),
            "6EF6CAF4-CA7A-4A90-897A-DC3655BF2A19", "7EF6CAF4-CA7A-4A90-897A-DC3655BF2A19")]
        [InlineData(typeof(CustomHealthCheck), nameof(CustomHealthCheck.ExecuteAsyncWithReturnHealthCheck),
            "1EA91652-462B-4BF7-912D-57AF3C8B3F45", "0EA91652-462B-4BF7-912D-57AF3C8B3F45")]
        [InlineData(typeof(CustomHealthCheck), nameof(CustomHealthCheck.ExecuteAsyncWithReturnString), "151E1BA8-2C56-4405-909A-48A213BD99CF",
            "6248EFD2-0306-4FF5-BE28-0C528131FA6B")]
        [InlineData(typeof(CustomHealthCheck), nameof(CustomHealthCheck.ExecuteWithReturnString), "151E1BA8-2C56-4405-909A-48A213BD99CE",
            "52710F05-9902-4D2B-8EBD-38979B5F2B97")]
        public async void GetSimpleHealthCheckWithArgs(Type type, string methodName, string responseResult, string consoleResult)
        {
            var config = ServerConfiguration.Create();

            config.HealthChecks.Items.Add(new HealthCheckConfiguration
                                          {
                                              Name = "hc-1",
                                              Type = type,
                                              Method = type.GetMethod(methodName)
                                          });

            var oldOut = Console.Out;

            try
            {
                var sb = new StringWriter();
                Console.SetOut(sb);

                using (var fixture = IntegrationTests.CreateFixture(config))
                {
                    var client = fixture.Client;

                    var url = "http://localhost/healthchecks/hc-1";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();

                    this.output.WriteLine(responseString);

                    response.StatusCode.Should().Be(HttpStatusCode.OK);

                    var result = JsonConvert.DeserializeObject<HealthCheckResult>(responseString);
                    result.Items.Count.Should().Be(1);
                    result.Items[0].Key.Should().Be("hc-1");
                    result.Items[0].Value.Data.Should().Be(responseResult);

                    var expected = consoleResult.Replace("\r", string.Empty).Trim();
                    sb.GetStringBuilder().ToString().Trim().Should().Be(expected);
                }
            }
            finally
            {
                Console.SetOut(oldOut);
            }
        }

        private static T As<T>(JValue obj) => (T) Convert.ChangeType(obj.Value?.ToString(), typeof(T));

        private static string As(JValue obj) => obj.Value?.ToString();

        [Theory]
//        [InlineData("{}")]
//        [InlineData("")]
        [InlineData(null)]
        public async void GetWithDictionaryTask(string content)
        {
            var config = ServerConfiguration.Create();

            config.Tasks.Add(new TaskConfiguration
                             {
                                 Name = "task-1",
                                 Type = typeof(CustomTaskWithParameters),
                                 Method = RouteManagers.MethodOf<CustomTaskWithParameters>(x => x.ExecuteAsync(null))
                             });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                var url = "http://localhost/tasks/task-1";

                var request = new HttpRequestMessage(HttpMethod.Post, url);

                if (content == null)
                {
                    var content1 = new StringContent("{}");
                    content1.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                    request.Content = content1;
                }

                var response = await client.SendAsync(request);

                var responseString = await response.Content.ReadAsStringAsync();
                response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                var expected = "{}".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }

        [Fact]
        public async void GetRouteWithIntParameter()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc/{i}",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetParameter(0)),
                                     Verb = HttpVerb.Get
                                 });
            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abcd",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetParameter(0)),
                                     Verb = HttpVerb.Get
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;
                {
                    var url = "http://localhost/abc/3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\",\"Item2\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
                {
                    var url = "http://localhost/abcd?i=3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\",\"Item2\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
                {
                    var url = "http://localhost/abcd";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
            }
        }

        [Fact]
        public async void GetRouteWithIntParameterNullable()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetParameterNullable(0)),
                                     Verb = HttpVerb.Get
                                 });
            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abcd/{i}",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetParameterNullable(0)),
                                     Verb = HttpVerb.Get
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;
                {
                    var url = "http://localhost/abc";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
                {
                    var url = "http://localhost/abc?i=3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\",\"Item2\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
                {
                    var url = "http://localhost/abcd";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.NotFound, responseString);
                }
                {
                    var url = "http://localhost/abcd/3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\",\"Item2\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
            }
        }

        [Fact]
        public async void GetRouteWithIntParameterNullableOptional()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetParameterNullableOptional(0)),
                                     Verb = HttpVerb.Get
                                 });
            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abcd/{i}",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetParameterNullableOptional(0)),
                                     Verb = HttpVerb.Get
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;
                {
                    var url = "http://localhost/abc";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
                {
                    var url = "http://localhost/abc?i=4";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\",\"Item2\",\"Item3\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
                {
                    var url = "http://localhost/abcd";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.NotFound, responseString);
                }
                {
                    var url = "http://localhost/abcd/3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\",\"Item2\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }
            }
        }

        [Fact]
        public async void GetRouteWithOptionalParameter()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetParameterOptional(0)),
                                     Verb = HttpVerb.Get
                                 });

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abcd/{i}",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.GetParameterOptional(0)),
                                     Verb = HttpVerb.Get
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                {
                    var url = "http://localhost/abc";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }

                {
                    var url = "http://localhost/abcd/3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                    var expected = "[\"Item0\",\"Item1\",\"Item2\"]".Replace("\r", string.Empty).Trim();
                    responseString.Trim().Should().Be(expected);
                }

                {
                    var url = "http://localhost/abcd?i=3";
                    var response = await client.GetAsync(url);

                    var responseString = await response.Content.ReadAsStringAsync();
                    response.StatusCode.Should().Be(HttpStatusCode.NotFound, responseString);
                }
            }
        }

        [Fact]
        public async void GetSimpleEndpoint()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.List()),
                                     Verb = HttpVerb.Get
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                var url = "http://localhost/abc";
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = "[\"Item1\",\"Item2\"]".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }

        [Fact]
        public async void GetSimpleEndpointWithParameters()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc",
                                     Type = typeof(CustomControllerWithParameters),
                                     Method = RouteManagers.MethodOf<CustomControllerWithParameters>(x => x.List()),
                                     Verb = HttpVerb.Get,
                                     Parameters = {{"suffix", "-test"}}
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                var url = "http://localhost/abc";
                var response = await client.GetAsync(url);

                var responseString = await response.Content.ReadAsStringAsync();
                response.StatusCode.Should().Be(HttpStatusCode.OK, responseString);

                var expected = "[\"Item1-test\",\"Item2-test\"]".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }

        [Fact]
        public async void GetSimpleHealthCheck()
        {
            var config = ServerConfiguration.Create();

            config.HealthChecks.Items.Add(new HealthCheckConfiguration
                                          {
                                              Name = "hc-1",
                                              Type = typeof(CustomHealthCheck),
                                              Method = RouteManagers.MethodOf<CustomHealthCheck>(x => x.ExecuteWithReturnString())
                                          });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                var url = "http://localhost/healthchecks/hc-1";
                var response = await client.GetAsync(url);

                var responseString = await response.Content.ReadAsStringAsync();
                this.output.WriteLine(responseString);
                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var result = JsonConvert.DeserializeObject<HealthCheckResult>(responseString);
                result.Items.Count.Should().Be(1);
                result.Items[0].Key.Should().Be("hc-1");
                result.Items[0].Value.Data.Should().Be("151E1BA8-2C56-4405-909A-48A213BD99CE");
            }
        }

        [Fact]
        public async void GetSimpleHealthCheckAggregate()
        {
            var config = ServerConfiguration.Create();

            config.HealthChecks.Items.Add(new HealthCheckConfiguration
                                          {
                                              Name = "hc-1",
                                              Type = typeof(CustomHealthCheck),
                                              Method = RouteManagers.MethodOf<CustomHealthCheck>(x => x.ExecuteAsyncWithReturnHealthCheck())
                                          });
            config.HealthChecks.Items.Add(new HealthCheckConfiguration
                                          {
                                              Name = "hc-2",
                                              Type = typeof(CustomHealthCheck),
                                              Method = RouteManagers.MethodOf<CustomHealthCheck>(x => x.ExecuteWithReturnHealthCheck())
                                          });

            config.HealthChecks.Items.Add(new HealthCheckConfiguration
                                          {
                                              Name = "hc-3",
                                              Type = typeof(CustomHealthCheck),
                                              Method = RouteManagers.MethodOf<CustomHealthCheck>(x => x.ExecuteAsyncWithReturnString())
                                          });
            config.HealthChecks.Items.Add(new HealthCheckConfiguration
                                          {
                                              Name = "hc-4",
                                              Type = typeof(CustomHealthCheck),
                                              Method = RouteManagers.MethodOf<CustomHealthCheck>(x => x.ExecuteWithReturnString())
                                          });

            config.HealthChecks.Items.Add(new HealthCheckConfiguration
                                          {
                                              Name = "hc-5",
                                              Type = typeof(CustomHealthCheck),
                                              Method = RouteManagers.MethodOf<CustomHealthCheck>(x => x.ExecuteAsync())
                                          });
            config.HealthChecks.Items.Add(new HealthCheckConfiguration
                                          {
                                              Name = "hc-6",
                                              Type = typeof(CustomHealthCheck),
                                              Method = RouteManagers.MethodOf<CustomHealthCheck>(x => x.Execute())
                                          });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                var url = "http://localhost/healthChecks";
                var response = await client.GetAsync(url);

                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var responseString = await response.Content.ReadAsStringAsync();
                var obj = JsonConvert.DeserializeObject<HealthCheckResult>(responseString);

                obj.When.Should().BeCloseTo(DateTime.UtcNow, 500);
                obj.Items.Should().HaveCount(6);
                obj.Items[0].Key.Should().Be("hc-1");
                obj.Items[1].Key.Should().Be("hc-2");
                obj.Items[2].Key.Should().Be("hc-3");
                obj.Items[3].Key.Should().Be("hc-4");
                obj.Items[4].Key.Should().Be("hc-5");
                obj.Items[5].Key.Should().Be("hc-6");
            }
        }

        [Fact]
        public async void GetSimpleTask()
        {
            var config = ServerConfiguration.Create();

            config.Tasks.Add(new TaskConfiguration
                             {
                                 Name = "task-1",
                                 Type = typeof(CustomTask),
                                 Method = RouteManagers.MethodOf<CustomTask>(x => x.ExecuteAsync())
                             });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                var url = "http://localhost/tasks/task-1";
                var content = new StringContent("{}");
                content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                var response = await client.PostAsync(url, content);

                response.StatusCode.Should().Be(HttpStatusCode.OK);

                var responseString = await response.Content.ReadAsStringAsync();
                var expected = "\"151E1BA8-2C56-4405-909A-48A213BD99CE\"".Replace("\r", string.Empty).Trim();
                responseString.Trim().Should().Be(expected);
            }
        }

        [Fact]
        public async void GetSwagger()
        {
            var config = ServerConfiguration.Create();

            config.Endpoints.Add(new EndpointConfiguration
                                 {
                                     Url = "abc",
                                     Type = typeof(CustomController),
                                     Method = RouteManagers.MethodOf<CustomController>(x => x.List()),
                                     Verb = HttpVerb.Get
                                 });

            using (var fixture = IntegrationTests.CreateFixture(config))
            {
                var client = fixture.Client;

                {
                    var url = "http://localhost/swagger/index.html";
                    var response = await client.GetAsync(url);
                    response.StatusCode.Should().Be(HttpStatusCode.OK);

                    var responseString = await response.Content.ReadAsStringAsync();
                    responseString.Trim().Should().Contain("<div id=\"swagger-ui\"></div>");
                }

                {
                    var url = "http://localhost/swagger/v1/swagger.json";
                    var response = await client.GetAsync(url);
                    response.StatusCode.Should().Be(HttpStatusCode.OK);

                    var responseString = await response.Content.ReadAsStringAsync();

                    dynamic actualDeserializeObject = JsonConvert.DeserializeObject(responseString);
                    As((JValue) actualDeserializeObject.openapi).Should().Be("3.0.1");
                    As((JValue) actualDeserializeObject.info.version).Should().Be("v1");
                    As((JValue) actualDeserializeObject.info.title).Should().Be("API");

                    var actualSerializeObject = JsonConvert.SerializeObject((object) actualDeserializeObject, Formatting.Indented);
                    actualSerializeObject.Should().Contain("\"/abc\"");

//                    responseString.Should().Be("{\"swagger\":\"2.0\",\"info\":{\"version\":\"v1\",\"title\":\"API\"},\"basePath\":\"/\",\"paths\":{\"/abc\":{\"get\":{\"tags\":[\"abc\"],\"operationId\":\"AbcGet\",\"consumes\":[],\"produces\":[\"text/plain\",\"application/json\",\"application/pretty-json\",\"application/xml\",\"text/xml\"],\"responses\":{\"200\":{\"description\":\"Success\",\"schema\":{\"type\":\"array\",\"items\":{\"type\":\"string\"}}},\"definitions\":{},\"securityDefinitions\":{}}");
                }
            }
        }
    }
}