﻿namespace Base2art.WebApiRunner.Server.Features.Integration.Cors
{
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Fixtures;
    using FluentAssertions;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Testability.Configuration;
    using Web.App.Configuration;
    using Xunit;

    public class CorsFeature
    {
        private static IServerConfiguration GetConfiguration()
        {
            var serverConfiguration = ServerConfiguration.Create();

            serverConfiguration.Cors.AddRange(
                                              new[]
                                              {
                                                  new CorsItemConfiguration
                                                  {
                                                      AllowAnyOrigin = true,
                                                      AllowAnyMethod = true,
                                                      AllowAnyHeader = true,
                                                      Origins = {"base2art.com"}
                                                  },
                                                  new CorsItemConfiguration
                                                  {
                                                      Headers = {"content-type", "accept", "content-type, accept"},
                                                      Origins = {"specified-origin.com"}
                                                  },
                                                  new CorsItemConfiguration
                                                  {
                                                      SupportsCredentials = true,
                                                      Origins = {"boofoo.net"}
                                                  }
                                              });

            serverConfiguration.Endpoints.Add(new EndpointConfiguration
                                              {
                                                  Url = "api/employees",
                                                  Type = typeof(CustomController),
                                                  Method = RouteManagers.MethodOf<CustomController>(x => x.List()),
                                                  Verb = HttpVerb.Get
                                              });
            return serverConfiguration;
        }

        [Fact]
        public async void RequestPublic()
        {
            using (var fixture = IntegrationTests.CreateFixture(GetConfiguration(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;
                // SET DATA

                var message = new HttpRequestMessage(HttpMethod.Options, "/api/employees");
                message.Headers.Add("Origin", "https://boofoo.net");
                message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
                message.Headers.Add("Access-Control-Request-Method", HttpMethod.Put.Method);

                var setupResponse = await client.SendAsync(message);
                HttpStatusCode.NoContent.Should().Be(setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                var dict = setupResponse.Headers.ToDictionary(x => x.Key, x => string.Join("", x.Value));
                dict.Should().ContainKey("Access-Control-Allow-Origin");
                dict.Should().ContainKey("Access-Control-Allow-Methods");
                dict.Should().ContainKey("Access-Control-Allow-Credentials");
                dict.Should().NotContainKey("Access-Control-Allow-Headers");
                dict["Access-Control-Allow-Origin"].Should().Be("https://boofoo.net");
                dict["Access-Control-Allow-Methods"].Should().Be("PUT");
                dict["Access-Control-Allow-Credentials"].Should().Be("true");
            }
        }

        [Fact]
        public async void RequestPublic_Headers()
        {
            using (var fixture = IntegrationTests.CreateFixture(GetConfiguration(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;

                //// SET DATA

                var message = new HttpRequestMessage(HttpMethod.Options, "/api/employees");
                message.Headers.Add("Origin", "https://specified-origin.com");
                message.Headers.Add("Access-Control-Request-Method", HttpMethod.Put.Method);
                message.Headers.Add("Access-Control-Request-Headers", "content-type, accept");
                var setupResponse = await client.SendAsync(message);
                HttpStatusCode.NoContent.Should().Be(setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                var dict = setupResponse.Headers.ToDictionary(x => x.Key, x => string.Join("", x.Value));
                dict.Should().ContainKey("Access-Control-Allow-Origin");
                dict.Should().ContainKey("Access-Control-Allow-Methods");

//                dict.Should().ContainKey("Access-Control-Allow-Credentials");
                dict.Should().ContainKey("Access-Control-Allow-Headers");
                dict["Access-Control-Allow-Origin"].Should().Be("https://specified-origin.com");
                dict["Access-Control-Allow-Methods"].Should().Be("PUT");

//                dict["Access-Control-Allow-Credentials"].Should().Be("true");
                dict["Access-Control-Allow-Headers"].Should().Be("accept,content-type");
            }
        }

        [Fact]
        public async void RequestPublicNoOrigin()
        {
            using (var fixture = IntegrationTests.CreateFixture(GetConfiguration(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;
// SET DATA
                var message = new HttpRequestMessage(HttpMethod.Options, "/api/employees");
// NOT REGISTERED
                message.Headers.Add("Origin", "https://boofoo.com");
                var setupResponse = await client.SendAsync(message);
                HttpStatusCode.NotFound.Should().Be(setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                var dict = setupResponse.Headers.ToDictionary(x => x.Key, x => string.Join("", x.Value));
                dict.Should().NotContainKey("Access-Control-Allow-Origin");
                dict.Should().NotContainKey("Access-Control-Allow-Credentials");
                dict.Should().NotContainKey("Access-Control-Allow-Methods");
            }
        }

        [Fact]
        public async void RequestPublicWithDefaults()
        {
            using (var fixture = IntegrationTests.CreateFixture(GetConfiguration(), new TestRouteConfigurationRegistrar()))
            {
                var client = fixture.Client;
// SET DATA
                var message = new HttpRequestMessage(HttpMethod.Options, "/api/employees");
                message.Headers.Add("Origin", "http://base2art.com");
                message.Headers.Add("Access-Control-Request-Method", HttpMethod.Put.Method);
                var setupResponse = await client.SendAsync(message);
                HttpStatusCode.NoContent.Should().Be(setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                var dict = setupResponse.Headers.ToDictionary(x => x.Key, x => string.Join("", x.Value));
                dict.Should().ContainKey("Access-Control-Allow-Origin");
                dict.Should().ContainKey("Access-Control-Allow-Methods");
                dict["Access-Control-Allow-Origin"].Should().Be("http://base2art.com");
//                dict["Access-Control-Allow-Credentials"].Should().Be("false");
                dict["Access-Control-Allow-Methods"].Should().Be("PUT");
            }
        }
    }
}