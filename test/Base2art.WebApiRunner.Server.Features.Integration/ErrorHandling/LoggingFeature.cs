﻿namespace Base2art.WebApiRunner.Server.Features.Integration.ErrorHandling
{
    using System;
    using System.IO;
    using System.Net;
    using Fixtures;
    using FluentAssertions;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Testability.Configuration;
    using Web.App.Configuration;
    using Web.App.Logging;
    using Xunit;
    using Xunit.Abstractions;

    public class LoggingFeature
    {
        private readonly ITestOutputHelper output;

        public LoggingFeature(ITestOutputHelper output) => this.output = output;

        private static IServerConfiguration GetConfiguration(bool allowStackTrace)
        {
            var serverConfiguration = ServerConfiguration.Create();

            serverConfiguration.Endpoints.Add(new EndpointConfiguration
                                              {
                                                  Url = "api/default/{exceptionType}",
                                                  Type = typeof(ErrorController),
                                                  Method = RouteManagers.MethodOf<ErrorController>(x => x.List("")),
                                                  Verb = HttpVerb.Get
                                              });

            serverConfiguration.Application.Exceptions.AllowStackTraceInOutput = allowStackTrace;
            serverConfiguration.Application.Exceptions.RegisterCommonHandlers = true;
            serverConfiguration.Application.Exceptions.Loggers.Add(new ExecutableInstanceConfiguration
                                                                   {
                                                                       Type = typeof(ExceptionLogger),
                                                                       Method = RouteManagers.MethodOf<ExceptionLogger>(x => x.HandleException(null)),
                                                                       Parameters =
                                                                       {
                                                                           {"siteName", ""},
                                                                           {"basePath", TempPath()}
                                                                       }
                                                                   });

            return serverConfiguration;
        }

        private static string TempPath()
        {
            var tempPath = Path.GetTempPath();
            tempPath = Path.Combine(tempPath, "TEST-LOG-DIR");
            return Directory.CreateDirectory(tempPath).FullName;
        }

        [Theory]
        [InlineData(typeof(InvalidCastException), true, true, true, HttpStatusCode.InternalServerError)]
        [InlineData(typeof(InvalidCastException), false, false, true, HttpStatusCode.InternalServerError)]
        [InlineData(typeof(InvalidOperationException), true, false, false, HttpStatusCode.Conflict)]
        [InlineData(typeof(InvalidOperationException), false, false, false, HttpStatusCode.Conflict)]
        public async void RequestPublic(
            Type exceptionType,
            bool allowStackTrace,
            bool expectedHasDetails,
            bool expectedHasLogFile,
            HttpStatusCode statusCode)
        {
            Directory.Delete(TempPath(), true);

            using (var fixture = IntegrationTests.CreateFixture(GetConfiguration(allowStackTrace), new TestRouteConfigurationRegistrar()))
            {
                Directory.GetFiles(TempPath(), "*.log", SearchOption.AllDirectories).Length.Should().Be(0);
                var client = fixture.Client;
                // SET DATA
                var url = $"/api/default/{exceptionType.FullName}";
                var setupResponse = await client.GetAsync(url);
                var content = await setupResponse.Content.ReadAsStringAsync();

                setupResponse.StatusCode.Should().Be(statusCode, content);

                if (expectedHasDetails)
                {
                    content.Should().Contain("\"details\"", content);
                    this.output.WriteLine(content);
                }
                else
                {
                    content.Should().NotContain("\"details\"");
                }
            }

            Directory.GetFiles(TempPath(), "*.log", SearchOption.AllDirectories).Length.Should().Be(expectedHasLogFile ? 1 : 0);
        }
    }
}