﻿namespace Base2art.WebApiRunner.Server.Features.Integration.TaskScheduling
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Fixtures;
    using FluentAssertions;
    using Standard.WebApiRunner.Samples.Public.Tasks;
    using Testability;
    using Testability.Configuration;
    using Xunit;

    public class BindingTaskFeature
    {
        private async Task<string> GetSwagger(HttpClient client)
        {
            var url = "http://localhost/swagger/v1/swagger.json";
            var response = await client.GetAsync(url);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            return await response.Content.ReadAsStringAsync();
        }

        private static async Task<string> GetRealSimpleTask(Func<HttpClient, Func<Task>, Task<string>> producer)
        {
            var methodType = typeof(Dictionary<string, object>);

            var executeAsyncName = nameof(FileWritingTask.ExecuteAsync);
            var methodInfo = typeof(FileWritingTask).GetMethod(executeAsyncName, new[] {methodType});

            var expected = DateTime.UtcNow.ToString();
            var config = ServerConfiguration.Create();

            var delay = TimeSpan.FromSeconds(1.0);
            var interval = TimeSpan.FromDays(1);

            config.Tasks.Add(new TaskConfiguration
                             {
                                 Delay = delay,
                                 Name = "abc",
                                 Type = typeof(FileWritingTask),
                                 Method = methodInfo,
                                 Interval = interval
                             });

            var hashCode = methodInfo.GetParameters()[0].ParameterType.FullName.GetHashCode();

            var outputPath = Path.Combine(Path.GetTempPath(), $"{nameof(FileWritingTask)}.{nameof(FileWritingTask.ExecuteAsync)}-{hashCode}.log");
            try
            {
                using (var fixture = IntegrationTests.CreateFixture(config))
                {
                    var x = fixture.Client;

                    return await producer(x, async () =>
                    {
                        await Task.Delay(TimeSpan.FromSeconds(3.0));
                        File.Exists(outputPath).Should().BeTrue();
                        var content = File.ReadAllText(outputPath);
                        var dt = DateTime.Parse(content);
                        dt.Should().BeCloseTo(DateTime.Parse(expected).AddSeconds(1), TimeSpan.FromSeconds(1));
                    });
                }
            }
            finally
            {
                File.Delete(outputPath);
            }
        }

        [Fact]
        public async void GetSimpleTask1()
        {
            var swaggerText1 = await GetRealSimpleTask(async (client, next) =>
            {
                await next();
                var result = await this.GetSwagger(client);
                return result;
            });
            await Task.Delay(TimeSpan.FromSeconds(2));

            var swaggerText2 = await GetRealSimpleTask(async (client, next) =>
            {
                var result = await this.GetSwagger(client);
                await next();
                return result;
            });

            await Task.Delay(TimeSpan.FromSeconds(2));

            swaggerText1.Should().Be(swaggerText2);
        }
    }
}