﻿namespace Base2art.WebApiRunner.Server.Features.Integration.TaskScheduling
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using Fixtures;
    using FluentAssertions;
    using Server.TaskScheduling;
    using Standard.WebApiRunner.Samples.Public.Tasks;
    using Testability;
    using Testability.Configuration;
    using Xunit;

    public class SimpleTaskFeature
    {
        private static async Task GetRealSimpleTask(
            MethodInfo methodInfo,
            object input,
            string expected,
            bool mustDelete,
            bool passNull)
        {
            var config = ServerConfiguration.Create();

            var delay = TimeSpan.FromSeconds(1.0);
            var interval = TimeSpan.FromDays(1);

            if (input != null)
            {
                delay = TimeSpan.Zero;
                interval = TimeSpan.Zero;
            }

            config.Tasks.Add(new TaskConfiguration
                             {
                                 Delay = delay,
                                 Name = "abc",
                                 Type = typeof(FileWritingTask),
                                 Method = methodInfo,
                                 Interval = interval
                             });

            var hashCode = methodInfo.GetParameters()[0].ParameterType.FullName.GetHashCode();

            var outputPath = Path.Combine(Path.GetTempPath(), $"{nameof(FileWritingTask)}.{nameof(FileWritingTask.ExecuteAsync)}-{hashCode}.log");
            try
            {
                using (var fixture = IntegrationTests.CreateFixture(config))
                {
                    var _ = fixture.Client;

                    if (input == null)
                    {
                        if (passNull)
                        {
                            await ProdTaskRegistrar.ExecuteJob("abc", null);
                            await Task.Delay(TimeSpan.FromSeconds(0.1));
                            File.Exists(outputPath).Should().BeTrue();
                            var content = File.ReadAllText(outputPath);
                            var dt = DateTime.Parse(content);
                            dt.Should().BeCloseTo(DateTime.Parse(expected).AddSeconds(1), TimeSpan.FromSeconds(1));
                        }
                        else
                        {
                            await Task.Delay(TimeSpan.FromSeconds(3.0));
                            File.Exists(outputPath).Should().BeTrue();
                            var content = File.ReadAllText(outputPath);
                            var dt = DateTime.Parse(content);
                            dt.Should().BeCloseTo(DateTime.Parse(expected).AddSeconds(1), TimeSpan.FromSeconds(1));
                        }
                    }
                    else
                    {
                        await ProdTaskRegistrar.ExecuteJob("abc", input);
                        await Task.Delay(TimeSpan.FromSeconds(0.1));
                        File.Exists(outputPath).Should().BeTrue();
                        var content = File.ReadAllText(outputPath);
                        content.Should().Be(expected);
                    }
                }
            }
            finally
            {
                try
                {
                    File.Delete(outputPath);
                }
                catch (Exception)
                {
                    if (mustDelete)
                    {
                        throw;
                    }
                }
            }
        }

        [Theory]
        [InlineData(typeof(Dictionary<string, object>), null, null, true)]
        [InlineData(typeof(IDictionary<string, object>), null, null, true)]
        [InlineData(typeof(IReadOnlyDictionary<string, object>), null, null, true)]
        [InlineData(typeof(Dictionary<string, string>), null, null, true)]
        [InlineData(typeof(IDictionary<string, string>), null, null, true)]
        [InlineData(typeof(IDictionary<string, int>), null, null, true)]
        [InlineData(typeof(IReadOnlyDictionary<string, string>), null, null, true)]
        [InlineData(typeof(Dictionary<string, object>), null, null, false)]
        [InlineData(typeof(IDictionary<string, object>), null, null, false)]
        [InlineData(typeof(IReadOnlyDictionary<string, object>), null, null, false)]
        [InlineData(typeof(Dictionary<string, string>), null, null, false)]
        [InlineData(typeof(IDictionary<string, string>), null, null, false)]
        [InlineData(typeof(IDictionary<string, int>), null, null, false)]
        [InlineData(typeof(IReadOnlyDictionary<string, string>), null, null, false)]
        [InlineData(typeof(Dictionary<string, object>), typeof(Dictionary<string, object>), "1", false)]
        [InlineData(typeof(IDictionary<string, object>), typeof(Dictionary<string, object>), "2", false)]
        [InlineData(typeof(IReadOnlyDictionary<string, object>), typeof(Dictionary<string, object>), "3", false)]
        [InlineData(typeof(Dictionary<string, string>), typeof(Dictionary<string, string>), "4", false)]
        [InlineData(typeof(IDictionary<string, string>), typeof(Dictionary<string, string>), "5", false)]
        [InlineData(typeof(IReadOnlyDictionary<string, string>), typeof(Dictionary<string, string>), "6", false)]
        [InlineData(typeof(IDictionary<string, object>), typeof(Dictionary<string, string>), "7", false)]
        [InlineData(typeof(IDictionary<string, string>), typeof(Dictionary<string, object>), "8", false)]
        [InlineData(typeof(IDictionary<string, string>), typeof(Dictionary<string, object>), "SjY", false)]
        public async void GetSimpleTask1(Type methodType, Type realType, string valueData, bool passNull)
        {
            var executeAsyncName = nameof(FileWritingTask.ExecuteAsync);
            var methodInfo = typeof(FileWritingTask).GetMethod(executeAsyncName, new[] {methodType});

            if (realType == null)
            {
                await this.Next(methodInfo, null, DateTime.UtcNow.ToString(), passNull);
            }
            else
            {
                dynamic item = Activator.CreateInstance(realType);

                item.Add("Name", valueData);

                await this.Next(methodInfo, (object) item, valueData, passNull);
            }
        }

        private async Task Next(MethodInfo methodOf, object input, string expected, bool passNull)
        {
            try
            {
                await GetRealSimpleTask(methodOf, input, expected, true, passNull);
                await Task.Delay(TimeSpan.FromSeconds(2));
                return;
            }
            catch (Exception)
            {
            }

            await GetRealSimpleTask(methodOf, input, expected, false, passNull);
        }
    }
}