namespace Base2art.WebApiRunner.Server.Features.Integration.Aspects
{
    using System;
    using System.Net.Http;
    using System.Text;
    using Fixtures;
    using FluentAssertions;
    using Standard.WebApiRunner.Samples.Aspects;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Testability.Configuration;
    using Xunit;

    public class SimpleTasksAspectFeature
    {
        [Theory]
        [InlineData("http://localhost/tasks/abc/test", true, false, "[\"Item0\",\"Item1\",\"Item2\",\"Item3\"]")]
        [InlineData("http://localhost/tasks/abc/test", false, true, "[\"Item0\",\"Item1\",\"Item2\",\"Item3\"]")]
        [InlineData("http://localhost/tasks/abc/test", true, true, "[\"Item0\",\"Item0\",\"Item1\",\"Item2\",\"Item3\",\"Item3\"]")]
        public async void ShouldProcessTask(string url, bool aspectOnMethod, bool aspectOnCollection, string expected)
        {
            var serverConfiguration = ServerConfiguration.Create();
            serverConfiguration.Tasks.Add(new TaskConfiguration
                                          {
                                              Name = "abc/test",
                                              Type = typeof(CustomController),
                                              Method = RouteManagers.MethodOf<CustomController>(x => x.List())
                                          });

            if (aspectOnMethod)
            {
                serverConfiguration.Tasks[0].Aspects.Add(
                                                         new AspectConfiguration
                                                         {
                                                             Method = RouteManagers.MethodOf<CustomAspect>(x => x.AspectIt()),
                                                             Type = typeof(CustomAspect)
                                                         });
            }

            if (aspectOnCollection)
            {
                serverConfiguration.Tasks[0].Aspects.Add(
                                                         new AspectConfiguration
                                                         {
                                                             Method = RouteManagers.MethodOf<CustomAspect>(x => x.AspectIt()),
                                                             Type = typeof(CustomAspect)
                                                         });
            }

            using (var fixture = IntegrationTests.CreateFixture(serverConfiguration))
            {
                var client = fixture.Client;
                var request = new HttpRequestMessage
                              {
                                  RequestUri = new Uri(url),
                                  Method = HttpMethod.Post,
                                  Content = new StringContent("{test:'abc'}", Encoding.UTF8, "application/json")
                              };

                var response = await client.SendAsync(request);

                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                responseString.Trim().Should().Be(expected.Replace("\r", string.Empty).Trim());
            }
        }
    }
}