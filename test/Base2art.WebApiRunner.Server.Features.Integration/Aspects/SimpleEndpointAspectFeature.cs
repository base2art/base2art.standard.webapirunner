namespace Base2art.WebApiRunner.Server.Features.Integration.Aspects
{
    using System;
    using System.Net.Http;
    using Fixtures;
    using FluentAssertions;
    using Standard.WebApiRunner.Samples.Aspects;
    using Standard.WebApiRunner.Samples.Public.Endpoints;
    using Testability;
    using Testability.Configuration;
    using Web.App.Configuration;
    using Xunit;

    public class SimpleEndpointAspectFeature
    {
        [Theory]
        [InlineData("http://localhost/", true, false, "[\"Item0\",\"Item1\",\"Item2\",\"Item3\"]")]
        [InlineData("http://localhost/", false, true, "[\"Item0\",\"Item1\",\"Item2\",\"Item3\"]")]
        [InlineData("http://localhost/", true, true, "[\"Item0\",\"Item0\",\"Item1\",\"Item2\",\"Item3\",\"Item3\"]")]
        public async void ShouldProcessEndpoint(string url, bool aspectOnMethod, bool aspectOnCollection, string expected)
        {
            var serverConfiguration = ServerConfiguration.Create();
            serverConfiguration.Endpoints.Add(new EndpointConfiguration
                                              {
                                                  Verb = HttpVerb.Get,
                                                  Url = "",
                                                  Type = typeof(CustomController),
                                                  Method = RouteManagers.MethodOf<CustomController>(x => x.List())
                                              });

            if (aspectOnMethod)
            {
                serverConfiguration.Endpoints[0].Aspects.Add(
                                                             new AspectConfiguration
                                                             {
                                                                 Method = RouteManagers.MethodOf<CustomAspect>(x => x.AspectIt()),
                                                                 Type = typeof(CustomAspect)
                                                             });
            }

            if (aspectOnCollection)
            {
                serverConfiguration.Endpoints[0].Aspects.Add(
                                                             new AspectConfiguration
                                                             {
                                                                 Method = RouteManagers.MethodOf<CustomAspect>(x => x.AspectIt()),
                                                                 Type = typeof(CustomAspect)
                                                             });
            }

            using (var fixture = serverConfiguration.CreateFixture())
            {
                var client = fixture.Client;
                var request = new HttpRequestMessage
                              {
                                  RequestUri = new Uri(url),
                                  Method = HttpMethod.Get
                              };

                var response = await client.SendAsync(request);

                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                responseString.Trim().Should().Be(expected.Replace("\r", string.Empty).Trim());
            }
        }
    }
}