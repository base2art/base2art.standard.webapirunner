namespace Base2art.WebApiRunner.Server.Features.Integration.Aspects
{
    using System;
    using System.Net.Http;
    using Fixtures;
    using FluentAssertions;
    using Newtonsoft.Json;
    using Server.Routing.Models;
    using Standard.WebApiRunner.Samples.Aspects;
    using Standard.WebApiRunner.Samples.Public.HealthChecks;
    using Testability;
    using Testability.Configuration;
    using Xunit;
    using Xunit.Abstractions;

    public class SimpleHealthChecksAspectFeature
    {
        private readonly ITestOutputHelper output;

        public SimpleHealthChecksAspectFeature(ITestOutputHelper output) => this.output = output;

        [Theory]
        [InlineData("http://localhost/healthchecks", true, false, "[{\"Key\":\"abc\",\"Value\":{\"Data\":null,\"Message\":\"\",\"IsHealthy\":true}}]",
            true)]
        [InlineData("http://localhost/healthchecks", false, false,
            "[{\"Key\":\"abc\",\"Value\":{\"Data\":null,\"Message\":\"\",\"IsHealthy\":true}}]", true)]
        [InlineData("http://localhost/healthchecks", false, true,
            "[{\"Key\":\"abc\",\"Value\":{\"Data\":{\"next\":\"here\"},\"Message\":\"\",\"IsHealthy\":true}}]", true)]
        [InlineData("http://localhost/healthchecks", true, true,
            "[{\"Key\":\"abc\",\"Value\":{\"Data\":{\"next\":\"here\"},\"Message\":\"\",\"IsHealthy\":true}}]", true)]
        [InlineData("http://localhost/healthchecks/abc", true, false,
            "[{\"Key\":\"abc\",\"Value\":{\"Data\":null,\"Message\":\"\",\"IsHealthy\":true}}]", true)]
        [InlineData("http://localhost/healthchecks/abc", false, false,
            "[{\"Key\":\"abc\",\"Value\":{\"Data\":null,\"Message\":\"\",\"IsHealthy\":true}}]", true)]
        [InlineData("http://localhost/healthchecks/abc", false, true,
            "[{\"Key\":\"abc\",\"Value\":{\"Data\":{\"next\":\"here\"},\"Message\":\"\",\"IsHealthy\":true}}]", true)]
        [InlineData("http://localhost/healthchecks/abc", true, true,
            "[{\"Key\":\"abc\",\"Value\":{\"Data\":{\"next\":\"here\"},\"Message\":\"\",\"IsHealthy\":true}}]", true)]
        public async void ShouldProcessHealthChecks(string url, bool aspectOnMethod, bool aspectOnCollection, string expected, bool isHealthy)
        {
            var serverConfiguration = ServerConfiguration.Create();
            serverConfiguration.HealthChecks.Items.Add(new HealthCheckConfiguration
                                                       {
                                                           Name = "abc",
                                                           Type = typeof(CustomHealthCheck),
                                                           Method = RouteManagers.MethodOf<CustomHealthCheck>(x => x.ExecuteAsync())
                                                       });

            if (aspectOnMethod)
            {
                this.output.WriteLine(aspectOnCollection.ToString());
            }

            if (aspectOnCollection)
            {
                serverConfiguration.HealthChecks.Aspects.Add(
                                                             new AspectConfiguration
                                                             {
                                                                 Method = RouteManagers.MethodOf<CustomHealthCheckAspect>(x => x.AspectIt()),
                                                                 Type = typeof(CustomHealthCheckAspect)
                                                             });
            }

            using (var fixture = serverConfiguration.CreateFixture())
            {
                var client = fixture.Client;
                var request = new HttpRequestMessage
                              {
                                  RequestUri = new Uri(url),
                                  Method = HttpMethod.Get
                              };

                var response = await client.SendAsync(request);

                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<HealthCheckResult>(responseString);
                result.IsHealthy.Should().Be(isHealthy);
                responseString = JsonConvert.SerializeObject(result.Items);
                responseString.Trim().Should().Be(expected.Replace("\r", string.Empty).Trim());
            }
        }
    }
}