namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Models
{
    public class Employee : Person
    {
        public string EmployeId { get; set; }
    }
}