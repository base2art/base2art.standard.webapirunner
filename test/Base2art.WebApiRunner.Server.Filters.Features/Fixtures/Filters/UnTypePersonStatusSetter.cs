namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System;
    using System.Net;
    using Models;

    public class UnTypePersonStatusSetter
    {
        public void SetStatusCode(Person content, Action<HttpStatusCode> setValue)
        {
            setValue((HttpStatusCode) content.Name.Length);
        }
    }
}