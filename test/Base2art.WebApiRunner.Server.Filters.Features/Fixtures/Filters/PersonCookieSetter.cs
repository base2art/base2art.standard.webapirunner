namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System.Collections.Generic;
    using System.Net;
    using Models;
    using Web.Filters.Specialized;

    public class PersonCookieSetter : SetCookieFilter<Person>
    {
        protected override void SetCookies(Person content, IList<Cookie> cookies)
        {
//            cookies.Append("name", content.Name);
            cookies.Add(new Cookie("name", content.Name));
        }
    }
}