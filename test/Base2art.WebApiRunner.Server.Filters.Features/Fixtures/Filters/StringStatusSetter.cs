namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System;
    using System.Net;
    using Web.Filters.Specialized;

    public class StringStatusSetter : SetStatusCodeFilter<string>
    {
        protected override void SetStatusCode(string content, Action<HttpStatusCode> setValue)
        {
            setValue((HttpStatusCode) content.Length);
        }
    }
}