namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System.Collections.Generic;
    using Models;

    public class UnTypePersonHeaderSetter
    {
        public void SetHeaders(Person content, ICollection<KeyValuePair<string, string>> headers)
        {
            headers.Add(new KeyValuePair<string, string>("Authorization", "Bearer " + content.Name));
        }
    }
}