namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System.Collections.Generic;
    using Models;
    using Web.Filters.Specialized;

    public class PersonHeaderSetter : SetHeadersFilter<Person>
    {
        protected override void SetHeaders(Person content, ICollection<KeyValuePair<string, string>> headers)
        {
            headers.Add(new KeyValuePair<string, string>("Authorization", "Bearer " + content.Name));
        }
    }
}