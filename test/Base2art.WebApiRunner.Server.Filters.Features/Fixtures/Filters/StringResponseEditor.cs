namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using Web.Filters;
    using Web.Http;

    public class StringResponseEditor : IResponseFilter<string>
    {
        public void Filter(string content, IHttpRequest request, IHttpResponse response)
        {
        }

        void IResponseFilter.Filter(object content, IHttpRequest request, IHttpResponse response)
            => this.Filter((string) content, request, response);
    }
}