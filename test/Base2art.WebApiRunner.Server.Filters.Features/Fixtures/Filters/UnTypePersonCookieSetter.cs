namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System.Collections.Generic;
    using System.Net;
    using Models;

    public class UnTypePersonCookieSetter
    {
        public void SetCookies(Person content, IList<Cookie> cookies)
        {
            cookies.Add(new Cookie("name", content.Name));
        }
    }
}