namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System;
    using System.Net;
    using Models;
    using Web.Filters.Specialized;

    public class PersonRedirectSetter : RedirectFilter<Person>
    {
        protected override void SetRedirect(Person content, Action<HttpStatusCode, string> setRedirect)
        {
            setRedirect(HttpStatusCode.Redirect, content.Name);
        }
    }
}