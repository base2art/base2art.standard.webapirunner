namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System;
    using System.Net;
    using Web.Filters.Specialized;

    public class StringRedirectSetter : RedirectFilter<string>
    {
        protected override void SetRedirect(string content, Action<HttpStatusCode, string> setRedirect)
        {
            setRedirect(HttpStatusCode.Redirect, content);
        }
    }
}