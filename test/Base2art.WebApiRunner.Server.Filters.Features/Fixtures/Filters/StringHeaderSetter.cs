namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System.Collections.Generic;
    using Web.Filters.Specialized;

    public class StringHeaderSetter : SetHeadersFilter<string>
    {
        protected override void SetHeaders(string content, ICollection<KeyValuePair<string, string>> headers)
        {
            headers.Add(new KeyValuePair<string, string>("Authorization", "Bearer " + content));
        }
    }
}