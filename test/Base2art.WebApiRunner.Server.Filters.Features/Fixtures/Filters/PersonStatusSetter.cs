namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System;
    using System.Net;
    using Models;
    using Web.Filters.Specialized;

    public class PersonStatusSetter : SetStatusCodeFilter<Person>
    {
        protected override void SetStatusCode(Person content, Action<HttpStatusCode> setValue)
        {
            setValue((HttpStatusCode) content.Name.Length);
        }
    }
}