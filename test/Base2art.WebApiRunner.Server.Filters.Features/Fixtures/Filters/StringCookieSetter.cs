namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System.Collections.Generic;
    using System.Net;
    using Web.Filters.Specialized;

    public class StringCookieSetter : SetCookieFilter<string>
    {
        protected override void SetCookies(string content, IList<Cookie> cookies)
        {
//            cookies.Append("name", content);
            cookies.Add(new Cookie("name", content));
        }
    }
}