namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System.Collections.Generic;

    public class UnTypeStringHeaderSetter
    {
        public void SetHeaders(string content, ICollection<KeyValuePair<string, string>> headers)
        {
            headers.Add(new KeyValuePair<string, string>("Authorization", "Bearer " + content));
        }
    }
}