namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System;
    using System.Net;
    using Models;

    public class UnTypePersonRedirectSetter
    {
        public void SetRedirect(Person content, Action<HttpStatusCode, string> setRedirect)
        {
            setRedirect(HttpStatusCode.Redirect, content.Name);
        }
    }
}