namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using Models;
    using Web.Filters;
    using Web.Http;

    public class PersonResponseEditor : IResponseFilter<Person>
    {
        public void Filter(Person content, IHttpRequest request, IHttpResponse response)
        {
        }

        void IResponseFilter.Filter(object content, IHttpRequest request, IHttpResponse response)
            => this.Filter((Person) content, request, response);
    }
}