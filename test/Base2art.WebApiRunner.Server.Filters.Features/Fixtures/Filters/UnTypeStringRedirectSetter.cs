namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System;
    using System.Net;

    public class UnTypeStringRedirectSetter
    {
        public void SetRedirect(string content, Action<HttpStatusCode, string> setRedirect)
        {
            setRedirect(HttpStatusCode.Redirect, content);
        }
    }
}