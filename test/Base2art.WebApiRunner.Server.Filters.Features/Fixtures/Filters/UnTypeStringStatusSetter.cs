namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System;
    using System.Net;

    public class UnTypeStringStatusSetter
    {
        public void SetStatusCode(string content, Action<HttpStatusCode> setValue)
        {
            setValue((HttpStatusCode) content.Length);
        }
    }
}