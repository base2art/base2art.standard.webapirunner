namespace Base2art.WebApiRunner.Server.Filters.Features.Fixtures.Filters
{
    using System.Collections.Generic;
    using System.Net;

    public class UnTypeStringCookieSetter
    {
        public void SetCookies(string content, IList<Cookie> cookies)
        {
            cookies.Add(new Cookie("name", content));
        }
    }
}