namespace Base2art.WebApiRunner.Server.Filters.Features
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Fixtures.Filters;
    using Fixtures.Models;
    using FluentAssertions;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Testability.Configuration;
    using Web.Filters;
    using Xunit;

    public class UnitTest1
    {
        [Theory]
        [InlineData(typeof(StringCookieSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringCookieSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonCookieSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonCookieSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonCookieSetter), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypeStringCookieSetter), typeof(IResponseFilter<object>), typeof(string), true)]
//        [InlineData(typeof(UnTypeStringCookieSetter), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypePersonCookieSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
//        [InlineData(typeof(UnTypePersonCookieSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
//        [InlineData(typeof(UnTypePersonCookieSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        //
        [InlineData(typeof(StringHeaderSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringHeaderSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonHeaderSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonHeaderSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonHeaderSetter), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypeStringHeaderSetter), typeof(IResponseFilter<object>), typeof(string), true)]
//        [InlineData(typeof(UnTypeStringHeaderSetter), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypePersonHeaderSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
//        [InlineData(typeof(UnTypePersonHeaderSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
//        [InlineData(typeof(UnTypePersonHeaderSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        //
        [InlineData(typeof(StringRedirectSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringRedirectSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonRedirectSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonRedirectSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonRedirectSetter), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypeStringRedirectSetter), typeof(IResponseFilter<object>), typeof(string), true)]
//        [InlineData(typeof(UnTypeStringRedirectSetter), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypePersonRedirectSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
//        [InlineData(typeof(UnTypePersonRedirectSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
//        [InlineData(typeof(UnTypePersonRedirectSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        //
        [InlineData(typeof(StringStatusSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringStatusSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonStatusSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonStatusSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonStatusSetter), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypeStringStatusSetter), typeof(IResponseFilter<object>), typeof(string), true)]
//        [InlineData(typeof(UnTypeStringStatusSetter), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypePersonStatusSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
//        [InlineData(typeof(UnTypePersonStatusSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
//        [InlineData(typeof(UnTypePersonStatusSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        //
        [InlineData(typeof(StringResponseEditor), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringResponseEditor), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonResponseEditor), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonResponseEditor), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonResponseEditor), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypeStringResponseEditor), typeof(IResponseFilter<object>), typeof(string), true)]
//        [InlineData(typeof(UnTypeStringResponseEditor), typeof(IResponseFilter<object>), typeof(object), false)]
//        [InlineData(typeof(UnTypePersonResponseEditor), typeof(IResponseFilter<object>), typeof(Employee), true)]
//        [InlineData(typeof(UnTypePersonResponseEditor), typeof(IResponseFilter<object>), typeof(Person), true)]
//        [InlineData(typeof(UnTypePersonResponseEditor), typeof(IResponseFilter<object>), typeof(object), false)]
        public void Test1(Type filterType, Type baseType, Type supportedCheck, bool expectedCheck)
        {
            var executableInstanceConfiguration = new ExecutableInstanceConfiguration
                                                  {
                                                      Type = filterType
                                                  };
            var item = FilterRegistrar.CreateResponseEditorFilter(new TempServiceProvider(), executableInstanceConfiguration);

//            item.Should().BeAssignableTo(baseType);

            item.Should().NotBeNull();

            this.Supports(item, supportedCheck).Should().Be(expectedCheck);
        }

//        [Fact(Skip = "SjY")]
//        public void Test2()
//        {
//            var executableInstanceConfiguration = new ExecutableInstanceConfiguration
//                                                  {
//                                                      Type = typeof(MultiSetter),
//                                                      Parameters = { },
//                                                      Properties = { }
//                                                  };
//            var items = FilterRegistrar.CreateResponseEditorFilter(null, executableInstanceConfiguration);
////            items.Should().HaveCount(3).And.Subject.Should().NotContainNulls();
//
////            foreach (var item in items)
////            {
////                this.Supports(item, typeof(Person)).Should().Be(true);
////            }
//        }

        private bool Supports(IFilterMetadata item, Type type)
        {
            var itemTyped = (dynamic) item;
            return (bool) itemTyped.SupportsType(type);
        }

        private class TempServiceProvider : IServiceProvider
        {
            public object GetService(Type serviceType) => new TempCreator();
        }

        private class TempCreator : ICreator
        {
            public object Create(Type type, IReadOnlyDictionary<string, object> parameters, IReadOnlyDictionary<string, object> properties)
                => Activator.CreateInstance(type);

            public object Create(TypeInfo type, IReadOnlyDictionary<string, object> parameters, IReadOnlyDictionary<string, object> properties)
                => Activator.CreateInstance(type);

            public object CreateAndInvoke(Type type, MethodInfo methodInfo, IReadOnlyDictionary<string, object> parameters,
                                          IReadOnlyDictionary<string, object> properties)
                => Activator.CreateInstance(type);

            public object CreateAndInvoke(
                TypeInfo type,
                MethodInfo methodInfo,
                IReadOnlyDictionary<string, object> parameters,
                IReadOnlyDictionary<string, object> properties)
                => Activator.CreateInstance(type);
        }

//        private class MultiSetter : IResponseFilter<Person>, IResponseFilter<Person>, IResponseFilter<Person>
//        {
//            public void SetCookies(Person content, IList<Cookie> cookies)
//            {
//            }
//
//            public void SetHeaders(Person content, ICollection<KeyValuePair<string, string>> headers)
//            {
//            }
//
//            public void SetRedirect(Person content, Action<HttpStatusCode, string> setRedirect)
//            {
//            }
//        }
    }
}

/*
 *
 *
        [Theory]
        [InlineData(typeof(StringCookieSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringCookieSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonCookieSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonCookieSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonCookieSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypeStringCookieSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(UnTypeStringCookieSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypePersonCookieSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(UnTypePersonCookieSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(UnTypePersonCookieSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        //
        [InlineData(typeof(StringHeaderSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringHeaderSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonHeaderSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonHeaderSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonHeaderSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypeStringHeaderSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(UnTypeStringHeaderSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypePersonHeaderSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(UnTypePersonHeaderSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(UnTypePersonHeaderSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        //
        [InlineData(typeof(StringRedirectSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringRedirectSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonRedirectSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonRedirectSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonRedirectSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypeStringRedirectSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(UnTypeStringRedirectSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypePersonRedirectSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(UnTypePersonRedirectSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(UnTypePersonRedirectSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        //
        [InlineData(typeof(StringStatusSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringStatusSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonStatusSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonStatusSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonStatusSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypeStringStatusSetter), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(UnTypeStringStatusSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypePersonStatusSetter), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(UnTypePersonStatusSetter), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(UnTypePersonStatusSetter), typeof(IResponseFilter<object>), typeof(object), false)]
        //
        [InlineData(typeof(StringResponseEditor), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(StringResponseEditor), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(PersonResponseEditor), typeof(IResponseFilter<object>), typeof(Employee), true)]
        [InlineData(typeof(PersonResponseEditor), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(PersonResponseEditor), typeof(IResponseFilter<object>), typeof(object), false)]
        [InlineData(typeof(UnTypeStringResponseEditor), typeof(IResponseFilter<object>), typeof(string), true)]
        [InlineData(typeof(UnTypeStringResponseEditor), typeof(IResponseFilter<object>), typeof(object),
            false)]
        [InlineData(typeof(UnTypePersonResponseEditor), typeof(IResponseFilter<object>), typeof(Employee),
            true)]
        [InlineData(typeof(UnTypePersonResponseEditor), typeof(IResponseFilter<object>), typeof(Person), true)]
        [InlineData(typeof(UnTypePersonResponseEditor), typeof(IResponseFilter<object>), typeof(object),
            false)]
        public void Test1(Type filterType, Type baseType, Type supportedCheck, bool expectedCheck)
        {
            var executableInstanceConfiguration = new ExecutableInstanceConfiguration
                                                  {
                                                      Type = filterType,
                                                      Parameters = { },
                                                      Properties = { }
                                                  };
            var item = FilterRegistrar.CreateResponseEditorFilter(null, executableInstanceConfiguration);

            item.Should().BeAssignableTo(baseType);

            item.Should().NotBeNull();

            this.Supports(item, supportedCheck).Should().Be(expectedCheck);
        }
 * 
 */