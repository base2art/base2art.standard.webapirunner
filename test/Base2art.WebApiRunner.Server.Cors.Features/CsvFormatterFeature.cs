﻿namespace Base2art.WebApiRunner.Server.Cors.Features
{
    using FluentAssertions;
    using Testability.Configuration;
    using Xunit;

    public class CsvFormatterFeature
    {
        [Fact]
        public void ShouldMapHosts()
        {
            var item = new CorsItemConfiguration();
            item.Origins.Add("base2art.com");
            item.Origins.Add("https://test.base2art.com");
            item.Origins.Add("http://google.com/");
            item.Origins.Add("*.test.com");
            item.Origins.Add("*");

            var result = item.Hosts();

            result.Length.Should().Be(5);
            result[0].Should().Be("base2art.com");
            result[1].Should().Be("test.base2art.com");
            result[2].Should().Be("google.com");
            result[3].Should().Be("*.test.com");
            result[4].Should().Be("*");
        }

        [Fact]
        public void ShouldMapOrigins()
        {
            var item = new CorsItemConfiguration();
            item.Origins.Add("base2art.com");
            item.Origins.Add("https://test.base2art.com");
            item.Origins.Add("http://google.com/");
            item.Origins.Add("*.test.com");
            item.Origins.Add("*");

            var result = item.NormalizedOrigins();

            result.Length.Should().Be(8);
            result[0].Should().Be("http://base2art.com");
            result[1].Should().Be("https://base2art.com");
            result[2].Should().Be("https://test.base2art.com");
            result[3].Should().Be("http://google.com");
            result[4].Should().Be("http://*.test.com");
            result[5].Should().Be("https://*.test.com");
            result[6].Should().Be("*");
        }
    }
}