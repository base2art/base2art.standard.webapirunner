namespace Base2art.WebApiRunner.Server.Runners.Features
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using FluentAssertions;
    using RouteGeneration;
    using Standard.WebApiRunner.Samples;
    using Standard.WebApiRunner.Samples.Public.Resources;
    using Xunit;

    public class UnitTest1
    {
        [Fact]
        public void RouterCompileTest()
        {
            Router.CustomController.Exception.Link(nameof(InvalidCastException))
                  .Should().Be("/api/exception/InvalidCastException");
            Router.CustomController.Exception.Link(nameof(InvalidCastException))
                  .Should().Be("/api/exception/InvalidCastException");
            Router.CustomController.Exception.Link(nameof(InvalidCastException), "item")
                  .Should().Be("/api/exception/InvalidCastException?additionalData=item");

            Router.CustomController.List.Link().Should().Be("/api/test");
            Router.CustomController.AugmentPerson.Link().Should().Be("/api/test-person");
            Router.CustomController.AugmentPersons.Link().Should().Be("/api/test-person");
            Router.CustomController.GetFiltered.Link(new PaginationData {PageIndex = 1, PageSize = 2}).Should()
                  .Be("/api/test-paged?paginationData.PageIndex=1&paginationData.PageSize=2");
            Router.CustomController.GetParameter.Link(1).Should().Be("/api/test-parameter?i=1");
            Router.CustomController.GetParameterNullable.Link(1).Should().Be("/api/test-parameter-nullable?i=1");

            Router.CustomController.GetParameterOptional.Link().Should().Be("/api/test-parameter-optional");
            Router.CustomController.GetParameterOptional.Link().Should().Be("/api/test-parameter-optional");

            Router.CustomController.GetParameterOptional.Link(1).Should().Be("/api/test-parameter-optional?i=1");
            Router.CustomController.GetParameterOptional.Link(1).Should().Be("/api/test-parameter-optional?i=1");

            Router.CustomController.Exception.RouteInfo(nameof(InvalidCastException)).route
                  .Should().Be("/api/exception/InvalidCastException");
            Router.CustomController.Exception.RouteInfo(nameof(InvalidCastException)).route
                  .Should().Be("/api/exception/InvalidCastException");
            Router.CustomController.Exception.RouteInfo(nameof(InvalidCastException), "item").route
                  .Should().Be("/api/exception/InvalidCastException?additionalData=item");

            Router.CustomController.List.RouteInfo().route.Should().Be("/api/test");
            Router.CustomController.AugmentPerson.RouteInfo().route.Should().Be("/api/test-person");
            Router.CustomController.AugmentPersons.RouteInfo().route.Should().Be("/api/test-person");
            Router.CustomController.GetFiltered.RouteInfo(new PaginationData {PageIndex = 1, PageSize = 2}).route.Should()
                  .Be("/api/test-paged?paginationData.PageIndex=1&paginationData.PageSize=2");
            Router.CustomController.GetParameter.RouteInfo(1).route.Should().Be("/api/test-parameter?i=1");
            Router.CustomController.GetParameterNullable.RouteInfo(1).route.Should().Be("/api/test-parameter-nullable?i=1");

            Router.CustomController.GetParameterOptional.RouteInfo().route.Should().Be("/api/test-parameter-optional");
            Router.CustomController.GetParameterOptional.RouteInfo().route.Should().Be("/api/test-parameter-optional");

            Router.CustomController.GetParameterOptional.RouteInfo(1).route.Should().Be("/api/test-parameter-optional?i=1");
            Router.CustomController.GetParameterOptional.RouteInfo(1).route.Should().Be("/api/test-parameter-optional?i=1");

            Router.CustomController.Exception.RouteInfo(nameof(InvalidCastException)).method
                  .Should().Be(HttpMethod.Get);
            Router.CustomController.Exception.RouteInfo(nameof(InvalidCastException)).method
                  .Should().Be(HttpMethod.Get);
            Router.CustomController.Exception.RouteInfo(nameof(InvalidCastException), "item").method
                  .Should().Be(HttpMethod.Get);

            Router.CustomController.List.RouteInfo().method.Should().Be(HttpMethod.Get);
            Router.CustomController.AugmentPerson.RouteInfo().method.Should().Be(HttpMethod.Put);
            Router.CustomController.AugmentPersons.RouteInfo().method.Should().Be(new HttpMethod("Fake"));
            Router.CustomController.GetFiltered.RouteInfo(new PaginationData()).method.Should().Be(HttpMethod.Get);
            Router.CustomController.GetParameter.RouteInfo(1).method.Should().Be(HttpMethod.Get);
            Router.CustomController.GetParameterNullable.RouteInfo(1).method.Should().Be(HttpMethod.Get);

            Router.CustomController.GetParameterOptional.RouteInfo().method.Should().Be(HttpMethod.Get);
            Router.CustomController.GetParameterOptional.RouteInfo().method.Should().Be(HttpMethod.Get);

            Router.CustomController.GetParameterOptional.RouteInfo(1).method.Should().Be(HttpMethod.Get);
            Router.CustomController.GetParameterOptional.RouteInfo(1).method.Should().Be(HttpMethod.Get);

            Router.HealthCheckController.ExecuteAsync.Link().Should().Be("/healthchecks");
            Router.HealthCheckController.ExecuteAsync.RouteInfo().route.Should().Be("/healthchecks");
            Router.HealthCheckController.ExecuteAsync.RouteInfo().method.Should().Be(HttpMethod.Get);
        }

        [Fact]
        public void Test1()
        {
            var routes = typeof(Dictionary<string, Dictionary<string, int?>>).GetFriendlyName();

            routes.Should().Be("System.Collections.Generic.Dictionary<string, System.Collections.Generic.Dictionary<string, System.Nullable<int>>>");
        }
    }
}