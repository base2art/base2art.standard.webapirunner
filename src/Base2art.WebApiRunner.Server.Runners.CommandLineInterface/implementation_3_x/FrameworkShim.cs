namespace Base2art.WebApiRunner.Server.Runners.CommandLineInterface
{
    using System;
    using System.Reflection;
    using Microsoft.AspNetCore.Mvc.ActionConstraints;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.DependencyInjection;

    public class FrameworkShim : IFrameworkShim
    {
        public IRouter GetDefaultRouteHandler(IServiceProvider provider)
            => this.GetInstanceFrom<HttpMethodAttribute, IRouter>(provider, "Microsoft.AspNetCore.Mvc.Routing.MvcRouteHandler");

        public IRouter CreateMegaRoute(IServiceProvider provider)
        {
            var type = this.GetTypeFrom<HttpMethodAttribute>("Microsoft.AspNetCore.Mvc.Routing.AttributeRouting");

            var method = type.GetMethod("CreateAttributeMegaRoute", BindingFlags.Static | BindingFlags.Public);
            var megaRoute = method.Invoke(null, new object[] {provider});
            return (IRouter) megaRoute;
        }

        public IFilterMetadata CreateControllerActionFilter()
            => this.GetInstanceFrom<ActionFilterAttribute, IFilterMetadata>("Microsoft.AspNetCore.Mvc.Filters.ControllerActionFilter");

        public IFilterMetadata CreateControllerResultFilter()
            => this.GetInstanceFrom<ActionFilterAttribute, IFilterMetadata>("Microsoft.AspNetCore.Mvc.Filters.ControllerResultFilter");

        public IActionConstraintMetadata CreateHttpMethodActionConstraint(string[] methodNames)
            => new HttpMethodActionConstraint(methodNames);

        private TReturn GetInstanceFrom<TAssemblyContainer, TReturn>(string typeName)
            => (TReturn) Activator.CreateInstance(this.GetTypeFrom<TAssemblyContainer>(typeName));

        private TReturn GetInstanceFrom<TAssemblyContainer, TReturn>(IServiceProvider provider, string typeName)
            => (TReturn) provider.GetRequiredService(this.GetTypeFrom<TAssemblyContainer>(typeName));

        private Type GetTypeFrom<TAssemblyContainer>(string typeName)
            => typeof(TAssemblyContainer).Assembly
                                         .GetType(typeName, true, true);
    }
}