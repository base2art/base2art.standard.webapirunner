param (
    [Parameter(Mandatory = $true)][string]$outputPath
)


#windows build issue
#quote at the end messes it up
$outputPath = $outputPath.Trim()

$in_path = Join-Path $outputPath "Base2art.WebApiRunner.Server.Runners.CommandLineInterface.deps.json"
$out_path = Join-Path $outputPath "Base2art.WebApiRunner.Server.Runners.CommandLineInterface._altered_.deps.json"
cp "$( $in_path )" "$( $out_path )"

$data = Get-Content -Raw -Path $out_path | ConvertFrom-Json

$targets = $data.targets

$libraries = $data.libraries

$props = Get-Member -InputObject $targets -MemberType NoteProperty

$primaryTarget = { }
foreach ($prop in $props)
{
    $primaryTarget = $targets | Select-Object -ExpandProperty $prop.Name
}



$assms = Get-Member -InputObject $primaryTarget -MemberType NoteProperty

foreach ($prop in $assms)
{

    $asmKey = $prop.Name

    if ($asmKey.StartsWith("Base2art.") -and -not $asmKey.StartsWith("Base2art.WebApiRunner.Server.Runners.CommandLineInterface"))
    {
        $assm = $primaryTarget | Select-Object -ExpandProperty $asmKey


        $runtime = $assm.runtime
        $compile = $assm.compile

        $runtimeDllProps = Get-Member -InputObject $runtime -MemberType NoteProperty

        foreach ($runtimeDllProp in $runtimeDllProps)
        {
            $dllName = $runtimeDllProp.Name
            $dllValue = $runtime | Select-Object -ExpandProperty $dllName
            if (-not $dllName.StartsWith("lib"))
            {
                $children = Get-ChildItem -Path "../$( $asmKey.split("/")[0] )/bin/Release" -Directory
                $childName = $children[0].Name

                $runtime | Add-Member -Name "lib/$( $childName )/$( $dllName )" -Type NoteProperty -Value @{ }
                $runtime.PSObject.Properties.Remove($dllName)
            }
        }

        if ($compile)
        {
            $compileDllProps = Get-Member -InputObject $compile -MemberType NoteProperty
            foreach ($runtimeDllProp in $compileDllProps)
            {
                $dllName = $runtimeDllProp.Name
                $dllValue = $compile | Select-Object -ExpandProperty $dllName
                if (-not $dllName.StartsWith("lib"))
                {
                    $children = Get-ChildItem -Path "../$( $asmKey.split("/")[0] )/bin/Release" -Directory
                    $childName = $children[0].Name

                    $compile | Add-Member -Name "lib/$( $childName )/$( $dllName )" -Type NoteProperty -Value @{ }
                    $compile.PSObject.Properties.Remove($dllName)
                }
            }
        }
        #        echo ($runtime -eq $null)
    }
}


$props = Get-Member -InputObject $libraries -MemberType NoteProperty


foreach ($prop in $props)
{
    if ($prop.Name.StartsWith("Base2art.") -and -not $prop.Name.StartsWith("Base2art.WebApiRunner.Server.Runners.CommandLineInterface"))
    {
        $library = $libraries | Select-Object -ExpandProperty $prop.Name
        if ($library.type -eq "project")
        {
            $library.type = "package"
            $library.serviceable = $true

            $library | Add-Member -Name "path" -Type NoteProperty -Value $prop.Name.ToLowerInvariant()
        }
    }
}

$dataString = ConvertTo-Json $data -Depth 99
Set-Content -Path $out_path -Value $dataString
