$toolPath = $PSScriptRoot
. "$( $toolPath )/__WebapiDeployment_structs.ps1"


class __WebapiDeploymentInputNormalizer
{

}


function Normalize-Deployment-Inputs($inputFile, $outputFile)
{
    Write-Host "Processing File... $( $inputFile ) -> $( $outputFile )"

    $exploder = New-Object __WebapiDeploymentInputNormalizer

    $newDataLookup = (Get-Content $inputFile | Out-String | ConvertFrom-Json)

    $frames = @{ }
    

    foreach ($prop in $newDataLookup.psobject.properties)
    {

        $key = $prop.name

        Write-Host "Normalizing $( $key )"
        $newData = [__WebapiDeploymentData]$prop.value
        $environment = $newData.environment
        $framework = $newData.framework

        $frameDeploymentData = New-Object __WebapiDeploymentData

        foreach ($ii in $newData.bins)
        {
            $envii = $ii.Environment;
            if ($envii -eq $null -or $envii -eq "" -or $envii -eq $environment)
            {
                $iij = New-Object __WebapiDeploymentFile
                $iij.Name = $ii.Name
                $iij.Destination = "bin"
                $iij.Environment = $environment

                $frameDeploymentData.bins.Add($iij)
            }
        }

        foreach ($ii in $newData.assets)
        {
            $envii = $ii.Environment;
            if ($envii -eq $null -or $envii -eq "" -or $envii -eq $environment)
            {
                $iij = New-Object __WebapiDeploymentFile
                $iij.Name = $ii.Name
                $iij.Destination = $ii.Destination
                $iij.Environment = $environment

                $frameDeploymentData.assets.Add($iij)
            }
        }

        foreach ($ii in $newData.configs)
        {
            $envii = $ii.Environment;
            if ($envii -eq $null -or $envii -eq "" -or $envii -eq $environment)
            {
                $iij = New-Object __WebapiDeploymentFile
                $iij.Name = $ii.Name
                $iij.Destination = $ii.Destination
                $iij.Environment = $environment

                $frameDeploymentData.configs.Add($iij)
            }
        }

        $frameDeploymentData.packages.AddRange($newData.packages)
        $frameDeploymentData.framework = $framework
        $frameDeploymentData.environment = $environment
        $frameDeploymentData.binDir = $newData.binDir
        $frameDeploymentData.appBinDir = $newData.appBinDir

        $frames.Add($key, $frameDeploymentData)
    }



    $output = ConvertTo-Json $frames -Depth 15

    write-Host "Writing File: $( $outputFile )"
    Set-Content -Path $outputFile -Value $output
}