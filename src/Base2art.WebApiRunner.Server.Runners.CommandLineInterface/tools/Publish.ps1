param (
    [Parameter(Mandatory = $true)][string]$solutionDir,
    [Parameter(Mandatory = $true)][string]$projectDir,
    [Parameter(Mandatory = $true)][string]$outputPath,
    [Parameter(Mandatory = $true)][string]$publishPath
)

$solutionDir = $solutionDir.Trim()
$projectDir = $projectDir.Trim()
$outputPath = $outputPath.Trim()
$publishPath = $publishPath.Trim()

$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition


$path1 = Get-Item $solutionDir;
$path2 = Get-Item $projectDir;


$relParts = @()

while ((Join-Path $path1.FullName '') -ne (Join-Path $path2.FullName ''))
{
    $relParts += $path2.Name
    $path2 = $path2.Parent;
    
}

[array]::Reverse($relParts)

$relOutputPath = [System.String]::Join("/", $relParts + $outputPath)
$projectPath = [System.String]::Join("/", $relParts)


$scriptPathItem = Get-Item $scriptPath
#$packageRootPathItem = $scriptPathItem.Parent
$versionPathItem = $scriptPathItem.Parent
$packagePathItem = $versionPathItem.Parent
#$versionDir = Split-Path -Parent $scriptPath
#$packageDir = Split-Path -Parent $versionDir
#
#$packagesPath = Split-Path -Parent $packageDir


$content = @"
param ([string]`$toolsPath)

if ([System.String]::IsNullOrWhitespace(`$toolsPath))
{
  `$base = Resolve-Path "~"
  `$toolsPath = "`$base/.nuget/packages/$($packagePathItem.Name)/$($versionPathItem.Name)/tools"
}

. "`$(`$toolsPath)/Deployment.ps1"

`$deploymentProcessor = New-Webapi-Deployment

`$deploymentProcessor.bin('$relOutputPath')

# PAckaaging Example
#`$deploymentProcessor.package("Newtonsoft.Json", "12.0.1")

# config example
#`$deploymentProcessor.config("input-file.yaml", "environment", "destinationName.yaml")
#`$deploymentProcessor.config("conf-processor.yaml", "Prod-Processor", "configuration.yaml")

Write-Host `$deploymentProcessor.deploy()


Exit 0

"@


Write-Host "Running Publish Setup..."
$packageFile = Join-Path $solutionDir "package-webapi.ps1"
if (-not (Test-Path $packageFile) ) {
    Set-Content -Path $packageFile -Value $content
}
Write-Host "Finished Publish Setup..."

Write-Host $packageFile
Write-Host "Running Publish..."

$old = $PWD
Write-Host "Package Paths:"
Write-Host "& "
Write-Host $packageFile
Write-Host "-toolsPath"
Write-Host $scriptPath
try
{
    cd $solutionDir
    Invoke-Expression "& `"$($packageFile)`" -toolsPath `"$scriptPath`""
}
finally
{
    cd $old
}



Write-Host "Finished Publish..."


#
#Write-Host $outputPath
#
#
#$versionDir = Split-Path -Parent $scriptPath
#$packageDir = Split-Path -Parent $versionDir
#
#$packagesPath = Split-Path -Parent $packageDir
