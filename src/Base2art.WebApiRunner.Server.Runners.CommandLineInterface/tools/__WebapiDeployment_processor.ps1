$toolPath = $PSScriptRoot
. "$( $toolPath )/__WebapiDeployment_structs.ps1"
. "$( $toolPath )/__WebapiDeployment_InputNormalizer.ps1"
. "$( $toolPath )/__WebapiDeployment_DependencyExploder.ps1"
. "$( $toolPath )/__WebapiDeployment_PackageExploder.ps1"
. "$( $toolPath )/__WebapiDeployment_ExplodedDeployer.ps1"



class __WebapiDeployment
{
    # Hidden Properties
    hidden [System.Collections.Generic.List[__WebapiDeploymentFile]] $bins

    hidden [System.Collections.Generic.List[__WebapiDeploymentFile]] $assets

    hidden [System.Collections.Generic.List[__WebapiDeploymentFile]] $configs

    hidden [System.Collections.Generic.List[__WebapiDeploymentPackage]] $packages
    
    hidden [System.Collections.Generic.List[string]] $frameworkItems

    hidden [String] $root = ".deploy"


    # Parameterless Constructor
    __WebapiDeployment()
    {
        $target = $this
        $target.bins = New-Object System.Collections.Generic.List[__WebapiDeploymentFile]
        $target.assets = New-Object System.Collections.Generic.List[__WebapiDeploymentFile]
        $target.configs = New-Object System.Collections.Generic.List[__WebapiDeploymentFile]
        $target.packages = New-Object System.Collections.Generic.List[__WebapiDeploymentPackage]
        $target.frameworkItems = New-Object System.Collections.Generic.List[string]

    }


    # Method
    [void]
    framework([String] $Name)
    {
        $target = $this
        $target.frameworkItems.Add($Name)
    }

    # Method
    [void]
    package([String] $name, [String] $version)
    {
        $file = [__WebapiDeploymentPackage]::new()

        $file.PackageName = "$( $name )"
        $file.PackageVersion = "$( $version )"
        $file.Destination = "bin"

        $target = $this
        $target.packages.Add($file)
    }

    # Method
    [void]
    bin([String] $Name)
    {
        $file = [__WebapiDeploymentFile]::new()

        $file.Name = "$( $Name )"

        $target = $this
        $target.bins.Add($file)
    }

    [void]
    bin([String] $Name, [String] $env)
    {
        $file = [__WebapiDeploymentFile]::new()

        $target = $this

        $file.Name = "$( $Name )"
        $file.Environment = "$( $env )"

        $target.bins.Add($file)
    }

    [void]
    asset([String] $Name)
    {
        $file = [__WebapiDeploymentFile]::new()

        $target = $this

        $file.Name = "$( $Name )"


        $target.assets.Add($file)
    }


    [void]
    asset([String] $Name, [String] $env, [String] $dest)
    {
        $file = [__WebapiDeploymentFile]::new()

        $target = $this

        $file.Name = "$( $Name )"
        $file.Environment = "$( $env )"
        $file.Destination = "$( $dest )"


        $target.assets.Add($file)
    }

    [void]
    config([String] $Name)
    {
        $file = [__WebapiDeploymentFile]::new()

        $target = $this

        $file.Name = "$( $Name )"
        $file.Environment = $null
        $file.Destination = $null


        $target.configs.Add($file)
    }

    [void]
    config([String] $Name, [String] $env, [String] $dest)
    {
        $file = [__WebapiDeploymentFile]::new()

        $target = $this

        $file.Name = "$( $Name )"
        $file.Environment = "$( $env )"
        $file.Destination = "$( $dest )"


        $target.configs.Add($file)
    }

    [System.Collections.Generic.List[String]]
    environments()
    {
        $target = $this

        $envs = New-Object 'System.Collections.Generic.HashSet[string]'

        foreach ($item in $target.bins)
        {
            $envs.Add($item.Environment)
        }

        foreach ($item in $target.assets)
        {
            $envs.Add($item.Environment)
        }

        foreach ($item in $target.configs)
        {
            $envs.Add($item.Environment)
        }

        $envs = $envs | Where-Object -FilterScript { $_ -ne '' -and $_ -ne $null }


        if ($envs.Count -eq 0)
        {
            $envs.Add("Production");
        }

        return $envs
    }

    [System.Collections.Generic.List[String]]
    frameworks()
    {
        $target = $this

        $frames = New-Object 'System.Collections.Generic.List[string]'

        if ($target.frameworkItems.Count -eq 0)
        {
            $frames.Add("netcoreapp2.1");
            $frames.Add("netcoreapp2.2");
        }
        else
        {
            foreach ($frame in $target.frameworkItems)
            {
                $frames.Add($frame);
            }
        }


        return $frames
    }


    [Void]
    deploy()
    {
        $myRoot = Get-Item $PSScriptRoot
        $target = $this
        #echo $target.environments()
        #echo $target.frameworks()

        if (-not(Test-Path $target.root))
        {
            mkdir $target.root
        }

        $objPath = "$( $target.root )/obj"
        if (-not(Test-Path $objPath))
        {
            mkdir $objPath
        }

        $data = New-Object __WebapiDeploymentData

        $data.packages.addRange($target.packages)
        $data.bins.addRange($target.bins)
        $data.assets.addRange($target.assets)
        $data.configs.addRange($target.configs)

        $log = ConvertTo-Json $data

        $baseVersion = "0.0.6.4"

        $deployDir = Get-Item $PSScriptRoot
        if ($deployDir.Name -eq "tools")
        {
            $baseVersion = $deployDir.Parent.Name
            Write-Host "baseVersion: $( $baseVersion )"
        }

        $frames = @{ }
        foreach ($framework in $this.frameworks())
        {
            foreach ($environment in $this.environments())
            {
                $template = ConvertFrom-Json $log
                $template.framework = $framework;
                $template.environment = $environment;

                $myPackages = New-Object System.Collections.Generic.List[__WebapiDeploymentPackage]

                #Write-Host $myPackages.GetType()
                foreach ($item1 in $template.packages)
                {
                    [__WebapiDeploymentPackage]$item2 = $item1

                    $rootPackage = new-Object __WebapiDeploymentPackage
                    $rootPackage.PackageName = $item2.PackageName
                    $rootPackage.PackageVersion = $item2.PackageVersion
                    $rootPackage.Destination = $item2.Destination

                    $myPackages.Add($rootPackage);
                }

                if ($framework -match "^iis-")
                {
                    $template.binDir = "bin";
                    $template.appBinDir = "bin/app";

                    $rootPackage = new-Object __WebapiDeploymentPackage
                    $rootPackage.PackageName = "Base2art.WebApiRunner.Server.Runners.IIS"
                    $rootPackage.PackageVersion = $baseVersion
                    $rootPackage.Destination = ""

                    $myPackages.Insert(0, $rootPackage);


                    $rootPackage = new-Object __WebapiDeploymentPackage
                    $rootPackage.PackageName = "Owin"
                    $rootPackage.PackageVersion = "1.0.0"
                    $rootPackage.Destination = ""

                    $myPackages.Insert(1, $rootPackage);
                }
                else
                {
                    $template.binDir = "";
                    $template.appBinDir = "bin";

                    $rootPackage = new-Object __WebapiDeploymentPackage
                    $rootPackage.PackageName = "Base2art.WebApiRunner.Server.Runners.CommandLineInterface"
                    $rootPackage.PackageVersion = $baseVersion
                    $rootPackage.Destination = ""


                    $myPackages.Insert(0, $rootPackage);
                }

                $template.packages = $myPackages

                $frames.Add("$( $framework )-$( $environment )", $template);
            }
        }

        $output = ConvertTo-Json $frames -Depth 15

        $dataPath = "$( $target.root )/obj/data.deploy"
        Set-Content -Path $dataPath -Value $output

        #Set-Content -Path $dataPath -Value $log
        Normalize-Deployment-Inputs "$( $target.root )/obj/data.deploy" "$( $target.root )/obj/data.environmentalized.deploy"
        Explode-Deployment-Dependencies "$( $target.root )/obj/data.environmentalized.deploy" "$( $target.root )/obj/data.exploded.deploy"

        Explode-Deployment-Packages "$( $target.root )/obj/data.exploded.deploy" "$( $target.root )/obj/data.packages.deploy"

        Deploy-Exploded "$( $target.root )/obj/data.packages.deploy" $target.root

        #return;
        #Write-Host $log
    }
}

