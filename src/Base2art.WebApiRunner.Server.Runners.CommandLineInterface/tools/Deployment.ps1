$toolPath = $PSScriptRoot


. "$( $toolPath )/__WebapiDeployment_structs.ps1"
. "$( $toolPath )/__WebapiDeployment_processor.ps1"


# Make .NET's current directory follow PowerShell's
# current directory, if possible.
if ($PWD.Provider.Name -eq 'FileSystem')
{
    [System.IO.Directory]::SetCurrentDirectory($PWD)
}

function New-Webapi-Deployment()
{

    $obj = [__WebapiDeployment]::new()
    return $obj
}


