$ServiceName =  "$($args[0])"
if ($serviceName -eq $null){ $ServiceName = "dotnet-core-service:" + [System.Guid]::NewGuid().ToString("n") }
$ConfigurationPath = Join-Path $PSScriptRoot "config/configuration.yaml"
$exePath = Join-Path $PSScriptRoot "Base2art.WebApiRunner.Server.Runners.CommandLineInterface.exe"
sc create "$($serviceName)" type=own  binpath="$($exePath) windows-service $($ConfigurationPath)" start=delayed-auto
