﻿$toolPath = $PSScriptRoot
. "$( $toolPath )/__WebapiDeployment_structs.ps1"


class __WebapiDeploymentPackageExploder
{
    [string[]]
    PathParts()
    {
        $pathVar = $env:PATH;
        if (-not([string]::IsNullOrWhiteSpace($pathVar)))
        {
            $pathParts = $pathVar.Split([System.IO.Path]::PathSeparator) |
                    Where-Object { ![String]::IsNullOrWhiteSpace($_) } |
                    Foreach-Object { $_.Trim() };
            return $pathParts;
        }

        return @()
    }

    [bool]
    TestReparsePoint([string]$path)
    {
        $file = Get-Item $path -Force -ea SilentlyContinue
        return [bool]($file.Attributes -band [IO.FileAttributes]::ReparsePoint)
    }

    [string]
    FindFileInPath([string[]]$exeNames)
    {
        $pathParts = $this.PathParts();

        foreach ($pathPart in $pathParts)
        {
            foreach ($exeName in $exeNames)
            {
                $next = [system.IO.Path]::Combine($pathPart, $exeName)

                if (Test-Path $next)
                {
                    if (-Not($this.TestReparsePoint($next)))
                    {
                        return $next;
                    }

                    $readlink = $this.FindFileInPath(@("readlink", "readlink.exe"))

                    if ($readlink -eq $null)
                    {
                        return $next;
                    }

                    $newPath = (readlink $next) | Out-String
                    $newPath = $newPath.Trim()

                    if (Test-Path $newPath)
                    {
                        return $newPath
                    }

                    return $next
                }
            }
        }

        return $null;
    }

    [String]
    GetCompatibleFramework($frameworks, $framework)
    {
        $netStandards = @(
        "netstandard2.0",
        "netstandard1.6",
        "netstandard1.5",
        "netstandard1.4",
        "netstandard1.3",
        "netstandard1.2",
        "netstandard1.1",
        "netstandard1.0",
        "netstandard"
        );

        $netStandards21 = @("netstandard2.1") + $netStandards
        
        $net460Base = @("net46", "net452", "net451", "net45", "net40", "net");
        $net460 = $net460Base + $netStandards
        $net461 = @("net461") + $net460
        $net462 = @("net462") + $net461
        $net470 = @("net47") + $net462
        $net471 = @("net471") + $net470
        $net472 = @("net472") + $net471
        $net480 = @("net48") + $net472


        $netcore21 = @("netcoreapp2.1", "netcoreapp2.0", "netcoreapp") + $netStandards;
        $netcore22 = @("netcoreapp2.2") + $netcore21
        $netcore30 = @("netcoreapp3.0", "netcoreapp2.2", "netcoreapp2.1", "netcoreapp2.0", "netcoreapp") + $netStandards21
        $netcore31 = @("netcoreapp3.1") + $netcore30
        $net50 = @("net5.0") + $netcore31
        $net60 = @("net6.0") + $net50
        $net70 = @("net7.0") + $net60
        $net80 = @("net8.0") + $net70


        $lookup = @{
            "net8.0" = $net80;
            "net7.0" = $net70;
            "net6.0" = $net60;
            "net5.0" = $net50;
            "netcoreapp3.1" = $netcore31;
            "netcoreapp3.0" = $netcore30;
            "netcoreapp2.2" = $netcore22;
            "netcoreapp2.1" = $netcore21;

            "net48" = $net480;

            "net472" = $net472;
            "net471" = $net471;
            "net47" = $net470;

            "net462" = $net462;
            "net461" = $net461;
            "net46" = $net460;

            "iis-net48" = $net480;

            "iis-net472" = $net472;
            "iis-net471" = $net471;
            "iis-net47" = $net470;

            "iis-net462" = $net462;
            "iis-net461" = $net461;
            "iis-net46" = $net460;
        };


        #Write-Host "$framework"

        $matches = $netStandards
        if ( $lookup.ContainsKey($framework))
        {
            $matches = $lookup[$framework]
        }


        foreach ($match in $matches)
        {
            foreach ($frameQuery in $frameworks)
            {
                if ($frameQuery -eq $match)
                {
                    return $frameQuery
                }
            }
        }

        return $null
    }

    [string]
    GetBasePath($package)
    {
        $packagesPathBase = Resolve-Path "~/.nuget/packages"
        $fallbackPathBase = $this.FindFileInPath(@("dotnet", "dotnet.exe"))
        $fallbackPathBase = Join-Path $fallbackPathBase "../sdk/NuGetFallbackFolder"

        $packagesPath = Join-Path $packagesPathBase "$($package.PackageName.ToLower() )/$($package.PackageVersion.ToLower() )"
        $fallbackPath = Join-Path $fallbackPathBase "$($package.PackageName.ToLower() )/$($package.PackageVersion.ToLower() )"

        $basePath = $null
        if (Test-Path $packagesPath)
        {
            $basePath = $packagesPath

        }
        elseif (Test-Path $fallbackPath)
        {
            $basePath = $fallbackPath
        }

        return $basePath;
    }

    [string[]]
    ExplodeDeploymentPackageLib($package, $framework)
    {
        $basePath = $this.GetBasePath($package)

        $results = @();

        if ($basePath -ne $null -and $basePath.length -gt 0)
        {
            $libPath = Join-Path $basePath "lib"
            if (Test-Path $libPath)
            {
                #Write-Host $libPath
                $framePaths = Get-ChildItem -Path $libPath
                $frameNames = $framePaths | select-Object -Property "Name" -ExpandProperty "Name"
                $choosenFrame = $this.GetCompatibleFramework($frameNames, $framework)

                if ($choosenFrame -ne $null)
                {
                    #write-Host ($package.PackageName, $choosenFrame)
                    $pathOf = Join-Path $libPath $choosenFrame
                    #Write-Host $pathOf
                    $results += $pathOf;
                }
            }
        }

        return $results;
    }

    [__WebapiDeploymentFile[]]
    ExplodeDeploymentPackageRuntimes($package, $framework, $environment)
    {
        $basePath = $this.GetBasePath($package);
        $results = @();

        if ($basePath -ne $null -and $basePath.length -gt 0)
        {
            # runtimes\win-x86\native
            # runtimes\unix\lib\netcoreapp2.1
            $runtimesPath = Join-Path $basePath "runtimes"

            if (Test-Path $runtimesPath)
            {
                $osDirs = Get-ChildItem -Path $runtimesPath
                foreach ($osDir in $osDirs)
                {
                    $libPath = Join-Path $osDir.FullName "lib"
                    $nativePath = Join-Path $osDir.FullName "native"


                    if (Test-Path $libPath)
                    {
                        $profDirs = Get-ChildItem -Path $libPath
                        foreach ($profDir in $profDirs)
                        {
                            $pusher = new-Object __WebapiDeploymentFile
                            $pusher.Name = $profDir.FullName
                            $pusher.Destination = "runtimes/$( $osDir.Name )/lib/$( $profDir.Name )"
                            $pusher.Environment = $environment
                            $results += $pusher
                        }

                        if ($framework -Match "^iis-" -and ( $osDir.name -eq "win" -or $osDir.name -eq "win-x64"))
                        {
                            #Write-Host $libPath
                            $framePaths = Get-ChildItem -Path $libPath
                            $frameNames = $framePaths | select-Object -Property "Name" -ExpandProperty "Name"
                            $choosenFrame = $this.GetCompatibleFramework($frameNames, $framework)

                            if ($choosenFrame -ne $null)
                            {
                                #write-Host ($package.PackageName, $choosenFrame)
                                $pathOf = Join-Path $libPath $choosenFrame

                                $pusher = new-Object __WebapiDeploymentFile
                                $pusher.Name = $pathOf
                                $pusher.Destination = ""
                                $pusher.Environment = $environment
                                $results += $pusher
                            }
                        }
                    }

                    if (Test-Path $nativePath)
                    {
                        $pusher = new-Object __WebapiDeploymentFile
                        $pusher.Name = $nativePath
                        $pusher.Destination = "runtimes/$( $osDir.Name )/native"
                        $pusher.Environment = $environment
                        $results += $pusher
                    }
                }


                #$results += $libPath;
            }
        }

        return $results;
    }
}

function Explode-Deployment-Packages($inputFile, $outputFile)
{
    Write-Host "Processing File... $( $inputFile ) -> $( $outputFile )"

    $exploder = New-Object __WebapiDeploymentPackageExploder


    $newDataLookup = (Get-Content $inputFile | Out-String | ConvertFrom-Json)

    $frames = @{ }


    foreach ($prop in $newDataLookup.psobject.properties)
    {

        $key = $prop.name
        Write-Host "Package Exploding $( $key )"
        #Write-Host $key
        $newData = [__WebapiDeploymentData]$prop.value

        $frameDeploymentData = New-Object __WebapiDeploymentData

        $frameDeploymentData.configs.AddRange($newData.configs)
        $frameDeploymentData.assets.AddRange($newData.assets)
        $frameDeploymentData.bins.AddRange($newData.bins)
        #$frameDeploymentData.packages.AddRange($packages)
        $frameDeploymentData.framework = $newData.framework
        $frameDeploymentData.environment = $newData.environment
        $frameDeploymentData.binDir = $newData.binDir
        $frameDeploymentData.appBinDir = $newData.appBinDir



        $frames.Add($key, $frameDeploymentData);


        foreach ($package in $newData.packages)
        {
            $libResults = $exploder.ExplodeDeploymentPackageLib($package, $newData.framework)
            if ($libResults.count -gt 0)
            {
                foreach ($item in $libResults)
                {
                    $file = New-Object __WebapiDeploymentFile
                    $file.Name = $item
                    $file.Environment = $newData.environment
                    $file.Destination = $package.Destination
                    $frameDeploymentData.bins.Add($file)
                }
            }

            $runtimeResults = $exploder.ExplodeDeploymentPackageRuntimes($package, $newData.framework, $newData.environment)
            if ($runtimeResults.count -gt 0)
            {
                foreach ($runtimeResult in $runtimeResults | where { $_.destination -ne "" })
                {
                    #$frameDeploymentData.assets.AddRange($runtimeResults)
                    $frameDeploymentData.assets.Add($runtimeResult)
                }

                foreach ($runtimeResult in $runtimeResults | where { $_.destination -eq "" })
                {
                    $frameDeploymentData.bins.Add($runtimeResult)
                    #$frameDeploymentData.assets.AddRange($runtimeResults)
                }
            }
        }
    }



    $output = ConvertTo-Json $frames -Depth 15

    Set-Content -Path $outputFile -Value $output
}

