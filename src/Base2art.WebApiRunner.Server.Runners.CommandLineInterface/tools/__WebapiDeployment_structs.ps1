class __WebapiDeploymentPackage
{
    [string] $PackageName

    [string] $PackageVersion

    [string] $Destination

    [String]
    toString()
    {
        return "PackageName: {0} PackageVersion: {1} " -f $this.PackageName, $this.PackageVersion
    }
}

class __WebapiDeploymentFile
{
    [string] $Name

    [string] $Environment

    [string] $Destination

    [String]
    toString()
    {
        return "Name: {0} Environment: {1} Destination: {2}" -f $this.Name, $this.Environment, $this.Destination
    }
}



class __WebapiDeploymentData
{
    [System.Collections.Generic.List[__WebapiDeploymentFile]] $bins

    [System.Collections.Generic.List[__WebapiDeploymentFile]] $assets

    [System.Collections.Generic.List[__WebapiDeploymentFile]] $configs

    [System.Collections.Generic.List[__WebapiDeploymentPackage]] $packages

    [string] $framework

    [string] $binDir

    [string] $appBinDir

    [string] $environment

    [String] $root = ".deploy"

    __WebapiDeploymentData()
    {
        $target = $this

        $target.bins = New-Object System.Collections.Generic.List[__WebapiDeploymentFile]
        $target.assets = New-Object System.Collections.Generic.List[__WebapiDeploymentFile]
        $target.configs = New-Object System.Collections.Generic.List[__WebapiDeploymentFile]
        $target.packages = New-Object System.Collections.Generic.List[__WebapiDeploymentPackage]
        #$target.frameworks = New-Object System.Collections.Generic.List[string]
        #$target.environments = New-Object System.Collections.Generic.List[string]
    }
    #[String]
    #toString()
    #{
    #    return "Name: {0} Environment: {1} Destination: {2}" -f $this.Name, $this.Environment, $this.Destination
    #}
}