$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition

function PathParts()
{
    $pathVar = $env:PATH;
    if (-not([string]::IsNullOrWhiteSpace($pathVar)))
    {
        $pathParts = $pathVar.Split([System.IO.Path]::PathSeparator) |
                Where-Object { ![String]::IsNullOrWhiteSpace($_) } |
                Foreach-Object { $_.Trim() };
        return $pathParts;
    }

    return @()
}

function Test-ReparsePoint([string]$path)
{
    $file = Get-Item $path -Force -ea SilentlyContinue
    return [bool]($file.Attributes -band [IO.FileAttributes]::ReparsePoint)
}

function FindFileInPath($exeNames)
{
    $pathParts = PathParts;

    foreach ($pathPart in pathParts)
    {
        foreach ($exeName in $exeNames)
        {
            $next = [system.IO.Path]::Combine($pathPart, $exeName)

            if (Test-Path $next)
            {
                if (-Not(Test-ReparsePoint $next))
                {
                    return $next;
                }

                $readlink = FindFileInPath @("readlink", "readlink.exe")

                if ($readlink -eq $null)
                {
                    return $next;
                }

                $newPath = (readlink $next) | Out-String
                $newPath = $newPath.Trim()

                if (Test-Path $newPath)
                {
                    return $newPath
                }

                return $next
            }
        }
    }

    return $null;
}



$versionDir = Split-Path -Parent $scriptPath
$packageDir = Split-Path -Parent $versionDir

$packagesPath = Split-Path -Parent $packageDir

#$packagesPath

$libDir = Join-Path $versionDir "lib"

$frameworkDirs = Get-ChildItem -Path $libDir -Directory

foreach ($frameworkDir in $frameworkDirs)
{
    $runtimeConfigPathAug = Join-Path $frameworkDir.FullName "Base2art.WebApiRunner.Server.Runners.CommandLineInterface.runtimeconfig.json"
    $runtimeConfigPathOriginal = Join-Path $frameworkDir.FullName "Base2art.WebApiRunner.Server.Runners.CommandLineInterface.runtimeconfig.original.json"
    if (-not(Test-Path $runtimeConfigPathOriginal))
    {
        if (Test-Path $runtimeConfigPathAug)
        {
            cp $runtimeConfigPathAug $runtimeConfigPathOriginal

            $data = Get-Content -Raw -Path $runtimeConfigPathAug | ConvertFrom-Json

            #            $dotnetPath = Split-Path -Parent (Get-Command dotnet)[0].Source

            $fallbackPathBase = FindFileInPath @("dotnet", "dotnet.exe")
            $dotnetPath = Join-Path $fallbackPathBase ".."

            $rutimeOptions = $data.runtimeOptions
            if ($rutimeOptions.additionalProbingPaths)
            {
                $rutimeOptions.additionalProbingPaths += "$([System.Environment]::GetFolderPath("UserProfile") )/.dotnet/store/|arch|/|tfm|"
                $rutimeOptions.additionalProbingPaths += "$([System.Environment]::GetFolderPath("UserProfile") )/.nuget/packages"
                $rutimeOptions.additionalProbingPaths += "$( $dotnetPath )/sdk/NuGetFallbackFolder"
            }
            else
            {
                $rutimeOptions | Add-Member -Name "additionalProbingPaths" -Type NoteProperty -Value @(
                "$([System.Environment]::GetFolderPath("UserProfile") )/.dotnet/store/|arch|/|tfm|"
                "$([System.Environment]::GetFolderPath("UserProfile") )/.nuget/packages"
                "$( $dotnetPath )/sdk/NuGetFallbackFolder"
                )
            }

            $dataString = ConvertTo-Json $data -Depth 99
            Set-Content -Path $runtimeConfigPathAug -Value $dataString
        }
    }
}


