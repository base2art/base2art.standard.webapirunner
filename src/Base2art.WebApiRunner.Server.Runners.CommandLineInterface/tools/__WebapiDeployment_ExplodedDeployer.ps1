﻿$toolPath = $PSScriptRoot
. "$( $toolPath )/__WebapiDeployment_structs.ps1"

class MyLoadContext: System.Runtime.Loader.AssemblyLoadContext
{
    MyLoadContext(): base()
    {
    }


    [System.Reflection.Assembly]
    Load([System.Reflection.AssemblyName] $name)
    {
        return $null;
    }
}

class __WebapiDeploymentExplodedDeployer
{

    [string[]]
    PathParts()
    {
        $pathVar = $env:PATH;
        if (-not([string]::IsNullOrWhiteSpace($pathVar)))
        {
            $pathParts = $pathVar.Split([System.IO.Path]::PathSeparator) |
                    Where-Object { ![String]::IsNullOrWhiteSpace($_) } |
                    Foreach-Object { $_.Trim() };
            return $pathParts;
        }

        return @()
    }

    [bool]
    TestReparsePoint([string]$path)
    {
        $file = Get-Item $path -Force -ea SilentlyContinue
        return [bool]($file.Attributes -band [IO.FileAttributes]::ReparsePoint)
    }

    [string]
    FindFileInPath([string[]]$exeNames)
    {
        $pathParts = $this.PathParts();

        foreach ($pathPart in $pathParts)
        {
            foreach ($exeName in $exeNames)
            {
                $next = [system.IO.Path]::Combine($pathPart, $exeName)

                if (Test-Path $next)
                {
                    if (-Not($this.TestReparsePoint($next)))
                    {
                        return $next;
                    }

                    $readlink = $this.FindFileInPath(@("readlink", "readlink.exe"))

                    if ($readlink -eq $null)
                    {
                        return $next;
                    }

                    $newPath = (readlink $next) | Out-String
                    $newPath = $newPath.Trim()

                    if (Test-Path $newPath)
                    {
                        return $newPath
                    }

                    return $next
                }
            }
        }

        return $null;
    }


    [Void]
    deployFile($package, $files, $suffix, $ignoreDest, $exts)
    {
        $basePath = ".deploy/$( $package.environment )/$( $package.framework )/$suffix"
        [System.IO.Directory]::CreateDirectory($basePath);

        foreach ($config in $files)
        {
            if (Test-Path -Path $config.Name -PathType leaf)
            {
                $dest = Join-Path $basePath $config.Destination
                copy-Item $config.Name $dest
            }

            if (Test-Path -Path $config.Name -PathType Container)
            {
                $dest = Join-Path $basePath $config.Destination
                if ($ignoreDest)
                {
                    $dest = $basePath
                }

                [System.IO.Directory]::CreateDirectory($dest);

                if ($exts.length -gt 0)
                {
                    foreach ($ext in $exts)
                    {
                        $dlls = Get-ChildItem -Path $config.Name -Filter $ext

                        foreach ($dll in $dlls)
                        {
                            if ( $dll.name.ToLower().StartsWith("microsoft.aspnetcore"))
                            {
                                $destFilePath = Join-Path $dest $dll.Name
                                if (Test-Path $destFilePath)
                                {
                                    #Write-Host "Already Exists $( $dll.Name )"
                                }
                                else
                                {
                                    #Write-Host "Copying from: $($dll.FullName)"
                                    copy-Item $dll.FullName $dest
                                }
                            }
                            else
                            {
                                #Write-Host "Copying from: $($dll.FullName)"
                                copy-Item $dll.FullName $dest
                            }
                        }
                    }
                }
                else
                {
                }
            }
        }
    }

    [Void]
    removeFile($package, $files, $suffix, $exts)
    {
        $basePath = ".deploy/$( $package.environment )/$( $package.framework )/$suffix"
        [System.IO.Directory]::CreateDirectory($basePath);

        foreach ($config in $files)
        {
            $dest = Join-Path $basePath $config.Destination
            if (Test-Path -Path $dest -PathType leaf)
            {
                Remove-Item $dest
            }

            if (Test-Path -Path $dest -PathType Container)
            {
                if ($exts.length -gt 0)
                {
                    foreach ($ext in $exts)
                    {
                        $dlls = Get-ChildItem -Path $dest -Filter $ext

                        foreach ($dll in $dlls)
                        {
                            Remove-Item $dll.FullName
                        }
                    }
                }
                else
                {
                }
            }
        }
    }

    [Void]
    deploy($content)
    {
        $loader = New-Object MyLoadContext

        [System.IO.Directory]::CreateDirectory(".deploy/$( $content.environment )/$( $content.framework )/wwwroot");
        [System.IO.Directory]::CreateDirectory(".deploy/$( $content.environment )/$( $content.framework )/logs");

        $appBins = $content.bins | where { $_.Destination -ne "" };
        $hostBins = $content.bins | where { $_.Destination -eq "" };

        $fallbackPathBase = $this.FindFileInPath(@("dotnet", "dotnet.exe"))
        $fallbackPathBase = Join-Path $fallbackPathBase "../shared/"

        Write-Host $fallbackPathBase
        foreach ($file in $content.bins)
        {
            $runtimes = Get-ChildItem $file.Name -Filter "*.runtimeconfig.json"
            foreach ($runtime in $runtimes)
            {
                Write-Host $runtime.FullName

                $runtimeObj = Get-Content -Raw -Path $runtime.FullName | ConvertFrom-Json
                $rtname = $runtimeObj.runtimeOptions.framework.name
                $rtversion = $runtimeObj.runtimeOptions.framework.version

                Write-Host "Name: $( $rtname )"
                Write-Host "Version: $( $rtversion )"
                if ($rtversion -ne $null)
                {
                    $rtVersionShort = $rtversion.SubString(0, 3)

                    $appPath = Join-Path $fallbackPathBase $rtname
                    $rtDir = Get-ChildItem -Path $appPath -Filter "$( $rtVersionShort ).*"
                    $filesToMove = Get-ChildItem -Path $rtdir

                    foreach ($fileToMove in $filesToMove)
                    {
                        $basePath = ".deploy/$( $content.environment )/$( $content.framework )"
                        [System.IO.Directory]::CreateDirectory($basePath);
                        Copy-Item $fileToMove.FullName $basePath
                    }
                }
                
                $frameworks = $runtimeObj.runtimeOptions.frameworks
                if ($frameworks -ne $null)
                {
                    foreach($framework in $frameworks)
                    {
                        $rtname = $framework.name
                        $rtversion = $framework.version

                        Write-Host "Name: $( $rtname )"
                        Write-Host "Version: $( $rtversion )"
                        if ($rtversion -ne $null)
                        {
                            $rtVersionShort = $rtversion.SubString(0, 3)

                            $appPath = Join-Path $fallbackPathBase $rtname
                            $rtDir = Get-ChildItem -Path $appPath -Filter "$( $rtVersionShort ).*"
                            $filesToMove = Get-ChildItem -Path $rtdir

                            foreach ($fileToMove in $filesToMove)
                            {
                                $basePath = ".deploy/$( $content.environment )/$( $content.framework )"
                                [System.IO.Directory]::CreateDirectory($basePath);
                                Copy-Item $fileToMove.FullName $basePath
                            }
                        }
                    }
                }
            }
        }
        #$this.deployFile($content, $hostBins, $content.binDir, $true, @("*.exe", "*.config", "*.dll", "*.pdb", "*.xml", "*.json"));

        $this.deployFile($content, $content.configs, "config", $false, @());
        $this.deployFile($content, $hostBins, $content.binDir, $true, @("*.exe", "*.config", "*.dll", "*.pdb", "*.xml", "*.json"));
        $this.deployFile($content, $appBins, $content.appBinDir, $true, @("*.exe", "*.config", "*.dll", "*.pdb", "*.xml", "*.json"));
        $this.deployFile($content, $content.assets, "", $false, @("*.*"));

        $findPath = ".deploy/$( $content.environment )/$( $content.framework )"
        $originals = Get-ChildItem -Path $findPath -Filter "*.original.*";
        foreach ($original in $originals)
        {
            $destOriginal = Join-Path $original.Directory.FullName $original.Name.Replace(".original.", ".")
            move-Item $original.FullName  $destOriginal -force
        }


        $toolPath = $PSScriptRoot

        $templates = Get-ChildItem -Path "$( $toolPath )/templates/$( $content.framework )/"
        foreach ($template in $templates)
        {
            Copy-Item $template ".deploy/$( $content.environment )/$( $content.framework )/$( $template.Name )"
        }

        $xmls = Get-ChildItem -Path ".deploy/$( $content.environment )/$( $content.framework )" -Filter "*.config"
        foreach ($xmlFile in $xmls)
        {

            [xml]$XmlDocument = Get-Content -Path $xmlFile.FullName
            $ns = New-Object System.Xml.XmlNamespaceManager($XmlDocument.NameTable)
            $ns.AddNamespace("ns", "urn:schemas-microsoft-com:asm.v1")
            $node = $XmlDocument.SelectSingleNode("//ns:assemblyBinding", $ns)
            if ($node -ne $null)
            {
                #<dependentAssembly>
                #  <assemblyIdentity name="Base2art.Bcl" culture="neutral" publicKeyToken="025e133696936e8d" />
                #  <bindingRedirect oldVersion="0.0.0.0-1.0.0.0" newVersion="1.0.0.0" />
                #</dependentAssembly>

                $dllFiles = Get-ChildItem -Path ".deploy/$( $content.environment )/$( $content.framework )/$( $content.binDir )" -Filter "*.dll"
                foreach ($dllFile in $dllFiles)
                {
                    $asm = $null

                    Try
                    {
                        $asm = $loader.LoadFromAssemblyPath($dllFile.FullName);
                    }
                    catch
                    {
                    }

                    if ($asm -ne $null)
                    {
                        $asmName = $asm.GetName();
                        $dependentAssembly = $XmlDocument.CreateNode("element", "dependentAssembly", "urn:schemas-microsoft-com:asm.v1")
                        $assemblyIdentity = $XmlDocument.CreateNode("element", "assemblyIdentity", "urn:schemas-microsoft-com:asm.v1")
                        $bindingRedirect = $XmlDocument.CreateNode("element", "bindingRedirect", "urn:schemas-microsoft-com:asm.v1")

                        $assemblyIdentity.setAttribute("name", "$( $asmName.Name )");
                        $assemblyIdentity.setAttribute("culture", "neutral");
                        $assemblyIdentity.setAttribute("publicKeyToken", "$([BitConverter]::ToString($asmName.GetPublicKeyToken()).Replace('-', '').ToLower() )");

                        $bindingRedirect.setAttribute("oldVersion", "0.0.0.0-9999.9999.9999.9999");
                        $bindingRedirect.setAttribute("newVersion", "$( $asmName.Version )");

                        #$generated.SetAttribute("enabled","false")
                        $dependentAssembly.AppendChild($assemblyIdentity)
                        $dependentAssembly.AppendChild($bindingRedirect)
                        $node.AppendChild($dependentAssembly)
                        #Write-Host "$($dllFile.FullName)"
                    }
                }

                $xmlDocument.Save($xmlFile.FullName);
            }
        }


        #Copy-Item "$($toolPath)/Base2art.WebApiRunner.Server.Runners.CommandLineInterface.runtimeconfig.json" ".deploy/$($content.environment)/$($content.framework)/Base2art.WebApiRunner.Server.Runners.CommandLineInterface.runtimeconfig.json"
        #Copy-Item "$($toolPath)/runtime.json" ".deploy/$($content.environment)/$($content.framework)/runtime.json"

    }
}

function Deploy-Exploded($inputFile, $outputFile)
{
    Write-Host "Processing File... ($inputFile)"

    $exploder = New-Object __WebapiDeploymentExplodedDeployer


    $newDataLookup = (Get-Content $inputFile | Out-String | ConvertFrom-Json)

    $frames = @{ }


    foreach ($prop in $newDataLookup.psobject.properties)
    {
        $key = $prop.name
        #Write-Host "Copying to output $( $key )"
        #Write-Host $key
        $newData = [__WebapiDeploymentData]$prop.value

        $exploder.deploy($newData)
    }



    #$output = ConvertTo-Json $frames -Depth 15
    #
    #Set-Content -Path $outputFile -Value $output
}

