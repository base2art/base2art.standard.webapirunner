﻿$toolPath = $PSScriptRoot
. "$( $toolPath )/__WebapiDeployment_structs.ps1"


class __WebapiDeploymentDependencyExploder
{
    [string[]]
    PathParts()
    {
        $pathVar = $env:PATH;
        if (-not([string]::IsNullOrWhiteSpace($pathVar)))
        {
            $pathParts = $pathVar.Split([System.IO.Path]::PathSeparator) |
                    Where-Object { ![String]::IsNullOrWhiteSpace($_) } |
                    Foreach-Object { $_.Trim() };
            return $pathParts;
        }

        return @()
    }

    [bool]
    TestReparsePoint([string]$path)
    {
        $file = Get-Item $path -Force -ea SilentlyContinue
        return [bool]($file.Attributes -band [IO.FileAttributes]::ReparsePoint)
    }

    [string]
    FindFileInPath([string[]]$exeNames)
    {
        $pathParts = $this.PathParts();

        foreach ($pathPart in $pathParts)
        {
            foreach ($exeName in $exeNames)
            {
                $next = [system.IO.Path]::Combine($pathPart, $exeName)

                if (Test-Path $next)
                {
                    if (-Not($this.TestReparsePoint($next)))
                    {
                        return $next;
                    }

                    $readlink = $this.FindFileInPath(@("readlink", "readlink.exe"))

                    if ($readlink -eq $null)
                    {
                        return $next;
                    }

                    $newPath = (readlink $next) | Out-String
                    $newPath = $newPath.Trim()

                    if (Test-Path $newPath)
                    {
                        return $newPath
                    }

                    return $next
                }
            }
        }

        return $null;
    }

    [String]
    GetCompatibleFramework($frameworks, $framework)
    {
        $netStandards = @(
        ".NETStandard2.0",
        ".NETStandard1.6",
        ".NETStandard1.5",
        ".NETStandard1.4",
        ".NETStandard1.3",
        ".NETStandard1.2",
        ".NETStandard1.1",
        ".NETStandard1.0",
        ".NETStandard"
        );

        $netStandards21 = @(".NETStandard2.1") + $netStandards

        $net460Base = @(".NETFramework4.6", ".NETFramework4.5.2", ".NETFramework4.5.1", ".NETFramework4.5", ".NETFramework");
        $net460 = $net460Base + $netStandards
        $net461 = @(".NETFramework4.6.1") + $net460
        $net462 = @(".NETFramework4.6.2") + $net461
        $net470 = @(".NETFramework4.7") + $net462
        $net471 = @(".NETFramework4.7.1") + $net470
        $net472 = @(".NETFramework4.7.2") + $net471
        $net480 = @(".NETFramework4.8") + $net472


        $netcore21 = @(".NETCoreApp2.1", ".NETCoreApp2.0", ".NETCoreApp") + $netStandards;
        $netcore22 = @(".NETCoreApp2.2") + $netcore21
        $netcore30 = @(".NETCoreApp3.0", ".NETCoreApp2.2", ".NETCoreApp2.1", ".NETCoreApp2.0", ".NETCoreApp") + $netStandards21;
        $netcore31 = @(".NETCoreApp3.1") + $netcore30
        $net50 = @("net5.0") + $netcore31
        $net60 = @("net6.0") + $net50
        $net70 = @("net7.0") + $net60
        $net80 = @("net8.0") + $net70

        $lookup = @{
            "net8.0" = $net80;
            "net7.0" = $net70;
            "net6.0" = $net60;
            "net5.0" = $net50;
            "netcoreapp3.1" = $netcore31;
            "netcoreapp3.0" = $netcore30;
            "netcoreapp2.2" = $netcore22;
            "netcoreapp2.1" = $netcore21;

            "net48" = $net480;

            "net472" = $net472;
            "net471" = $net471;
            "net47" = $net470;

            "net462" = $net462;
            "net461" = $net461;
            "net46" = $net460;


            "iis-net48" = $net480;

            "iis-net472" = $net472;
            "iis-net471" = $net471;
            "iis-net47" = $net470;

            "iis-net462" = $net462;
            "iis-net461" = $net461;
            "iis-net46" = $net460;
        };


        #Write-Host "$framework"

        $matches = $netStandards
        if ( $lookup.ContainsKey($framework))
        {
            $matches = $lookup[$framework]
        }


        foreach ($match in $matches)
        {
            foreach ($frameQuery in $frameworks)
            {
                if ($frameQuery -eq $match)
                {
                    return $frameQuery
                }
            }
        }

        return $null
    }

    [bool]
    ContainsPackage($packages, $asset)
    {
        foreach ($package in $packages)
        {
            ## -and ($package.PackageVersion -eq $asset.PackageVersion) 
            if (($package.PackageName -eq $asset.PackageName) -and ($package.PackageVersion -eq $asset.PackageVersion))
            {

                return $true;
            }
        }

        return $false;
    }

    [Void]
    ExplodeDeploymentPackage($asset, $packages, $framework)
    {
        if ( $this.ContainsPackage($packages, $asset))
        {
            return;
        }

        $packages.Add($asset);

        $packagesPathBase = Resolve-Path "~/.nuget/packages"
        $fallbackPathBase = $this.FindFileInPath(@("dotnet", "dotnet.exe"))
        $fallbackPathBase = Join-Path $fallbackPathBase "../sdk/NuGetFallbackFolder"

        $versionString = "$( $asset.PackageVersion )"

        if ($versionString -eq "[4.0.11, )" -or $versionString -eq "[4.0.12, )")
        {
            $versionString = "4.3.0"
        }

        if ($versionString -eq "[4.0.1, )")
        {
            $versionString = "4.3.0"
        }

        if ($versionString -eq "[4.1.0, )" -or $versionString -eq "[4.1.1, )")
        {
            $versionString = "4.3.0"
        }

        if ($versionString -eq "[5.2.7, 5.3.0)")
        {
            $versionString = "5.3.0"
        }

        if ($versionString -eq "[1.3.3, 2.0.0)")
        {
            $versionString = "2.0.0"
        }




        $packagesPath = Join-Path $packagesPathBase "$($asset.PackageName.ToLower() )/$($versionString.ToLower() )/$($asset.PackageName.ToLower() ).nuspec"
        $fallbackPath = Join-Path $fallbackPathBase "$($asset.PackageName.ToLower() )/$($versionString.ToLower() )/$($asset.PackageName.ToLower() ).nuspec"

        $nuspecPath = $null
        #Try {
        if (Test-Path $packagesPath)
        {
            $nuspecPath = $packagesPath

        }
        elseif (Test-Path $fallbackPath)
        {
            $nuspecPath = $fallbackPath
        }
        #} Catch{
        #    Write-Host $packagesPath
        #}

        if ($nuspecPath -ne $null)
        {
            #write-Host $nuspecPath
            $xmlFile = ([xml](Get-Content $nuspecPath))
            $dependencies = $xmlFile.package.metadata.dependencies.dependency
            $groups = $xmlFile.package.metadata.dependencies.group

            if ($groups -ne $null)
            {
                $targetFrameworks = $groups.targetFramework;
                $targetFramework = $this.GetCompatibleFramework($targetFrameworks, $framework)


                $nextGroups = $groups | Where { $_.targetFramework -eq $targetFramework }
                foreach ($group in $nextGroups)
                {
                    #Write-Host "  $($group.targetFramework)"
                    foreach ($dep in $group.dependency)
                    {
                        $newPackage = New-Object __WebapiDeploymentPackage
                        $newPackage.PackageName = $dep.id;
                        $newPackage.PackageVersion = $dep.version;
                        $newPackage.Destination = $asset.Destination;
                        #Write-Host "    $($dep.id):$($dep.version)"
                        $this.ExplodeDeploymentPackage($newPackage, $packages, $framework)

                    }
                }
            }

            if ($dependencies -ne $null)
            {
                foreach ($dep in $dependencies)
                {
                    $newPackage = New-Object __WebapiDeploymentPackage
                    $newPackage.PackageName = $dep.id;
                    $newPackage.PackageVersion = $dep.version;
                    $newPackage.Destination = $asset.Destination;
                    #Write-Host "    $($dep.id):$($dep.version)"
                    $this.ExplodeDeploymentPackage($newPackage, $packages, $framework)
                }
            }
        }
    }
}

function Explode-Deployment-Dependencies($inputFile, $outputFile)
{
    Write-Host "Processing File... $( $inputFile ) -> $( $outputFile )"

    $exploder = New-Object __WebapiDeploymentDependencyExploder

    $newDataLookup = (Get-Content $inputFile | Out-String | ConvertFrom-Json)

    $frames = @{ }


    foreach ($prop in $newDataLookup.psobject.properties)
    {

        $key = $prop.name
        Write-Host "Dependency Exploding $( $key )"
        $newData = [__WebapiDeploymentData]$prop.value

        $framework = $newData.framework
        $environment = $newData.environment

        $packages = New-Object System.Collections.Generic.List[__WebapiDeploymentPackage]

        foreach ($pack in $newData.packages | where { $_.Destination -eq "" })
        {

            $exploder.ExplodeDeploymentPackage($pack, $packages, $framework)
        }


        foreach ($pack in $newData.packages | where { $_.Destination -ne "" })
        {

            $exploder.ExplodeDeploymentPackage($pack, $packages, $framework)
        }


        foreach ($binPath in $newData.bins)
        {
            $deps = Get-ChildItem -Path $binPath.Name -Filter "*.deps.json"

            foreach ($dep in $deps)
            {
                #Write-Host $dep.FullName
                $depContent = (Get-Content $dep | Out-String | ConvertFrom-Json -AsHashtable)
                foreach ($tr in $depContent["targets"])
                {
                    foreach ($key1 in $tr.Values)
                    {
                        foreach ($key2 in $key1.Keys)
                        {
                            $parts = $key2.Split("/");
                            #Write-Host ($parts[0], $parts[1])
                            $package = New-Object __WebapiDeploymentPackage
                            $package.PackageName = "$( $parts[0] )"
                            $package.PackageVersion = "$( $parts[1] )"
                            $package.Destination = "bin"
                            #Write-Host "$($parts[0])"
                            $exploder.ExplodeDeploymentPackage($package, $packages, $framework)
                        }
                    }
                }
            }
        }


        $packageVersions = @{ }

        foreach ($keyOfPack in $packages)
        {

            $newVersionString = "$( $keyOfPack.PackageVersion )"

            if (-not($packageVersions.ContainsKey($keyOfPack.PackageName)))
            {
                $packageVersions[$keyOfPack.PackageName] = $keyOfPack
            }

            $oldVersionString = "$( $packageVersions[$keyOfPack.PackageName].PackageVersion )"

            if ($oldVersionString -match "^\d{1,4}\.\d{1,4}(\.\d{1,4}(\.\d{1,4})?)?$" -and $newVersionString -match "^\d{1,4}\.\d{1,4}(\.\d{1,4}(\.\d{1,4})?)?$")
            {
                $oldVersion = [Version]$oldVersionString
                $newVersion = [Version]$newVersionString

                if ($oldVersion -lt $newVersion)
                {
                    #Write-Host ($keyOfPack.PackageName, $newVersionString)
                    $oldLocation = $packageVersions[$keyOfPack.PackageName].Destination
                    $newLocation = $keyOfPack.Destination
                    if ($oldLocation -ne $newLocation -and ($oldLocation -eq "") -or ($newLocation -eq ""))
                    {
                        $keyOfPack.Destination = "";
                    }

                    $packageVersions[$keyOfPack.PackageName] = $keyOfPack
                }
            }
            else
            {
                if ($oldVersionString -lt $newVersionString)
                {

                    $oldLocation = $packageVersions[$keyOfPack.PackageName].Destination
                    $newLocation = $keyOfPack.Destination

                    if ($oldLocation -ne $newLocation -and ($oldLocation -eq "") -or ($newLocation -eq ""))
                    {
                        $keyOfPack.Destination = "";
                    }

                    $packageVersions[$keyOfPack.PackageName] = $keyOfPack
                }
            }
        }

        $frameDeploymentData = New-Object __WebapiDeploymentData

        $frameDeploymentData.configs.AddRange($newData.configs)
        $frameDeploymentData.assets.AddRange($newData.assets)
        $frameDeploymentData.bins.AddRange($newData.bins)

        $frameDeploymentData.framework = $framework
        $frameDeploymentData.environment = $environment
        $frameDeploymentData.binDir = $newData.binDir
        $frameDeploymentData.appBinDir = $newData.appBinDir

        foreach ($package in $packageVersions.Values)
        {
            if (-Not($package.PackageName -eq $null -or $package.PackageName -eq ""))
            {
                #Write-Host "Calling... $($package.toString())"
                $frameDeploymentData.packages.Add($package)
            }
        }

        $frames.Add($key, $frameDeploymentData)
    }


    $output = ConvertTo-Json $frames -Depth 15

    write-Host "Writing File: $( $outputFile )"
    Set-Content -Path $outputFile -Value $output
}

