﻿namespace Base2art.WebApiRunner.Server.Runners.CommandLineInterface
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using Commands;
    using Logging;
    using Web.App.Configuration;

    internal class SetEnvironmentBaseDirCommand : ICommand
    {
        public Task<int> Run(IServerConfiguration configuration)
        {
            Logger.Log(LogLevel.Info, $"Current-Path: `{Environment.CurrentDirectory}`");
            var location = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            Environment.CurrentDirectory = location;

            Logger.Log(LogLevel.Info, $"New-Path: `{Environment.CurrentDirectory}`");

            return Task.FromResult(0);
        }

        public Task Shutdown() => Task.CompletedTask;
    }
}