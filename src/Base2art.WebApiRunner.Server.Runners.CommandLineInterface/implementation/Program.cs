﻿namespace Base2art.WebApiRunner.Server.Runners.CommandLineInterface
{
    using System.Threading.Tasks;
    using Logging;

    public static class Program
    {
        private static Runner runner;

        private static bool restart;
        private static Task<int> startup;

        public static async Task<int> Main(string[] args)
        {
            var verb = args.Length < 1 ? "Unknown" : args[0];

            runner = new Runner(verb, args);
            var result = await runner.Start();

            Logger.Level = LogLevel.Debug;

            while (restart)
            {
                startup = runner.Start();
                restart = false;
                result = await startup;
            }

            return result;
        }

        public static async Task Restart()
        {
            restart = true;
            await runner.Shutdown();
        }
    }
}