﻿namespace Base2art.WebApiRunner.Server.Runners.CommandLineInterface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Commands;
    using Configuration;
    using Logging;
    using Reflection.Discovery;
    using RouteGeneration;
    using Web.App.Configuration;

    public class Runner
    {
        private readonly string[] args;
        private readonly Stack<ICommand> running = new Stack<ICommand>();
        private readonly string verb;

        public Runner(string verb, string[] args)
        {
            this.verb = verb;
            this.args = args;
        }

        public async Task<int> Start()
        {
            var (commands, config) = GetCommand(this.verb, this.args.Skip(1).ToArray());

            try
            {
                foreach (var command in commands ?? new ICommand[0])
                {
                    this.running.Push(command);
                    var result = await command.Run(config);

                    if (result != 0)
                    {
                        return await Exit(this.running, result);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.Fatal, e);
                await Exit(this.running, -42);
                throw;
            }

            return await Exit(this.running, 0);
        }

        public Task Shutdown() => Exit(this.running, 0);

        private static async Task<int> Exit(Stack<ICommand> running, int result)
        {
            while (running.TryPop(out var cmd))
            {
                try
                {
                    await cmd.Shutdown();
                }
                catch (Exception e)
                {
                    Logger.Log(LogLevel.Error, e);
                }
            }

            return result;
        }

        private static Tuple<ICommand[], IServerConfiguration> GetCommand(string verb, string[] args)
        {
            var (configPrimitive, config, registrations) = ConsoleServerConfigurations.CreateConsoleServerConfiguration(
                                                                                                                        args.FirstOrDefault(),
                                                                                                                        GetFrameworkType(),
                                                                                                                        GetFrameworkVersions(),
                                                                                                                        new AssemblyFactory());

            switch (verb?.ToLowerInvariant())
            {
                case "windows-service":

                    return Tuple.Create(new ICommand[]
                                        {
                                            new SetLogLevelCommand(),
                                            new SetConsoleToFileCommand("service"),
                                            new SetEnvironmentBaseDirCommand(),
                                            new ConsoleServerCommand(
                                                                     new ConsoleApplication(),
                                                                     configPrimitive,
                                                                     registrations,
                                                                     new FrameworkShim(),
                                                                     RunType.Service)
                                        }, config);
                case "server":
                    return Tuple.Create(new ICommand[]
                                        {
                                            new SetLogLevelCommand(),
                                            new ConsoleServerCommand(
                                                                     new ConsoleApplication(),
                                                                     configPrimitive,
                                                                     registrations,
                                                                     new FrameworkShim(),
                                                                     RunType.Console)
                                        }, config);
                case "server-with-verify":
                    return Tuple.Create(new ICommand[]
                                        {
                                            new SetLogLevelCommand(),
                                            new VerifyCommand(configPrimitive, registrations, new FrameworkShim()),
                                            new ConsoleServerCommand(
                                                                     new ConsoleApplication(),
                                                                     configPrimitive,
                                                                     registrations,
                                                                     new FrameworkShim(),
                                                                     RunType.Console)
                                        },
                                        config);
                case "iis-server":
                    return Tuple.Create(new ICommand[]
                                        {
                                            new SetLogLevelCommand(),
                                            new ConsoleServerCommand(
                                                                     new ConsoleApplication(),
                                                                     configPrimitive,
                                                                     registrations,
                                                                     new FrameworkShim(),
                                                                     RunType.IIS)
                                        }, config);
                case "iis-server-with-verify":
                    return Tuple.Create(new ICommand[]
                                        {
                                            new SetLogLevelCommand(),
                                            new VerifyCommand(configPrimitive, registrations, new FrameworkShim()),
                                            new ConsoleServerCommand(
                                                                     new ConsoleApplication(),
                                                                     configPrimitive,
                                                                     registrations,
                                                                     new FrameworkShim(),
                                                                     RunType.IIS)
                                        }, config);
                case "verify":
                    return Tuple.Create(new ICommand[]
                                        {
                                            new SetLogLevelCommand(),
                                            new VerifyCommand(configPrimitive, registrations, new FrameworkShim())
                                        }, config);
                case "generate-injection":
                    return Tuple.Create(new ICommand[]
                                        {
                                            new SetLogLevelCommand(),
                                            new GenerateInjectionCommand()
                                        }, config);
                case "generate-routes-cs":
                    return Tuple.Create(new ICommand[]
                                        {
                                            new SetLogLevelCommand(),
                                            new GenerateRoutesCommand(
                                                                      configPrimitive,
                                                                      registrations,
                                                                      args.Skip(1).FirstOrDefault() ?? "__NAMESPACE__",
                                                                      new FrameworkShim())
                                        }, config);
            }

            return Tuple.Create(new ICommand[]
                                {
                                    new UsageCommand()
                                }, config);
        }

        private static FrameworkType GetFrameworkType()
        {
#if NETFRAMEWORK
            return FrameworkType.NetFx;
#elif NETCOREAPP
            return FrameworkType.NetCore;
#else
            return FrameworkType.NetStandard;
#endif
        }

        private static string[] GetFrameworkVersions()
        {
            // <TargetFrameworks>net472;net48;netstandard1.3;netstandard2.0;netcoreapp2.1;netcoreapp2.2;netcoreapp3.0</TargetFrameworks>
            var netStandards = new[]
                               {
                                   "netstandard2.0",
                                   "netstandard1.6",
                                   "netstandard1.5",
                                   "netstandard1.4",
                                   "netstandard1.3",
                                   "netstandard1.2",
                                   "netstandard1.1",
                                   "netstandard"
                               };

#if NETFRAMEWORK
            var net472 =
 new string[] {"net472","net471","net47","net462","net461","net46","net452","net451","net45","net"}.Concat(netStandards).ToArray();
            var net480 = new string[] {"net48"}.Concat(net472).ToArray();
#if NET472
            return net472;
#elif NET48
            return net480;
#else
            return netStandards;
#endif

#elif NETCOREAPP
            var netcore21 = new[] {"netcoreapp2.1", "netcoreapp2.0", "netcoreapp"}.Concat(netStandards).ToArray();
            var netcore22 = new[] {"netcoreapp2.2"}.Concat(netcore21).ToArray();
            var netcore30 = new[] {"netcoreapp3.0"}.Concat(netcore22).ToArray();
            var netcore31 = new[] {"netcoreapp3.1"}.Concat(netcore30).ToArray();
            var net50 = new[] {"net5.0"}.Concat(netcore31).ToArray();
            var net60 = new[] {"net6.0"}.Concat(net50).ToArray();
            var net70 = new[] {"net7.0"}.Concat(net60).ToArray();
            var net80 = new[] {"net8.0"}.Concat(net70).ToArray();

#if NET8_0
                return net80;
#elif NET7_0
                return net70;
#elif NET6_0
                return net60;
#elif NET5_0
                return net50;
#elif NETCOREAPP3_1
                return netcore31;
#elif NETCOREAPP3_0
                return netcore30;
#elif NETCOREAPP2_2
            return netcore22;
#elif NETCOREAPP2_1
            return netcore21;
#else
                return netStandards;
#endif
#else
            return netStandards;
#endif
        }
    }
}