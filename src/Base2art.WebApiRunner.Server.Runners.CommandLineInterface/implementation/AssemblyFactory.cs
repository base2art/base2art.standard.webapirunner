namespace Base2art.WebApiRunner.Server.Runners.CommandLineInterface
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Loader;
    using Logging;
    using Microsoft.DotNet.PlatformAbstractions;
    using Reflection;
    using Reflection.Discovery;

    public class AssemblyFactory : IAssemblyFactory
    {
#if NETCOREAPP
        private readonly AssemblyLoadContext context;
        private readonly List<IPackagedNativeDll> unmanaged = new List<IPackagedNativeDll>();
        private readonly RuntimeIndentification runtimeMeta;

        private readonly Dictionary<string, Assembly> loaded = new Dictionary<string, Assembly>(StringComparer.OrdinalIgnoreCase);

        public AssemblyFactory()
        {
            this.context = new CustomAssemblyLoadContext(this);
            // TODO: FIX PATH
            // SEARCH: PACKAGES OR LOCAL.
            // new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath
            var exePath = new Uri(Assembly.GetExecutingAssembly().GetName().CodeBase).LocalPath;
            var dirPath = Path.GetDirectoryName(exePath);
            var path = Path.Combine(dirPath, "runtime.json");
//            "/home/tyoung/.nuget/packages/microsoft.netcore.platforms/2.2.3/runtime.json"
            this.runtimeMeta = new RuntimeIndentification(path);
        }

        public Assembly CreateFromPath(string path, bool forceRootLoadingLoader)
        {
            var key = Path.GetFileNameWithoutExtension(path);
            if (this.loaded.ContainsKey(key))
            {
                return this.loaded[key];
            }

            if (key.StartsWith("Microsoft.AspNetCore.", StringComparison.OrdinalIgnoreCase) || forceRootLoadingLoader)
            {
                var item1 = Assembly.LoadFile(path);
                this.loaded[key] = item1;
                return item1;
            }

            var item = this.context.LoadFromAssemblyPath(path);
            this.loaded[key] = item;
            return item;
        }

        public void RegisterUnmanaged(IReadOnlyList<IPackagedNativeDll> unmanagedItems)
        {
            this.unmanaged.AddRange(unmanagedItems);
        }

        public bool Supports(string rid)
        {
#if NETCOREAPP2_1 || NETCOREAPP2_2 || NETCOREAPP3_0 || NETCOREAPP3_1
            var myRid = RuntimeEnvironment.GetRuntimeIdentifier();
#else
            var myRid = System.Runtime.InteropServices.RuntimeInformation.RuntimeIdentifier;
#endif
            return this.runtimeMeta.FindRuntimeDependencies(myRid, true).Contains(rid, StringComparer.OrdinalIgnoreCase);
        }

        private class CustomAssemblyLoadContext : AssemblyLoadContext
        {
            private readonly AssemblyFactory assemblyFactory;

            public CustomAssemblyLoadContext(AssemblyFactory assemblyFactory) => this.assemblyFactory = assemblyFactory;

            protected override Assembly Load(AssemblyName assemblyName)
                => null;

            protected override IntPtr LoadUnmanagedDll(string unmanagedDllName)
            {
                Logger.Log(LogLevel.Debug, $"Looking for: {unmanagedDllName}");

#if NETCOREAPP2_1 || NETCOREAPP2_2 || NETCOREAPP3_0 || NETCOREAPP3_1
                var rid = RuntimeEnvironment.GetRuntimeIdentifier();
#else
                var rid = System.Runtime.InteropServices.RuntimeInformation.RuntimeIdentifier;
#endif

                var rids = this.assemblyFactory.runtimeMeta.FindRuntimeLevelDependencies(rid, true)
                               .Select(x => x.Rid)
                               .ToHashSet();

                foreach (var item in this.assemblyFactory.unmanaged)
                {
                    Logger.Log(LogLevel.Debug, item.FullPath);
                }

                // TODO: Group By name then scan rid on level?
                var compatible = this.assemblyFactory.unmanaged
                                     .Where(x => rids.Contains(x.RuntimeIdentifier))
                                     .ToArray();

                var firstByName = new Dictionary<string, IPackagedNativeDll>();
                foreach (var nameUnmanaged in compatible.GroupBy(x => x.Name))
                {
                    firstByName[nameUnmanaged.Key] = nameUnmanaged.GroupBy(x => x.Version)
                                                                  .OrderByDescending(x => x.Key)
                                                                  .First()
                                                                  .FirstOrDefault();
                }

                foreach (var pair in firstByName)
                {
                    if (pair.Key.Contains(unmanagedDllName))
                    {
                        return this.LoadUnmanagedDllFromPath(pair.Value.FullPath);
                    }
                }

                Logger.Log(LogLevel.Debug, unmanagedDllName);
                return base.LoadUnmanagedDll(unmanagedDllName);
            }
        }
#else
        public bool Supports(string rid) => false;
        public Assembly CreateFromPath(string path, bool forceRootLoadingLoader) => Assembly.LoadFile(path);
        public void RegisterUnmanaged(IReadOnlyList<IPackagedNativeDll> valueTupleUnmanaged)
        {
        }
#endif
    }
}