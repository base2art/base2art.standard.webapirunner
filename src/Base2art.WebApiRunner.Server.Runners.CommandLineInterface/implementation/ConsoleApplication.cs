namespace Base2art.WebApiRunner.Server.Runners.CommandLineInterface
{
    using System.Threading.Tasks;
    using TaskScheduling;
    using Web.App.LifeCycle;

    public class ConsoleApplication : IApplication
    {
        public async void Reload()
        {
            await Program.Restart();
        }

        public Task ExecuteJob(string name)
            => ProdTaskRegistrar.ExecuteJob(name);

        public Task ExecuteJobWithParameters(string name, object parameters)
            => ProdTaskRegistrar.ExecuteJob(name, parameters);
    }
}