namespace Base2art.WebApiRunner.Server.Runners.CommandLineInterface
{
    using System;
    using System.Threading.Tasks;

    public static class Program
    {
        public static Task<int> Main(string[] args)
        {
            Console.WriteLine("This is a reference implementation you must use a real implementation.");
            return Task.FromResult(-100);
        }
    }
}