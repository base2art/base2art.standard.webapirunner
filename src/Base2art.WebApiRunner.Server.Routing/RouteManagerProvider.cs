﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System;
    using Microsoft.AspNetCore.Routing;
    using Web.Server.Registration;

    public class RouteManagerProvider : IRouteManagerProvider
    {
        public IRouteManager CreateRouteManager(Func<RouteBuilder> routerProvider) => new RouteManager(routerProvider);
    }
}