﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Reflection;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.ActionConstraints;
    using Microsoft.AspNetCore.Mvc.ApplicationModels;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
    using Microsoft.Extensions.Options;
    using Web.Server.Registration;

    public class CustomApplicationModelProvider : IApplicationModelProvider
    {
        private readonly IRouteManager builder;
        private readonly IFrameworkShim shim;
        private readonly ICollection<IFilterMetadata> globalFilters;
        private readonly IList<IMetadataDetailsProvider> globalModelBinderProviders;

        public CustomApplicationModelProvider(IOptions<MvcOptions> mvcOptionsAccessor, IRouteManager builder, IFrameworkShim shim)
        {
            this.globalFilters = mvcOptionsAccessor.Value.Filters;
            this.globalModelBinderProviders = mvcOptionsAccessor.Value.ModelMetadataDetailsProviders;
            this.builder = builder;
            this.shim = shim;
        }

        /// <inheritdoc />
        public int Order => -1000;

        /// <inheritdoc />
        public virtual void OnProvidersExecuting(ApplicationModelProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            foreach (var globalFilter in this.globalFilters)
            {
                context.Result.Filters.Add(globalFilter);
            }

            var groups = this.builder.Routes.GroupBy(x => x.Controller).ToArray();
            foreach (var route in groups)
            {
                var controllerType = route.Key.GetTypeInfo();
                var controllerModel = this.CreateControllerModel(controllerType);
                if (controllerModel != null)
                {
                    this.AddNewItem(context, controllerModel, route, controllerType);
                }
            }
        }

        /// <inheritdoc />
        public virtual void OnProvidersExecuted(ApplicationModelProviderContext context)
        {
        }

        private void AddNewItem(
            ApplicationModelProviderContext context,
            ControllerModel controllerModel,
            IGrouping<Type, ISimpleRoute> route,
            TypeInfo controllerType)
        {
            context.Result.Controllers.Add(controllerModel);
            controllerModel.Application = context.Result;

            foreach (var methodRoute in route)
            {
                var method = methodRoute.Action;

                var actionModel = this.CreateActionModel(controllerType, method, methodRoute);
                if (actionModel != null)
                {
                    actionModel.Controller = controllerModel;
                    controllerModel.Actions.Add(actionModel);
                    actionModel.RouteValues.Add("5123D25E-BBA5-44D6-B306-EA915F3B7909", methodRoute.Id);
                    actionModel.RouteValues.Add("internalTagC4F47CA0A9D44F739E3A0BB182CF27D7", methodRoute.Tag);
                    foreach (var parameter in actionModel.ActionMethod.GetParameters())
                    {
                        var parameterModel = this.CreateParameterModel(parameter, actionModel, methodRoute);

                        if (parameterModel != null)
                        {
                            actionModel.Parameters.Add(parameterModel);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ApplicationModels.ControllerModel" /> for the given
        ///     <see cref="T:System.Reflection.TypeInfo" />.
        /// </summary>
        /// <param name="typeInfo">The <see cref="T:System.Reflection.TypeInfo" />.</param>
        /// <returns>
        ///     A <see cref="T:Microsoft.AspNetCore.Mvc.ApplicationModels.ControllerModel" /> for the given
        ///     <see cref="T:System.Reflection.TypeInfo" />.
        /// </returns>
        protected virtual ControllerModel CreateControllerModel(TypeInfo typeInfo)
        {
            if (typeInfo == null)
            {
                throw new ArgumentNullException(nameof(typeInfo));
            }

            var controllerModel = new ControllerModel(typeInfo, new List<object>());
            controllerModel.ControllerName = typeInfo.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)
                                                 ? typeInfo.Name.Substring(0, typeInfo.Name.Length - "Controller".Length)
                                                 : typeInfo.Name;

            if (typeof(IAsyncActionFilter).GetTypeInfo().IsAssignableFrom(typeInfo) || typeof(IActionFilter).GetTypeInfo().IsAssignableFrom(typeInfo))
            {
                controllerModel.Filters.Add(this.shim.CreateControllerActionFilter());
            }

            if (typeof(IAsyncResultFilter).GetTypeInfo().IsAssignableFrom(typeInfo) || typeof(IResultFilter).GetTypeInfo().IsAssignableFrom(typeInfo))
            {
                controllerModel.Filters.Add(this.shim.CreateControllerResultFilter());
            }

            return controllerModel;
        }

        /// <summary>
        ///     Creates the <see cref="T:Microsoft.AspNetCore.Mvc.ApplicationModels.ActionModel" /> instance for the given action
        ///     <see cref="T:System.Reflection.MethodInfo" />.
        /// </summary>
        /// <param name="typeInfo">The controller <see cref="T:System.Reflection.TypeInfo" />.</param>
        /// <param name="methodInfo">The action <see cref="T:System.Reflection.MethodInfo" />.</param>
        /// <returns>
        ///     An <see cref="T:Microsoft.AspNetCore.Mvc.ApplicationModels.ActionModel" /> instance for the given action
        ///     <see cref="T:System.Reflection.MethodInfo" /> or
        ///     <c>null</c> if the <paramref name="methodInfo" /> does not represent an action.
        /// </returns>
        protected virtual ActionModel CreateActionModel(TypeInfo typeInfo, MethodInfo methodInfo, ISimpleRoute route)
        {
            if (typeInfo == null)
            {
                throw new ArgumentNullException(nameof(typeInfo));
            }

            if (methodInfo == null)
            {
                throw new ArgumentNullException(nameof(methodInfo));
            }

            var customAttributes = methodInfo.GetCustomAttributes(true);
            var actionModel = new ActionModel(methodInfo, customAttributes);

            foreach (var obj in customAttributes.OfType<IFilterMetadata>())
            {
                actionModel.Filters.Add(obj);
            }

            actionModel.ActionName = route.Action.Name;

            actionModel.Selectors.Add(CreateSelectorModel(route, this.shim));
            return actionModel;
        }

        /// <summary>
        ///     Creates a <see cref="T:Microsoft.AspNetCore.Mvc.ApplicationModels.ParameterModel" /> for the given
        ///     <see cref="T:System.Reflection.ParameterInfo" />.
        /// </summary>
        /// <param name="parameterInfo">The <see cref="T:System.Reflection.ParameterInfo" />.</param>
        /// <param name="actionModel"></param>
        /// <param name="route"></param>
        /// <returns>
        ///     A <see cref="T:Microsoft.AspNetCore.Mvc.ApplicationModels.ParameterModel" /> for the given
        ///     <see cref="T:System.Reflection.ParameterInfo" />.
        /// </returns>
        protected virtual ParameterModel CreateParameterModel(ParameterInfo parameterInfo, ActionModel actionModel, ISimpleRoute route)
        {
            if (parameterInfo == null)
            {
                throw new ArgumentNullException(nameof(parameterInfo));
            }

            var customAttributes = parameterInfo.GetCustomAttributes(true);

            var context = new BindingMetadataProviderContext(
                                                             ModelMetadataIdentity.ForParameter(parameterInfo),
                                                             ModelAttributes.GetAttributesForParameter(parameterInfo));

            var originalBindingSource = context.BindingMetadata.BindingSource;

            var bindingMetadataProviders = (this.globalModelBinderProviders ?? new List<IMetadataDetailsProvider>())
                .OfType<IBindingMetadataProvider>();

            foreach (var mbp in bindingMetadataProviders)
            {
                mbp.CreateBindingMetadata(context);
            }

            var newBindingSource = context.BindingMetadata.BindingSource;

            if (!route.RouteTemplate.Contains("{" + parameterInfo.Name + "}") && route.Method != HttpMethod.Get)
            {
                customAttributes = customAttributes.Union(new[] {new FromBodyAttribute()}).ToArray();
            }

            var bindingInfo = BindingInfo.GetBindingInfo(customAttributes);

            if (bindingInfo != null && parameterInfo?.Name != null)
            {
                bindingInfo.BinderModelName = parameterInfo?.Name;
            }

            if (originalBindingSource != newBindingSource)
            {
                if (bindingInfo != null)
                {
                    bindingInfo.BindingSource = context.BindingMetadata.BindingSource;
                }
            }

            var info = new ParameterModel(parameterInfo, customAttributes)
                       {
                           BindingInfo = bindingInfo,
                           ParameterName = parameterInfo.Name
                       };

            info.Action = actionModel;
            return info;
        }

        private static SelectorModel CreateSelectorModel(ISimpleRoute route, IFrameworkShim shim)
        {
            var selectorModel = new SelectorModel();
            selectorModel.AttributeRouteModel = new AttributeRouteModel
                                                {
                                                    Name = $"{route.Method}_{route.RouteTemplate?.Replace('/', ':')}",
                                                    Template = route.RouteTemplate
                                                };

            IActionConstraintMetadata constraint = shim.CreateHttpMethodActionConstraint(new[] {route.Method.Method});

            selectorModel.ActionConstraints.Add(constraint);

            return selectorModel;
        }
    }
}