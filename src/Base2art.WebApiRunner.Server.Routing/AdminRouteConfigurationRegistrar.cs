﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class AdminRouteConfigurationRegistrar : RegistrationBase
    {
        public override void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
        {
            base.RegisterRoutes(router, provider, config);

            foreach (var methodConfiguration in config.Tasks ?? new List<ITaskConfiguration>())
            {
                router.MapRoute(
                                HttpMethod.Post,
                                methodConfiguration.Url,
                                methodConfiguration.Type,
                                methodConfiguration.Method,
                                methodConfiguration.Parameters,
                                methodConfiguration.Properties,
                                RouteType.Task);
            }

            foreach (var methodConfiguration in config?.HealthChecks?.Items ?? new List<IHealthCheckConfiguration>())
            {
                router.MapRoute<HealthCheckController>(
                                                       HttpMethod.Get,
                                                       $"healthchecks/{methodConfiguration.Name}",
                                                       x => x.ExecuteAsync(""),
                                                       methodConfiguration.Name,
                                                       methodConfiguration.Parameters,
                                                       methodConfiguration.Properties,
                                                       RouteType.HealthCheck);
//                    router.MapRoute(HttpMethod.Get, "healthchecks/" + methodConfiguration.Name, methodConfiguration.Type, methodConfiguration.Method);
            }

            router.MapRoute<HealthCheckController>(
                                                   HttpMethod.Get,
                                                   "healthchecks",
                                                   x => x.ExecuteAsync(""),
                                                   new Dictionary<string, object>(),
                                                   new Dictionary<string, object>(),
                                                   RouteType.HealthCheck);
        }
    }
}