﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System;
    using System.Net.Http;
    using Microsoft.AspNetCore.Builder;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class PublicRouteConfigurationRegistrar : RegistrationBase
    {
        public override void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
        {
            base.RegisterRoutes(router, provider, config);

            foreach (var methodConfiguration in config.Endpoints)
            {
                router.MapRoute(this.Map(methodConfiguration.Verb),
                                methodConfiguration.Url,
                                methodConfiguration.Type,
                                methodConfiguration.Method,
                                methodConfiguration.Parameters,
                                methodConfiguration.Properties,
                                RouteType.Endpoint);
            }
        }

        private HttpMethod Map(HttpVerb method) => new HttpMethod(method.Method);

        public override void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
            base.RegisterNativeMiddleware(app, config);
            app.UseDefaultFiles();

            app.UseStaticFiles();
        }
    }
}