﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Reflection;
    using Microsoft.AspNetCore.Routing;
    using Web.Server.Registration;

    internal class RouteManager : IRouteManager
    {
        private readonly Func<RouteBuilder> func;
        private readonly List<ISimpleRoute> routes = new List<ISimpleRoute>();

        public RouteManager(Func<RouteBuilder> func) => this.func = func;

        public IEnumerable<ISimpleRoute> Routes => this.routes;

        public void MapRoute(
            HttpMethod method,
            string routeTemplate,
            Type controller,
            MethodInfo action,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties,
            RouteType routeType)
        {
            this.routes.Add(new SimpleRoute
                            {
                                Method = method,
                                RouteTemplate = routeTemplate,
                                Controller = controller,
                                Action = action,
                                Parameters = parameters,
                                Properties = properties,
                                RouteType = routeType
                            });
        }

        public void MapRoute(
            HttpMethod method,
            string routeTemplate,
            Type controller,
            MethodInfo action,
            string tag,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties,
            RouteType routeType)
        {
            this.routes.Add(new SimpleRoute
                            {
                                Method = method,
                                RouteTemplate = routeTemplate,
                                Controller = controller,
                                Action = action,
                                Tag = tag,
                                Parameters = parameters,
                                Properties = properties,
                                RouteType = routeType
                            });
        }

        public void Insert(int index, IRouter router)
        {
            this.func().Routes.Insert(0, router);
        }
    }
}