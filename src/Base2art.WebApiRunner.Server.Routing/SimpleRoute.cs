﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Reflection;
    using Web.Server.Registration;

    internal class SimpleRoute : ISimpleRoute
    {
//        public IReadOnlyDictionary<string, object> ConfigurationOptions { get; }
        public HttpMethod Method { get; set; }
        public string RouteTemplate { get; set; }
        public Type Controller { get; set; }
        public MethodInfo Action { get; set; }
        public IReadOnlyDictionary<string, object> Parameters { get; set; }
        public IReadOnlyDictionary<string, object> Properties { get; set; }

        public string Tag { get; set; }
        public string Id => this.Id();
        public RouteType RouteType { get; set; }
    }
}