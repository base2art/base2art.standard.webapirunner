﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System;
    using System.Reflection;
    using ComponentModel.Composition;
    using ErrorHandling;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Mvc.ApplicationModels;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using Microsoft.Extensions.Options;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class RouteRegistrar : RegistrationBase
    {
        private readonly IFrameworkShim shim;

        public RouteRegistrar(IFrameworkShim shim)
        {
            this.shim = shim;
        }

        public override void RegisterSystemServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
            base.RegisterSystemServices(builder, services, config);

            var descriptor1 = new ServiceDescriptor(
                                                    typeof(IApplicationModelProvider),
                                                    typeof(CustomApplicationModelProvider),
                                                    ServiceLifetime.Transient);
            builder.Services.Replace(descriptor1);
        }

        public override void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
        {
            base.RegisterRoutes(router, provider, config);

            var megaRoute = this.shim.CreateMegaRoute(provider);
            router.Insert(0, megaRoute);
        }

        public override void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
            base.RegisterNativeMiddleware(app, config);

            var allowStackTraceInOutput = config?.Application?.Exceptions?.AllowStackTraceInOutput == true;
            var options = Options.Create(new ExceptionPageOptions
                                         {
                                             AllowStackTrace = allowStackTraceInOutput
                                         });
            app.UseMiddleware<ExceptionPageMiddleware>(options);
        }
    }
}