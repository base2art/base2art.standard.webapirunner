namespace Base2art.WebApiRunner.Server.Routing.Models
{
    using Web.Exceptions.Models;

    public class Error
    {
        public string Message { get; set; }

        public string Code { get; set; }

        public ErrorField[] Fields { get; set; }
    }
}