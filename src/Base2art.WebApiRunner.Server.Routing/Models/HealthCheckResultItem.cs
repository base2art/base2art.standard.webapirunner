﻿namespace Base2art.WebApiRunner.Server.Routing.Models
{
    public class HealthCheckResultItem
    {
        public object Data { get; set; }

        public string Message { get; set; }

        public bool? IsHealthy { get; set; }
    }
}