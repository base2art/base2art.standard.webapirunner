namespace Base2art.WebApiRunner.Server.Routing.Models
{
    public class ErrorWithDetails : Error
    {
        public object Details { get; set; }
    }
}