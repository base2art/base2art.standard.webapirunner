﻿namespace Base2art.WebApiRunner.Server.Routing.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class HealthCheckResult
    {
        public DateTime When { get; set; }

        public List<KeyValuePair<string, HealthCheckResultItem>> Items { get; set; }

        public bool? IsHealthy
        {
            get
            {
                var values = this.Items?.Select(x => x.Value).ToArray();
                if (values == null)
                {
                    return null;
                }

                if (values.Any(x => !x.IsHealthy.GetValueOrDefault(true)))
                {
                    return false;
                }

                if (values.Any(x => !x.IsHealthy.HasValue))
                {
                    return null;
                }

                return true;
            }
        }
    }
}