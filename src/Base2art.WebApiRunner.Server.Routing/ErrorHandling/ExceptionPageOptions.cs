﻿namespace Base2art.WebApiRunner.Server.Routing.ErrorHandling
{
    public class ExceptionPageOptions
    {
        public bool AllowStackTrace { get; set; }
    }
}