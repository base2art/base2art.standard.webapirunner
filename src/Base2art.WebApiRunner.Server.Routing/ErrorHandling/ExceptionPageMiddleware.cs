﻿namespace Base2art.WebApiRunner.Server.Routing.ErrorHandling
{
    using System;
    using System.Net;
    using System.Runtime.ExceptionServices;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Abstractions;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Options;
    using Models;

    public class ExceptionPageMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ExceptionPageOptions options;

        /// <summary>
        ///     Initializes a new instance of the
        ///     <see cref="T:Microsoft.AspNetCore.Diagnostics.DeveloperExceptionPageMiddleware" /> class
        /// </summary>
        /// <param name="next"></param>
        /// <param name="options"></param>
        public ExceptionPageMiddleware(RequestDelegate next, IOptions<ExceptionPageOptions> options)
        {
            this.next = next ?? throw new ArgumentNullException(nameof(next));
            this.options = options.Value;
        }

        /// <summary>Process an individual request.</summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            var exceptionPageMiddleware = this;
            var num = 0;
            Exception obj = null;
            try
            {
                await exceptionPageMiddleware.next(context);
            }
            catch (Exception ex)
            {
                obj = ex;
                num += 1;
            }

            if (num == 1)
            {
//                exceptionPageMiddleware._logger.LogError((EventId) 0, ex, "An unhandled exception has occurred while executing the request",
//                                                         Array.Empty<object>());
                if (context.Response.HasStarted)
                {
//                    exceptionPageMiddleware._logger.LogWarning("The response has already started, the error page middleware will not be executed.",
//                                                               Array.Empty<object>());
                    ExceptionDispatchInfo.Capture(obj).Throw();
                }

                try
                {
                    context.Response.Clear();
                    context.Response.StatusCode = 500;
                    await exceptionPageMiddleware.DisplayException(context, obj);
//                    if (!exceptionPageMiddleware._diagnosticSource.IsEnabled("Microsoft.AspNetCore.Diagnostics.UnhandledException"))
//                        return;
//                    exceptionPageMiddleware._diagnosticSource.Write("Microsoft.AspNetCore.Diagnostics.UnhandledException", (object) new
//                                                                                                                                    {
//                                                                                                                                        httpContext =
//                                                                                                                                        context,
//                                                                                                                                        exception = ex
//                                                                                                                                    });
                    return;
                }
                catch (Exception)
                {
//                    exceptionPageMiddleware._logger.LogError((EventId) 0, ex1, "An exception was thrown attempting to display the error page.",
//                                                             Array.Empty<object>());
                }

                ExceptionDispatchInfo.Capture(obj).Throw();
            }
        }

        private Task DisplayException(HttpContext context, Exception ex) => this.DisplayRuntimeException(context, ex);

        private Task DisplayRuntimeException(HttpContext context, Exception ex)
        {
            var request = context.Request;

            var error = this.options.AllowStackTrace ? new ErrorWithDetails {Details = this.CreateDetails(ex)} : new Error();

            error.Message = ex.Message;
            error.Code = ex.GetType().Name;

            var response = request.CreateResponse(HttpStatusCode.InternalServerError, error);
            return response.ExecuteResultAsync(new ActionContext(context, new RouteData(), new ActionDescriptor()));
//            context. = request.CreateResponse(HttpStatusCode.OK, error);
        }

        private object CreateDetails(Exception exception) => exception.ToString();
    }
}

/*
        private Task DisplayCompilationException(HttpContext context, ICompilationException compilationException)
        {
            CompilationErrorPageModel compilationErrorPageModel = new CompilationErrorPageModel()
                                                                  {
                                                                      Options = this._options
                                                                  };
            CompilationErrorPage compilationErrorPage = new CompilationErrorPage()
                                                        {
                                                            Model = compilationErrorPageModel
                                                        };
            if (compilationException.CompilationFailures == null)
                return compilationErrorPage.ExecuteAsync(context);
            foreach (CompilationFailure compilationFailure in compilationException.CompilationFailures)
            {
                if (compilationFailure != null)
                {
                    List<StackFrameSourceCodeInfo> frameSourceCodeInfoList = new List<StackFrameSourceCodeInfo>();
                    ExceptionDetails exceptionDetails = new ExceptionDetails()
                                                        {
                                                            StackFrames = (IEnumerable<StackFrameSourceCodeInfo>) frameSourceCodeInfoList,
                                                            ErrorMessage = compilationFailure.FailureSummary
                                                        };
                    compilationErrorPageModel.ErrorDetails.Add(exceptionDetails);
                    compilationErrorPageModel.CompiledContent.Add(compilationFailure.CompiledContent);
                    if (compilationFailure.Messages != null)
                    {
                        string sourceFileContent = compilationFailure.SourceFileContent;
                        string[] strArray1;
                        if (sourceFileContent == null)
                            strArray1 = (string[]) null;
                        else
                            strArray1 = sourceFileContent.Split(new string[1]
                                                                {
                                                                    Environment.NewLine
                                                                }, StringSplitOptions.None);
                        string[] strArray2 = strArray1;
                        foreach (DiagnosticMessage message in compilationFailure.Messages)
                        {
                            if (message != null)
                            {
                                StackFrameSourceCodeInfo frame = new StackFrameSourceCodeInfo()
                                                                 {
                                                                     File = compilationFailure.SourceFilePath,
                                                                     Line = message.StartLine,
                                                                     Function = string.Empty
                                                                 };
                                if (strArray2 != null)
                                    this._exceptionDetailsProvider.ReadFrameContent(frame, (IEnumerable<string>) strArray2, message.StartLine,
                                                                                    message.EndLine);
                                frame.ErrorDetails = message.Message;
                                frameSourceCodeInfoList.Add(frame);
                            }
                        }
                    }
                }
            }

            return compilationErrorPage.ExecuteAsync(context);
        }
*/