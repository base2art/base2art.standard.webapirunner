﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;
    using Web.App.Configuration;

    public class HealthCheckController
    {
        private readonly IServerConfiguration config;
        private readonly ICreator creator;

        public HealthCheckController(IServerConfiguration config, ICreator creator)
        {
            this.config = config;
            this.creator = creator;
        }

        public async Task<HealthCheckResult> ExecuteAsync(string internalTagC4F47CA0A9D44F739E3A0BB182CF27D7 = "")
        {
            var healthCheckResultItems = new List<KeyValuePair<string, HealthCheckResultItem>>();

            if (string.IsNullOrWhiteSpace(internalTagC4F47CA0A9D44F739E3A0BB182CF27D7))
            {
                foreach (var item in this.GetAllHealthChecks())
                {
                    var resultItem = await this.GetHealthCheckResultItem(item);

                    healthCheckResultItems.Add(new KeyValuePair<string, HealthCheckResultItem>(item.Name, resultItem));
                }
            }
            else
            {
                foreach (var item in this.GetAllHealthChecks())
                {
                    if (string.Equals(item.Name, internalTagC4F47CA0A9D44F739E3A0BB182CF27D7, StringComparison.OrdinalIgnoreCase))
                    {
                        var resultItem = await this.GetHealthCheckResultItem(item);

                        healthCheckResultItems.Add(new KeyValuePair<string, HealthCheckResultItem>(item.Name, resultItem));
                    }
                }
            }

            return new HealthCheckResult
                   {
                       Items = healthCheckResultItems,
                       When = DateTime.UtcNow
                   };
        }

        private IEnumerable<IHealthCheckConfiguration> GetAllHealthChecks()
            => this?.config?.HealthChecks?.Items ?? new List<IHealthCheckConfiguration>();

        private async Task<HealthCheckResultItem> GetHealthCheckResultItem(IHealthCheckConfiguration item)
        {
            var resultItem = new HealthCheckResultItem();

            var method = item.Method;

            var returnType = method.ReturnType;

            object rezult = null;

            if (returnType == null)
            {
                this.creator.CreateAndInvoke(
                                             item.Type,
                                             item.Method,
                                             new Dictionary<string, object>(),
                                             new Dictionary<string, object>());
            }
            else if (returnType == typeof(Task))
            {
                await (Task) this.creator.CreateAndInvoke(
                                                          item.Type,
                                                          item.Method,
                                                          new Dictionary<string, object>(),
                                                          new Dictionary<string, object>());
            }
            else if (returnType.IsGenericType && returnType.GetGenericTypeDefinition() == typeof(Task<>))
            {
                rezult = await TaskUnwrapper.Get(returnType.GenericTypeArguments[0],
                                                 (Task) this.creator.CreateAndInvoke(
                                                                                     item.Type,
                                                                                     item.Method,
                                                                                     new Dictionary<string, object>(),
                                                                                     new Dictionary<string, object>()));
            }
            else
            {
                rezult = this.creator.CreateAndInvoke(
                                                      item.Type,
                                                      item.Method,
                                                      new Dictionary<string, object>(),
                                                      new Dictionary<string, object>());
            }

            try
            {
                if (rezult is HealthCheckResultItem healthCheckResultItem)
                {
                    resultItem = healthCheckResultItem;
                }
                else
                {
                    resultItem.IsHealthy = true;
                    resultItem.Message = "";
                    resultItem.Data = rezult;
                }
            }
            catch (Exception e)
            {
                resultItem.IsHealthy = false;
                resultItem.Message = e.Message;
                resultItem.Data = e;
            }

            return resultItem;
        }
    }
}