﻿namespace Base2art.WebApiRunner.Server.Routing
{
    using System.Globalization;

    public static class Errors
    {
        /// <summary>
        ///     (none)
        /// </summary>
        internal static string AttributeRoute_NullTemplateRepresentation => "AttributeRoute_NullTemplateRepresentation";

        /// <summary>
        ///     Error {0}:{1}{2}
        /// </summary>
        public static string FormatAttributeRoute_AggregateErrorMessage_ErrorNumber(object p0, object p1, object p2) =>
            string.Format(CultureInfo.CurrentCulture, "AttributeRoute_AggregateErrorMessage_ErrorNumber `{0}` `{1}` `{2}`", p0, p1, p2);

        /// <summary>
        ///     The action '{0}' has ApiExplorer enabled, but is using conventional routing. Only actions which use attribute
        ///     routing support ApiExplorer.
        /// </summary>
        internal static string FormatApiExplorer_UnsupportedAction(object p0) =>
            string.Format(CultureInfo.CurrentCulture, "ApiExplorer_UnsupportedAction `{9}`", p0);

        /// <summary>
        ///     The following errors occurred with attribute routing information:{0}{0}{1}
        /// </summary>
        internal static string FormatAttributeRoute_AggregateErrorMessage(object p0, object p1) =>
            string.Format(CultureInfo.CurrentCulture, "AttributeRoute_AggregateErrorMessage `{0}` `{1}`", p0, p1);

        /// <summary>For action: '{0}'{1}Error: {2}</summary>
        internal static string FormatAttributeRoute_IndividualErrorMessage(object p0, object p1, object p2) =>
            string.Format(CultureInfo.CurrentCulture, "AttributeRoute_IndividualErrorMessage `{0}` `{1}` `{2}` ", p0, p1, p2);

        /// <summary>
        ///     The attribute route '{0}' cannot contain a parameter named '{{{1}}}'. Use '[{1}]' in the route template to insert
        ///     the value '{2}'.
        /// </summary>
        internal static string FormatAttributeRoute_CannotContainParameter(object p0, object p1, object p2) =>
            string.Format(CultureInfo.CurrentCulture, "AttributeRoute_CannotContainParameter `{0}` `{1}` `{2}`", p0, p1, p2);

        /// <summary>
        ///     A method '{0}' must not define attribute routed actions and non attribute routed actions at the same
        ///     time:{1}{2}{1}{1}Use 'AcceptVerbsAttribute' to create a single route that allows multiple HTTP verbs and defines a
        ///     route, or set a route template in all attributes that constrain HTTP verbs.
        /// </summary>
        internal static string FormatAttributeRoute_MixedAttributeAndConventionallyRoutedActions_ForMethod(object p0, object p1, object p2) =>
            string.Format(
                          CultureInfo.CurrentCulture,
                          "AttributeRoute_MixedAttributeAndConventionallyRoutedActions_ForMethod `{0}` `{1}` `{2}`",
                          p0,
                          p1,
                          p2);

        /// <summary>
        ///     Action: '{0}' - Route Template: '{1}' - HTTP Verbs: '{2}'
        /// </summary>
        internal static string FormatAttributeRoute_MixedAttributeAndConventionallyRoutedActions_ForMethod_Item(object p0, object p1, object p2) =>
            string.Format(
                          CultureInfo.CurrentCulture,
                          "AttributeRoute_MixedAttributeAndConventionallyRoutedActions_ForMethod_Item `{0}` `{1}` `{2}`",
                          p0,
                          p1,
                          p2);

        /// <summary>
        ///     Attribute routes with the same name '{0}' must have the same template:{1}{2}
        /// </summary>
        internal static string FormatAttributeRoute_DuplicateNames(object p0, object p1, object p2) =>
            string.Format(CultureInfo.CurrentCulture, "AttributeRoute_DuplicateNames `{0}` `{1}` `{2}`", p0, p1, p2);

        /// <summary>
        ///     Action: '{0}' - Template: '{1}'
        /// </summary>
        internal static string FormatAttributeRoute_DuplicateNames_Item(object p0, object p1) =>
            string.Format(CultureInfo.CurrentCulture, "AttributeRoute_DuplicateNames_Item `{0} `{1}``", p0, p1);
    }
}