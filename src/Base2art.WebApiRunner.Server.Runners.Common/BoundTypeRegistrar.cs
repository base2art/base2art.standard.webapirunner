namespace Base2art.WebApiRunner.Server.Runners
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    public class BoundTypeRegistrar : IBoundTypeRegistrar, IBoundTypeLookup
    {
        private readonly Dictionary<Type, BindingSource> lookup = new Dictionary<Type, BindingSource>();

        public void AddType<T>(BindingSource source)
        {
            this.AddType(typeof(T), source);
        }

        public void AddType(Type type, BindingSource source)
        {
            this.lookup[type] = source;
        }

        public bool Contains(Type modelType) => this.lookup.ContainsKey(modelType);

        public BindingSource this[Type modelType] => this.lookup[modelType];
    }
}