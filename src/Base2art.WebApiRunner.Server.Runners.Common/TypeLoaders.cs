﻿namespace Base2art.WebApiRunner.Server.Runners
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Server.Configuration;
    using Server.Configuration.Types;
    using Web.App.Configuration;

    public static class TypeLoaders
    {
        public static ITypeInstanceConfiguration GetTypeInstance(this ITypeLoader typeLoader, InstanceConfiguration instancer)
        {
            var ic = new ExecutableInstanceConfigurationWrapper();
            ic.Type = LoadType(typeLoader, instancer.Type);
            ic.Parameters = instancer.Parameters;
            ic.Properties = instancer.Properties;
            return ic;
        }

        public static ICallableMethodConfiguration GetExecutableInstance(
            this ITypeLoader typeLoader,
            InstanceConfiguration instancer,
            string methodName)
        {
            var ic = new ExecutableInstanceConfigurationWrapper();
            ic.Type = LoadType(typeLoader, instancer.Type);
            ic.Method = ic.Type.LoadMethod(methodName);
            ic.Parameters = instancer.Parameters;
            ic.Properties = instancer.Properties;
            return ic;
        }

        public static Type LoadType(this ITypeLoader typeLoader, TypeIdentifier methodConfigurationType)
        {
            if (methodConfigurationType == null)
            {
                return null;
            }

            return typeLoader.Load(methodConfigurationType.Value) ?? throw new KeyNotFoundException(methodConfigurationType.Value);
        }

        private static Type LoadType(this ITypeLoader typeLoader, InstanceConfiguration instancer) => typeLoader.LoadType(instancer.Type);

        private static MethodInfo LoadMethod(this ITypeLoader typeLoader, InstanceConfiguration instancer, string methodName) =>
            typeLoader.LoadType(instancer.Type).LoadMethod(methodName);

        private static MethodInfo LoadMethod(this Type type, MethodConfiguration methodConfiguration1) =>
            type.GetMethod(methodConfiguration1.Method) ?? throw new KeyNotFoundException(methodConfiguration1.Method);

        private static MethodInfo LoadMethod(this Type type, string methodConfiguration) =>
            type.GetMethod(methodConfiguration) ?? throw new KeyNotFoundException(methodConfiguration);

        private class ExecutableInstanceConfigurationWrapper : ICallableMethodConfiguration
        {
            public Type Type { get; set; }
            public MethodInfo Method { get; set; }
            public IReadOnlyDictionary<string, object> Parameters { get; set; }
            public IReadOnlyDictionary<string, object> Properties { get; set; }
        }
    }
}