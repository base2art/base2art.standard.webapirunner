namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using AssemblyLoading;
    using Reflection.Discovery;
    using Server.Configuration;
    using Server.Configuration.CommandLine;
    using Server.Configuration.Loader;
    using Web.App.Configuration;

    public static class ConsoleServerConfigurations
    {
        public static Tuple<List<string>, List<string>> GetUrls(this ConsoleServerConfiguration configPrimitive)
        {
            var empty = new List<string>();
            if (configPrimitive.ServerSettings.Http.Count == 0 && configPrimitive.AdminServerSettings.Http.Count == 0)
            {
                return Tuple.Create(new List<string> {MapToUrl(new HttpServerConfiguration())}, empty);
            }

            if (configPrimitive.AdminServerSettings.Http.Count == 0)
            {
                return Tuple.Create(configPrimitive.ServerSettings.Http.Select(MapToUrl).ToList(), empty);
            }

            var publicUrls = new List<string>();
            var adminUrls = new List<string>();

            if (configPrimitive.ServerSettings.Http.Count > 0 && configPrimitive.ServerSettings.Http.Count > 0)
            {
                publicUrls.AddRange(configPrimitive.ServerSettings.Http.Select(MapToUrl));
                adminUrls.AddRange(configPrimitive.AdminServerSettings.Http.Select(MapToUrl));
            }
            else if (configPrimitive.ServerSettings.Http.Count == 0)
            {
                publicUrls.Add(MapToUrl(new HttpServerConfiguration()));
                adminUrls.AddRange(configPrimitive.AdminServerSettings.Http.Select(MapToUrl));
            }
            else if (configPrimitive.AdminServerSettings.Http.Count == 0)
            {
                publicUrls.AddRange(configPrimitive.ServerSettings.Http.Select(MapToUrl));
                adminUrls.Add(MapToUrl(new HttpServerConfiguration()));
            }

            var publicUrlArray = publicUrls.ToArray().ToList();
            var adminUrlArray = adminUrls.ToArray().ToList();
            return Tuple.Create(publicUrlArray, adminUrlArray);
        }

        public static Tuple<ConsoleServerConfiguration, IServerConfiguration, IRegistrationHolder> CreateConsoleServerConfiguration(
            string localPath,
            FrameworkType frameworkType,
            string[] supportedFrameworkVersions,
            IAssemblyFactory assemblyLoaderFactory)
        {
            return CreateGenericServerConfiguration<ConsoleServerConfiguration>(localPath, configPrimitive =>
            {
                ObjectInitializer.SetDefaultValue(() => configPrimitive.ServerSettings);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.ServerSettings.Http);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.AdminServerSettings);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.AdminServerSettings.Http);

                ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies.Names);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies.SearchPaths);

                ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints.Aspects);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints.Urls);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.EndpointsConfigurationItems);

                ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks.Aspects);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks.Items);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.TasksConfigurationItems);

                ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks.Aspects);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks.Items);
                ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecksConfigurationItems);

                return configPrimitive;
            }, (workingDirectory, provider) =>
            {
                return new AssemblyLoader(
                                          assemblyLoaderFactory,
                                          workingDirectory,
                                          provider.BasePath,
                                          provider.JointAssemblies().Select(MapAssembly).ToArray(),
                                          frameworkType,
                                          supportedFrameworkVersions);
            });
        }

        private static IAssemblyLookupData MapAssembly(AssemblyConfiguration arg) =>
            AssemblyLookupDataMapper.Map(arg.Names, arg.SearchPaths, arg.Packages, arg.SystemAssemblies);

//        public static Tuple<ServerConfiguration, IServerConfiguration> CreateServerConfiguration(string localPath)
//        {
//            return CreateGenericServerConfiguration<ServerConfiguration>(localPath, (configPrimitive) =>
//            {
////            ObjectInitializer.SetDefaultValue(() => configPrimitive.ServerSettings);
////            ObjectInitializer.SetDefaultValue(() => configPrimitive.ServerSettings.Http);
////            ObjectInitializer.SetDefaultValue(() => configPrimitive.AdminServerSettings);
////            ObjectInitializer.SetDefaultValue(() => configPrimitive.AdminServerSettings.Http);
//
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies.Names);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Assemblies.SearchPaths);
//
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints.Aspects);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Endpoints.Urls);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.EndpointsConfigurationItems);
//
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks.Aspects);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.Tasks.Items);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.TasksConfigurationItems);
//
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks.Aspects);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecks.Items);
//                ObjectInitializer.SetDefaultValue(() => configPrimitive.HealthChecksConfigurationItems);
//
//                return configPrimitive;
//            }, provider => new CurrentAppDomainAssemblyLoader());
//        }

        public static Tuple<T, IServerConfiguration, IRegistrationHolder> CreateGenericServerConfiguration<T>(
            string localPath,
            Func<T, T> setupBuilder,
            Func<IWorkingDirectoryProvider, ConfigurationProvider<T>, IAssemblyLoader> assemblyLoader)
            where T : ServerConfiguration, new()
        {
            var provider = new ConfigurationProvider<T>(Path.Combine(Environment.CurrentDirectory, localPath));

            var configPrimitive = setupBuilder(provider.Configuration ?? new T());

            var assemblyLoaderObj = assemblyLoader(new WorkingDirectoryProvider(), provider);
            var config = new ServerConfigurationWrapper(assemblyLoaderObj,
                                                        new TypeLoader(assemblyLoaderObj),
                                                        new AspectLookup(),
                                                        provider);
            IServerConfiguration config2 = config;
            IRegistrationHolder config3 = config;

            return Tuple.Create(configPrimitive, config2, config3);
        }

        private static string MapToUrl(HttpServerConfiguration httpServerConfiguration)
        {
            var random = new Random();

            var port = httpServerConfiguration.Port.GetValueOrDefault(0);
            if (port == 0)
            {
                port = random.Next(1025, 65000);
            }

            var host = httpServerConfiguration.Host;
            if (string.IsNullOrWhiteSpace(host))
            {
                host = "*";
            }

            var rootPath = httpServerConfiguration.RootPath;
            if (string.IsNullOrWhiteSpace(rootPath))
            {
                rootPath = "/";
            }

            var url = $"http://{host}:{port}{rootPath}";
            return url;
        }

        private class WorkingDirectoryProvider : IWorkingDirectoryProvider
        {
            public string WorkingDirectory => Environment.CurrentDirectory;
        }
    }
}