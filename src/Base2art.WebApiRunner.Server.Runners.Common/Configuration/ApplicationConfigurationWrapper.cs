﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class ApplicationConfigurationWrapper : IApplicationConfiguration
    {
        private readonly ExceptionConfiguration[] exceptions;
        private readonly ITypeInstanceConfiguration[] filterConfigurations;
        private readonly InstanceConfiguration[] filters;
        private readonly ITypeInstanceConfiguration[] inputFormatterConfigurations;
        private readonly InstanceConfiguration[] inputFormatters;
        private readonly ITypeInstanceConfiguration[] outputFormatterConfigurations;
        private readonly InstanceConfiguration[] outputFormatters;
        private readonly Dictionary<string, string>[] mediaTypes;
        private readonly IModelBindingConfiguration[] modelBindingConfigurations;
        private readonly ModelBindingConfiguration[] modelBindings;
        private readonly IExceptionConfiguration[] otherExceptions;
        private readonly IReadOnlyDictionary<string, string>[] otherMediaTypes;
        private readonly ITypeLoader typeLoader;

        public ApplicationConfigurationWrapper(
            ITypeLoader typeLoader,
            InstanceConfiguration[] filters,
            ITypeInstanceConfiguration[] filterConfigurations,
            InstanceConfiguration[] inputFormatters,
            ITypeInstanceConfiguration[] inputFormatterConfigurations,
            InstanceConfiguration[] outputFormatters,
            ITypeInstanceConfiguration[] outputFormatterConfigurations,
            ModelBindingConfiguration[] modelBindings,
            IModelBindingConfiguration[] modelBindingConfigurations,
            Dictionary<string, string>[] mediaTypes,
            IReadOnlyDictionary<string, string>[] otherMediaTypes,
            ExceptionConfiguration[] exceptions,
            IExceptionConfiguration[] otherExceptions)
        {
            this.typeLoader = typeLoader;
            this.filters = filters;
            this.filterConfigurations = filterConfigurations;
            this.inputFormatters = inputFormatters;
            this.inputFormatterConfigurations = inputFormatterConfigurations;
            this.outputFormatters = outputFormatters;
            this.outputFormatterConfigurations = outputFormatterConfigurations;
            this.modelBindings = modelBindings;
            this.modelBindingConfigurations = modelBindingConfigurations;
            this.mediaTypes = mediaTypes;
            this.otherMediaTypes = otherMediaTypes;
            this.exceptions = exceptions;
            this.otherExceptions = otherExceptions;
        }

        public IReadOnlyList<ITypeInstanceConfiguration> Filters => this.filters.Select(this.Create)
                                                                        .Concat(this.filterConfigurations)
                                                                        .ToList();

        public IReadOnlyList<IModelBindingConfiguration> ModelBindings => this.modelBindings.Select(this.Create)
                                                                              .Concat(this.modelBindingConfigurations)
                                                                              .ToList();

        public IReadOnlyList<ITypeInstanceConfiguration> InputFormatters => this.inputFormatters.Select(this.Create)
                                                                                .Concat(this.inputFormatterConfigurations)
                                                                                .ToList();

        public IReadOnlyList<ITypeInstanceConfiguration> OutputFormatters => this.outputFormatters.Select(this.Create)
                                                                                 .Concat(this.outputFormatterConfigurations)
                                                                                 .ToList();

        public IReadOnlyDictionary<string, string> MediaTypes => this.Create(this.mediaTypes, this.otherMediaTypes);
        public IExceptionConfiguration Exceptions => this.Create(this.exceptions);

        private IExceptionConfiguration Create(ExceptionConfiguration[] configurations)
        {
            configurations = configurations.GetValueOrDefault();

            return new ExceptionConfigurationWrapper(
                                                     this.typeLoader,
                                                     configurations.Any(x => x.RegisterCommonHandlers)
                                                     || this.otherExceptions.Any(x => x.RegisterCommonHandlers),
                                                     configurations.Any(x => x.AllowStackTraceInOutput)
                                                     || this.otherExceptions.Any(x => x.AllowStackTraceInOutput),
//                                                     configurations.SelectMany(x => x?.Handlers.GetValueOrDefault()).ToList(),
//                                                     this.otherExceptions.SelectMany(x => x?.Handlers.GetValueOrDefault()).ToArray(),
                                                     configurations.SelectMany(x => x.Loggers.GetValueOrDefault()).ToList(),
                                                     this.otherExceptions.SelectMany(x => x?.Loggers.GetValueOrDefault()).ToArray());
        }

        private IReadOnlyDictionary<string, string> Create(
            Dictionary<string, string>[] mediaTypesConfiguration,
            IReadOnlyDictionary<string, string>[] mediaTypesBindings)
        {
            mediaTypesConfiguration = mediaTypesConfiguration ?? new Dictionary<string, string>[0];

            if (mediaTypesConfiguration.Length == 1 && mediaTypesBindings.Length == 0)
            {
                return mediaTypesConfiguration[0];
            }

            var items = new Dictionary<string, string>();
            foreach (var pair in mediaTypesConfiguration.SelectMany(dictionary => dictionary))
            {
                items[pair.Key] = pair.Value;
            }

            foreach (var pair in mediaTypesBindings.SelectMany(dictionary => dictionary))
            {
                items[pair.Key] = pair.Value;
            }

            return items;
        }

        private IModelBindingConfiguration Create(ModelBindingConfiguration modelBindingConfiguration) =>
            new ModelBindingConfigurationWrapper(this.typeLoader, modelBindingConfiguration);

        private ITypeInstanceConfiguration Create(InstanceConfiguration arg1) => new TypeInstanceConfiguration
                                                                                 {
                                                                                     Type = this.typeLoader.LoadType(arg1.Type),
                                                                                     Parameters = arg1.Parameters ?? new Dictionary<string, object>(),
                                                                                     Properties = arg1.Properties ?? new Dictionary<string, object>()
                                                                                 };

        private class TypeInstanceConfiguration : ITypeInstanceConfiguration
        {
            public Type Type { get; set; }
            public IReadOnlyDictionary<string, object> Parameters { get; set; }
            public IReadOnlyDictionary<string, object> Properties { get; set; }
        }
    }
}