﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class AspectConfigurationWrapper : ITypeInstanceConfiguration
    {
        private readonly AspectConfiguration aspectConfiguration;
        private readonly IAspectLookup aspectLookup;
        private readonly ITypeLoader typeLoader;

        public AspectConfigurationWrapper(ITypeLoader typeLoader, IAspectLookup aspectLookup, AspectConfiguration aspectConfiguration)
        {
            this.typeLoader = typeLoader;
            this.aspectLookup = aspectLookup;
            this.aspectConfiguration = aspectConfiguration;
        }

        public Type Type => this.aspectLookup.GetByName(this.aspectConfiguration.Name).Type;
        public IReadOnlyDictionary<string, object> Parameters => this.aspectLookup.GetByName(this.aspectConfiguration.Name).Parameters;
        public IReadOnlyDictionary<string, object> Properties => this.aspectLookup.GetByName(this.aspectConfiguration.Name).Properties;

//        public MethodInfo Method => this.aspectLookup.GetByName(this.aspectConfiguration.Name).==;

        public static IReadOnlyList<ITypeInstanceConfiguration> Create(
            ITypeLoader typeLoader,
            IAspectLookup aspectLookup,
            IEnumerable<AspectConfiguration> x)
        {
            return (x ?? new List<AspectConfiguration>())
                   ?.Select(y => Map(typeLoader, aspectLookup, y))
                   ?.ToList();
        }

        private static ITypeInstanceConfiguration Map(ITypeLoader typeLoader, IAspectLookup aspectLookup, AspectConfiguration aspectConfiguration)
            => new AspectConfigurationWrapper(typeLoader, aspectLookup, aspectConfiguration);
    }
}