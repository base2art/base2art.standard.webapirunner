﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class CorsItemConfigurationWrapper : ICorsItemConfiguration
    {
        private readonly CorsItemConfiguration corsItemConfiguration;

        public CorsItemConfigurationWrapper(CorsItemConfiguration corsItemConfiguration) => this.corsItemConfiguration = corsItemConfiguration;

        public bool AllowAnyHeader => this.corsItemConfiguration.AllowAnyHeader;
        public bool AllowAnyMethod => this.corsItemConfiguration.AllowAnyMethod;
        public bool AllowAnyOrigin => this.corsItemConfiguration.AllowAnyOrigin;
        public IReadOnlyList<string> ExposedHeaders => this.corsItemConfiguration.ExposedHeaders;
        public IReadOnlyList<string> Headers => this.corsItemConfiguration.Headers;
        public IReadOnlyList<string> Methods => this.corsItemConfiguration.Methods;
        public IReadOnlyList<string> Origins => this.corsItemConfiguration.Origins;
        public TimeSpan? PreflightMaxAge => this.corsItemConfiguration.PreflightMaxAge;
        public bool SupportsCredentials => this.corsItemConfiguration.SupportsCredentials;
    }
}