﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Reflection.Activation;
    using Server.Configuration;
    using Server.Configuration.Loader;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class ServerConfigurationWrapper : IServerConfiguration, IRegistrationHolder
    {
        private readonly IAspectLookup aspectLookup;
        private readonly IConfigurationProvider<ServerConfiguration> config;
        private readonly Lazy<IReadOnlyList<object>> modulesBacking;

        public ServerConfigurationWrapper(
            IAssemblyLoader asmLoader,
            ITypeLoader typeLoader,
            IAspectLookup aspectLookup,
            IConfigurationProvider<ServerConfiguration> config)
        {
            this.AssemblyLoader = asmLoader;
            this.TypeLoader = typeLoader;
            this.aspectLookup = aspectLookup;
            this.config = config;

            this.modulesBacking = new Lazy<IReadOnlyList<object>>(() => config
                                                                        .JointModules()
                                                                        .Select(typeLoader.GetTypeInstance)
                                                                        .Select(x => x.Type.CreateFrom(null, x.Parameters, x.Properties))
                                                                        .ToList());
        }

        public IReadOnlyList<IModule> Modules => this.modulesBacking.Value.OfType<IModule>().ToList();

        public ITypeLoader TypeLoader { get; }

        public IReadOnlyList<IRegistration> Registrations
        {
            get
            {
                var items = this.modulesBacking.Value.OfType<IRegistration>().ToList();
                return items;
            }
        }

        public IAssemblyLoader AssemblyLoader { get; }

        public string WebRoot => this.config?.Configuration?.WebRoot ?? "";

        public IReadOnlyList<IEndpointConfiguration> Endpoints
            => this.Map(
                        this.config.JointEndpoints(),
                        this.Modules.SelectMany(x => x?.Endpoints.GetValueOrDefault()).ToArray(),
                        this.Modules.SelectMany(x => x?.GlobalEndpointAspects.GetValueOrDefault()).ToArray());

        public IReadOnlyList<ITaskConfiguration> Tasks
            => this.Map(
                        this.config.JointTasks(),
                        this.Modules.SelectMany(x => x?.Tasks.GetValueOrDefault()).ToArray(),
                        this.Modules.SelectMany(x => x?.GlobalTaskAspects.GetValueOrDefault()).ToArray());

        public IHealthChecksConfiguration HealthChecks => this.Map(
                                                                   this.config.JointHealthChecks(),
                                                                   this.Modules
                                                                       .Select(x => x?.HealthChecks)
                                                                       .Where(x => x != null)
                                                                       .ToArray());

        public IApplicationConfiguration Application => this.Map(
                                                                 this.config.JointApplications(),
                                                                 this.Modules
                                                                     .Select(x => x?.Application)
                                                                     .Where(x => x != null)
                                                                     .ToArray());

        public IReadOnlyList<IInjectionItemConfiguration> Injection => this.Map(
                                                                                this.config.JointInjections(),
                                                                                this.Modules
                                                                                    .SelectMany(x => x?.Injection.GetValueOrDefault())
                                                                                    .ToArray());

        public IReadOnlyList<ICorsItemConfiguration> Cors => this.Map(
                                                                      this.config.JointCors(),
                                                                      this.Modules
                                                                          .SelectMany(x => x?.Cors.GetValueOrDefault())
                                                                          .ToArray());

        private IReadOnlyList<IEndpointConfiguration> Map(
            EndpointConfiguration[] jointEndpoints,
            IReadOnlyList<IEndpointConfiguration> others,
            IReadOnlyList<ITypeInstanceConfiguration> aspects)
        {
            return jointEndpoints.GetValueOrDefault()
                                 .SelectMany(x => x?.Urls.GetValueOrDefault().Select(y => this.Create(x, y)))
                                 .Concat(others)
                                 .Select(x => new EndpointConfigurationOther(x, aspects))
                                 .ToList();
        }

        private IReadOnlyList<ITaskConfiguration> Map(
            TasksConfiguration[] jointTasks,
            IReadOnlyList<ITaskConfiguration> others,
            IReadOnlyList<ITypeInstanceConfiguration> aspects)
        {
            return jointTasks.GetValueOrDefault()
                             .SelectMany(x => x?.Items.GetValueOrDefault().Select(y => this.Create(x, y)))
                             .Concat(others)
                             .Select(x => new TaskConfigurationOther(x, aspects))
                             .ToList();
        }

        private IHealthChecksConfiguration Map(
            HealthChecksConfiguration[] jointHealthChecks,
            IReadOnlyList<IHealthChecksConfiguration> others)
        {
            var wrapper = new HealthChecksConfigurationWrapper(
                                                               this.TypeLoader,
                                                               this.aspectLookup,
                                                               jointHealthChecks.SelectMany(x => x?.Items.GetValueOrDefault()).ToArray(),
                                                               others.SelectMany(x => x?.Items.GetValueOrDefault()).ToArray(),
                                                               jointHealthChecks.SelectMany(x => x?.Aspects.GetValueOrDefault()).ToArray(),
                                                               others.SelectMany(x => x?.Aspects.GetValueOrDefault()).ToArray());
            return wrapper;
        }

        private IApplicationConfiguration Map(
            ApplicationConfiguration[] jointApplications,
            IReadOnlyList<IApplicationConfiguration> others)
        {
            jointApplications = jointApplications.GetValueOrDefault();
            return new ApplicationConfigurationWrapper(this.TypeLoader,
                                                       jointApplications.SelectMany(x => x?.Filters.GetValueOrDefault()).ToArray(),
                                                       others.SelectMany(x => x?.Filters.GetValueOrDefault()).ToArray(),
                                                       
                                                       jointApplications.SelectMany(x => x?.InputFormatters.GetValueOrDefault()).ToArray(),
                                                       others.SelectMany(x => x?.InputFormatters.GetValueOrDefault()).ToArray(),
                                                       jointApplications.SelectMany(x => x?.OutputFormatters.GetValueOrDefault()).ToArray(),
                                                       others.SelectMany(x => x?.OutputFormatters.GetValueOrDefault()).ToArray(),
                                                       
                                                       
                                                       jointApplications.SelectMany(x => x?.ModelBindings.GetValueOrDefault()).ToArray(),
                                                       others.SelectMany(x => x?.ModelBindings.GetValueOrDefault()).ToArray(),
                                                       jointApplications.Select(x => x?.MediaTypes.GetValueOrDefault()).ToArray(),
                                                       others.Select(x => x?.MediaTypes.GetValueOrDefault()).ToArray(),
                                                       jointApplications.Select(x => x?.Exceptions.GetValueOrDefault()).ToArray(),
                                                       others.Select(x => x?.Exceptions).Where(x => x != null).ToArray());
        }

        private IReadOnlyList<IInjectionItemConfiguration> Map(
            InjectionConfiguration[] jointInjections,
            IReadOnlyList<IInjectionItemConfiguration> others)
        {
            return jointInjections.GetValueOrDefault()
                                  .SelectMany(x => x?.Items
                                                    .GetValueOrDefault()
                                                    .Select(y => new InjectionItemConfigurationWrapper(this.TypeLoader, y)))
                                  .Concat(others)
                                  .ToList();
        }

        private IReadOnlyList<ICorsItemConfiguration> Map(
            CorsConfiguration[] jointCors,
            IReadOnlyList<ICorsItemConfiguration> others)
        {
            var iitems = jointCors.GetValueOrDefault().SelectMany(x => x?.Items.GetValueOrDefault()).ToArray();

            if (iitems.Length == 0 && others.Count == 0)
            {
                return new List<ICorsItemConfiguration>();
            }

            return iitems.Select(x => new CorsItemConfigurationWrapper(x))
                         .Concat(others)
                         .ToList();
        }

        private ITaskConfiguration Create(TasksConfiguration tasksConfiguration, TaskConfiguration taskConfiguration) =>
            new TaskConfigurationWrapper(this.TypeLoader, this.aspectLookup, tasksConfiguration, taskConfiguration);

        private IEndpointConfiguration Create(EndpointConfiguration endpointConfiguration, MethodConfiguration methodConfiguration) =>
            new EndpointConfigurationWrapper(this.TypeLoader, this.aspectLookup, endpointConfiguration, methodConfiguration);

        private class EndpointConfigurationOther : IEndpointConfiguration
        {
            private readonly IEnumerable<ITypeInstanceConfiguration> aspects;
            private readonly IEndpointConfiguration tasksConfiguration;

            public EndpointConfigurationOther(IEndpointConfiguration tasksConfiguration, IEnumerable<ITypeInstanceConfiguration> aspects)
            {
                this.tasksConfiguration = tasksConfiguration;
                this.aspects = aspects;
            }

            public Type Type => this.tasksConfiguration.Type;

            public IReadOnlyDictionary<string, object> Parameters => this.tasksConfiguration.Parameters;

            public IReadOnlyDictionary<string, object> Properties => this.tasksConfiguration.Properties;

            public MethodInfo Method => this.tasksConfiguration.Method;

            public HttpVerb Verb => this.tasksConfiguration.Verb;

            public string Url => this.tasksConfiguration.Url;

            public IReadOnlyList<ITypeInstanceConfiguration> Aspects => this.tasksConfiguration.Aspects.Concat(this.aspects).ToList();
        }

        private class TaskConfigurationOther : ITaskConfiguration
        {
            private readonly IEnumerable<ITypeInstanceConfiguration> aspects;
            private readonly ITaskConfiguration tasksConfiguration;

            public TaskConfigurationOther(ITaskConfiguration tasksConfiguration, IEnumerable<ITypeInstanceConfiguration> aspects)
            {
                this.tasksConfiguration = tasksConfiguration;
                this.aspects = aspects;
            }

            public Type Type => this.tasksConfiguration.Type;

            public IReadOnlyDictionary<string, object> Parameters => this.tasksConfiguration.Parameters;

            public IReadOnlyDictionary<string, object> Properties => this.tasksConfiguration.Properties;

            public MethodInfo Method => this.tasksConfiguration.Method;

            public HttpVerb Verb => this.tasksConfiguration.Verb;

            public string Url => this.tasksConfiguration.Url;

            public IReadOnlyList<ITypeInstanceConfiguration> Aspects => this.tasksConfiguration.Aspects.Concat(this.aspects).ToList();

            public string Name => this.tasksConfiguration.Name;

            public TimeSpan Delay => this.tasksConfiguration.Delay;

            public TimeSpan Interval => this.tasksConfiguration.Interval;
        }
    }
}