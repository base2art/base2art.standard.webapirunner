﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System.Collections.Generic;
    using System.Linq;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class ExceptionConfigurationWrapper : IExceptionConfiguration
    {
        public ExceptionConfigurationWrapper(
            ITypeLoader loader,
            bool registerCommonHandlers,
            bool allowStackTraceInOutput,
//            IReadOnlyList<InstanceConfiguration> handlers,
//            ICallableMethodConfiguration[] otherHandlers,
            IReadOnlyList<InstanceConfiguration> loggers,
            ITypeInstanceConfiguration[] otherLoggers)
        {
            this.RegisterCommonHandlers = registerCommonHandlers;
            this.AllowStackTraceInOutput = allowStackTraceInOutput;
//            this.Handlers = handlers.Select(x => loader.GetExecutableInstance(x, "ExecuteAsync"))
//                                    .Concat(otherHandlers)
//                                    .ToArray();

            this.Loggers = loggers.Select(x => loader.GetExecutableInstance(x, "ExecuteAsync"))
                                  .Concat(otherLoggers)
                                  .ToArray();
        }

        public bool RegisterCommonHandlers { get; }

        public bool AllowStackTraceInOutput { get; }

//        public IReadOnlyList<ICallableMethodConfiguration> Handlers { get; }
        public IReadOnlyList<ITypeInstanceConfiguration> Loggers { get; }
    }
}