﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class InjectionItemConfigurationWrapper : IInjectionItemConfiguration
    {
        private readonly InjectionItemConfiguration injectionConfiguration;
        private readonly ITypeLoader typeLoader;

        public InjectionItemConfigurationWrapper(ITypeLoader typeLoader, InjectionItemConfiguration injectionConfiguration)
        {
            this.typeLoader = typeLoader;
            this.injectionConfiguration = injectionConfiguration;
        }

        public Type FulfillingType => this.typeLoader.LoadType(this.injectionConfiguration.FulfillingType);
        public Type RequestedType => this.typeLoader.LoadType(this.injectionConfiguration.RequestedType);

        public IReadOnlyDictionary<string, object> Parameters
            => this.injectionConfiguration.Parameters.MapParametersFor(this.FulfillingType ?? this.RequestedType);

        public IReadOnlyDictionary<string, object> Properties
            => this.injectionConfiguration.Properties.MapPropertiesFor(this.FulfillingType ?? this.RequestedType);

        public bool IsSingleton => this.injectionConfiguration.Singleton;
    }
}