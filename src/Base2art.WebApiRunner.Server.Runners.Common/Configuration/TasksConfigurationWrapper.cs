﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class TaskConfigurationWrapper : ITaskConfiguration
    {
        private readonly IAspectLookup aspectLookup;
        private readonly Lazy<ICallableMethodConfiguration> instance;
        private readonly TaskConfiguration taskConfiguration;
        private readonly TasksConfiguration tasksConfiguration;
        private readonly ITypeLoader typeLoader;

        public TaskConfigurationWrapper(
            ITypeLoader typeLoader,
            IAspectLookup aspectLookup,
            TasksConfiguration tasksConfiguration,
            TaskConfiguration taskConfiguration)
        {
            this.typeLoader = typeLoader;
            this.aspectLookup = aspectLookup;
            this.tasksConfiguration = tasksConfiguration;
            this.taskConfiguration = taskConfiguration;
            this.instance =
                new Lazy<ICallableMethodConfiguration>(() => this.typeLoader.GetExecutableInstance(this.taskConfiguration, "ExecuteAsync"));
        }

        private ICallableMethodConfiguration ExecutableInstance => this.instance.Value;

        public string Name => this.taskConfiguration.Name;
        public TimeSpan Delay => this.taskConfiguration.Delay;
        public TimeSpan Interval => this.taskConfiguration.Interval;
        public HttpVerb Verb => HttpVerb.Post;
        public string Url => $"tasks/{this.taskConfiguration.Name}";

        public Type Type => this.ExecutableInstance.Type;
        public MethodInfo Method => this.ExecutableInstance.Method;
        public IReadOnlyDictionary<string, object> Parameters => this.ExecutableInstance.Parameters.MapParametersFor(this.Type);
        public IReadOnlyDictionary<string, object> Properties => this.ExecutableInstance.Properties.MapPropertiesFor(this.Type);

        public IReadOnlyList<ITypeInstanceConfiguration> Aspects =>
            AspectConfigurationWrapper.Create(
                                              this.typeLoader,
                                              this.aspectLookup,
                                              this.taskConfiguration.Aspects.GetValueOrDefault()
                                                  .Union(this.tasksConfiguration.Aspects.GetValueOrDefault()));
    }
}