﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class ModelBindingConfigurationWrapper : IModelBindingConfiguration
    {
        private readonly ITypeLoader loader;
        private readonly ModelBindingConfiguration modelBindingConfiguration;

        public ModelBindingConfigurationWrapper(ITypeLoader loader, ModelBindingConfiguration modelBindingConfiguration)
        {
            this.loader = loader;
            this.modelBindingConfiguration = modelBindingConfiguration;
        }

        public Type BoundType => this.loader.LoadType(this.modelBindingConfiguration.BoundType);
        public Type BinderType => this.loader.LoadType(this.modelBindingConfiguration.Type);

        public IReadOnlyDictionary<string, object> BinderParameters
            => this.modelBindingConfiguration.Parameters.MapParametersFor(this.BinderType);

        public IReadOnlyDictionary<string, object> BinderProperties
            => this.modelBindingConfiguration.Properties.MapPropertiesFor(this.BinderType);

        public BindingType BindingType => this.Map(this.modelBindingConfiguration.BindingType);

        private BindingType Map(ConfigurationBindingType bindingType)
        {
            switch (bindingType)
            {
                case ConfigurationBindingType.Default:
                    return BindingType.Default;
                case ConfigurationBindingType.Hidden:
                    return BindingType.Hidden;
                default:
                    throw new ArgumentOutOfRangeException(nameof(bindingType), bindingType, null);
            }
        }
    }
}