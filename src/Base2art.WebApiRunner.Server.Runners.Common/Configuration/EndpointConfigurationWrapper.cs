﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class EndpointConfigurationWrapper : IEndpointConfiguration
    {
        private readonly IAspectLookup aspectLookup;
        private readonly EndpointConfiguration endpointConfiguration;
        private readonly Lazy<ICallableMethodConfiguration> instance;
        private readonly MethodConfiguration methodConfiguration;
        private readonly ITypeLoader typeLoader;

        public EndpointConfigurationWrapper(
            ITypeLoader typeLoader,
            IAspectLookup aspectLookup,
            EndpointConfiguration endpointConfiguration,
            MethodConfiguration methodConfiguration)
        {
            this.typeLoader = typeLoader;
            this.aspectLookup = aspectLookup;
            this.endpointConfiguration = endpointConfiguration;
            this.methodConfiguration = methodConfiguration;
            this.instance = new Lazy<ICallableMethodConfiguration>(this.CreateConfig);
        }

        private ICallableMethodConfiguration ExecutableInstance => this.instance.Value;

        public HttpVerb Verb => new HttpVerb(this.methodConfiguration.Verb);
        public string Url => this.methodConfiguration.Url;

        public Type Type => this.ExecutableInstance.Type;
        public MethodInfo Method => this.ExecutableInstance.Method;

        public IReadOnlyDictionary<string, object> Parameters =>
            this.ExecutableInstance.Parameters.MapParametersFor(this.Type);

        public IReadOnlyDictionary<string, object> Properties =>
            this.ExecutableInstance.Properties.MapPropertiesFor(this.Type);

        public IReadOnlyList<ITypeInstanceConfiguration> Aspects
            => AspectConfigurationWrapper.Create(
                                                 this.typeLoader,
                                                 this.aspectLookup,
                                                 this.methodConfiguration.Aspects.GetValueOrDefault()
                                                     .Union(this.endpointConfiguration.Aspects.GetValueOrDefault()));

        private ICallableMethodConfiguration CreateConfig()
            => this.typeLoader.GetExecutableInstance(this.methodConfiguration, this.methodConfiguration.Method);
    }
}