﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System.Collections.Generic;
    using System.Linq;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class HealthChecksConfigurationWrapper : IHealthChecksConfiguration
    {
        private readonly AspectConfiguration[] aspectConfigurations;
        private readonly IAspectLookup aspectLookup;
        private readonly HealthCheckConfiguration[] healthCheckConfigurations;
        private readonly ITypeInstanceConfiguration[] otherAspectConfigurations;
        private readonly IHealthCheckConfiguration[] otherHealthChecks;
        private readonly ITypeLoader typeLoader;

        public HealthChecksConfigurationWrapper(
            ITypeLoader typeLoader,
            IAspectLookup aspectLookup,
            HealthCheckConfiguration[] healthCheckConfigurations,
            IHealthCheckConfiguration[] otherHealthChecks,
            AspectConfiguration[] aspectConfigurations,
            ITypeInstanceConfiguration[] otherAspectConfigurations)
        {
            this.typeLoader = typeLoader;
            this.aspectLookup = aspectLookup;
            this.otherHealthChecks = otherHealthChecks;
            this.otherAspectConfigurations = otherAspectConfigurations;
            this.healthCheckConfigurations = healthCheckConfigurations.GetValueOrDefault();
            this.aspectConfigurations = aspectConfigurations.GetValueOrDefault();
        }

        public IReadOnlyList<IHealthCheckConfiguration> Items
            => this.healthCheckConfigurations.GetValueOrDefault()
                   .Select(this.Create)
                   .Concat(this.otherHealthChecks)
                   .ToList();

        public IReadOnlyList<ITypeInstanceConfiguration> Aspects
            => AspectConfigurationWrapper.Create(this.typeLoader, this.aspectLookup, this.aspectConfigurations.GetValueOrDefault())
                                         .Concat(this.otherAspectConfigurations)
                                         .ToList();

        private IHealthCheckConfiguration Create(HealthCheckConfiguration healthCheckConfiguration)
            => new HealthCheckConfigurationWrapper(this.typeLoader, healthCheckConfiguration);
    }
}