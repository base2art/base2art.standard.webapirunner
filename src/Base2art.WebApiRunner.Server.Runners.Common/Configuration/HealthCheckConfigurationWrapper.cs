﻿namespace Base2art.WebApiRunner.Server.Runners.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Server.Configuration;
    using Web.App.Configuration;

    internal class HealthCheckConfigurationWrapper : IHealthCheckConfiguration
    {
        private readonly HealthCheckConfiguration healthCheckConfiguration;
        private readonly Lazy<ICallableMethodConfiguration> instance;
        private readonly ITypeLoader typeLoader;

        public HealthCheckConfigurationWrapper(
            ITypeLoader typeLoader,
            HealthCheckConfiguration healthCheckConfiguration)
        {
            this.typeLoader = typeLoader;
            this.healthCheckConfiguration = healthCheckConfiguration;
            this.instance =
                new Lazy<ICallableMethodConfiguration>(() => this.typeLoader.GetExecutableInstance(this.healthCheckConfiguration, "ExecuteAsync"));
        }

        private ICallableMethodConfiguration ExecutableInstance => this.instance.Value;

        public string Name => this.healthCheckConfiguration.Name;
        public Type Type => this.ExecutableInstance.Type;
        public MethodInfo Method => this.ExecutableInstance.Method;
        public IReadOnlyDictionary<string, object> Parameters => this.ExecutableInstance.Parameters.MapParametersFor(this.Type);
        public IReadOnlyDictionary<string, object> Properties => this.ExecutableInstance.Properties.MapPropertiesFor(this.Type);
    }
}