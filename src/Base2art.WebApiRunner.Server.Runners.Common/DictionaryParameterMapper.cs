namespace Base2art.WebApiRunner.Server.Runners
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Newtonsoft.Json;

    public static class DictionaryParameterMapper
    {
        public static Dictionary<string, object> MapParametersFor(
            this IReadOnlyDictionary<string, object> instanceParameters,
            Type instanceType)
        {
            var ctors = instanceType?.GetConstructors()?.ToArray() ?? new ConstructorInfo[0];

            var readOnlyDictionary = instanceParameters ?? new Dictionary<string, object>();
            var firstOrDefault = ctors.OrderByDescending(x => Matches(x.GetParameters(), readOnlyDictionary))
                                      .FirstOrDefault();

            return Convert(
                           readOnlyDictionary,
                           () => { return firstOrDefault?.GetParameters().ToDictionary(x => x.Name, x => x); },
                           items => items?.ParameterType);
        }

        public static Dictionary<string, object> MapPropertiesFor(
            this IReadOnlyDictionary<string, object> instanceProperties,
            Type instanceType)
        {
            return Convert(
                           instanceProperties ?? new Dictionary<string, object>(),
                           () => instanceType?.GetProperties().ToDictionary(x => x.Name, x => x),
                           items => items?.PropertyType);
        }

        private static int Matches(ParameterInfo[] getParameters, IReadOnlyDictionary<string, object> instanceParameters)
        {
            if (getParameters == null)
            {
                return -1;
            }

            return getParameters.Count(x => instanceParameters.ContainsKey(x.Name));
        }

        private static Dictionary<string, object> Convert<T>(
            IReadOnlyDictionary<string, object> instanceParameters,
            Func<IDictionary<string, T>> lookup,
            Func<T, Type> typeLookup)
            where T : class
        {
            var result = new Dictionary<string, object>();

            var items = lookup();

            var processed = new HashSet<string>();
            foreach (var keyValuePair in instanceParameters)
            {
                T propOrParam = null;
                if (items.ContainsKey(keyValuePair.Key))
                {
                    propOrParam = items[keyValuePair.Key];
                }

                if (propOrParam != null)
                {
                    var value = keyValuePair.Value;
                    processed.Add(keyValuePair.Key);

                    var strContent = JsonConvert.SerializeObject(value);
                    var valueContent = JsonConvert.DeserializeObject(strContent, typeLookup(propOrParam));
                    result[keyValuePair.Key] = valueContent;
                }
            }

            foreach (var item in items)
            {
                if (!processed.Contains(item.Key))
                {
                    var type = typeLookup(item.Value);
                    if (type.IsArray && IsPrimitiveOrString(type.GetElementType()))
                    {
                        result[item.Key] = Array.CreateInstance(type.GetElementType(), 0);
                    }
                }
            }

            return result;
        }

        private static bool IsPrimitiveOrString(Type type) => type == typeof(string) || type.GetTypeInfo().IsValueType || type.IsNullableValueType();

        public static bool IsNullableValueType(this Type type) => type.GetTypeInfo().IsValueType && type.GetTypeInfo().IsGenericType
                                                                                                 && type.GetGenericTypeDefinition()
                                                                                                 == typeof(Nullable<>);
    }
}