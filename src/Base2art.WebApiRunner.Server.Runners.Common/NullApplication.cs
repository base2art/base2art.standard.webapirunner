namespace Base2art.WebApiRunner.Server.Runners
{
    using System.Threading.Tasks;
    using Web.App.LifeCycle;

    public class NullApplication : IApplication
    {
        public void Reload()
        {
        }

        public Task ExecuteJob(string name) => Task.CompletedTask;

        public Task ExecuteJobWithParameters(string name, object parameters) => Task.CompletedTask;
    }
}