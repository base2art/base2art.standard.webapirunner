﻿namespace Base2art.WebApiRunner.Server.Runners
{
    using System.Linq;
    using ApiExploration;
    using ApiExploration.Swagger;
    using Aspects;
    using Bindings;
    using Cors;
    using Filters;
    using Formatters;
    using Injection;
    using Microsoft.AspNetCore.Builder;
    using Registrations;
    using Routing;
    using Web.App.Configuration;
    using Web.App.LifeCycle;
    using Web.Server.Registration;
    using Web.ServerEnvironment;

    public class CommonStartup : StartupBase
    {
        private readonly IApplication application;
        private readonly IRegistrationHolder registrations;

        public CommonStartup(
            IServerConfiguration configuration,
            IApplication application, 
            IRegistrationHolder registrations,
            IFrameworkShim shim, 
            IWebServerSettings webServerSettings,
            IBoundTypeRegistrar registrar)
            : base(configuration, application, new RouteManagerProvider(), shim, webServerSettings, registrar)
        {
            this.application = application;
            this.registrations = registrations;
        }

        protected override IRegistration[] Registrations => this.KnownRegistrations
                                                                .Union(this.AdditionalRegistrations)
                                                                .Union(this.registrations?.Registrations ?? new IRegistration[0])
                                                                .ToArray();

        private IRegistration[] KnownRegistrations => new IRegistration[]
                                                      {
                                                          new FormatRegistrar(),
                                                          new FilterRegistrar(),
                                                          new SystemServicesRegistrar(this.application),
                                                          new RouteRegistrar(this.Shim),
                                                          new ApiExplorationRegistrar(),
                                                          new ApiExplorationSwaggerRegistrar(),
                                                          new AspectsRegistrar(),
                                                          new CorsRegistrar(),
                                                          new BindingsRegistrar(),
                                                          new InjectionRegistrar()
                                                      };

        protected virtual IRegistration[] AdditionalRegistrations => new IRegistration[0];

        protected override void RegisterMiddleware(IApplicationBuilder app)
        {
            base.RegisterMiddleware(app);
//            app.UseDeveloperExceptionPage();
            app.UseDeveloperExceptionPage();
        }
    }
}