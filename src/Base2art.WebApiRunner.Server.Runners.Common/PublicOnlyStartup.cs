﻿namespace Base2art.WebApiRunner.Server.Runners
{
    using Routing;
    using Web.App.Configuration;
    using Web.App.LifeCycle;
    using Web.Server.Registration;
    using Web.ServerEnvironment;

    public class PublicOnlyStartup : CommonStartup
    {
        public PublicOnlyStartup(
            IServerConfiguration configuration,
            IApplication application,
            IRegistrationHolder registrations,
            IFrameworkShim shim,
            IWebServerSettings webServerSettings,
            IBoundTypeRegistrar registrar)
            : base(configuration, application, registrations, shim, webServerSettings, registrar)
        {
        }

        protected override IRegistration[] AdditionalRegistrations => new IRegistration[]
                                                                      {
                                                                          new PublicRouteConfigurationRegistrar()
                                                                      };
    }
}