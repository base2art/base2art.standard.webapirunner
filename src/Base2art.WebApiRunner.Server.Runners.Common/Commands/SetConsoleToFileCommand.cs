﻿namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using Logging;
    using Web.App.Configuration;

    public class SetConsoleToFileCommand : ICommand
    {
        private readonly string prefix;
        private TextWriter newOut;
        private TextWriter oldOut;

        public SetConsoleToFileCommand(string prefix) => this.prefix = prefix;

        public Task<int> Run(IServerConfiguration configuration)
        {
            this.oldOut = Console.Out;
            var newOutLocal = this.CreateLogFileWriter();
            if (newOutLocal != null)
            {
                this.newOut = newOutLocal;
                Console.SetOut(new Wrapper(newOutLocal));
            }

            return Task.FromResult(0);
        }

        public Task Shutdown()
        {
            Console.SetOut(this.oldOut);
            try
            {
                var textWriter = this.newOut;
                textWriter?.Dispose();
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.Error, e);
            }

            return Task.CompletedTask;
        }

        private TextWriter CreateLogFileWriter()
        {
            var entryAssembly = Assembly.GetEntryAssembly();
            var currentDllLocation = entryAssembly?.Location;

            if (string.IsNullOrWhiteSpace(currentDllLocation))
            {
                var codeBase = entryAssembly?.CodeBase;
                if (!string.IsNullOrWhiteSpace(codeBase))
                {
                    currentDllLocation = new Uri(codeBase).LocalPath;
                }
            }

            var location = Environment.CurrentDirectory;
            if (currentDllLocation != null)
            {
                location = Path.GetDirectoryName(currentDllLocation);
            }

            var dir = Path.Combine(location, "logs");
            var logs = Directory.CreateDirectory(dir);

            var pid = 0;
            var startTime = DateTime.UtcNow;

            try
            {
                var currentProcess = Process.GetCurrentProcess();
                pid = currentProcess.Id;
                startTime = currentProcess.StartTime;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            try
            {
                var file = $"{this.prefix}_{startTime:yyyy-MM-dd_HHmmss}_{pid}.log";
                var logFile = Path.Combine(logs.FullName, file);
                var stream = new FileStream(logFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);

                return new StreamWriter(stream)
                       {
                           AutoFlush = true
                       };
            }
            catch (IOException e)
            {
                Logger.Log(LogLevel.Error, "Cannot open Redirect.txt for writing");
                Logger.Log(LogLevel.Error, e);
                return null;
            }
        }

        private class Wrapper : TextWriter
        {
            private readonly TextWriter textWriter;

            public Wrapper(TextWriter textWriter) => this.textWriter = textWriter;

            public override Encoding Encoding => this.textWriter.Encoding;

            public override void Write(char value)
                => this.Try(x => x.Write(value));

            public override void Write(char[] buffer, int index, int count)
                => this.Try(x => x.Write(buffer, index, count));

            public override void Write(string value)
                => this.Try(x => x.Write(value));

            public override Task WriteAsync(char value)
                => this.TryAsync(x => x.WriteAsync(value));

            public override Task WriteAsync(string value)
                => this.TryAsync(x => x.WriteAsync(value));

            public override Task WriteAsync(char[] buffer, int index, int count)
                => this.TryAsync(x => x.WriteAsync(buffer, index, count));

            public override Task WriteLineAsync(char value)
                => this.TryAsync(x => x.WriteLineAsync(value));

            public override Task WriteLineAsync(string value)
                => this.TryAsync(x => x.WriteLineAsync(value));

            public override Task WriteLineAsync(char[] buffer, int index, int count) => this.TryAsync(x => x.WriteLineAsync(buffer, index, count));

            public override Task FlushAsync() => this.TryAsync(x => x.FlushAsync());

            private async Task TryAsync(Func<TextWriter, Task> func)
            {
                try
                {
                    var writer = this.textWriter;
                    await func(writer);
                }
                catch (Exception)
                {
                }
            }

            private void Try(Action<TextWriter> func)
            {
                try
                {
                    var writer = this.textWriter;
                    func(writer);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}