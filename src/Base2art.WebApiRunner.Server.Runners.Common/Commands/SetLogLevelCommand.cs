namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Logging;
    using Web.App.Configuration;

    public class SetLogLevelCommand : ICommand
    {
        public Task<int> Run(IServerConfiguration configuration)
        {
            FileInfo FirstFile(string location)
            {
                try
                {
                    var di = new DirectoryInfo(location);
                    var fileInfo = di.GetFiles("*.log-level").OrderByDescending(x => x.Name).FirstOrDefault();
                    return fileInfo;
                }
                catch
                {
                    return null;
                }
            }

            FileInfo FileOf() =>
                FirstFile(Environment.CurrentDirectory) ?? FirstFile(Path.GetDirectoryName(Assembly.GetEntryAssembly()?.Location));

            var firstFile = FileOf();
            if (firstFile != null)
            {
                Logger.Level = this.MapToLevel(firstFile.Name);
            }
            else
            {
#if DEBUG
                Logger.Level = LogLevel.Debug;
#else
                Logger.Level = LogLevel.Info;
#endif
            }

            return Task.FromResult(0);
        }

        public Task Shutdown() => Task.CompletedTask;

        private LogLevel MapToLevel(string firstFileName)
        {
            var key = firstFileName.Replace(".log-level", "").ToUpperInvariant();
            switch (key)
            {
                case "DEBUG":
                    return LogLevel.Debug;
                case "ALL":
                    return LogLevel.All;
                case "ERROR":
                    return LogLevel.Error;
                case "FATAL":
                    return LogLevel.Fatal;
                case "INFO":
                    return LogLevel.Info;
                case "WARNING":
                    return LogLevel.Warning;
                default:
                    return LogLevel.Info;
            }
        }
    }
}