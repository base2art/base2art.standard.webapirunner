namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using Web.ServerEnvironment;

    public class WebServerSettings : IWebServerSettings
    {
        public string CurrentDirectory { get; set; }
        public string ContentRoot { get; set; }
        public string WebRoot { get; set; }
    }
}