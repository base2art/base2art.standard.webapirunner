namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    public enum RunType
    {
        Console,
        IIS,
        Service,
        IIS_Legacy
    }
}