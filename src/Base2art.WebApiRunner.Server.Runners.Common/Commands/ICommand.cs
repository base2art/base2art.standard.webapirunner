namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using System.Threading.Tasks;
    using Web.App.Configuration;

    public interface ICommand
    {
        Task<int> Run(IServerConfiguration configuration);

        Task Shutdown();
    }
}