namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.ServiceProcess;
    using System.Threading;
    using System.Threading.Tasks;
    using Configuration;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Hosting.WindowsServices;
    using Microsoft.Extensions.DependencyInjection;
    using Server.Configuration.CommandLine;
    using Web.App.Configuration;
    using Web.App.LifeCycle;

    public class ConsoleServerCommand : ICommand
    {
        private readonly IApplication application;
        private readonly CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        private readonly ConsoleServerConfiguration configPrimitive;
        private readonly List<Action<IServiceCollection>> middlewareServicesRegistrations = new List<Action<IServiceCollection>>();
        private readonly IRegistrationHolder registrations;
        private readonly IFrameworkShim shim;
        private readonly RunType runType;

        public ConsoleServerCommand(
            IApplication application,
            ConsoleServerConfiguration configPrimitive,
            IRegistrationHolder registrations,
            IFrameworkShim shim,
            RunType runType)
        {
            this.application = application;
            this.configPrimitive = configPrimitive;
            this.registrations = registrations;
            this.shim = shim;
            this.runType = runType;
        }

        public Task<int> Run(IServerConfiguration config)
        {
            var (publicUrlArray, adminUrlArray) = this.configPrimitive.GetUrls();
            if (adminUrlArray?.Count == 0)
            {
                return ServerCommands.RunSingleSite(this.application,
                                                    config,
                                                    () => publicUrlArray.ToArray(),
                                                    this.Decorate,
                                                    this.Start,
                                                    this.registrations,
                                                    this.shim);
            }

            return ServerCommands.RunMultiSite(
                                               this.application,
                                               config,
                                               () => publicUrlArray.ToArray(),
                                               () => adminUrlArray.ToArray(),
                                               this.Decorate,
                                               (x, y, z) => this.Start(x, y),
                                               this.registrations,
                                               this.shim);
        }

        public Task Shutdown()
        {
            this.cancelTokenSource.Cancel();
            return Task.CompletedTask;
        }

        private IWebHostBuilder Decorate(IWebHostBuilder builder, IServerConfiguration config, WebServerSettings serverSettings)
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var wwwroot = config.WebRoot;
            if (string.IsNullOrWhiteSpace(wwwroot))
            {
                wwwroot = "wwwroot";
            }

            var basedir = Directory.CreateDirectory(Path.Combine(currentDirectory, wwwroot));

            builder = builder.UseContentRoot(currentDirectory);
            builder = builder.UseWebRoot(basedir.FullName);

            serverSettings.WebRoot = basedir.FullName;
            serverSettings.ContentRoot = currentDirectory;
            serverSettings.CurrentDirectory = currentDirectory;

            foreach (var middlewareServicesRegistration in this.middlewareServicesRegistrations.Where(x => x != null))
            {
                builder = builder.ConfigureServices(x => middlewareServicesRegistration?.Invoke(x));
            }

            builder = IntegrateBuilder(builder, this.runType);

            return builder;
        }

        public ConsoleServerCommand AddMiddlewareServicesRegistrations(Action<IServiceCollection> collection)
        {
            this.middlewareServicesRegistrations.Add(collection);
            return this;
        }

        private static IWebHostBuilder IntegrateBuilder(IWebHostBuilder builder, RunType iisIntegration)
        {
            switch (iisIntegration)
            {
                case RunType.Console:
                    return builder;
                case RunType.IIS:
                    return builder.UseIISIntegration();
                case RunType.IIS_Legacy:
                    return builder.UseIISIntegration();
                case RunType.Service:
                    return builder;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private async Task<int> Start(IWebHost webHost, IServerConfiguration serverConfiguration)
        {
            var currentDirectory = Directory.GetCurrentDirectory();
//            var current = new NativeDllLoader(new DirectoryInfo(currentDirectory));
//            current.Load();

            if (this.runType == RunType.Service)
            {
                var webHostService = new WebHostService(webHost);
                this.cancelTokenSource.Token.Register(() => webHostService.Stop());
                ServiceBase.Run(webHostService);
            }
            else if (this.runType == RunType.IIS_Legacy)
            {
                webHost.Start();
            }
            else
            {
                await webHost.RunAsync(this.cancelTokenSource.Token);
            }

            return 0;
        }
    }
}