namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using ComponentModel.Composition;
    using Logging;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http.Features;
    using Microsoft.Extensions.DependencyInjection;
    using Registrations;
    using Web.App.Configuration;
    using Web.App.LifeCycle;
    using Web.ServerEnvironment;

    public static class ServerCommands
    {
        private static IWebHostBuilder Decorate(
            IWebHostBuilder builder,
            IServerConfiguration config,
            IApplication application,
            IRegistrationHolder registrations,
            IWebServerSettings settings,
            IFrameworkShim shim)
        { 
            var registrar = new BoundTypeRegistrar();
            return builder.UseKestrel(options =>
                          {
                              options.AllowSynchronousIO = true;
                              options.Limits.MaxRequestBodySize = int.MaxValue / 2;
                          })
                          .ConfigureServices(x =>
                          {
                              x.Add(ServiceDescriptor.Singleton(config));
                              x.Add(ServiceDescriptor.Singleton(application));
                              x.Add(ServiceDescriptor.Singleton(registrations));
                              x.Add(ServiceDescriptor.Singleton(shim));
                              x.Add(ServiceDescriptor.Singleton(settings));
                              x.Add(ServiceDescriptor.Singleton(typeof(IBoundTypeRegistrar), registrar));
                              x.Add(ServiceDescriptor.Singleton(typeof(IBoundTypeLookup), registrar));
                          });
        }

        public static Task<int> RunSingleSite(
            IApplication application,
            IServerConfiguration config,
            Func<string[]> urlProvider,
            Func<IWebHostBuilder, IServerConfiguration, WebServerSettings, IWebHostBuilder> webBuilder,
            Func<IWebHost, IServerConfiguration, Task<int>> launcher,
            IRegistrationHolder registrations,
            IFrameworkShim shim)
            => RunForSite<SharedStartup>(application, config, urlProvider, webBuilder, (x, y, z) => launcher(x, y), registrations, shim);

        public static async Task<int> RunMultiSite(
            IApplication application,
            IServerConfiguration config,
            Func<string[]> publicUrlProvider,
            Func<string[]> adminUrlProvider,
            Func<IWebHostBuilder, IServerConfiguration, WebServerSettings, IWebHostBuilder> webBuilder,
            Func<IWebHost, IServerConfiguration, Type, Task<int>> launcher,
            IRegistrationHolder registrations,
            IFrameworkShim shim)
        {
            var publicSite = RunForSite<PublicOnlyStartup>(application, config, publicUrlProvider, webBuilder, launcher, registrations, shim);
            var adminSite = RunForSite<AdminOnlyStartup>(application, config, adminUrlProvider, webBuilder, launcher, registrations, shim);

            var items = await Task.WhenAll(publicSite, adminSite);

            return items.FirstOrDefault(item => item != 0);
        }

        private static async Task<int> RunForSite<T1>(IApplication application,
                                                      IServerConfiguration config,
                                                      Func<string[]> urlProvider,
                                                      Func<IWebHostBuilder, IServerConfiguration, WebServerSettings, IWebHostBuilder> webBuilder,
                                                      Func<IWebHost, IServerConfiguration, Type, Task<int>> launcher,
                                                      IRegistrationHolder registrations,
                                                      IFrameworkShim shim)
            where T1 : class
        {
            var webServerSettings = new WebServerSettings();
            var builder = webBuilder(Decorate(new WebHostBuilder(), config, application, registrations, webServerSettings, shim).UseStartup<T1>(), config, webServerSettings);

            if (urlProvider != null)
            {
                var urls = urlProvider();
                builder.UseUrls(urls);
            }

            try
            {
                var webHost = builder.Build();

                using (webHost)
                {
                    return await launcher(webHost, config, typeof(T1));
                }
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.Fatal, e);
                throw;
            }
        }
    }
}