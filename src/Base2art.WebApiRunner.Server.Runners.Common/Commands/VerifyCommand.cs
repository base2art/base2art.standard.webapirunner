﻿namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Configuration;
    using Logging;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Server.Configuration.CommandLine;
    using Web.App.Configuration;

    public class VerifyCommand : ICommand
    {
        private readonly ConsoleServerConfiguration configPrimitive;
        private readonly IRegistrationHolder registrations;
        private readonly IFrameworkShim shim;

        public VerifyCommand(ConsoleServerConfiguration configPrimitive, IRegistrationHolder registrations, IFrameworkShim shim)
        {
            this.configPrimitive = configPrimitive;
            this.registrations = registrations;
            this.shim = shim;
        }

        public Task<int> Run(IServerConfiguration configuration)
        {
            var (publicUrlArray, adminUrlArray) = this.configPrimitive.GetUrls();
            if (adminUrlArray?.Count == 0)
            {
                return ServerCommands.RunSingleSite(
                                                    new NullApplication(),
                                                    configuration,
                                                    () => publicUrlArray.ToArray(),
                                                    (builder, y, z) => builder,
                                                    (host, serverConfiguration) => this.Start(host, serverConfiguration),
                                                    this.registrations,
                                                    this.shim);
            }

            return ServerCommands.RunMultiSite(new NullApplication(),
                                               configuration,
                                               () => publicUrlArray.ToArray(),
                                               () => adminUrlArray.ToArray(),
                                               (builder, y, x) => builder,
                                               (x, y, type) => this.Start(x, y, type),
                                               this.registrations,
                                               this.shim);
        }

        public Task Shutdown() => Task.CompletedTask;

        private Task<int> Start(IWebHost webHost, IServerConfiguration config)
        {
            var creator = webHost.Services.GetService<ICreator>();
            var verifier = new Verifier(x => creator.Create(x, null, null));
            verifier.VerifyPublic(config);
            verifier.VerifyAdmin(config);
            verifier.Verify();

            return Task.FromResult(0);
        }

        private Task<int> Start(IWebHost webHost, IServerConfiguration config, Type startup)
        {
            var creator = webHost.Services.GetService<ICreator>();
            var verifier = new Verifier(x => creator.Create(x, null, null));
            if (startup == typeof(AdminOnlyStartup))
            {
                verifier.VerifyAdmin(config);
            }
            else
            {
                verifier.VerifyPublic(config);
            }

            verifier.Verify();

            return Task.FromResult(0);
        }

//        public static void VerifyAll(ICreator hostServices, IServerConfiguration config)
//        {
//        }
//
//        public static void VerifyPublic(ICreator hostServices, IServerConfiguration config)
//        {
//            var verifier = new Verifier(x => hostServices.Create(x, null, null));
//            verifier.VerifyPublic(config);
//            verifier.Verify();
//        }
//
//        public static void VerifyAdmin(ICreator hostServices, IServerConfiguration config)
//        {
//            var verifier = new Verifier(x => hostServices.Create(x, null, null));
//            verifier.VerifyAdmin(config);
//            verifier.Verify();
//        }

        private class Verifier
        {
            private readonly Func<Type, object> creator;
            private readonly List<string> exceptions = new List<string>();
            private readonly HashSet<Type> processedTypes = new HashSet<Type>();

            public Verifier(Func<Type, object> creator) => this.creator = creator;

            public Exception Exceptions
                => this.exceptions.Count == 0 ? null : new TypeLoadException(string.Join(Environment.NewLine, this.exceptions));

            public void VerifyAdmin(IServerConfiguration config)
            {
                this.VerifyCore(config);

                this.VerifyTypes(config.Tasks, x => x.Type, x => x.Type);
                this.VerifyTypes(config.HealthChecks.Items, x => x.Type, x => x.Type);
            }

            public void VerifyPublic(IServerConfiguration config)
            {
                this.VerifyCore(config);

                this.VerifyTypes(config.Endpoints, x => x.Type, x => x.Type);
            }

            private void VerifyCore(IServerConfiguration config)
            {
                this.VerifyTypes(config.Injection, x => x.RequestedType, x => x.FulfillingType);
            }

            private void VerifyTypes<T>(
                IReadOnlyList<T> items,
                Func<T, Type> requestedType,
                Func<T, Type> fulfillingType)
            {
                foreach (var injection in items)
                {
                    var requested = requestedType(injection);
                    var fulfilling = fulfillingType(injection) ?? requested;

                    if (this.processedTypes.Contains(requested))
                    {
                        continue;
                    }

                    this.processedTypes.Add(requested);

                    Logger.Log(LogLevel.Debug, $"Loading `{requested}`...");
//
//                    this.Assert(
//                                requested != null,
//                                "Type Not Found",
//                                "[RequestedType=`{0}` FulfillingType=`{1}`]",
//                                requested,
//                                fulfilling);

                    Exception e = null;
                    object obj = null;
                    try
                    {
                        obj = this.creator(requested);
                    }
                    catch (Exception exception)
                    {
                        e = exception;
                    }

                    this.Assert(
                                obj != null,
                                e,
                                "Object Not Found",
                                "[RequestedType=`{0}` FulfillingType=`{1}`]",
                                requested,
                                fulfilling);
                }
            }

            public void Assert(
                bool condition,
                Exception exception,
                string message,
                string detailMessageFormat,
                params object[] args)
            {
                if (condition)
                {
                    return;
                }

                var detailedMessage = string.Format(detailMessageFormat, args);
                this.exceptions.Add(detailedMessage);

                Console.WriteLine(message);

                void PrintException(Exception e, int i)
                {
                    if (e == null)
                    {
                        return;
                    }

                    Console.WriteLine("{0}{1}", new string(' ', i * 4), e.Message);
                    Console.WriteLine("{0}{1}", new string(' ', i * 4), e.StackTrace);
                    if (e is AggregateException ag)
                    {
                        foreach (var inner in ag.InnerExceptions)
                        {
                            PrintException(inner, i + 1);
                        }
                    }
                    else
                    {
                        PrintException(e.InnerException, i + 1);
                    }
                }

                PrintException(exception, 1);
            }

            public void Verify()
            {
                var exception = this.Exceptions;
                if (exception == null)
                {
                    return;
                }

                throw exception;
            }
        }
    }
}