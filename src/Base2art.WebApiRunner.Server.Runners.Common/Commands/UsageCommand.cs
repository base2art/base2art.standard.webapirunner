namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using System;
    using System.Threading.Tasks;
    using Web.App.Configuration;

    public class UsageCommand : ICommand
    {
        public Task<int> Run(IServerConfiguration configuration)
        {
            Console.WriteLine("dotnet <Program.dll> (server) configurationFile.yaml");
            return Task.FromResult(-1);
        }

        public Task Shutdown() => Task.CompletedTask;
    }
}