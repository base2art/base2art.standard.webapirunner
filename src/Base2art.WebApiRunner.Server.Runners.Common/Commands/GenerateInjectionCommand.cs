﻿namespace Base2art.WebApiRunner.Server.Runners.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Web.App.Configuration;

    public class GenerateInjectionCommand : ICommand
    {
        public Task<int> Run(IServerConfiguration config)
        {
            /*
injection:
  items:

    - requestedType: Poc.CiCd.IJobStoreService
      fulfillingType: Poc.CiCd.ArtifactStorage.FileSystem.JobStoreService
      parameters:
        info: /home/tyoung/cicd/jobs/

             * 
             */
            Console.WriteLine("injection:");
            Console.WriteLine("  items:");

            var set = new HashSet<Type>();

            foreach (var endpoint in config.Endpoints)
            {
                this.RunForContainerType(endpoint.Type, config, set);
            }

            foreach (var endpoint in config.Tasks)
            {
                this.RunForContainerType(endpoint.Type, config, set);
            }

            foreach (var endpoint in config.HealthChecks.Items)
            {
                this.RunForContainerType(endpoint.Type, config, set);
            }

            return Task.FromResult(0);
        }

        public Task Shutdown() => Task.CompletedTask;

        private void RunForContainerType(
            Type injectableType,
            IServerConfiguration config,
            HashSet<Type> set)
        {
            try
            {
                foreach (var constructor in injectableType.GetConstructors())
                {
                    foreach (var type in constructor.GetParameters().Select(x => x.ParameterType.TypeOrElementType()))
                    {
                        this.RunForType(type, config, set);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Running for concrete Type: {injectableType.FullName}");
                Console.WriteLine(e);
                throw;
            }
        }

        private void RunForType(Type type, IServerConfiguration config, HashSet<Type> set)
        {
            if (type.IsInterface)
            {
                if (set.Contains(type))
                {
                    return;
                }

                set.Add(type);

                var implementors = this.FindImplementors(type, config)
                                       .Where(x => x.IsConcreteClass())
                                       .ToArray();

                foreach (var implementor in implementors)
                {
                    var ctors = implementor.GetConstructors();
                    if (ctors.Length == 0)
                    {
                        continue;
                    }

                    Console.WriteLine("    - requestedType: " + type.FullName);
                    Console.WriteLine("      fulfillingType: " + implementor.FullName);

                    var ctor = ctors[0];
                    var parms = ctor.GetParameters()
                                    .Select(x => x)
                                    .Where(x => x.ParameterType.TypeOrElementType().IsConcreteClass())
                                    .ToArray();

                    if (parms.Length != 0)
                    {
                        Console.WriteLine("      parameters: ");
                        foreach (var parm in parms)
                        {
                            Console.WriteLine($"        {parm.Name}: {this.GetDefaultValueByType(parm)}");
                        }
                    }

                    Console.WriteLine();
                    this.RunForContainerType(implementor, config, set);
                }
            }
        }

        private object GetDefaultValueByType(ParameterInfo parm)
        {
            if (parm.ParameterType.IsValueType)
            {
                return Activator.CreateInstance(parm.ParameterType);
            }

            return $"{'"'}{parm.ParameterType}{'"'}";
        }

        private IEnumerable<Type> FindImplementors(Type requestedType, IServerConfiguration config)
        {
            var assemblies = config.AssemblyLoader.Assemblies();

            var types = new List<Type>();

            foreach (var assembly in assemblies)
            {
                try
                {
                    types.AddRange(Map(assembly.GetTypes(), requestedType));
                }
                catch (ReflectionTypeLoadException rte)
                {
                    Console.WriteLine(rte);
                    types.AddRange(Map(rte.Types, requestedType));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return types;
        }

        private static IEnumerable<Type> Map(IEnumerable<Type> types, Type requestedType)
        {
            return types.Where(x => x != null)
                        .Where(requestedType.IsAssignableFrom)
                        .ToArray();
        }
    }
}