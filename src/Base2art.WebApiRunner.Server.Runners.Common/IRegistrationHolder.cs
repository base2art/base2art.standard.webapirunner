namespace Base2art.WebApiRunner.Server.Runners
{
    using System.Collections.Generic;
    using Web.Server.Registration;

    public interface IRegistrationHolder
    {
        IReadOnlyList<IRegistration> Registrations { get; }
    }
}