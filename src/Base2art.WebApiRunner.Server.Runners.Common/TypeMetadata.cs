﻿namespace Base2art.WebApiRunner.Server.Runners
{
    using System;

    public static class TypeMetadata
    {
        public static Type TypeOrElementType(this Type type) => type.IsArray ? type.GetElementType() : type;

        public static bool IsConcreteClass(this Type type) => !type.IsAbstract
                                                              && !type.IsInterface
                                                              && !type.IsArray;

        public static bool IsArrayAndElementTypeIsConcreteClass(this Type type) => type.IsArray && type.GetElementType().IsConcreteClass();
    }
}