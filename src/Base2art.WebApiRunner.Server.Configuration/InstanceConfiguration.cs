﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class InstanceConfiguration
    {
        // ci
        public TypeIdentifier Type { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        public Dictionary<string, object> Properties { get; set; }
    }
}