namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;

    public class AssemblyConfiguration
    {
        public List<string> Names { get; set; }

        public List<string> SearchPaths { get; set; }

        public List<string> Packages { get; set; }

        public List<string> SystemAssemblies { get; set; }
    }
}