﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, false)]
    public class AspectsConfiguration : List<AspectConfiguration>
    {
    }
}