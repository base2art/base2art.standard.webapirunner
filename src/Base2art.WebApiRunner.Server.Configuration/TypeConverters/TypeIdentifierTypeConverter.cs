﻿namespace Base2art.WebApiRunner.Server.Configuration.TypeConverters
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using Types;

    // ci
    public class TypeIdentifierTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                var s = value.ToString();
                return new TypeIdentifier(s);
            }

            return base.ConvertFrom(context, culture, value);
        }
    }
}