namespace Base2art.WebApiRunner.Server.Configuration.CommandLine
{
    public class ConsoleServerConfiguration : ServerConfiguration
    {
        public ServerSettingsConfiguration ServerSettings { get; set; }

//        public List<MiddlewareConfiguration> OwinMiddleware { get; set; }
//
//        public List<MiddlewareConfiguration> OwinAdminMiddleware { get; set; }

        public ServerSettingsConfiguration AdminServerSettings { get; set; }
    }
}