namespace Base2art.WebApiRunner.Server.Configuration.CommandLine
{
    public class HttpServerConfiguration
    {
        public string Host { get; set; }

        public string RootPath { get; set; }

        public int? Port { get; set; }
    }
}