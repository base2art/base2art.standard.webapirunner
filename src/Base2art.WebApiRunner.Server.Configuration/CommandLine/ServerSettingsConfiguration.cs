namespace Base2art.WebApiRunner.Server.Configuration.CommandLine
{
    using System.Collections.Generic;

    public class ServerSettingsConfiguration
    {
        public List<HttpServerConfiguration> Http { get; set; }

//        public List<HttpsServerConfiguration> Https { get; set; }
    }
}