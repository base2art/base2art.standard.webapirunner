namespace Base2art.WebApiRunner.Server.Configuration
{
    using Types;

    [Inspectable(true, true)]
    public class MethodConfiguration : InstanceConfiguration
    {
        // ci

        public string Verb { get; set; }

        public string Method { get; set; }

        public string Url { get; set; }

        public AspectConfiguration[] Aspects { get; set; }
    }
}