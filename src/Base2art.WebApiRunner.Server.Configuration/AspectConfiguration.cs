﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class AspectConfiguration
    {
        public string Name { get; set; }
        public Dictionary<string, object> Parameters { get; set; }
    }
}