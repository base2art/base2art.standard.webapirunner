namespace Base2art.WebApiRunner.Server.Configuration
{
    using Types;

    [Inspectable(true, true)]
    public class ExceptionMapItemConfiguration
    {
        // ci
        public TypeIdentifier ExceptionType { get; set; }

        public int HttpResponseCode { get; set; }

        public string ResponseCode { get; set; }

        public string ResponseMessage { get; set; }

        public string ResponseMessageExceptionProperty { get; set; }
    }
}