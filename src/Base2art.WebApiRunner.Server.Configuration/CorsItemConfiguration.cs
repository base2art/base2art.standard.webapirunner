﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System;
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class CorsItemConfiguration
    {
        public string RequestHost { get; set; }

        public bool AllowAnyHeader { get; set; }

        public bool AllowAnyMethod { get; set; }

        public bool AllowAnyOrigin { get; set; }

        public List<string> ExposedHeaders { get; set; }

        public List<string> Headers { get; set; }

        public List<string> Methods { get; set; }

        public List<string> Origins { get; set; }

        public TimeSpan? PreflightMaxAge { get; set; }

        public bool SupportsCredentials { get; set; }
    }
}