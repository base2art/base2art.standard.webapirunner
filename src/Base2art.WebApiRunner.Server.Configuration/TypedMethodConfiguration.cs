﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using Types;

    [Inspectable(false, false)]
    public class TypedMethodConfiguration
    {
        public MethodConfiguration MethodConfiguration { get; set; }

        public ControllerType ControllerType { get; set; }
    }
}