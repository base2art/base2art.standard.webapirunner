﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using Types;

    [Inspectable(true, true)]
    public class ModelBindingConfiguration : InstanceConfiguration
    {
        // ci
        public TypeIdentifier BoundType { get; set; }

        public ConfigurationBindingType BindingType { get; set; }
    }
}