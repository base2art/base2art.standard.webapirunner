﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class CorsConfiguration
    {
        public bool Enabled { get; set; }

        public List<CorsItemConfiguration> Items { get; set; }
    }
}