﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class InjectionConfiguration
    {
        public List<InjectionItemConfiguration> Items { get; set; }
    }
}