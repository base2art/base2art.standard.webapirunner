namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class ServerConfiguration
    {
        public string WebRoot { get; set; }

        public EndpointConfiguration Endpoints { get; set; }

        public PathList EndpointsConfigurationItems { get; set; }

        public TasksConfiguration Tasks { get; set; }

        public PathList TasksConfigurationItems { get; set; }

        public HealthChecksConfiguration HealthChecks { get; set; }

        public PathList HealthChecksConfigurationItems { get; set; }

        public ApplicationConfiguration Application { get; set; }

        public PathList ApplicationConfigurationItems { get; set; }

        public InjectionConfiguration Injection { get; set; }

        public PathList InjectionConfigurationItems { get; set; }

        // ci
//        public TypeIdentifier DependencyResolverTypeName { get; set; }

//        public List<MiddlewareConfiguration> Middleware { get; set; }

//        public PathList MiddlewareConfigurationItems { get; set; }

        public AssemblyConfiguration Assemblies { get; set; }

        public PathList AssembliesConfigurationItems { get; set; }

        public CorsConfiguration Cors { get; set; }

        public PathList CorsConfigurationItems { get; set; }

        public List<InstanceConfiguration> Modules { get; set; }
    }
}