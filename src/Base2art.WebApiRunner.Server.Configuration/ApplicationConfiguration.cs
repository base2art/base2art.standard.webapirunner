namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class ApplicationConfiguration
    {
        public List<InstanceConfiguration> Filters { get; set; }

        public List<ModelBindingConfiguration> ModelBindings { get; set; }

        public List<InstanceConfiguration> InputFormatters { get; set; }

        public List<InstanceConfiguration> OutputFormatters { get; set; }

        public Dictionary<string, string> MediaTypes { get; set; }

        public ExceptionConfiguration Exceptions { get; set; }

        //        public List<string> ControllerSuffixes { get; set; }
    }
}