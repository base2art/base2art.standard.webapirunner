namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class EndpointConfiguration
    {
        public List<MethodConfiguration> Urls { get; set; }

        public AspectsConfiguration Aspects { get; set; }
    }
}