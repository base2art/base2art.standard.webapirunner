﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class HealthChecksConfiguration
    {
        public List<HealthCheckConfiguration> Items { get; set; }

        public AspectsConfiguration Aspects { get; set; }
    }
}