﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System;
    using Types;

    [Inspectable(true, true)]
    public class TaskConfiguration : InstanceConfiguration
    {
        // ci

        public string Name { get; set; }

        public TimeSpan Delay { get; set; }

        public TimeSpan Interval { get; set; }

        public AspectsConfiguration Aspects { get; set; }
    }
}