﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class InjectionItemConfiguration
    {
        // ci
        public TypeIdentifier RequestedType { get; set; }

        public TypeIdentifier FulfillingType { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        public Dictionary<string, object> Properties { get; set; }

        public bool Singleton { get; set; }
    }
}