﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    public enum ControllerType
    {
        Service,
        Task,
        HealthCheck
    }
}