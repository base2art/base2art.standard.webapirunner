﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using System.Collections.Generic;
    using Types;

    [Inspectable(true, true)]
    public class ExceptionConfiguration
    {
        public bool RegisterCommonHandlers { get; set; }

        public bool AllowStackTraceInOutput { get; set; }

//        public List<InstanceConfiguration> Handlers { get; set; }

        public List<InstanceConfiguration> Loggers { get; set; }

//        public List<ExceptionMapItemConfiguration> ResponseMapItems { get; set; }
    }
}