﻿namespace Base2art.WebApiRunner.Server.Configuration.Types
{
    using System.Collections.Generic;

    /// <summary>
    ///     A list of application-root-relative paths, to other onfiguration files.
    /// </summary>
    [Inspectable(true, false)]
    public class PathList : List<string>
    {
    }
}