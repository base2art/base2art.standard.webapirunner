﻿namespace Base2art.WebApiRunner.Server.Configuration.Types
{
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class InspectableAttribute : Attribute
    {
        public InspectableAttribute(bool isInspectable, bool areChildrenInspectable)
        {
            this.AreChildrenInspectable = areChildrenInspectable;
            this.IsInspectable = isInspectable;
        }

        public bool IsInspectable { get; }

        public bool AreChildrenInspectable { get; }
    }
}