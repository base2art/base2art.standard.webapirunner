﻿namespace Base2art.WebApiRunner.Server.Configuration.Types
{
    using System.ComponentModel;
    using TypeConverters;

    /// <summary>
    ///     A string that represents a full class name.
    /// </summary>
    /// <example>
    ///     System.Data.SqlClient.SqlConnection
    /// </example>
    /// <example>
    ///     System.Data.SqlClient.SqlConnection, System.Data
    /// </example>
    [Inspectable(true, false)]
    [TypeConverter(typeof(TypeIdentifierTypeConverter))]
    public class TypeIdentifier
    {
        /// <summary>
        ///     Initializes a new instance of the TypeIdentifier class.
        /// </summary>
        /// <param name="value">The actual value.</param>
        public TypeIdentifier(string value) => this.Value = value;

        /// <summary>
        ///     The backing value.
        /// </summary>
        public string Value { get; }

        /// <summary>Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</summary>
        /// <returns>A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString() => this.Value;

        /// <summary>
        ///     Converts the object to a string.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static implicit operator string(TypeIdentifier d) => d == null ? null : d.ToString();

        /// <summary>
        ///     Converts the object to a string.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static implicit operator TypeIdentifier(string d) => string.IsNullOrWhiteSpace(d) ? null : new TypeIdentifier(d);
    }
}