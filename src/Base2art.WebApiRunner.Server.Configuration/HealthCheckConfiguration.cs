﻿namespace Base2art.WebApiRunner.Server.Configuration
{
    using Types;

    [Inspectable(true, true)]
    public class HealthCheckConfiguration : InstanceConfiguration
    {
        public string Name { get; set; }
    }
}