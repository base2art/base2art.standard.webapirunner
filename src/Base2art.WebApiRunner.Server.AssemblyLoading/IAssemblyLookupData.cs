namespace Base2art.WebApiRunner.Server.AssemblyLoading
{
    using System.Collections.Generic;

    public interface IAssemblyLookupData
    {
        IReadOnlyList<string> Names { get; }

        IReadOnlyList<string> SearchPaths { get; }

        IReadOnlyList<string> Packages { get; }

        IReadOnlyList<string> SystemAssemblies { get; }
    }
}