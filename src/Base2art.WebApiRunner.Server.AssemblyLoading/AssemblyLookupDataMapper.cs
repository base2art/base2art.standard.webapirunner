namespace Base2art.WebApiRunner.Server.AssemblyLoading
{
    using System.Collections.Generic;

    public static class AssemblyLookupDataMapper
    {
        public static IAssemblyLookupData Map(
            List<string> argNames,
            List<string> argSearchPaths,
            List<string> argPackages,
            List<string> argSystemAssemblies) =>
            new AssemblyLookupData(argNames, argSearchPaths, argPackages, argSystemAssemblies);

        private class AssemblyLookupData : IAssemblyLookupData
        {
            public AssemblyLookupData(List<string> argNames, List<string> argSearchPaths, List<string> argPackages, List<string> argSystemAssemblies)
            {
                this.Names = argNames ?? new List<string>();
                this.SearchPaths = argSearchPaths ?? new List<string>();
                this.Packages = argPackages ?? new List<string>();
                this.SystemAssemblies = argSystemAssemblies ?? new List<string>();
            }

            public IReadOnlyList<string> Names { get; }
            public IReadOnlyList<string> SearchPaths { get; }
            public IReadOnlyList<string> Packages { get; }
            public IReadOnlyList<string> SystemAssemblies { get; }
        }
    }
}