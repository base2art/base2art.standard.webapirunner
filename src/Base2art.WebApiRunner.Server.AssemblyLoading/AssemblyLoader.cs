﻿namespace Base2art.WebApiRunner.Server.AssemblyLoading
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using IO;
    using Logging;
    using Models;
    using Reflection.Discovery;
    using Text;

    public class AssemblyLoader : AssemblyLoaderBase
    {
        private readonly IAssemblyFactory assemblyLoaderFactory;
        private readonly DirectoryInfo configPath;
        private readonly IAssemblyLookupData[] jointAssemblies;
        private readonly string[] supportedFrameworkVersions;
        private readonly IWorkingDirectoryProvider workingDirectory;

        public AssemblyLoader(IAssemblyFactory assemblyLoaderFactory,
                              IWorkingDirectoryProvider workingDirectory,
                              string configPath,
                              IAssemblyLookupData[] jointAssemblies,
                              FrameworkType frameworkType,
                              string[] supportedFrameworkVersions)
            : base(assemblyLoaderFactory, frameworkType, supportedFrameworkVersions)
        {
            this.assemblyLoaderFactory = assemblyLoaderFactory;
            this.workingDirectory = workingDirectory;
            this.configPath = new DirectoryInfo(configPath);
            this.jointAssemblies = jointAssemblies;
            this.supportedFrameworkVersions = supportedFrameworkVersions;
        }

        protected override string[] SearchPaths()
        {
            var baseValues = base.SearchPaths();
            var paths1 = this.jointAssemblies.SelectMany(x => x.SearchPaths)
                             .Union(baseValues).ToArray();

            var dllPaths = paths1.SelectMany(path => path.Contains("*")
                                                         ? this.configPath
                                                               .FindMatchingDirectories(new[] {path}, MatchingMode.AllowExitingRoot)
                                                               .Select(x => x.FileEntry.FullName)
                                                         : new[] {Path.Combine(this.configPath.FullName, path)})
                                 .Union(baseValues.Select(x => Path.Combine(this.workingDirectory.WorkingDirectory, x)))
                                 .Union(baseValues)
                                 .ToArray();
            return dllPaths;
        }

        protected override string[] SystemAssemblies()
            => base.SystemAssemblies().Concat(this.jointAssemblies.SelectMany(x => x.SystemAssemblies)).ToArray();

        protected override Assembly[] Create()
        {
            var asms = base.Create();

            var set = new HashSet<string>(asms.Select(x => x.GetName().Name));

            var packagesList = this.jointAssemblies.SelectMany(x => x.Packages ?? new List<string>()).ToList();
            var names = this.jointAssemblies.SelectMany(x => x.Names).ToArray();
            var systemAssemblies = this.SystemAssemblies();
            var searchPaths = this.SearchPaths();

            var loaded = new List<Assembly>(asms);

            var explicitSearchPaths = this.jointAssemblies.SelectMany(x => x.SearchPaths);

            var dllPaths = explicitSearchPaths.SelectMany(path => path.Contains("*")
                                                                      ? this.configPath
                                                                            .FindMatchingDirectories(new[] {path}, MatchingMode.AllowExitingRoot)
                                                                            .Select(x => x.FileEntry.FullName)
                                                                      : new[] {Path.Combine(this.configPath.FullName, path)})
                                              .ToArray();

            var files = dllPaths.Where(x => Directory.Exists(x)).SelectMany(x => Directory.GetFiles(x, "*.deps.json"));

            var ser = new CamelCasingSimpleJsonSerializer();

            foreach (var file in files)
            {
                Logger.Log(LogLevel.Debug, $"Adding items from: {file}");
                try
                {
                    var fileContent = File.ReadAllText(file);
                    var deps = ser.Deserialize<Deps>(fileContent);
                    var asmPackageIds = deps.targets.SelectMany(x => x.Value.Keys)
                                            .Where(x => x.Contains("/"))
                                            .Select(x => x.Split('/'))
                                            .Where(x => x.Length == 2)
                                            .Select(x => x[0] + ":" + x[1]);
                    packagesList.AddRange(asmPackageIds);
                }
                catch (Exception e)
                {
                    Logger.Log(LogLevel.Error, e);
                }
            }

            var packages = packagesList.ToArray();
            this.RegisterByPackage(packages, set);
            //
            this.RegisterRuntimesNative();

            // Can't register native by name...
            // this.RegisterByName(names, set, null);
            this.RegisterSearchPaths(searchPaths, names, set);

            this.LoadByPackage(packages, set, loaded, systemAssemblies);
            this.LoadByName(names, set, loaded);
            LoadBySearchPaths(searchPaths, names, set, loaded);

//            Logger.WriteLine(Environment.GetEnvironmentVariable("PATH"));
            return loaded.ToArray();
        }

        private void RegisterRuntimesNative()
        {
            var runtimesPath = Path.Combine(this.workingDirectory.WorkingDirectory, "runtimes");

            if (Directory.Exists(runtimesPath))
            {
                var runtimesDir = new DirectoryInfo(runtimesPath);
                var ridDirs = runtimesDir.GetDirectories();

                foreach (var ridDir in ridDirs)
                {
                    if (this.assemblyLoaderFactory.Supports(ridDir.Name))
                    {
                        var nativeDir = ridDir.GetDirectories()
                                              .FirstOrDefault(x => string.Equals(x.Name, "native", StringComparison.OrdinalIgnoreCase));
                        if (nativeDir != null)
                        {
                            var files = nativeDir.GetFiles();
                            foreach (var file in files)
                            {
                                IPackagedNativeDll unmanaged = new FakeNative(file);
                                this.assemblyLoaderFactory.RegisterUnmanaged(new[] {unmanaged});
                            }
                        }
                    }
                }
            }
        }

        private void RegisterSearchPaths(string[] searchPaths, string[] names, HashSet<string> setB)
        {
        }

        private static void LoadBySearchPaths(
            string[] searchPaths,
            string[] names,
            HashSet<string> set,
            List<Assembly> loaded)
        {
            LoadBySearchPathsAction(searchPaths, names, set.Contains, file =>
            {
                set.Add(file);

                loaded.Add(Assembly.Load(file));
            });
        }

        private static void LoadBySearchPathsAction(string[] searchPaths, string[] names, Func<string, bool> hasItem, Action<string> takeItem)
        {
            foreach (var path in searchPaths)
            {
                Logger.Log(LogLevel.Debug, $"SearchPath: {path}");

                if (Directory.Exists(path))
                {
                    var files = Directory.GetFiles(path, "*.dll")
                                         .Union(Directory.GetFiles(path, "*.exe"))
                                         .Select(Path.GetFileNameWithoutExtension)
                                         .Where(x => !string.IsNullOrWhiteSpace(x));

                    foreach (var file in files)
                    {
                        var matchingNames = names.Where(x => x.Contains("*") || x.Contains("?"));
                        foreach (var nameMatcher in matchingNames)
                        {
                            if (nameMatcher.IsWildCardMatch(file, true) && !hasItem(file))
                            {
                                takeItem(file);
                            }
                        }
                    }
                }
            }
        }

        private IPackagedDll GetItems(
            IEnumerable<IPackagedDll> items,
            string fileName,
            bool allowAnyRid,
            Version maxVersion,
            string supportedFramework)
        {
            if (allowAnyRid)
            {
                return items.Where(x => x.Version == maxVersion)
                            .Where(x => x.Name == fileName)
                            .Where(x => string.Equals(x.RuntimeIdentifier, "any", StringComparison.OrdinalIgnoreCase))
                            .Where(x => string.Equals(x.FrameworkVersion, supportedFramework, StringComparison.OrdinalIgnoreCase))
                            .OrderByDescending(x => x.VersionName)
                            .FirstOrDefault();
            }

            return items.Where(x => x.Version == maxVersion)
                        .Where(x => x.Name == fileName)
                        .Where(x => !string.Equals(x.RuntimeIdentifier, "any", StringComparison.OrdinalIgnoreCase)
                                    && this.assemblyLoaderFactory.Supports(x.RuntimeIdentifier))
                        .Where(x => string.Equals(x.FrameworkVersion, supportedFramework, StringComparison.OrdinalIgnoreCase))
                        .OrderByDescending(x => x.FrameworkVersion)
                        .FirstOrDefault();
        }

        private void LoadByPackage(string[] packages, HashSet<string> set, List<Assembly> loaded, string[] systemAssemblies)
        {
            this.LoadByPackageAction(packages, set.Contains, allDllFiles =>
            {
                var dllFiles = allDllFiles.Managed;
                var fileNames = dllFiles.ToList();

                var versions = new[]
                               {
                                   fileNames.Max(x => x.Version)
                               };

                var maxVersion = versions.Max();

                foreach (var fileName in fileNames)
                {
                    var name = fileName.Name.EndsWith(".dll", StringComparison.OrdinalIgnoreCase)
                                   ? fileName.Name.Substring(0, fileName.Name.Length - 4)
                                   : fileName.Name;

                    if (!set.Contains(name))
                    {
                        var item = this.GetAssembly(fileNames, fileName.Name, maxVersion, systemAssemblies);
                        if (item != null)
                        {
                            loaded.Add(item);

                            set.Add(item.GetName().Name);
                        }
                    }
                }
            });
        }

        private void RegisterByPackage(string[] packages, HashSet<string> set)
        {
            this.LoadByPackageAction(packages, set.Contains, dllFiles => { this.Register(dllFiles.Unmanaged); });
        }

        private void Register(IReadOnlyList<IPackagedNativeDll> unmanaged)
        {
            this.assemblyLoaderFactory.RegisterUnmanaged(unmanaged);
        }

        private void LoadByPackageAction(
            string[] packages,
            Func<string, bool> hasItem,
            Action<(IReadOnlyList<IPackagedDll> Managed, IReadOnlyList<IPackagedNativeDll> Unmanaged)> takeItem)
        {
            var strict = packages.Where(x => !x.Contains("*") && !x.Contains("?"));

            var loose = packages.Where(x => x.Contains("*") || x.Contains("?")).ToArray();
            var looseMatches = this.PackageLookup.GetAllPackages().Where(known => loose.Any(wc => wc.IsWildCardMatch(known, true))).ToArray();

            foreach (var asmDescriptor in strict.Concat(looseMatches))
            {
                var parts = asmDescriptor.Split(new[] {':'}, 2, StringSplitOptions.RemoveEmptyEntries);
                var name = parts.Skip(0).FirstOrDefault();
                var version = parts.Skip(1).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(name) && !hasItem(name))
                {
                    var dllFiles = this.GetPackageLibraries(name, version);

                    takeItem(dllFiles);
                }
            }
        }

        private List<IPackagedDll> FilterBy(
            IEnumerable<IPackagedDll> dlls,
//            FrameworkType frameworkTypeLookup,
            string[] supportedFrameworkVersionLookup)
        {
            foreach (var frameworkVersion in supportedFrameworkVersionLookup ?? new string[0])
            {
                var result = dlls.Where(x => string.Equals(x.FrameworkVersion, frameworkVersion, StringComparison.OrdinalIgnoreCase))
                                 .Where(x => string.Equals(x.RuntimeIdentifier, "ANY", StringComparison.OrdinalIgnoreCase))
//                                                      .GroupBy(x => x.FrameworkVersion)
//                                                      .OrderByDescending(x => x.Key)
                                 .ToList();
                if (result.Count > 0)
                {
                    return result;
                }
            }

            return new List<IPackagedDll>();
        }

        private Assembly GetAssembly(List<IPackagedDll> currentFramework, string fileName, Version maxVersion, string[] systemAssemblies)
        {
            // First Check runtimes

            foreach (var supportedFramework in this.supportedFrameworkVersions)
            {
                var item = this.GetItems(currentFramework, fileName, false, maxVersion, supportedFramework);
                if (item != null)
                {
                    Logger.Log(LogLevel.Debug, $"Loading By Package {{CurrentFramework SpecRid}} ... {item.FullPath}");
                    return this.AssemblyLoadFile(item.FullPath, systemAssemblies.Contains(item.Name, StringComparer.OrdinalIgnoreCase));
                }
            }

            // then check ANY, but with supported framework
//            item = this.GetItems(neutralFramework, fileName, false, maxVersion);
//            if (item != null)
//            {
//                Logger.Log(LogLevel.Debug, $"Loading By Package {{NeutralFramework SpecRid}} ... {item.FullPath}");
//                return AssemblyLoadFile(item.FullPath);
//            }

            foreach (var supportedFramework in this.supportedFrameworkVersions)
            {
                var item = this.GetItems(currentFramework, fileName, true, maxVersion, supportedFramework);
                if (item != null)
                {
                    Logger.Log(LogLevel.Debug, $"Loading By Package {{CurrentFramework AnyRid}} ... {item.FullPath}");
                    return this.AssemblyLoadFile(item.FullPath, systemAssemblies.Contains(item.Name, StringComparer.OrdinalIgnoreCase));
                }
            }

//            item = this.GetItems(neutralFramework, fileName, true, maxVersion);
//            if (item != null)
//            {
//                Logger.Log(LogLevel.Debug, $"Loading By Package {{NeutralFramework AnyRid}} ... {item.FullPath}");
//                return AssemblyLoadFile(item.FullPath);
//            }

            return null;
        }

        private void LoadByName(string[] names, HashSet<string> set, List<Assembly> loaded)
        {
            foreach (var asm in names.Where(x => !x.Contains("*") && !x.Contains("?")))
            {
                if (!set.Contains(asm))
                {
                    var assembly = Assembly.Load(asm);
//                    takeItem(assembly);
                    loaded.Add(assembly);
                    set.Add(assembly.GetName().Name);
                }
            }
        }

        private class PackageEquater : IEqualityComparer<IPackagedDll>
        {
            bool IEqualityComparer<IPackagedDll>.Equals(IPackagedDll x, IPackagedDll y)
                => string.Equals(x?.Name, y?.Name, StringComparison.OrdinalIgnoreCase);

            int IEqualityComparer<IPackagedDll>.GetHashCode(IPackagedDll obj) => (obj?.Name?.GetHashCode()).GetValueOrDefault();
        }
    }
}

//                    Array.ForEach(dlls.ToArray(), x => { });

//                    var maxVersion = dlls.GroupBy(x => x.Version).Max(x => x.Key);

//                            .OrderByDescending(x => x.Key)
//                            .FirstOrDefault()
//                            .Key;

//                    dlls.RemoveAll(x => x.Version != maxVersion);
//                    }
//                    else
//                    {
//                        this.AddItems(dlls, this.GetDlls(name, null, this.FrameworkType));
//
//                        this.AddItems(dlls, this.GetDlls(name, null, this.FrameworkType));
//                        this.AddItems(dlls, this.GetDlls(name, null, FrameworkType.NetStandard));
//
//                        var maxVersion = dlls.GroupBy(x => x.Version).Max(x => x.Key);
////                            .OrderByDescending(x => x.Key)
////                            .FirstOrDefault()
////                            .Key;
//
//                        dlls.RemoveAll(x => x.Version != maxVersion);
////                        loaded.Add(Assembly.Load(name));
//                    }