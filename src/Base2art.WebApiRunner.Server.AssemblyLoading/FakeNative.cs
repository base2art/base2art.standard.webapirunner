namespace Base2art.WebApiRunner.Server.AssemblyLoading
{
    using System;
    using System.IO;
    using Reflection.Discovery;

    public class FakeNative : IPackagedNativeDll
    {
        public FakeNative(FileInfo fileInfo)
        {
            this.Name = fileInfo.Name;
            this.PackageName = "EB2C74CFA9314486B14D76F3E98F80B2";
            this.VersionName = "0.0.0.0";
            this.Version = new Version(0,0,0,0);
            this.RuntimeIdentifier = fileInfo.Directory.Parent.Name;
            this.FullPath = fileInfo.FullName;
        }


        public string Name { get; }
        public string PackageName { get; }
        public string VersionName { get; }
        public Version Version { get; }
        public string RuntimeIdentifier { get; }
        public string FullPath { get; }
    }
}