﻿namespace Base2art.WebApiRunner.Server.AssemblyLoading
{
    public interface IWorkingDirectoryProvider
    {
        string WorkingDirectory { get; }
    }
}