namespace Base2art.WebApiRunner.Server.AssemblyLoading.Models
{
    using System.Collections.Generic;

    public class Deps
    {
        public Dictionary<string, Dictionary<string, TargetAsssembly>> targets { get; set; }
    }
}