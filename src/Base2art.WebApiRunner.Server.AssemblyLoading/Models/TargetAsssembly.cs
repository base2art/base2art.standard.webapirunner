namespace Base2art.WebApiRunner.Server.AssemblyLoading.Models
{
    public class TargetAsssembly
    {
        public object dependencies { get; set; }
        public object runtime { get; set; }
        public object compile { get; set; }
    }
}