﻿namespace Base2art.WebApiRunner.Server.Injection
{
    using System;
    using System.Collections.Generic;
    using ComponentModel.Composition;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class InjectionRegistrar : RegistrationBase
    {
        public override void RegisterSystemServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
            base.RegisterSystemServices(builder, services, config);

            RegisterTypes(services, config);
        }

        public static void RegisterTypes(IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
            var registeredTypes = new HashSet<Type>();

            foreach (var modelBinding in config.Injection ?? new IInjectionItemConfiguration[0])
            {
                // fft must come first
                var fulfillingType = modelBinding.FulfillingType;

                registeredTypes.Add(modelBinding.RequestedType);
                fulfillingType = fulfillingType ?? modelBinding.RequestedType;
                services.Register(modelBinding.RequestedType,
                                  fulfillingType,
                                  ToDictionary(modelBinding.Parameters),
                                  ToDictionary(modelBinding.Properties),
                                  modelBinding.IsSingleton);
            }

            foreach (var modelBinding in config.Injection ?? new IInjectionItemConfiguration[0])
            {
                var fulfillingType = modelBinding.FulfillingType ?? modelBinding.RequestedType;

                if (fulfillingType != modelBinding.RequestedType && !registeredTypes.Contains(fulfillingType))
                {
                    registeredTypes.Add(fulfillingType);
                    services.Register(fulfillingType,
                                      fulfillingType,
                                      ToDictionary(modelBinding.Parameters),
                                      ToDictionary(modelBinding.Properties),
                                      modelBinding.IsSingleton);
                }
            }

            Register(services, config.Tasks, registeredTypes);
            Register(services, config.Endpoints, registeredTypes);
            Register(services, config.HealthChecks.Items, registeredTypes);
        }

        private static void Register<T>(IBindableServiceLoaderInjector services, IReadOnlyList<T> items, HashSet<Type> registeredTypes)
            where T : ICallableMethodConfiguration
        {
            foreach (var modelBinding in items ?? new T[0])
            {
                if (!registeredTypes.Contains(modelBinding.Type))
                {
                    services.Register(modelBinding.Type,
                                      modelBinding.Type,
                                      ToDictionary(modelBinding.Parameters),
                                      ToDictionary(modelBinding.Properties),
                                      false);
                }
            }
        }
    }
}