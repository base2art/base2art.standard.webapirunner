﻿namespace Base2art.WebApiRunner.Server.Cors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Cors.Infrastructure;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class CorsRegistrar : RegistrationBase
    {
        public override void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
            base.RegisterNativeMiddleware(app, config);
            app.UseMiddleware<CorsHandlerMiddleware>();
        }

        public override void RegisterCors(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterCors(builder, services, config);

            builder.Services.TryAdd(ServiceDescriptor.Transient<ICorsService, CorsService>());
            builder.Services.TryAdd(ServiceDescriptor.Transient<ICorsPolicyProvider, CustomCorsPolicyProvider>());

            builder.Services.Configure<CustomCorsOptions>(x =>
            {
                var cors = config.Cors;

                if (cors != null && cors.Count > 0)
                {
                    foreach (var corItem in cors)
                    {
                        var origins = corItem.NormalizedOrigins();

                        x.AddPolicy(origins, corItem, (r, t, policy2) => this.Build(r, origins, t, policy2));
                    }
                }
            });
        }

        private CorsPolicy Build(HttpContext httpContext, string[] origins, CorsPolicyBuilder corsPolicyBuilder, ICorsItemConfiguration policy)
        {
            if (policy.AllowAnyHeader)
            {
                corsPolicyBuilder.AllowAnyHeader();
            }

            if (policy.AllowAnyMethod)
            {
                corsPolicyBuilder.AllowAnyMethod();
            }

            if (policy.AllowAnyOrigin && origins.Length == 1 && origins[0] == "*")
            {
                corsPolicyBuilder.AllowAnyOrigin();
            }

            if (policy.SupportsCredentials)
            {
                corsPolicyBuilder.AllowCredentials();
            }

            if (policy.PreflightMaxAge.HasValue)
            {
                corsPolicyBuilder.SetPreflightMaxAge(policy.PreflightMaxAge.Value);
            }

            corsPolicyBuilder.WithOrigins(origins);
            corsPolicyBuilder.WithMethods(policy.Methods.ToArray());

            var set = new HashSet<string>();
            foreach (var header in policy.Headers)
            {
                if (header.Contains(","))
                {
                    var parts = header.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                                      .Select(x => x.Trim())
                                      .Where(x => !string.IsNullOrWhiteSpace(x))
                                      .ToArray();

                    Array.ForEach(parts, x => set.Add(x));
                }
                else
                {
                    set.Add(header);
                }
            }

            corsPolicyBuilder.WithHeaders(set.OrderBy(x => x).ToArray());

            if (policy.Methods.Count == 0 && !policy.AllowAnyMethod)
            {
                this.AddHeader(httpContext.Request, "Access-Control-Request-Method", x => corsPolicyBuilder.WithMethods(x));
            }

            if (policy.Headers.Count == 0 && !policy.AllowAnyHeader)
            {
                this.AddHeader(httpContext.Request, "Access-Control-Request-Headers", x => corsPolicyBuilder.WithHeaders(x));
            }

            var result = corsPolicyBuilder.Build();

            return result;
        }

        private void AddHeader(HttpRequest request, string header, Action<string[]> coll)
        {
            var headerValue = this.Header(request, header);
            if (headerValue.Length != 0)
            {
                coll(headerValue);
            }
        }

        private string[] Header(HttpRequest request, string header)
        {
            if (!request.Headers.TryGetValue(header, out var originValues))
            {
                return new string[0];
            }

            var origin = string.Join(string.Empty, originValues.ToArray() ?? new string[0]);
            return origin.Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        }
    }
}