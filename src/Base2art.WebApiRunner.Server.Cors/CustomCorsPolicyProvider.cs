﻿namespace Base2art.WebApiRunner.Server.Cors
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Cors.Infrastructure;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;

    public class CustomCorsPolicyProvider : ICorsPolicyProvider
    {
        private readonly CustomCorsOptions options;

        /// <summary>
        ///     Creates a new instance of <see cref="T:Microsoft.AspNetCore.Cors.Infrastructure.DefaultCorsPolicyProvider" />.
        /// </summary>
        /// <param name="options">The options configured for the application.</param>
        public CustomCorsPolicyProvider(IOptions<CustomCorsOptions> options) => this.options = options.Value;

        /// <inheritdoc />
        public Task<CorsPolicy> GetPolicyAsync(HttpContext context, string policyName)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return Task.FromResult(this.options.GetPolicy(context, policyName));
        }
    }
}