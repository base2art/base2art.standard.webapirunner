﻿namespace Base2art.WebApiRunner.Server.Cors
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Cors.Infrastructure;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Primitives;

    public class CorsHandlerMiddleware
    {
        private static readonly HashSet<int> StatusCodes = new HashSet<int>
                                                           {
                                                               (int) HttpStatusCode.NotFound,
                                                               (int) HttpStatusCode.BadRequest,
                                                               (int) HttpStatusCode.MethodNotAllowed
                                                           };

        private readonly ICorsPolicyProvider corsPolicyProvider;
        private readonly ICorsService corsService;
        private readonly RequestDelegate next;

        /// <summary>
        ///     Instantiates a new <see cref="T:Microsoft.AspNetCore.Cors.Infrastructure.CorsMiddleware" />.
        /// </summary>
        /// <param name="next">The next middleware in the pipeline.</param>
        /// <param name="corsService">An instance of <see cref="T:Microsoft.AspNetCore.Cors.Infrastructure.ICorsService" />.</param>
        public CorsHandlerMiddleware(RequestDelegate next, ICorsService corsService, ICorsPolicyProvider corsPolicyProvider) //, CorsPolicy policy)
        {
            if (next == null)
            {
                throw new ArgumentNullException(nameof(next));
            }

            if (corsService == null)
            {
                throw new ArgumentNullException(nameof(corsService));
            }

            this.next = next;
            this.corsService = corsService;
            this.corsPolicyProvider = corsPolicyProvider;
        }

        /// <inheritdoc />
        public async Task Invoke(HttpContext context)
        {
//            var corsMiddleware = this;
//            if (context.Request.Headers.ContainsKey(CorsConstants.Origin))
//            {
//                var corsPolicy = await (this.corsPolicyProvider?.GetPolicyAsync(context, null) ?? Task.FromResult<CorsPolicy>(null));
//
//                var policy1 = corsPolicy;
//                if (policy1 != null)
//                {
//                    var policy2 = this.corsService.EvaluatePolicy(context, policy1);
//                    this.corsService.ApplyResult(policy2, context.Response);
//                    var header = context.Request.Headers[CorsConstants.AccessControlRequestMethod];
//                    if (string.Equals(context.Request.Method, CorsConstants.PreflightHttpMethod, StringComparison.OrdinalIgnoreCase)
//                        && !StringValues.IsNullOrEmpty(header))
//                    {
//                        context.Response.StatusCode = 204;
//                        return;
//                    }
//                }
//            }

            await this.next(context);

            var request = context.Request;
            var response = context.Response;
            if (request.Method == HttpMethod.Options.Method)
            {
                if (StatusCodes.Contains(response.StatusCode))
                {
                    var newResponse = request.CreateResponse(HttpStatusCode.OK, new { });

                    Func<string, bool> hasHeader = request.Headers.ContainsKey;

                    if (hasHeader("Origin"))
                    {
                        var origin = request.Headers["Origin"];

                        var task = this.corsPolicyProvider?.GetPolicyAsync(context, origin) ?? Task.FromResult<CorsPolicy>(null);

                        var corsPolicy = await task;

                        if (corsPolicy != null)
                        {
                            var policy2 = this.corsService.EvaluatePolicy(context, corsPolicy);
                            this.corsService.ApplyResult(policy2, context.Response);
                            var header = context.Request.Headers[CorsConstants.AccessControlRequestMethod];
                            if (string.Equals(context.Request.Method, CorsConstants.PreflightHttpMethod, StringComparison.OrdinalIgnoreCase)
                                && !StringValues.IsNullOrEmpty(header))
                            {
                                context.Response.StatusCode = 204;
                            }
                        }
                    }
                }
            }
        }
    }
}