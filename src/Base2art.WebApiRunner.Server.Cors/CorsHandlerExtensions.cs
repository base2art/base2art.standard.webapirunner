﻿namespace Base2art.WebApiRunner.Server.Cors
{
    using System;
    using System.Linq;
    using Web.App.Configuration;

    public static class CorsHandlerExtensions
    {
        public static string[] NormalizedOrigins(this ICorsItemConfiguration corItem)
        {
            return corItem.Origins.SelectMany(Map).Select(x => x.TrimEnd('/')).ToArray();
        }

        public static string[] Hosts(this ICorsItemConfiguration corItem) => corItem.Origins.Select(MapSingle).ToArray();

        private static string MapSingle(string input)
        {
            if (Uri.TryCreate(input, UriKind.Absolute, out var str))
            {
                return str.Host;
            }

            return input;
        }

        private static string[] Map(string input)
        {
            if (Uri.TryCreate(input, UriKind.Absolute, out var str))
            {
                return new[] {input};
            }

            return new[]
                   {
                       CreateUrl(Uri.UriSchemeHttp, input),
                       CreateUrl(Uri.UriSchemeHttps, input)
                   };
        }

        private static string CreateUrl(string schema, string host)
        {
            if (host == "*")
            {
                return "*";
            }

            host = host.Replace("*", "1478bc94b73f44efabedf281f281f3e9");
            var builder = new UriBuilder();
            builder.Scheme = schema;
            builder.Host = host;

            return builder.Uri.ToString().Replace("1478bc94b73f44efabedf281f281f3e9", "*");
        }
    }
}