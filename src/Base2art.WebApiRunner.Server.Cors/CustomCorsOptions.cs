﻿namespace Base2art.WebApiRunner.Server.Cors
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Cors.Infrastructure;
    using Microsoft.AspNetCore.Http;
    using Web.App.Configuration;

    public class CustomCorsOptions
    {
        private readonly IDictionary<string, ICorsItemConfiguration> basePolicyMap
            = new Dictionary<string, ICorsItemConfiguration>(StringComparer.OrdinalIgnoreCase);

        private readonly IDictionary<string, Func<HttpContext, CorsPolicyBuilder, ICorsItemConfiguration, CorsPolicy>> policyMap
            = new Dictionary<string, Func<HttpContext, CorsPolicyBuilder, ICorsItemConfiguration, CorsPolicy>>(StringComparer.OrdinalIgnoreCase);

        /// <summary>Adds a new policy.</summary>
        /// <param name="origins">The name of the policy.</param>
        /// <param name="policy">The <see cref="T:Microsoft.AspNetCore.Cors.Infrastructure.CorsPolicy" /> policy to be added.</param>
        public void AddPolicy(string[] origins, ICorsItemConfiguration basePolicy,
                              Func<HttpContext, CorsPolicyBuilder, ICorsItemConfiguration, CorsPolicy> policy)
        {
            if (origins == null || origins.Length == 0)
            {
                throw new ArgumentNullException(nameof(origins));
            }

            if (policy == null)
            {
                throw new ArgumentNullException(nameof(policy));
            }

            foreach (var domain in origins)
            {
                this.policyMap[domain] = policy;
                this.basePolicyMap[domain] = basePolicy;
            }
        }

        /// <summary>
        ///     Gets the policy based on the <paramref name="domain" />
        /// </summary>
        /// <param name="domain">The name of the policy to lookup.</param>
        /// <returns>
        ///     The <see cref="T:Microsoft.AspNetCore.Cors.Infrastructure.CorsPolicy" /> if the policy was added.<c>null</c>
        ///     otherwise.
        /// </returns>
        public CorsPolicy GetPolicy(HttpContext context, string domain)
        {
            if (domain == null)
            {
                throw new ArgumentNullException(nameof(domain));
            }

            if (this.policyMap.ContainsKey(domain))
            {
                return this.policyMap[domain](context, new CorsPolicyBuilder(), this.basePolicyMap[domain]);
            }

            if (this.policyMap.ContainsKey("*"))
            {
                return this.policyMap["*"](context, new CorsPolicyBuilder(), this.basePolicyMap["*"]);
            }

            return null;
        }
    }
}