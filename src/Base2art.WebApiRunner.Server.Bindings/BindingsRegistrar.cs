﻿namespace Base2art.WebApiRunner.Server.Bindings
{
    using System;
    using System.Reflection;
    using ComponentModel.Composition;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;
    using Web.Bindings;
    using Web.Server.Registration;

    public class BindingsRegistrar : RegistrationBase
    {
        public override void RegisterSystemServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
            base.RegisterSystemServices(builder, services, config);

            foreach (var modelBinding in config?.Application?.ModelBindings ?? new IModelBindingConfiguration[0])
            {
                services.Register(modelBinding.BinderType,
                                  modelBinding.BinderType,
                                  ToDictionary(modelBinding.BinderParameters),
                                  ToDictionary(modelBinding.BinderProperties),
                                  false);
            }
        }

        public override void RegisterBindings(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterBindings(builder, services, config);

            builder.AddMvcOptions(x =>
            {
                IBoundTypeRegistrar registrar = services.GetService<IBoundTypeRegistrar>();
                x.ModelBinderProviders.Insert(0, new HttpContextModelBinderProvider(registrar));

                foreach (var modelBinding in config?.Application?.ModelBindings ?? new IModelBindingConfiguration[0])
                {
                    var modelBindingBinderType = modelBinding.BinderType;

                    if (modelBindingBinderType.GetTypeInfo().IsAssignableTo(typeof(IModelBinding)))
                    {
                        var wrappingEntityBinderProvider = new WrappingRequestedEntityBinderProvider(services,
                                                                                                     modelBindingBinderType,
                                                                                                     modelBinding.BoundType);
                        x.ModelBinderProviders.Insert(0, wrappingEntityBinderProvider);
                    }
                    else if (modelBindingBinderType.GetTypeInfo().IsAssignableTo(typeof(IResponseEditingHttpModelBinding)))
                    {
                        var wrappingEntityBinderProvider = new WrappingHttpFullEntityBinderProvider(services,
                                                                                                    modelBindingBinderType,
                                                                                                    modelBinding.BoundType);
                        x.ModelBinderProviders.Insert(0, wrappingEntityBinderProvider);
                    }
                    else if (modelBindingBinderType.GetTypeInfo().IsAssignableTo(typeof(IHttpModelBinding)))
                    {
                        var wrappingEntityBinderProvider = new WrappingHttpRequestedEntityBinderProvider(services,
                                                                                                         modelBindingBinderType,
                                                                                                         modelBinding.BoundType);
                        x.ModelBinderProviders.Insert(0, wrappingEntityBinderProvider);
                    }
                    else
                    {
                        var wrappingEntityBinderProvider = new WrappingEntityBinderProvider(services,
                                                                                            modelBindingBinderType,
                                                                                            modelBinding.BoundType);
                        x.ModelBinderProviders.Insert(0, wrappingEntityBinderProvider);
                    }
                }
            });
        }
    }
}