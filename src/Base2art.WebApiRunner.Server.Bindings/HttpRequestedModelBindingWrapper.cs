﻿namespace Base2art.WebApiRunner.Server.Bindings
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Web.Bindings;

    public class HttpRequestedModelBindingWrapper : IModelBinder
    {
        private readonly IHttpModelBinding service;

        public HttpRequestedModelBindingWrapper(IHttpModelBinding service) => this.service = service;

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request();
            var item = await this.service.BindModelAsync(request);

            if (item != null)
            {
                bindingContext.Result = ModelBindingResult.Success(item);
            }
        }
    }
}