namespace Base2art.WebApiRunner.Server.Bindings
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Web.Bindings;

    public class HttpFullModelBindingWrapper : IModelBinder
    {
        private readonly IResponseEditingHttpModelBinding service;

        public HttpFullModelBindingWrapper(IResponseEditingHttpModelBinding service) => this.service = service;

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request();
            var response = bindingContext.HttpContext.Response();
            var item = await this.service.BindModelAsync(request, response);

            if (item != null)
            {
                bindingContext.Result = ModelBindingResult.Success(item);
            }
        }
    }
}