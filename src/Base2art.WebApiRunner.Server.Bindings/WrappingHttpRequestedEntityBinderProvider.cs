﻿namespace Base2art.WebApiRunner.Server.Bindings
{
    using System;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Web.Bindings;

    public class WrappingHttpRequestedEntityBinderProvider : IModelBinderProvider
    {
        private readonly Type binderType;
        private readonly Type modelType;
        private readonly IServiceProvider provider;

        public WrappingHttpRequestedEntityBinderProvider(IServiceProvider provider, Type binderType, Type modelType)
        {
            this.provider = provider;
            this.binderType = binderType;
            this.modelType = modelType;
        }

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == this.modelType)
            {
                return new HttpRequestedModelBindingWrapper((IHttpModelBinding) this.provider.GetService(this.binderType));
            }

            return null;
        }
    }
}