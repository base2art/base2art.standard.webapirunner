namespace Base2art.WebApiRunner.Server.Bindings
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Web.Http;

    public class HttpContextModelBinderProvider : IModelBinderProvider
    {
        public HttpContextModelBinderProvider(IBoundTypeRegistrar registrar)
        {
            if (registrar != null)
            {
                registrar.AddType<IHttpRequest>(BindingSource.Special);
                registrar.AddType<IHttpResponse>(BindingSource.Special);
                registrar.AddType<IHttpResponseCookies>(BindingSource.Special);
            }
        }

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(IHttpRequest))
            {
                return new RequestModelBinder();
            }

            if (context.Metadata.ModelType == typeof(IHttpResponse))
            {
                return new ResponseModelBinder();
            }

            if (context.Metadata.ModelType == typeof(IHttpResponseCookies))
            {
                return new ResponseCookiesModelBinder();
            }

            return null;
        }

        private class RequestModelBinder : IModelBinder
        {
            public Task BindModelAsync(ModelBindingContext bindingContext)
            {
                bindingContext.Result = ModelBindingResult.Success(bindingContext.HttpContext.Request());
                bindingContext.BindingSource = BindingSource.Special;
                return Task.CompletedTask;
            }
        }

        private class ResponseModelBinder : IModelBinder
        {
            public Task BindModelAsync(ModelBindingContext bindingContext)
            {
                bindingContext.Result = ModelBindingResult.Success(bindingContext.HttpContext.Response());
                bindingContext.BindingSource = BindingSource.Special;
                return Task.CompletedTask;
            }
        }

        private class ResponseCookiesModelBinder : IModelBinder
        {
            public Task BindModelAsync(ModelBindingContext bindingContext)
            {
                bindingContext.Result = ModelBindingResult.Success(bindingContext.HttpContext.Response().Cookies);
                bindingContext.BindingSource = BindingSource.Special;
                return Task.CompletedTask;
            }
        }
    }
}