﻿namespace Base2art.WebApiRunner.Server.Bindings
{
    using System;
    using System.Reflection;

    internal static class AspectManagers
    {
        public static bool IsAssignableTo(this Type type, Type methodReturnType) =>
            type == methodReturnType || methodReturnType.GetTypeInfo().IsAssignableFrom(type.GetTypeInfo());
    }
}