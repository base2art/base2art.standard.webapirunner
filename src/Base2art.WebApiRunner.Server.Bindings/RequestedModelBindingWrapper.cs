﻿namespace Base2art.WebApiRunner.Server.Bindings
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Web.Bindings;

    public class RequestedModelBindingWrapper : IModelBinder
    {
        private readonly IModelBinding service;

        public RequestedModelBindingWrapper(IModelBinding service) => this.service = service;

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request();
            var item = await this.service.BindModelAsync(
                                                         request.Uri,
                                                         request.Cookies,
                                                         request.Headers);

            if (item != null)
            {
                bindingContext.Result = ModelBindingResult.Success(item);
            }
        }
    }
}