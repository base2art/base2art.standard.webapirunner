namespace Base2art.WebApiRunner.Server.Runners.RouteGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;

    public static class Types
    {
        private static readonly Dictionary<string, string> httpMethods = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        static Types()
        {
            void addItem(Dictionary<string, string> methods, string method)
            {
                methods[method] = method;
            }

            addItem(httpMethods, nameof(HttpMethod.Delete));
            addItem(httpMethods, nameof(HttpMethod.Get));
            addItem(httpMethods, nameof(HttpMethod.Head));
            addItem(httpMethods, nameof(HttpMethod.Options));
//            addItem(httpMethods, nameof(HttpMethod.Patch));
            addItem(httpMethods, nameof(HttpMethod.Post));
            addItem(httpMethods, nameof(HttpMethod.Put));
            addItem(httpMethods, nameof(HttpMethod.Trace));
        }

        public static IReadOnlyDictionary<string, string> HttpMethods => httpMethods;

        public static string GetFriendlyName(this Type type)
        {
            if (type == typeof(int))
            {
                return "int";
            }

            if (type == typeof(short))
            {
                return "short";
            }

            if (type == typeof(byte))
            {
                return "byte";
            }

            if (type == typeof(bool))
            {
                return "bool";
            }

            if (type == typeof(long))
            {
                return "long";
            }

            if (type == typeof(float))
            {
                return "float";
            }

            if (type == typeof(double))
            {
                return "double";
            }

            if (type == typeof(decimal))
            {
                return "decimal";
            }

            if (type == typeof(string))
            {
                return "string";
            }

            if (type.IsGenericType)
            {
                var name = type.Name.Split('`')[0];
                return $"{type.Namespace}.{name}<{string.Join(", ", type.GetGenericArguments().Select(GetFriendlyName).ToArray())}>";
            }

            return type.FullName;
        }
    }
}