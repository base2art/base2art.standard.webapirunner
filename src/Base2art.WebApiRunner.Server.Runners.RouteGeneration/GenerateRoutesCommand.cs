namespace Base2art.WebApiRunner.Server.Runners.RouteGeneration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using CodeGen;
    using Commands;
    using Configuration;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.AspNetCore.Mvc.Controllers;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using Microsoft.Extensions.DependencyInjection;
    using Server.Configuration.CommandLine;
    using Web.App.Configuration;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
    using TypeInfo = System.Reflection.TypeInfo;

    public class GenerateRoutesCommand : ICommand
    {
        private readonly ConsoleServerConfiguration configPrimitive;
        private readonly IRegistrationHolder registrations;
        private readonly string routerNamespace;
        private readonly IFrameworkShim shim;

        public GenerateRoutesCommand(
            ConsoleServerConfiguration configPrimitive,
            IRegistrationHolder registrations,
            string routerNamespace,
            IFrameworkShim shim)
        {
            this.configPrimitive = configPrimitive;
            this.registrations = registrations;
            this.routerNamespace = routerNamespace;
            this.shim = shim;
        }

        public Task<int> Run(IServerConfiguration configuration)
        {
            var (publicUrlArray, adminUrlArray) = this.configPrimitive.GetUrls();
            if (adminUrlArray?.Count == 0)
            {
                return ServerCommands.RunSingleSite(
                                                    new NullApplication(),
                                                    configuration,
                                                    () => publicUrlArray.ToArray(),
                                                    (builder, y, z) => builder,
                                                    (host, serverConfiguration) => this.Start(host, serverConfiguration),
                                                    this.registrations,
                                                    this.shim);
            }

            return ServerCommands.RunMultiSite(new NullApplication(),
                                               configuration,
                                               () => publicUrlArray.ToArray(),
                                               () => adminUrlArray.ToArray(),
                                               (builder, y, zz) => builder,
                                               (x, y, type) => this.Start(x, y, type),
                                               this.registrations,
                                               this.shim);
        }

        public Task Shutdown() => Task.CompletedTask;

        private async Task<int> Start(IWebHost webHost, IServerConfiguration config)
        {
            await this.PrintMethods(webHost, config, webHost.RunAsync);

            return 0;
        }

        private async Task<int> Start(IWebHost webHost, IServerConfiguration config, Type startup)
        {
            await this.PrintMethods(webHost, config, webHost.RunAsync);

            return 0;
        }

        private async Task PrintMethods(IWebHost webHost, IServerConfiguration config, Func<CancellationToken, Task> tokenRunner)
        {
            var renderer = new Renderer(this.routerNamespace);
            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                var bound = new HashSet<Type>(config.Application.ModelBindings
                                                    .Where(x => x.BindingType == BindingType.Hidden)
                                                    .Select(x => x.BoundType)
                                                    .ToArray());

                var runner = tokenRunner(cancellationTokenSource.Token);

                var descriptionProvdier = webHost.Services.GetService<IApiDescriptionGroupCollectionProvider>();
                foreach (var group in descriptionProvdier.ApiDescriptionGroups.Items)
                {
                    foreach (var apiDescription in group.Items)
                    {
                        var controller = apiDescription.ActionDescriptor as ControllerActionDescriptor;
                        if (controller != null)
                        {
                            var path = "/" + apiDescription.RelativePath;
                            var controllr = controller.ControllerTypeInfo;
                            var meth = controller.MethodInfo;

                            var methParms = meth.GetParameters()
                                                .Where(x => !x.ParameterType.IsAbstract && !x.ParameterType.IsInterface)
                                                .Select(x => (
                                                                 parm: x,
                                                                 desc: apiDescription
                                                                       .ActionDescriptor.Parameters.FirstOrDefault(y => y.Name == x.Name)))
                                                .ToArray();

                            methParms = methParms.Where(y => !bound.Contains(y.parm.ParameterType))
                                                 .Where(y => y.desc?.BindingInfo?.BindingSource != BindingSource.Body)
                                                 .ToArray();

                            renderer.AddRoute(apiDescription.HttpMethod, path, controllr, meth, methParms.Select(x => x.parm).ToArray());
                        }
                    }
                }

                cancellationTokenSource.Cancel();
                await runner;

                var outpath = Path.Combine(Environment.CurrentDirectory, "__ROUTER__.cs");

                File.WriteAllText(outpath, renderer.AsClass().ToString());
            }
        }

        private static List<MemberDeclarationSyntax> Members(List<(string verb, string route, TypeInfo, MethodInfo, ParameterInfo[])> endpoints)
        {
            var groups = endpoints.GroupBy(x => x.Item3.Name);
            return groups.Select(x => Factories.StaticGetProperty(x.Key, x.Key + "Builder",
                                                                  Factories.Return(Factories.NewObject(x.Key + "Builder"))))
                         .Concat(groups.Select(x => Factories.PublicClassDeclaration(
                                                                                     x.Key + "Builder",
                                                                                     x.Select(CreateMethodHolder)
                                                                                    )))
                         .Concat(groups.SelectMany(x => x.Select(y => (x.Key, y)))
                                       .Select(x => Factories.PublicClassDeclaration(
                                                                                     $"{x.Key}_{x.y.Item4.Name}Builder",
                                                                                     new[]
                                                                                     {
//                                                                                         CreateCtor(x.y),
                                                                                         CreateRouteMethod(x.y),
                                                                                         CreateMethod(x.y)
                                                                                     })))
                         .ToList();

            // 
        }

//        private static MemberDeclarationSyntax CreateCtor((string verb, string route, TypeInfo, MethodInfo, ParameterInfo[]) x)
//        {
//            return ConstructorDeclaration(Identifier($"{x.Item3.Name}_{x.Item4.Name}Builder"))
//                   .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword)))
//                   .WithParameterList(Factories.Parameters(x.Item5.Select(y => (y.ParameterType.GetFriendlyName(), y.Name)).ToArray()))
//                   .WithBody(
//                             Block(
//                                   x.Item5.Select(y =>
//                                                      ExpressionStatement(
//                                                                          AssignmentExpression(
//                                                                                               SyntaxKind.SimpleAssignmentExpression,
//                                                                                               MemberAccessExpression(
//                                                                                                                      SyntaxKind
//                                                                                                                          .SimpleMemberAccessExpression,
//                                                                                                                      ThisExpression(),
//                                                                                                                      IdentifierName(y.Name)),
//                                                                                               IdentifierName(y.Name))))));
//        }

        private static MemberDeclarationSyntax CreateMethodHolder(
            (string verb, string route, TypeInfo Type, MethodInfo Method, ParameterInfo[]) methodConfiguration)
        {
            var ret = $"{methodConfiguration.Type.Name}_{methodConfiguration.Item4.Name}Builder";

            return Factories.GetProperty(
                                         Factories.TypeOf(ret),
                                         methodConfiguration.Method.Name,
                                         Factories.BlockItems(Factories.Return(Factories.NewObject(ret))));
        }

        private static MemberDeclarationSyntax CreateMethod(
            (string verb, string route, TypeInfo Type, MethodInfo Method, ParameterInfo[]) methodConfiguration)
        {
            var body1 = Factories.LocalDeclaration("url", methodConfiguration.route);

            var variables = GetVariableMap(methodConfiguration);
            var bodyLines = variables.Select(variable =>
            {
                var assign = Factories.Assign(
                                              "url",
                                              Factories.StaticMethodCall(
                                                                         MethodName(variable.type),
                                                                         Factories.VariableArg("url"),
                                                                         Factories.LiteralValueArg(variable.name),
                                                                         Factories.VariableArg(variable.lookup)));
                if (!variable.hasDefault)
                {
                    return assign;
                }

                return IfStatement(
                                   BinaryExpression(
                                                    SyntaxKind.NotEqualsExpression,
                                                    IdentifierName(variable.name),
                                                    Factories.LiteralOf(variable.type, variable.defaultValue)),
                                   Block(assign));
            });

            var body4 = Factories.Return(Factories.Variable("url"));

            return Factories.Method(
                                    Factories.TypeOf("string"),
                                    "Link",
                                    Factories.Parameters(methodConfiguration
                                                         .Item5.Select(y => (y.ParameterType, y.Name, y.HasDefaultValue, y.DefaultValue)).ToArray()),
//                                    Factories.Parameters(methodConfiguration
//                                                         .Item5.Select(y => (y.ParameterType.GetFriendlyName(), y.Name)).ToArray()),
//                                    Factories.Parameters(methodConfiguration
//                                                         .Item5.Select(x => (x.ParameterType.GetFriendlyName(), x.Name)).ToArray()),
                                    Factories.BlockItems(new[] {body1}.Concat(bodyLines).Concat(new[] {body4}).ToArray()));
        }

        private static MemberDeclarationSyntax CreateRouteMethod(
            (string verb, string route, TypeInfo Type, MethodInfo, ParameterInfo[]) methodConfiguration)
        {
            var body1 = Factories.LocalDeclaration("url", methodConfiguration.route);

            var variables = GetVariableMap(methodConfiguration);

            var bodyLines = variables.Select(variable =>
            {
                var assign = Factories.Assign(
                                              "url",
                                              Factories.StaticMethodCall(
                                                                         MethodName(variable.type),
                                                                         Factories.VariableArg("url"),
                                                                         Factories.LiteralValueArg(variable.name),
                                                                         Factories.VariableArg(variable.lookup)));
                if (!variable.hasDefault)
                {
                    return assign;
                }

                return IfStatement(
                                   BinaryExpression(
                                                    SyntaxKind.NotEqualsExpression,
                                                    IdentifierName(variable.name),
                                                    Factories.LiteralOf(variable.type, variable.defaultValue)),
                                   Block(assign));
            });

            ArgumentSyntax lookup;
            if (Types.HttpMethods.ContainsKey(methodConfiguration.verb))
            {
                lookup = Argument(Factories.MemberAccess("System.Net.Http.HttpMethod", Types.HttpMethods[methodConfiguration.verb]));
            }
            else
            {
                lookup = Argument(Factories.NewObject(
                                                      "System.Net.Http.HttpMethod",
                                                      Factories.LiteralValueArg(methodConfiguration.verb.ToUpperInvariant())));
            }

            var body4 = Factories.Return(Factories.TupleValue(lookup, Factories.VariableArg("url")));

            return Factories.Method(
                                    Factories.Tuple((typeof(HttpMethod).FullName, "method"), ("string", "route")),
                                    "RouteInfo",
                                    Factories.Parameters(methodConfiguration
                                                         .Item5.Select(y => (y.ParameterType, y.Name, y.HasDefaultValue, y.DefaultValue)).ToArray()),
//                                    Factories.Parameters(variables.Select(x => (x.type.GetFriendlyName(), x.name)).ToArray()),
                                    Factories.BlockItems(new[] {body1}.Concat(bodyLines).Concat(new[] {body4}).ToArray()));
        }

        private static (string name, string lookup, Type type, bool hasDefault, object defaultValue)[]
            GetVariableMap((string verb, string route, TypeInfo Type, MethodInfo, ParameterInfo[]) methodConfiguration)
        {
            (string name, string lookup, Type type, bool hasDefault, object defaultValue)[] ExplodeVariable(
                (string name, string lookup, Type type, bool hasDefault, object defaultValue) valueTuple)
            {
                if (MethodName(valueTuple.type) == "ReplaceInUrlObject")
                {
                    var properties = valueTuple.type.GetProperties();

                    return properties.Select(x => (
                                                      $"{valueTuple.name}.{x.Name}",
                                                      $"{valueTuple.name}?.{x.Name}",
                                                      x.PropertyType,
                                                      false,
                                                      x.PropertyType.IsValueType ? Activator.CreateInstance(x.PropertyType) : null))
                                     .ToArray();
                }

                return new[] {valueTuple};
            }

            var count = -1;
            var variables
                = methodConfiguration.Item5.Select(x => (
                                                            name: x.Name,
                                                            lookup: x.Name,
                                                            type: x.ParameterType,
                                                            hasDefault: x.HasDefaultValue,
                                                            defaultValue: x.DefaultValue)).ToArray();

            while (count != variables.Length)
            {
                count = variables.Length;
                variables = variables.SelectMany(x => ExplodeVariable(x)).ToArray();
            }

            return variables;
        }

        private static string MethodName(Type variableType)
        {
            if (variableType.IsEnum)
            {
                return "ReplaceInUrlEnum";
            }

            if (variableType.IsValueType)
            {
                return "ReplaceInUrlStruct";
            }

            if (variableType == typeof(string))
            {
                return "ReplaceInUrlString";
            }

            return "ReplaceInUrlObject";
        }

        public static (string literal, string variable)[] ExtractUrlVariables(TypeInfo type, string url)
        {
            var matches = Regex.Matches(url, @"\{\s*([A-Za-z0-9]*)\s*\}");
            return matches.OfType<Match>().Select(x => (x.Groups[0].Value, x.Groups[1].Value)).ToArray();
        }

        private class Renderer
        {
            private readonly Dictionary<string, (string verb, string route, TypeInfo, MethodInfo, ParameterInfo[])> items =
                new Dictionary<string, (string verb, string route, TypeInfo, MethodInfo, ParameterInfo[])>();

            private readonly string routerNamespace;

            public Renderer(string routerNamespace) => this.routerNamespace = routerNamespace;

            public void AddRoute(string verb, string path, TypeInfo controller, MethodInfo meth, ParameterInfo[] parameterInfos)
            {
                var lookup = $"{controller.FullName}::{meth.Name}";
                this.items[lookup] = (verb, path, controller, meth, parameterInfos);
            }

            public CompilationUnitSyntax AsClass()
            {
                var attr = Factories.AttributeValue(
                                                    "System.CodeDom.Compiler.GeneratedCode",
                                                    AttributeArgument(Factories.LiteralValue("SjY")),
                                                    AttributeArgument(Factories.LiteralValue("1.0.0.0")));

                var filteredMembers = this.items.Values.ToList();

                var klass = Factories.StaticClassDeclaration(
                                                             "Router",
                                                             Members(filteredMembers).Concat(new[]
                                                                                             {
                                                                                                 IncludedMethodFunc.Value,
                                                                                                 new IncludedMethodString().Value(),
                                                                                                 new IncludedMethodStruct().Value(),
                                                                                                 new IncludedMethodEnum().Value(),
                                                                                                 new IncludedMethodObject().Value()
                                                                                             }).ToList())
                                     .WithAttributeLists(new SyntaxList<AttributeListSyntax>(AttributeList(SingletonSeparatedList(attr))));

                var ns = Factories.NameSpace(this.routerNamespace, klass);

                var units = CompilationUnit()
                            .WithMembers(new SyntaxList<MemberDeclarationSyntax>(ns))
                            .NormalizeWhitespace();
                return units;
            }
        }
    }
}