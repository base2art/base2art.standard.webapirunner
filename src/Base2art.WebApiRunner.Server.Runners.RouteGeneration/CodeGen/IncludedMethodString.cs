namespace Base2art.WebApiRunner.Server.Runners.RouteGeneration.CodeGen
{
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class IncludedMethodString : IncludedMethodBase
    {
        protected override string Name => "ReplaceInUrlString";
        protected override TypeSyntax VariableType => Factories.StringType;

        protected override ExpressionSyntax ConditionalExpr() => IdentifierName("val1");
    }
}