namespace Base2art.WebApiRunner.Server.Runners.RouteGeneration.CodeGen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class Factories
    {
        public static TypeSyntax StringType => PredefinedType(Token(SyntaxKind.StringKeyword));

        public static MemberDeclarationSyntax StaticClassDeclaration(string name,
                                                                     List<MemberDeclarationSyntax> members,
                                                                     bool isPartial = false)
        {
            var tokens = new List<SyntaxToken> {Token(SyntaxKind.PublicKeyword), Token(SyntaxKind.StaticKeyword)};
            if (isPartial)
            {
                tokens.Add(Token(SyntaxKind.PartialKeyword));
            }

            return ClassDeclaration(name)
                   .WithModifiers(TokenList(tokens.ToArray()))
                   .WithMembers(List(members));
        }

        public static MemberDeclarationSyntax PublicClassDeclaration(
            string name,
            IEnumerable<MemberDeclarationSyntax> members,
            bool isPartial = false)
        {
            var tokens = new List<SyntaxToken> {Token(SyntaxKind.PublicKeyword)};
            if (isPartial)
            {
                tokens.Add(Token(SyntaxKind.PartialKeyword));
            }

            return ClassDeclaration(name)
                   .WithModifiers(TokenList(tokens.ToArray()))
                   .WithMembers(List(members));
        }

        public static SyntaxList<MemberDeclarationSyntax> NameSpace(string @namespace, SyntaxList<MemberDeclarationSyntax> klass) =>
            SingletonList<MemberDeclarationSyntax>(
                                                   NamespaceDeclaration(IdentifierName(@namespace))
                                                       .WithMembers(klass));

        public static NamespaceDeclarationSyntax NameSpace(string @namespace, MemberDeclarationSyntax klass) =>
            NamespaceDeclaration(IdentifierName(@namespace)).WithMembers(SingletonList(klass));

        public static MethodDeclarationSyntax StaticMethod(string name, string returnType, SyntaxList<StatementSyntax> body) =>
            MethodDeclaration(IdentifierName(returnType), Identifier(name))
                .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword), Token(SyntaxKind.StaticKeyword)))
                .WithBody(Block(body));

        public static MethodDeclarationSyntax StaticMethod(string name, string returnType, StatementSyntax body) =>
            StaticMethod(name, returnType, SingletonList(body));

        public static PropertyDeclarationSyntax StaticGetProperty(string name, string returnType, SyntaxList<StatementSyntax> body) =>
            PropertyDeclaration(IdentifierName(returnType), Identifier(name))
                .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword), Token(SyntaxKind.StaticKeyword)))
                .WithAccessorList(AccessorList(
                                               SingletonList(AccessorDeclaration(SyntaxKind.GetAccessorDeclaration)
                                                                 .WithBody(Block(body)))));

        public static PropertyDeclarationSyntax StaticGetProperty(string name, string returnType, StatementSyntax body) =>
            StaticGetProperty(name, returnType, SingletonList(body));

        public static ObjectCreationExpressionSyntax NewObject(string type, params ArgumentSyntax[] args) =>
            ObjectCreationExpression(IdentifierName(type))
                .WithArgumentList(ArgumentList(SeparatedList(args)));

        public static ObjectCreationExpressionSyntax NewObject(string type) =>
            ObjectCreationExpression(IdentifierName(type))
                .WithArgumentList(ArgumentList());

        public static TypeSyntax Tuple(params (SyntaxKind type, string name)[] types)
        {
            var parms = types.Select(x => TupleElement(PredefinedType(Token(x.type)))
                                         .WithIdentifier(Identifier(x.name)));
            var itms = CommaSeparated(parms.ToArray());
            return TupleType(SeparatedList<TupleElementSyntax>(itms));
        }

        public static TypeSyntax Tuple(params (string type, string name)[] types)
        {
            var parms = types.Select(x => TupleElement(IdentifierName(x.type))
                                         .WithIdentifier(Identifier(x.name)));
            var itms = CommaSeparated(parms.ToArray());
            return TupleType(SeparatedList<TupleElementSyntax>(itms));
        }

        public static ParameterListSyntax Parameters(params (string type, string name)[] types)
        {
            var parms = types.Select(x => Parameter(Identifier(x.name)).WithType(IdentifierName(x.type)));
            var itms = CommaSeparated(parms.ToArray());
            return ParameterList(SeparatedList<ParameterSyntax>(itms));
        }

        public static ParameterListSyntax Parameters(params (Type type, string name, bool hasDefault, object defaultValue)[] types)
        {
            var parms = types.Select(x =>
                                         x.hasDefault
                                             ? Parameter(Identifier(x.name)).WithType(IdentifierName(x.type.GetFriendlyName()))
                                                                            .WithDefault(EqualsValueClause(LiteralOf(x.type, x.defaultValue)))
                                             : Parameter(Identifier(x.name)).WithType(IdentifierName(x.type.GetFriendlyName())));
            var itms = CommaSeparated(parms.ToArray());
            return ParameterList(SeparatedList<ParameterSyntax>(itms));
        }

        public static LiteralExpressionSyntax LiteralOf(Type type, object x)
        {
            if (x == null)
            {
                return LiteralExpression(SyntaxKind.NullLiteralExpression);
            }

            if (type == typeof(string))
            {
                return LiteralExpression(
                                         SyntaxKind.StringLiteralExpression,
                                         Literal(x.ToString()));
            }

            switch (x)
            {
                case byte s1:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s1));
                case sbyte s2:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s2));
                case ushort s3:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s3));
                case uint s4:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s4));
                case ulong s5:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s5));
                case short s6:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s6));
                case int s7:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s7));
                case long s8:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s8));
                case decimal s9:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s9));
                case double s10:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s10));
                case float s11:
                    return LiteralExpression(SyntaxKind.NumericLiteralExpression, Literal(s11));
            }

            return LiteralExpression(
                                     SyntaxKind.NumericLiteralExpression,
                                     Literal(x.ToString()));
        }

        public static ArgumentListSyntax Arguments(params string[] names)
        {
            var parms = names.Select(x => Argument(LiteralExpression(SyntaxKind.StringLiteralExpression, Literal(x))));
            var itms = CommaSeparated(parms.ToArray());
            return ArgumentList(SeparatedList<ArgumentSyntax>(itms));
        }

        public static ArgumentListSyntax Arguments(params ArgumentSyntax[] args)
        {
            var parms = args.Select<ArgumentSyntax, SyntaxNode>(x => x);
            var itms = CommaSeparated(parms.ToArray());
            return ArgumentList(SeparatedList<ArgumentSyntax>(itms));
        }

        public static SyntaxNodeOrToken[] CommaSeparated(IEnumerable<SyntaxNode> parms) => CommaSeparated(parms.ToArray());

        public static SyntaxNodeOrToken[] CommaSeparated(params SyntaxNode[] parms)
        {
            var list = new List<SyntaxNodeOrToken>();

            if (parms == null || parms.Length == 0)
            {
                return new SyntaxNodeOrToken[0];
            }

            list.Add(parms[0]);

            for (var i = 1; i < parms.Length; i++)
            {
                list.Add(Token(SyntaxKind.CommaToken));
                list.Add(parms[i]);
            }

            return list.ToArray();
        }

        public static TypeSyntax TypeOf(string s) => IdentifierName(s);

        public static MemberDeclarationSyntax Method(TypeSyntax tuple, string name, ParameterListSyntax parameters,
                                                     SyntaxList<StatementSyntax> block) =>
            MethodDeclaration(tuple, Identifier(name))
                .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword)))
                .WithParameterList(parameters)
                .WithBody(Block(block));

        public static PropertyDeclarationSyntax GetProperty(TypeSyntax tuple, string name, SyntaxList<StatementSyntax> body) =>
            PropertyDeclaration(tuple, Identifier(name))
                .WithModifiers(TokenList(Token(SyntaxKind.PublicKeyword)))
                .WithAccessorList(AccessorList(
                                               SingletonList(AccessorDeclaration(SyntaxKind.GetAccessorDeclaration)
                                                                 .WithBody(Block(body)))));

        public static SyntaxList<StatementSyntax> BlockItems(params StatementSyntax[] statments) => List(statments);

        public static LiteralExpressionSyntax LiteralValue(string value) =>
            LiteralExpression(
                              SyntaxKind.StringLiteralExpression,
                              Literal(value));

        public static StatementSyntax LocalDeclaration(string name, string value) =>
            LocalDeclarationStatement(
                                      VariableDeclaration(IdentifierName("var"))
                                          .WithVariables(
                                                         SingletonSeparatedList(
                                                                                VariableDeclarator(Identifier(name))
                                                                                    .WithInitializer(EqualsValueClause(LiteralValue(value))))));

        public static StatementSyntax Assign(string name, ExpressionSyntax call) =>
            ExpressionStatement(
                                AssignmentExpression(
                                                     SyntaxKind.SimpleAssignmentExpression,
                                                     IdentifierName(name),
                                                     call));

        public static ExpressionSyntax MethodCall(string varName, string methodCall, params string[] parameters) =>
            InvocationExpression(
                                 MemberAccessExpression(
                                                        SyntaxKind.SimpleMemberAccessExpression,
                                                        IdentifierName(varName),
                                                        IdentifierName(methodCall)))
                .WithArgumentList(Arguments(parameters));

        public static ExpressionSyntax MethodCall(string varName, string methodCall, params ArgumentSyntax[] parameters)
        {
            var itms = CommaSeparated(parameters.ToArray());
            var args = ArgumentList(SeparatedList<ArgumentSyntax>(itms));

            return InvocationExpression(
                                        MemberAccessExpression(
                                                               SyntaxKind.SimpleMemberAccessExpression,
                                                               IdentifierName(varName),
                                                               IdentifierName(methodCall)))
                .WithArgumentList(args);
        }

        public static ExpressionSyntax StaticMethodCall(string methodCall, params ArgumentSyntax[] parameters)
        {
            var itms = CommaSeparated(parameters.ToArray());
            var args = ArgumentList(SeparatedList<ArgumentSyntax>(itms));

            return InvocationExpression(
                                        IdentifierName(methodCall))
                .WithArgumentList(args);
        }

        public static ExpressionSyntax ThisMethodCall(string methodCall, params ArgumentSyntax[] parameters)
        {
            var itms = CommaSeparated(parameters.ToArray());
            var args = ArgumentList(SeparatedList<ArgumentSyntax>(itms));

            return InvocationExpression(
                                        MemberAccessExpression(
                                                               SyntaxKind.SimpleMemberAccessExpression,
                                                               ThisExpression(),
                                                               IdentifierName(methodCall)))
                .WithArgumentList(args);
        }

        public static StatementSyntax Return(ExpressionSyntax value) => ReturnStatement(value);

        public static TupleExpressionSyntax TupleValue(params ArgumentSyntax[] items)
        {
            var itms = CommaSeparated(items.ToArray());
            return TupleExpression(SeparatedList<ArgumentSyntax>(itms));
        }

        public static ArgumentSyntax VariableArg(string name) => Argument(IdentifierName(name));

        public static IdentifierNameSyntax Variable(string name) => IdentifierName(name);

        public static ArgumentSyntax LiteralValueArg(string name) => Argument(LiteralValue(name));

        public static AttributeSyntax AttributeValue(string name, params SyntaxNode[] argValue)
        {
            var args = CommaSeparated(argValue);
            return Attribute(IdentifierName(name))
                .WithArgumentList(
                                  AttributeArgumentList(
                                                        SeparatedList<AttributeArgumentSyntax>(args)));
        }

        public static UsingDirectiveSyntax Using(string usingText) => UsingDirective(IdentifierName(usingText));

        public static MemberAccessExpressionSyntax MemberAccess(string parent, string child) =>
            MemberAccessExpression(
                                   SyntaxKind.SimpleMemberAccessExpression,
                                   IdentifierName(parent),
                                   IdentifierName(child));
    }
}