namespace Base2art.WebApiRunner.Server.Runners.RouteGeneration.CodeGen
{
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class IncludedMethodObject : IncludedMethodBase
    {
        protected override string Name => "ReplaceInUrlObject";
        protected override TypeSyntax VariableType => PredefinedType(Token(SyntaxKind.ObjectKeyword));

        protected override ExpressionSyntax ConditionalExpr()
            =>
                InvocationExpression(
                                     MemberAccessExpression(
                                                            SyntaxKind.SimpleMemberAccessExpression,
                                                            IdentifierName("val1"),
                                                            IdentifierName("ToString")));
    }
}