namespace Base2art.WebApiRunner.Server.Runners.RouteGeneration.CodeGen
{
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class IncludedMethodStruct : IncludedMethodBase
    {
        protected override string Name => "ReplaceInUrlStruct";
        protected override TypeSyntax VariableType => IdentifierName("System.ValueType");

        protected override ExpressionSyntax ConditionalExpr()
            => InvocationExpression(
                                    MemberAccessExpression(
                                                           SyntaxKind.SimpleMemberAccessExpression,
                                                           IdentifierName("val1"),
                                                           IdentifierName("ToString")));
    }
}