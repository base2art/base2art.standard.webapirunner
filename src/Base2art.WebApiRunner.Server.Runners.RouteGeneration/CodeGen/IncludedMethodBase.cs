namespace Base2art.WebApiRunner.Server.Runners.RouteGeneration.CodeGen
{
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public abstract class IncludedMethodBase
    {
        protected abstract string Name { get; }

        protected abstract TypeSyntax VariableType { get; }
//        protected abstract SyntaxList<StatementSyntax> Body();

        protected virtual SyntaxList<StatementSyntax> Body() =>
            SingletonList<StatementSyntax>(
                                           ReturnStatement(InvocationExpression(IdentifierName("ReplaceInUrl"))
                                                               .WithArgumentList(
                                                                                 Factories.Arguments(
                                                                                                     Factories.VariableArg("url"),
                                                                                                     Factories.VariableArg("var1"),
                                                                                                     Argument(this.ConditionalExpr())
                                                                                                    ))));

        protected abstract ExpressionSyntax ConditionalExpr();

        public MethodDeclarationSyntax Value()
            => MethodDeclaration(
                                 PredefinedType(Token(SyntaxKind.StringKeyword)),
                                 Identifier(this.Name))
               .WithModifiers(
                              TokenList(Token(SyntaxKind.PrivateKeyword), Token(SyntaxKind.StaticKeyword)))
               .WithParameterList(
                                  ParameterList(
                                                SeparatedList<ParameterSyntax>(
                                                                               new SyntaxNodeOrToken[]
                                                                               {
                                                                                   Parameter(Identifier("url")).WithType(Factories.StringType),
                                                                                   Token(SyntaxKind.CommaToken),
                                                                                   Parameter(Identifier("var1")).WithType(Factories.StringType),
                                                                                   Token(SyntaxKind.CommaToken),
                                                                                   Parameter(Identifier("val1")).WithType(this.VariableType)
                                                                               })))
               .WithBody(
                         Block(
                               this.Body()));
    }
}