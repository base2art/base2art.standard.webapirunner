namespace Base2art.WebApiRunner.Server.Runners.RouteGeneration.CodeGen
{
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class IncludedMethodEnum : IncludedMethodBase
    {
        protected override string Name => "ReplaceInUrlEnum";
        protected override TypeSyntax VariableType => IdentifierName("System.Enum");

        protected override ExpressionSyntax ConditionalExpr()
            => InvocationExpression(
                                    MemberAccessExpression(
                                                           SyntaxKind.SimpleMemberAccessExpression,
                                                           IdentifierName("val1"),
                                                           IdentifierName("ToString")))
                .WithArgumentList(
                                  ArgumentList(
                                               SingletonSeparatedList(
                                                                      Argument(
                                                                               LiteralExpression(
                                                                                                 SyntaxKind.StringLiteralExpression,
                                                                                                 Literal("G"))))));
    }
}