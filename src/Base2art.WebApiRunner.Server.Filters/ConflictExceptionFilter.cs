﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;

    public class ConflictExceptionFilter<T> : ExceptionFilterBase<T>
        where T : Exception
    {
        protected override HttpStatusCode StatusCode => HttpStatusCode.Conflict;

        protected override string Code(T exception) => "CONFLICT";

        protected override string Message(T exception) => exception.Message ?? "Operation would result in an invalid state";
    }
}