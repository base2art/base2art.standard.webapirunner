﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Models;
    using Web.Exceptions.Models;
    using Web.Filters;

    public class ExceptionFilterBase<T> : IAsyncExceptionFilter, IExceptionFilter
        where T : Exception
    {
        protected virtual HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

        /// <inheritdoc />
        public virtual Task OnExceptionAsync(ExceptionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            this.OnException(context);
            return Task.CompletedTask;
        }

        public virtual void OnException(ExceptionContext context)
        {
            if (context.Exception is T b)
            {
                context.Exception.SuppressLogging();

                var response = context.HttpContext.Request.CreateResponse(this.StatusCode,
                                                                          new Error
                                                                          {
                                                                              Message = this.Message(b),
                                                                              Code = this.Code(b),
                                                                              Fields = this.Errors(b)
                                                                          });

                context.Result = response;
            }
        }

        protected virtual ErrorField[] Errors(T exception) => new ErrorField[0];

        protected virtual string Message(T exception) => exception.Message;

        protected virtual string Code(T exception) => typeof(T).Name;
    }
}