namespace Base2art.WebApiRunner.Server.Filters
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;

    public abstract class AsyncObjectFilterBase<T> : AsyncActionFilterBase
        where T : class
    {
        protected override async Task OnActionExecutedAsync(ActionExecutedContext context)
        {
            await base.OnActionExecutedAsync(context);

            if (context.HttpContext.Response == null)
            {
                return;
            }

            var contextResult = context.Result;

            if (contextResult is ObjectResult oc)
            {
                var value = oc.Value;
                if (value is T fi)
                {
                    await this.SendAsync(context, fi);
                }
            }
        }

        protected abstract Task SendAsync(ActionExecutedContext context, T value);
    }
}