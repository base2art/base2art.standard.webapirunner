﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;

    public abstract class ObjectFilterBase<T> : ActionFilterBase
        where T : class
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            if (context.HttpContext.Response == null)
            {
                return;
            }

            if (!(context.Result is ObjectResult oc))
            {
                return;
            }

            if (!(oc.Value is T fi))
            {
                return;
            }

            this.Send(context, fi);
        }

        protected abstract void Send(ActionExecutedContext actionExecutedContext, T value);
    }
}