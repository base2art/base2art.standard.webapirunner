﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;

    public class NotAuthorizedExceptionFilter<T> : ExceptionFilterBase<T>
        where T : Exception
    {
        protected override HttpStatusCode StatusCode => HttpStatusCode.Forbidden;

        protected override string Code(T exception) => "NOT_AUTHORIZED";

        protected override string Message(T exception) => exception.Message ?? "Incorrect authorization";
    }
}