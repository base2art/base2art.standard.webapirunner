﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;

    public class NotAuthenticatedExceptionFilter<T> : ExceptionFilterBase<T>
        where T : Exception
    {
        protected override HttpStatusCode StatusCode => HttpStatusCode.Unauthorized;

        protected override string Code(T exception) => "NOT_AUTHENTICATED";

        protected override string Message(T exception) =>
            exception.Message ?? "You are not authenticated. Or are using an incorrect authentication mechanism";
    }
}