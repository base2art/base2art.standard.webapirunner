//namespace Base2art.WebApiRunner.Server.Filters
//{
//    using System;
//    using System.Linq.Expressions;
//    using System.Reflection;
//    using Api;
//    using Api.Models;
//    
//    using Microsoft.AspNetCore.Mvc.Filters;
//    
//
//    public class ConfigBasedExceptionResponseMapFilter : ExceptionFilterBase
//    {
//        private readonly HttpConfiguration config;
//
//        private readonly ExceptionMapItemConfiguration exceptionMap;
//        
//        public ConfigBasedExceptionResponseMapFilter(HttpConfiguration config, ExceptionMapItemConfiguration exceptionMap)
//        {
//            if (config == null)
//            {
//                throw new ArgumentNullException(nameof(config));
//            }
//            
//            this.exceptionMap = exceptionMap;
//            
//            this.config = config;
//        }
//
//        public override void OnException(ActionExecutedContext actionExecutedContext)
//        {
//            if (actionExecutedContext.Exception != null && actionExecutedContext.Response() == null)
//            {
//                var type = Type.GetType(this.exceptionMap.ExceptionType);
//                
//                if (type.IsInstanceOfType(actionExecutedContext.Exception))
//                {
//                    var error = new Error();
//                    
//                    if (!string.IsNullOrWhiteSpace(this.exceptionMap.ResponseCode))
//                    {
//                        error.Code = this.exceptionMap.ResponseCode;
//                    }
//                    
//                    if (!string.IsNullOrWhiteSpace(this.exceptionMap.ResponseMessage))
//                    {
//                        error.Message = this.exceptionMap.ResponseMessage;
//                    }
//                    
//                    if (!string.IsNullOrWhiteSpace(this.exceptionMap.ResponseMessageExceptionProperty))
//                    {
//                        var getter = GetPropGetter<Exception, string>(this.exceptionMap.ResponseMessageExceptionProperty);
//                        
//                        error.Message = getter(actionExecutedContext.Exception);
//                    }
//                    
//                    if (string.IsNullOrWhiteSpace(error.Code))
//                    {
//                        error.Code = actionExecutedContext.Exception.GetType().Name;
//                    }
//                    
//                    if (string.IsNullOrWhiteSpace(error.Message))
//                    {
//                        error.Message = actionExecutedContext.Exception.Message;
//                    }
//                    
//                    var responseCode = this.exceptionMap.HttpResponseCode <= 0 ? 500 : this.exceptionMap.HttpResponseCode;
//                    var statusCode = (System.Net.HttpStatusCode)responseCode;
//                    
//                    actionExecutedContext.Exception.SuppressLogging();
//                    actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(statusCode, error, this.config);
//                }
//            }
//
//            base.OnException(actionExecutedContext);
//        }
//        
//        public static Func<TObject, TProperty> GetPropGetter<TObject, TProperty>(string propertyName)
//        {
//            ParameterExpression paramExpression = Expression.Parameter(typeof(TObject), "value");
//
//            Expression propertyGetterExpression = Expression.Property(paramExpression, propertyName);
//
//            Func<TObject, TProperty> result =
//                Expression.Lambda<Func<TObject, TProperty>>(propertyGetterExpression, paramExpression).Compile();
//
//            return result;
//        }
//    }
//}

