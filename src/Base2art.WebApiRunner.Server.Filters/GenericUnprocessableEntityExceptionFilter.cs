﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;
    using Web.Exceptions.Models;

    public class UnprocessableArgumentEntityExceptionFilter<T> : ExceptionFilterBase<T>
        where T : Exception
    {
        private readonly Func<T, ErrorField> errorField;

        public UnprocessableArgumentEntityExceptionFilter(Func<T, ErrorField> errorField) => this.errorField = errorField;

        protected override HttpStatusCode StatusCode => (HttpStatusCode) 422;

        protected override ErrorField[] Errors(T b) => new[] {this.errorField(b)};

        protected override string Message(T exception) => "Bad Request Data (See Fields)...";

        protected override string Code(T exception) => "UNPROCESSABLE_ENTITY";
    }
}