namespace Base2art.WebApiRunner.Server.Filters
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Web.Pages;

    public class WebPageFilter : AsyncObjectFilterBase<WebPage>
    {
        protected override async Task SendAsync(ActionExecutedContext actionExecutedContext, WebPage value)
        {
            var resultValue = await value.Render();
            var contentResult = new ContentResult {Content = resultValue};

            if (!string.IsNullOrWhiteSpace(value.ContentType))
            {
                contentResult.ContentType = value.ContentType;
            }

            actionExecutedContext.Result = contentResult;
        }
    }
}