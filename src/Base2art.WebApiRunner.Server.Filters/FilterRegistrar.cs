﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Authentication;
    using System.Threading.Tasks;
    using ComponentModel.Composition;
    using ExceptionHandling;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.Extensions.DependencyInjection;
    using Registrations;
    using Web.App.Configuration;
    using Web.Exceptions;
    using Web.Exceptions.Models;
    using Web.Filters;
    using Web.Server.Registration;

    public class FilterRegistrar : RegistrationBase
    {
        public override void RegisterFilters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration serverConfiguration)
        {
            base.RegisterFilters(builder, services, serverConfiguration);

            builder.AddMvcOptions(x => x.Filters.Add(new FormatFilterAttribute()));
            var map = services.GetService<IMediaTypeMap>();
            builder.AddMvcOptions(x => x.Filters.Add(new FileInfoFilter(map)));
            builder.AddMvcOptions(x => x.Filters.Add(new WebPageFilter()));

            builder.AddMvcOptions(x => x.Filters.Add(new ConflictExceptionFilter<ConflictException>()));
            builder.AddMvcOptions(x => x.Filters.Add(new NotAuthenticatedExceptionFilter<NotAuthenticatedException>()));
            builder.AddMvcOptions(x => x.Filters.Add(new NotAuthorizedExceptionFilter<NotAuthorizedToResourceException>()));
            builder.AddMvcOptions(x => x.Filters.Add(new NotFoundExceptionFilter<NotFoundException>()));
            builder.AddMvcOptions(x => x.Filters.Add(new RedirectExceptionFilter<RedirectException>()));
            builder.AddMvcOptions(x => x.Filters.Add(new NotAuthorizedExceptionFilter<NotAuthorizedToResourceException>()));
            builder.AddMvcOptions(x => x.Filters.Add(new NotImplementedExceptionFilter()));
            builder.AddMvcOptions(x => x.Filters.Add(new UnprocessableEntityExceptionFilter()));
            builder.AddMvcOptions(x => x.Filters.Add(new FailedDependencyFilter<BindingException>()));

            foreach (var filter in serverConfiguration?.Application?.Filters ?? new ITypeInstanceConfiguration[0])
            {
                var responseEditorFilter = CreateResponseEditorFilter(services, filter);
                builder.AddMvcOptions(x => { x.Filters.Add(responseEditorFilter); });
            }

            /*
  - exceptionType: System.UnauthorizedAccessException
    httpResponseCode: 401
  - exceptionType: System.AccessViolationException
    httpResponseCode: 403
  - exceptionType: System.InvalidOperationException
    httpResponseCode: 409
  - exceptionType: System.ArgumentException
    httpResponseCode: 422
  - exceptionType: System.FormatException
    httpResponseCode: 422
             */
            var items = serverConfiguration?.Application?.Exceptions;
            if (items == null || items.RegisterCommonHandlers)
            {
                builder.AddMvcOptions(x => x.Filters.Add(new NotAuthenticatedExceptionFilter<UnauthorizedAccessException>()));
                builder.AddMvcOptions(x => x.Filters.Add(new NotAuthenticatedExceptionFilter<AuthenticationException>()));
                builder.AddMvcOptions(x => x.Filters.Add(new NotAuthorizedExceptionFilter<AccessViolationException>()));

                ErrorField ArgMap(ArgumentException b) => new ErrorField
                                                          {
                                                              Code = b.GetType().Name,
                                                              Message = b.Message,
                                                              Path = b.ParamName
                                                          };

                ErrorField FormatMap(Exception b) => new ErrorField
                                                     {
                                                         Code = b.GetType().Name,
                                                         Message = b.Message,
                                                         Path = "UNKNOWN"
                                                     };

                builder.AddMvcOptions(x => x.Filters.Add(new ConflictExceptionFilter<InvalidOperationException>()));
                builder.AddMvcOptions(x => x.Filters.Add(new UnprocessableArgumentEntityExceptionFilter<ArgumentException>(ArgMap)));
                builder.AddMvcOptions(x => x.Filters.Add(new UnprocessableArgumentEntityExceptionFilter<FormatException>(FormatMap)));
                builder.AddMvcOptions(x => x.Filters.Add(new UnprocessableArgumentEntityExceptionFilter<InvalidDataException>(FormatMap)));

                builder.AddMvcOptions(x => x.Filters.Add(new ExceptionLoggingFilter(services.GetService<ICreator>(),
                                                                                    serverConfiguration?.Application?.Exceptions?.Loggers
                                                                                    ?? new ITypeInstanceConfiguration[0])));
            }
        }

        public static IFilterMetadata CreateResponseEditorFilter(IServiceProvider services, ITypeInstanceConfiguration filter)
        {
            var lookup = services?.GetService<ICreator>();

            if (lookup == null)
            {
                return null;
            }

            var obj = lookup.Create(filter.Type, filter.Parameters, filter.Properties);

            switch (obj)
            {
                case IFilterMetadata metadata1:
                    return metadata1;
                case IResponseFilter metadata2:
                    return new ResponseEditorFilter(metadata2);
                default:
                    throw new InvalidCastException("type is not a filterable type.");
            }
        }

        public class ExceptionLoggingFilter : IOrderedFilter, IAsyncExceptionFilter
        {
            private readonly List<IExceptionLogger> callableMethodConfigurations;

            public ExceptionLoggingFilter(
                ICreator serviceLookup,
                IReadOnlyList<ITypeInstanceConfiguration> callableMethodConfigurations)
            {
                this.callableMethodConfigurations = callableMethodConfigurations
                                                    .Select(x => serviceLookup.Create(x.Type, x.Parameters, x.Properties))
                                                    .Cast<IExceptionLogger>()
                                                    .ToList();
            }

//            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
//            {
//            }

            public async Task OnExceptionAsync(ExceptionContext context)
            {
                var ex = context.Exception;
//                try
//                {
//                    await next();
//                }
//                catch (Exception e)
//                {
//                    ex = e;
//                }

                if (ex != null)
                {
                    foreach (var logger in this.callableMethodConfigurations)
                    {
                        try
                        {
                            await logger.HandleException(ex);
                        }
                        catch (OutOfMemoryException)
                        {
                            throw;
                        }
                        catch (StackOverflowException)
                        {
                            throw;
                        }
                        catch (BadImageFormatException)
                        {
                            throw;
                        }
                        catch (InvalidProgramException)
                        {
                            throw;
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            public int Order { get; } = -20000;
        }
    }
}