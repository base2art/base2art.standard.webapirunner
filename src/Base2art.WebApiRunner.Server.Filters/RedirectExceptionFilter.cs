﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Models;
    using Web.Exceptions;
    using Web.Exceptions.Models;
    using Web.Filters;

    public class RedirectExceptionFilter<T> : IAsyncExceptionFilter, IExceptionFilter
        where T : RedirectException
    {
        /// <inheritdoc />
        public virtual Task OnExceptionAsync(ExceptionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            this.OnException(context);
            return Task.CompletedTask;
        }

        public virtual void OnException(ExceptionContext context)
        {
            if (context.Exception is T b)
            {
                context.Exception.SuppressLogging();

                var httpStatusCode = this.GetStatusCode(b);
                var response = context.HttpContext.Request.CreateResponse(httpStatusCode,
                                                                          new Error
                                                                          {
                                                                              Message = this.Message(b),
                                                                              Code = this.Code(b),
                                                                              Fields = this.Errors(b)
                                                                          });

                context.Result = response;

                context.Response().StatusCode = (int) httpStatusCode;
                context.Response().Headers["Location"] = this.GetLocation(b);
            }
        }

        private string GetLocation(T redirectException) => redirectException.Location ?? "/";

        protected virtual HttpStatusCode GetStatusCode(T redirectException)
        {
            if (redirectException.IsPermanent)
            {
                return redirectException.PreserveMethod ? (HttpStatusCode) (308) : HttpStatusCode.MovedPermanently;
            }

            return redirectException.PreserveMethod ? HttpStatusCode.TemporaryRedirect : HttpStatusCode.SeeOther;
        }

        protected virtual ErrorField[] Errors(T exception) => new[]
                                                              {
                                                                  new ErrorField
                                                                  {
                                                                      Code = "LOCATION",
                                                                      Path = this.GetLocation(exception),
                                                                      Message = "See Path"
                                                                  }
                                                              };

        protected virtual string Message(T exception) => exception.Message;

        protected virtual string Code(T exception)
        {
            var httpStatusCode = this.GetStatusCode(exception);
            switch (httpStatusCode)
            {
                case HttpStatusCode.MovedPermanently:
                    return "MOVED_PERMANENTLY";
                case (HttpStatusCode)(308):
                    return "PERMANENT_REDIRECT";
                case HttpStatusCode.TemporaryRedirect:
                    return "TEMPORARY_REDIRECT";
                case HttpStatusCode.SeeOther:
                    return "SEE_OTHER";
                default:
                    return httpStatusCode.ToString("G");
            }
        }
    }
}