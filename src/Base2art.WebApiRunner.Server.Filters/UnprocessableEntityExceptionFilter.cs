﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System.Net;
    using Web.Exceptions;
    using Web.Exceptions.Models;

    public class UnprocessableEntityExceptionFilter : ExceptionFilterBase<UnprocessableEntityException>
    {
        protected override HttpStatusCode StatusCode => (HttpStatusCode) 422;

        protected override ErrorField[] Errors(UnprocessableEntityException b) => b.Errors ?? new[]
                                                                                              {
                                                                                                  new ErrorField
                                                                                                  {
                                                                                                      Code = "UNKNOWN",
                                                                                                      Message = "UNKNOWN",
                                                                                                      Path = "UNKNOWN"
                                                                                                  }
                                                                                              };

        protected override string Message(UnprocessableEntityException exception) => "Bad Request Data (See Fields)...";

        protected override string Code(UnprocessableEntityException exception) => "UNPROCESSABLE_ENTITY";
    }
}