namespace Base2art.WebApiRunner.Server.Filters
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.Filters;

    public class AsyncActionFilterBase : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            await this.OnActionExecutingAsync(context);
            var result = await next();
            await this.OnActionExecutedAsync(result);
        }

        protected virtual Task OnActionExecutingAsync(ActionExecutingContext context) => Task.CompletedTask;

        protected virtual Task OnActionExecutedAsync(ActionExecutedContext context) => Task.CompletedTask;
    }
}