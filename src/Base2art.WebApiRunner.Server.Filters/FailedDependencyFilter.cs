﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;

    public class FailedDependencyFilter<T> : ExceptionFilterBase<T>
        where T : Exception
    {
        protected override HttpStatusCode StatusCode => (HttpStatusCode) 424;

        protected override string Code(T exception) => "FAILED_DEPENDENCY";

        protected override string Message(T exception) =>
            exception.Message ?? "Dependency not configured properly";
    }
}