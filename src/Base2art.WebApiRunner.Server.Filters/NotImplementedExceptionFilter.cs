namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;

    public class NotImplementedExceptionFilter : ExceptionFilterBase<NotImplementedException>
    {
        protected override HttpStatusCode StatusCode => (HttpStatusCode) 501;

        protected override string Code(NotImplementedException exception) => "NOT_IMPLEMENTED";

        protected override string Message(NotImplementedException exception) => "Operation not implemented";
    }
}