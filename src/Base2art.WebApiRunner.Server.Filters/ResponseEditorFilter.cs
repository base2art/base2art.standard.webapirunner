﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Web.Filters;
    using MySameSiteMode = Web.Http.SameSiteMode;

    public class ResponseEditorFilter : ActionFilterBase
    {
        private readonly IResponseFilter filter;

        public ResponseEditorFilter(IResponseFilter filter) => this.filter = filter;

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var resp = context.HttpContext.Response;

            if (resp == null)
            {
                base.OnActionExecuted(context);
                return;
            }

            if (!(context.Result is ObjectResult oc))
            {
                return;
            }

            if (this.SupportsType(oc.DeclaredType))
            {
                var fi = oc.Value;
                if (fi == null)
                {
                    base.OnActionExecuted(context);
                    return;
                }

                this.Invoke(context, fi, this.filter);
            }

            base.OnActionExecuted(context);
        }

        public bool SupportsType(Type ocDeclaredType)
        {
            var filterType = this.filter?.GetType()?.GetInterfaces() ?? new Type[0];
            return filterType.Where(x => x.IsGenericType)
                             .Select(x => Tuple.Create(x.GetGenericTypeDefinition(), x.GetGenericArguments()))
                             .Where(x => IsMatch(ocDeclaredType, x))
                             .Any();
        }

        private static bool IsMatch(Type ocDeclaredType, Tuple<Type, Type[]> x)
        {
            var memberInfo = x.Item1;
            if (memberInfo != typeof(IResponseFilter<>))
            {
                return false;
            }

            var mainType = x.Item2[0];
            if (mainType.IsAssignableFrom(ocDeclaredType))
            {
                return true;
            }

            return false;
        }

        protected virtual void Invoke(ActionExecutedContext actionExecutedContext, object content, IResponseFilter instance)
        {
            instance.Filter(
                            content,
                            actionExecutedContext.HttpContext.Request(),
                            actionExecutedContext.HttpContext.Response());
        }
    }
}