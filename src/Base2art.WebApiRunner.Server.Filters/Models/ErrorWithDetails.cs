namespace Base2art.WebApiRunner.Server.Filters.Models
{
    public class ErrorWithDetails : Error
    {
        public object Details { get; set; }
    }
}