﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Filters;

    public static class Migration
    {
        public static HttpResponse Response(this ActionExecutedContext context) => context.HttpContext.Response;

        public static HttpResponse Response(this ActionExecutingContext context) => context.HttpContext.Response;

        public static HttpResponse Response(this ExceptionContext context) => context.HttpContext.Response;

        public static HttpRequest Request(this ActionExecutedContext context) => context.HttpContext.Request;

        public static HttpRequest Request(this ActionExecutingContext context) => context.HttpContext.Request;

        public static HttpRequest Request(this ExceptionContext context) => context.HttpContext.Request;
    }
}