﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.Net;

    public class NotFoundExceptionFilter<T> : ExceptionFilterBase<T>
        where T : Exception
    {
        protected override HttpStatusCode StatusCode => HttpStatusCode.NotFound;

        protected override string Code(T exception) => "NOT_FOUND";

        protected override string Message(T exception) => exception.Message ?? "Resource or endpoint not found";
    }
}