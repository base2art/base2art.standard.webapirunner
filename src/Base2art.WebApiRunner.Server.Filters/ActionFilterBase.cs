﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using Microsoft.AspNetCore.Mvc.Filters;

    public class ActionFilterBase : IActionFilter
    {
        public virtual void OnActionExecuting(ActionExecutingContext context)
        {
        }

        public virtual void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}