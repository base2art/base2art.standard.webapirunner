﻿namespace Base2art.WebApiRunner.Server.Filters
{
    using System;
    using System.IO;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Registrations;

    public class FileInfoFilter : ObjectFilterBase<FileInfo>
    {
        private readonly IMediaTypeMap map;

        public FileInfoFilter(IMediaTypeMap map)
        {
            if (map == null)
            {
                throw new ArgumentNullException(nameof(map));
            }

            this.map = map;
        }

        protected override void Send(ActionExecutedContext actionExecutedContext, FileInfo value)
        {
            var ext = this.map.GetMimeType(value.Extension);
            actionExecutedContext.Response().ContentType = ext;
            actionExecutedContext.Response().ContentLength = value.Length;

            actionExecutedContext.Result = new FileStreamResult(value.OpenRead(), ext);
        }
    }
}