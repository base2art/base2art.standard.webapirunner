using Base2art.Serialization.CodeGeneration;

[assembly: IncludeJsonSerializer("Base2art.WebApiRunner.Server.TaskScheduling.Serialization", MakePublic = false)]