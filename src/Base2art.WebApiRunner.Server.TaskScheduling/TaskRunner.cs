﻿namespace Base2art.WebApiRunner.Server.TaskScheduling
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Controllers;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.DependencyInjection;
    using Quartz;
    using Web.App.Configuration;

    public class TaskRunner : IJob
    {
        public TaskRunner(IServerConfiguration config)
        {
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var jobDataMap = context.MergedJobDataMap;
            var serviceProvider = (IServiceProvider) jobDataMap["serviceProvider"];
            var klass = (Type) jobDataMap["Class"];
            var method = (MethodInfo) jobDataMap["MethodInfo"];

            object parms = null;
            if (jobDataMap.ContainsKey("parameters"))
            {
                parms = jobDataMap["parameters"];
            }

            var provider = serviceProvider.GetService<IActionDescriptorCollectionProvider>();
            var actions = provider.ActionDescriptors.Items.OfType<ControllerActionDescriptor>();

            var ctlrContext = new ControllerContext
                              {
                                  ActionDescriptor = actions.First(x => x.ControllerTypeInfo == klass.GetTypeInfo() && x.MethodInfo == method),

                                  HttpContext = new DefaultHttpContext
                                                {
                                                    RequestServices = serviceProvider
                                                },

                                  RouteData = new RouteData()
                              };

            var parameters = ctlrContext.ActionDescriptor.MethodInfo.GetParameters();
            var actionDescriptorParameters = ctlrContext.ActionDescriptor.Parameters;

            for (var index = 0; index < actionDescriptorParameters.Count; index++)
            {
                var parameter = actionDescriptorParameters[index];
                var bindingInfo = new BindingInfo();
                bindingInfo.BinderType = typeof(CustomBinderType);
                bindingInfo.BindingSource = BindingSource.Body;

                if (index == 0 && parms != null)
                {
                    ctlrContext.ModelState.SetModelValue(CustomBinderType.ModelStateKey, parms, "");
                }

                if (parameter.BindingInfo.BindingSource == BindingSource.Body && parameter.BindingInfo.BinderType == null)
                {
                    parameter.BindingInfo = bindingInfo;
                }
            }

            var actionInvokerFactory = serviceProvider.GetService<IActionInvokerFactory>();

            try
            {
                var actionInvoker = actionInvokerFactory.CreateInvoker(ctlrContext);

                await actionInvoker.InvokeAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }
    }
}