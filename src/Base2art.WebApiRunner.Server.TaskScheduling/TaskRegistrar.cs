﻿namespace Base2art.WebApiRunner.Server.TaskScheduling
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.Extensions.DependencyInjection;
    using Quartz;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public abstract class TaskRegistrar : RegistrationBase
    {
        private IScheduler scheduler;

        public override void RegisterBindings(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterBindings(builder, services, config);

            builder.AddMvcOptions(x => { x.ModelBinderProviders.Insert(0, new TaskBinderProvider()); });

//            if (methodRoute.DefaultBinder)
//            {
//                var firsts = actionModel.Parameters
//                                        .Where(x => x.BindingInfo.BindingSource == BindingSource.Body && x.BindingInfo.BinderType == null)
//                                        .ToList();
//                if (firsts.Count == 1)
//                {
//                    var bindingInfo = new BindingInfo();
//                    bindingInfo.BinderType = typeof(CustomBinderType);
//                    bindingInfo.BindingSource = BindingSource.Body;
//
//                    firsts[0].BindingInfo = bindingInfo;
//                }
//            }
        }

        public override async void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
        {
            base.RegisterRoutes(router, provider, config);

            this.scheduler = await this.CreateScheduler();

            this.scheduler.JobFactory = new InjectingJobFactory(config);

            // and start it off
            await this.scheduler.Start();
            foreach (var item in config.Tasks ?? new List<ITaskConfiguration>())
            {
                var dataBuilder = new Dictionary<string, object>
                                  {
                                      {"Name", item.Name},
                                      {"ClassName", item.Type.AssemblyQualifiedName},
                                      {"Class", item.Type},
                                      {"MethodInfo", item.Method},
                                      {"config", config},
                                      {"serviceProvider", provider}
                                  };

                IDictionary<string, object> readOnlyDictionary = new ReadOnlyDictionary<string, object>(dataBuilder);
                var job = JobBuilder.Create<TaskRunner>()
                                    .SetJobData(new JobDataMap(readOnlyDictionary))
                                    .Build();

                // Trigger the job to run now, and then repeat every 10 seconds
                var delay = item.Delay;
                if (delay.TotalMilliseconds < 10)
                {
                    delay = item.Interval;
                }

                var triggerBuilder = TriggerBuilder.Create();
                if (item.Interval.TotalSeconds > 0)
                {
                    triggerBuilder = triggerBuilder.StartAt(DateTimeOffset.Now.Add(delay))
                                                   .WithSimpleSchedule(x => x.WithInterval(item.Interval)
                                                                             .RepeatForever());
                }
                else
                {
                    triggerBuilder = triggerBuilder.StartAt(DateTimeOffset.UtcNow.AddYears(20));
                }

                // Tell quartz to schedule the job using our trigger
                await this.scheduler.ScheduleJob(job, triggerBuilder.Build());
            }
        }

        protected abstract Task<IScheduler> CreateScheduler();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
            }
        }

        public class TaskBinderProvider : IModelBinderProvider
        {
            public IModelBinder GetBinder(ModelBinderProviderContext context) => null;
        }
    }
}