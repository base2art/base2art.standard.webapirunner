﻿namespace Base2art.WebApiRunner.Server.TaskScheduling
{
    using System;
    using Quartz;
    using Quartz.Spi;
    using Web.App.Configuration;

    public class InjectingJobFactory : IJobFactory
    {
        private readonly IServerConfiguration config;

        public InjectingJobFactory(IServerConfiguration config) => this.config = config;

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler) => (IJob) Activator.CreateInstance(bundle.JobDetail.JobType, this.config);

        public void ReturnJob(IJob job)
        {
        }
    }
}