namespace Base2art.WebApiRunner.Server.TaskScheduling
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Reflection.Activation;
    using Serialization;

    internal class CustomBinderType : IModelBinder
    {
        internal const string ModelStateKey = "6C10C6CD-0068-4D9B-A826-2B5CE236EB9E";

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var modelType = bindingContext.ModelType;

            if (bindingContext.ModelState.ContainsKey(ModelStateKey))
            {
                var modelStateEntry = bindingContext.ModelState[ModelStateKey];
                var rawValue = modelStateEntry.RawValue;

                if (!modelType.IsInstanceOfType(rawValue))
                {
                    var mappedModelType1 = this.GetModelType(modelType);
                    var serializer = new CamelCasingSimpleJsonSerializer();
                    var result = serializer.Serialize(rawValue);
                    try
                    {
                        bindingContext.Result = ModelBindingResult.Success(serializer.Deserialize(result, mappedModelType1));
                        return Task.CompletedTask;
                    }
                    catch (Exception)
                    {
                    }
                }

                bindingContext.Result = ModelBindingResult.Success(rawValue);
                return Task.CompletedTask;
            }

            var mappedModelType = this.GetModelType(modelType);

            try
            {
                var item = mappedModelType.CreateFrom(null, null, null);
                bindingContext.Result = ModelBindingResult.Success(item);
                return Task.CompletedTask;
            }
            catch (Exception)
            {
            }

            bindingContext.Result = ModelBindingResult.Success(null);

            return Task.CompletedTask;
        }

        private Type GetModelType(Type modelType)
        {
            if (modelType.IsGenericType)
            {
                var baseType = modelType.GetGenericTypeDefinition();

                if (typeof(IDictionary<,>) == baseType || typeof(IReadOnlyDictionary<,>) == baseType)
                {
                    return typeof(Dictionary<,>).MakeGenericType(modelType.GetGenericArguments());
                }
            }

            if (!modelType.IsAbstract && !modelType.IsInterface && modelType.IsClass)
            {
                return modelType;
            }

            return modelType;
        }
    }
}