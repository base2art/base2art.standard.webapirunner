﻿namespace Base2art.WebApiRunner.Server.TaskScheduling
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Logging;
    using Quartz;
    using Quartz.Impl;
    using Quartz.Impl.Matchers;

    public class ProdTaskRegistrar : TaskRegistrar
    {
        private static readonly Task<IScheduler> Scheduler;

        static ProdTaskRegistrar()
        {
            DirectSchedulerFactory.Instance.CreateVolatileScheduler(10);
            Scheduler = DirectSchedulerFactory.Instance.GetScheduler();
        }

        protected override Task<IScheduler> CreateScheduler() => Scheduler;

        public static async Task ExecuteJob(string name)
        {
            try
            {
                await ExecuteJobInternal(name, null);
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.Error, e);
            }
        }

        public static async Task ExecuteJob(string name, object parameters)
        {
            try
            {
                await ExecuteJobInternal(name, parameters);
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.Error, e);
            }
        }

        private static async Task ExecuteJobInternal(string name, object parameters)
        {
            parameters = parameters ?? new Dictionary<string, object>();
            var schedulerLocal = await Scheduler;

            var groupMatcher = GroupMatcher<JobKey>.AnyGroup();
            var keys = await schedulerLocal.GetJobKeys(groupMatcher);

            foreach (var key in keys)
            {
                var jobDetail = await schedulerLocal.GetJobDetail(key);
                var dataMap = jobDetail.JobDataMap;
                if (string.Equals(dataMap["Name"] as string, name, StringComparison.InvariantCultureIgnoreCase))
                {
                    var map = new JobDataMap();
                    foreach (var key1 in dataMap.Keys)
                    {
                        map[key1] = dataMap[key1];
                    }

                    map["parameters"] = parameters;

                    await schedulerLocal.TriggerJob(key, map);
                }
            }
        }
    }
}