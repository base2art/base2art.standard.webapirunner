﻿namespace Base2art.WebApiRunner.Server.Testability
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Net.Http;
    using Web.Server.Registration;

    public static class TestRouteManagers
    {
        public static void MapRoute<T>(
            this IRouteManager manager,
            HttpMethod method,
            string routeTemplate,
            Expression<Action<T>> action,
            RouteType routeType)
        {
            manager.MapRoute(
                             method,
                             routeTemplate,
                             typeof(T),
                             action.MethodOf(),
                             new Dictionary<string, object>(),
                             new Dictionary<string, object>(),
                             routeType);
        }

        public static void MapRoute<T>(
            this IRouteManager manager,
            HttpMethod method,
            string routeTemplate,
            Expression<Action<T>> action,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties,
            RouteType routeType)
        {
            manager.MapRoute(method, routeTemplate, typeof(T), action.MethodOf(), parameters, properties, routeType);
        }
    }
}