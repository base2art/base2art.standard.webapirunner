﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System.Collections.Generic;
    using Web.App.Configuration;

    public class ExceptionConfiguration : IExceptionConfiguration
    {
//        public List<ExecutableInstanceConfiguration> Handlers { get; } = new List<ExecutableInstanceConfiguration>();
        public List<ExecutableInstanceConfiguration> Loggers { get; } = new List<ExecutableInstanceConfiguration>();
        public bool RegisterCommonHandlers { get; set; } = true;
        public bool AllowStackTraceInOutput { get; set; } = true;

//        IReadOnlyList<ICallableMethodConfiguration> IExceptionConfiguration.Handlers => this.Handlers;
        IReadOnlyList<ITypeInstanceConfiguration> IExceptionConfiguration.Loggers => this.Loggers;
    }
}