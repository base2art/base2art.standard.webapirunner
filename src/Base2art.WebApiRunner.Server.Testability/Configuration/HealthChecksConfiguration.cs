﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System.Collections.Generic;
    using Web.App.Configuration;

    public class HealthChecksConfiguration : IHealthChecksConfiguration
    {
        public List<AspectConfiguration> Aspects { get; } = new List<AspectConfiguration>();
        public List<HealthCheckConfiguration> Items { get; } = new List<HealthCheckConfiguration>();
        IReadOnlyList<IHealthCheckConfiguration> IHealthChecksConfiguration.Items => this.Items;

        IReadOnlyList<ITypeInstanceConfiguration> IHealthChecksConfiguration.Aspects => this.Aspects;
    }
}