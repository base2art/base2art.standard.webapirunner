﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System;
    using System.Collections.Generic;
    using Web.App.Configuration;

    public class ModelBindingConfiguration : IModelBindingConfiguration
    {
        public Dictionary<string, object> BinderParameters { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> BinderProperties { get; } = new Dictionary<string, object>();
        public BindingType BindingType { get; set; }
        public Type BoundType { get; set; }
        public Type BinderType { get; set; }

        IReadOnlyDictionary<string, object> IModelBindingConfiguration.BinderParameters => this.BinderParameters;
        IReadOnlyDictionary<string, object> IModelBindingConfiguration.BinderProperties => this.BinderProperties;
    }
}