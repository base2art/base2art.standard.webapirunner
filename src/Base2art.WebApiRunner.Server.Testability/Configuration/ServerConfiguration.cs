﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System.Collections.Generic;
    using Web.App.Configuration;

    public class ServerConfiguration : IServerConfiguration
    {
        private ServerConfiguration(IAssemblyLoader assemblyLoader) => this.AssemblyLoader = assemblyLoader;

        public List<TaskConfiguration> Tasks { get; } = new List<TaskConfiguration>();

        public List<EndpointConfiguration> Endpoints { get; } = new List<EndpointConfiguration>();

        public HealthChecksConfiguration HealthChecks { get; } = new HealthChecksConfiguration();

        public ApplicationConfiguration Application { get; } = new ApplicationConfiguration();

        public List<CorsItemConfiguration> Cors { get; } = new List<CorsItemConfiguration>();

        public List<InjectionItemConfiguration> Injection { get; } = new List<InjectionItemConfiguration>();

        public string WebRoot { get; set; }

        public IAssemblyLoader AssemblyLoader { get; }

//        public TypeLoader TypeLoader => new TypeLoader(this.AssemblyLoader);

        IReadOnlyList<IEndpointConfiguration> IServerConfiguration.Endpoints => this.Endpoints;

        IReadOnlyList<ITaskConfiguration> IServerConfiguration.Tasks => this.Tasks;

        IHealthChecksConfiguration IServerConfiguration.HealthChecks => this.HealthChecks;

        IApplicationConfiguration IServerConfiguration.Application => this.Application;

        IReadOnlyList<ICorsItemConfiguration> IServerConfiguration.Cors => this.Cors;

        IReadOnlyList<IInjectionItemConfiguration> IServerConfiguration.Injection => this.Injection;

        public static ServerConfiguration Create(IAssemblyLoader loader = null)
            => new ServerConfiguration(loader ?? new CurrentAppDomainAssemblyLoader(
                                                                                    new TestAssemblyFactory(),
                                                                                    new[]
                                                                                    {
                                                                                        "netcoreapp2.2",
                                                                                        "netcoreapp2.1",
                                                                                        "netcoreapp2.0",
                                                                                        "netstandard2.0",
                                                                                        "netstandard1.6",
                                                                                        "netstandard1.5",
                                                                                        "netstandard1.4",
                                                                                        "netstandard1.3",
                                                                                        "netstandard1.2",
                                                                                        "netstandard1.1",
                                                                                        "netstandard1.0"
                                                                                    }));
    }
}