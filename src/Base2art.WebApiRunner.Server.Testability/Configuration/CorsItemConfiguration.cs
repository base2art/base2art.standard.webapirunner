﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System;
    using System.Collections.Generic;
    using Web.App.Configuration;

    public class CorsItemConfiguration : ICorsItemConfiguration
    {
        public string RequestHost { get; set; }

        public List<string> ExposedHeaders { get; } = new List<string>();
        public List<string> Headers { get; } = new List<string>();
        public List<string> Methods { get; } = new List<string>();
        public List<string> Origins { get; } = new List<string>();
        public bool AllowAnyHeader { get; set; }
        public bool AllowAnyMethod { get; set; }
        public bool AllowAnyOrigin { get; set; }
        public TimeSpan? PreflightMaxAge { get; set; }
        public bool SupportsCredentials { get; set; }

        IReadOnlyList<string> ICorsItemConfiguration.ExposedHeaders => this.ExposedHeaders;
        IReadOnlyList<string> ICorsItemConfiguration.Headers => this.Headers;
        IReadOnlyList<string> ICorsItemConfiguration.Methods => this.Methods;
        IReadOnlyList<string> ICorsItemConfiguration.Origins => this.Origins;
    }
}