﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System.Collections.Generic;
    using Web.App.Configuration;

    public class ApplicationConfiguration : IApplicationConfiguration
    {
        public ExceptionConfiguration Exceptions { get; } = new ExceptionConfiguration();
        public List<ITypeInstanceConfiguration> Filters { get; } = new List<ITypeInstanceConfiguration>();
        public List<ModelBindingConfiguration> ModelBindings { get; } = new List<ModelBindingConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> InputFormatters { get; } = new List<ITypeInstanceConfiguration>();
        public IReadOnlyList<ITypeInstanceConfiguration> OutputFormatters { get; } = new List<ITypeInstanceConfiguration>();
        public Dictionary<string, string> MediaTypes { get; } = new Dictionary<string, string>();

        IExceptionConfiguration IApplicationConfiguration.Exceptions => this.Exceptions;
        IReadOnlyList<IModelBindingConfiguration> IApplicationConfiguration.ModelBindings => this.ModelBindings;
        IReadOnlyList<ITypeInstanceConfiguration> IApplicationConfiguration.Filters => this.Filters;
        IReadOnlyDictionary<string, string> IApplicationConfiguration.MediaTypes => this.MediaTypes;
    }
}