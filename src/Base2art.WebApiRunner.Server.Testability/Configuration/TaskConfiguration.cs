﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Web.App.Configuration;

    public class TaskConfiguration : ITaskConfiguration
    {
        private string url;
        private bool urlSet;

        public Dictionary<string, object> Parameters { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> Properties { get; } = new Dictionary<string, object>();

        public List<AspectConfiguration> Aspects { get; } = new List<AspectConfiguration>();

        public string Name { get; set; }

        public TimeSpan Delay { get; set; }

        public TimeSpan Interval { get; set; }

        public HttpVerb Verb { get; set; } = HttpVerb.Post;

        public string Url
        {
            get => this.urlSet ? this.url : $"tasks/{this.Name}";
            set
            {
                this.url = value;
                this.urlSet = true;
            }
        }

        public Type Type { get; set; }

        public MethodInfo Method { get; set; }

        IReadOnlyList<ITypeInstanceConfiguration> IEndpointConfiguration.Aspects => this.Aspects;

        IReadOnlyDictionary<string, object> ITypeInstanceConfiguration.Parameters => this.Parameters;
        IReadOnlyDictionary<string, object> ITypeInstanceConfiguration.Properties => this.Properties;
    }
}