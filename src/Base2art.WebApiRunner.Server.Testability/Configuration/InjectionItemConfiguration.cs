﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System;
    using System.Collections.Generic;
    using Web.App.Configuration;

    public class InjectionItemConfiguration : IInjectionItemConfiguration
    {
        public Dictionary<string, object> Parameters { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> Properties { get; } = new Dictionary<string, object>();
        public Type RequestedType { get; set; }
        public Type FulfillingType { get; set; }

        public bool IsSingleton { get; set; }

        IReadOnlyDictionary<string, object> IInjectionItemConfiguration.Parameters => this.Parameters;
        IReadOnlyDictionary<string, object> IInjectionItemConfiguration.Properties => this.Properties;
    }
}