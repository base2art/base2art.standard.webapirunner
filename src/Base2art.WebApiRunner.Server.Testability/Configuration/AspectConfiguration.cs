﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Web.App.Configuration;

    public class AspectConfiguration : ICallableMethodConfiguration
    {
        public Dictionary<string, object> Parameters { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> Properties { get; } = new Dictionary<string, object>();
        public Type Type { get; set; }
        IReadOnlyDictionary<string, object> ITypeInstanceConfiguration.Parameters => this.Parameters;
        IReadOnlyDictionary<string, object> ITypeInstanceConfiguration.Properties => this.Properties;
        public MethodInfo Method { get; set; }
    }
}