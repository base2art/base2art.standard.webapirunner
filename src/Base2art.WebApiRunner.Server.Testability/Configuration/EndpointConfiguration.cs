﻿namespace Base2art.WebApiRunner.Server.Testability.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Web.App.Configuration;

    public class EndpointConfiguration : IEndpointConfiguration
    {
        public List<AspectConfiguration> Aspects { get; } = new List<AspectConfiguration>();
        public Dictionary<string, object> Parameters { get; } = new Dictionary<string, object>();
        public Dictionary<string, object> Properties { get; } = new Dictionary<string, object>();

        public HttpVerb Verb { get; set; }
        public string Url { get; set; }

        public Type Type { get; set; }
        public MethodInfo Method { get; set; }

        IReadOnlyList<ITypeInstanceConfiguration> IEndpointConfiguration.Aspects => this.Aspects;

        IReadOnlyDictionary<string, object> ITypeInstanceConfiguration.Parameters => this.Parameters;
        IReadOnlyDictionary<string, object> ITypeInstanceConfiguration.Properties => this.Properties;
    }
}