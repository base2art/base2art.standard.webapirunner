﻿namespace Base2art.WebApiRunner.Server.Testability
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.AspNetCore.TestHost;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;
    using Web.Server.Registration;
    using Web.ServerEnvironment;

    /// <summary>
    ///     A test fixture which hosts the target project (project we wish to test) in an in-memory server.
    /// </summary>
    /// <typeparam name="TStartup">Target project's startup type</typeparam>
    public class TestFixture<TStartup> : IDisposable
        where TStartup : class
    {
        private readonly CookieContainer cookies = new CookieContainer();
        private readonly TestServer server;

        public TestFixture(IServerConfiguration configuration, IFrameworkShim shim, IWebServerSettings webServerSettings, params IRegistration[] registrations)
        {
            var builder = new WebHostBuilder();
            builder.UseEnvironment("Development")
                   .ConfigureServices(x =>
                   {
                       x.Add(ServiceDescriptor.Singleton(configuration));
                       x.Add(ServiceDescriptor.Singleton(registrations));
                       x.Add(ServiceDescriptor.Singleton(shim));
                       var registrar = new BoundTypeRegistrar();
                       x.Add(ServiceDescriptor.Singleton(typeof(IBoundTypeRegistrar), registrar));
                       x.Add(ServiceDescriptor.Singleton(typeof(IWebServerSettings), webServerSettings));
                       x.Add(ServiceDescriptor.Singleton(typeof(IBoundTypeLookup), registrar));
                   })
                   .UseStartup<TStartup>();

            this.server = new TestServer(builder);

#if NET5_0 || NET6_0 || NET7_0 || NET8_0 || NETCOREAPP3_1 || NETCOREAPP3_0
            this.server.AllowSynchronousIO = true;
#endif

            this.Client = this.server.CreateClient();
            this.Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.Client.BaseAddress = new Uri("http://localhost");
        }

        public HttpClient Client { get; }

        public void Dispose()
        {
            this.Client.Dispose();
            this.server.Dispose();
        }

        private class BoundTypeRegistrar : IBoundTypeRegistrar, IBoundTypeLookup
        {
            private readonly Dictionary<Type, BindingSource> lookup = new Dictionary<Type, BindingSource>();

            public void AddType<T>(BindingSource source)
            {
                this.AddType(typeof(T), source);
            }

            public void AddType(Type type, BindingSource source)
            {
                this.lookup[type] = source;
            }

            public bool Contains(Type modelType) => this.lookup.ContainsKey(modelType);

            public BindingSource this[Type modelType] => this.lookup[modelType];
        }
    }
}