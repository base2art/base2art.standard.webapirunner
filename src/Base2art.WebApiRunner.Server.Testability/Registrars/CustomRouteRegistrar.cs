﻿namespace Base2art.WebApiRunner.Server.Testability.Registrars
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Net.Http;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class CustomRouteRegistrar<T> : RegistrationBase
    {
        private readonly Expression<Action<T>> expr;
        private readonly string routeTemplate;

        public CustomRouteRegistrar(Expression<Action<T>> expr) : this(string.Empty, expr)
        {
        }

        public CustomRouteRegistrar(string routeTemplate, Expression<Action<T>> expr)
        {
            this.routeTemplate = routeTemplate;
            this.expr = expr;
        }

        public override void RegisterRoutes(IRouteManager router, IServiceProvider provider, IServerConfiguration config)
        {
            base.RegisterRoutes(router, provider, config);

            router.MapRoute(
                            HttpMethod.Get,
                            this.routeTemplate,
                            this.expr,
                            new Dictionary<string, object>(),
                            new Dictionary<string, object>(),
                            RouteType.Endpoint);
        }
    }
}