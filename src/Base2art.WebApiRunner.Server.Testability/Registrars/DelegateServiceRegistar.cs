﻿namespace Base2art.WebApiRunner.Server.Testability.Registrars
{
    using System;
    using ComponentModel.Composition;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class DelegateServiceRegistar : RegistrationBase
    {
        private readonly Action<IBindableServiceLoaderInjector> services;

        public DelegateServiceRegistar(Action<IBindableServiceLoaderInjector> services) => this.services = services;

        public override void RegisterUserServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services, IServerConfiguration config)
        {
            base.RegisterUserServices(builder, services, config);

            this.services(services);
        }
    }
}