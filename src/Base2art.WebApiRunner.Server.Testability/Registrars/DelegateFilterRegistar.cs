﻿namespace Base2art.WebApiRunner.Server.Testability.Registrars
{
    using System;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class DelegateFilterRegistar : RegistrationBase
    {
        private readonly Func<IServiceProvider, IActionFilter> serviceRegistar;

        public DelegateFilterRegistar(Func<IServiceProvider, IActionFilter> serviceRegistar) => this.serviceRegistar = serviceRegistar;

        public override void RegisterFilters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterFilters(builder, services, config);

            var result = this.serviceRegistar(services);
            builder.AddMvcOptions(x => x.Filters.Add(result));
        }
    }
}