﻿namespace Base2art.WebApiRunner.Server.Configuration.Loader
{
    using System.IO;
    using YamlDotNet.Serialization;
    using YamlDotNet.Serialization.NamingConventions;

    public class YamlService : IConfigSerializer, IConfigDeserializer
    {
        public T Deserialize<T>(string input)
        {
            var deserializer = new DeserializerBuilder()
                               .WithNamingConvention(new CamelCaseNamingConvention())
                               .Build();

            using (var sr = new StringReader(input))
            {
                return deserializer.Deserialize<T>(sr);
            }
        }

        public string Serialize<T>(T value)
        {
            var deserializer = new SerializerBuilder()
                               .WithNamingConvention(new CamelCaseNamingConvention())
                               .Build();

            using (var sw = new StringWriter())
            {
                deserializer.Serialize(sw, value);
                sw.Flush();

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}