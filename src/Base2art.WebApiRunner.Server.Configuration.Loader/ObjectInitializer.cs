namespace Base2art.WebApiRunner.Server.Configuration.Loader
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    internal static class ObjectInitializer
    {
        public static void SetDefaultValue<T>(Expression<Func<T>> expr)
            where T : class, new()
        {
            var item = expr.Compile().Invoke();

            if (item != null)
            {
                return;
            }

            var memberExpression = (MemberExpression) expr.Body;
            var property = (PropertyInfo) memberExpression.Member;
            var setMethod = property.GetSetMethod();

            var parameterTProperty = Expression.Parameter(typeof(T), "y");
            var newExpression =
                Expression.Lambda<Action<T>>(
                                             Expression.Call(memberExpression.Expression, setMethod, parameterTProperty),
                                             parameterTProperty);

            newExpression.Compile()(new T());
        }

        public static T[] GetValueOrDefault<T>(this T[] item) => item ?? new T[0];

        public static T GetValueOrDefault<T>(this T item)
            where T : class, new() => item ?? new T();
    }
}