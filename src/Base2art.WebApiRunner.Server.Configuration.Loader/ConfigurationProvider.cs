﻿namespace Base2art.WebApiRunner.Server.Configuration.Loader
{
    using System;
    using System.IO;

    public class ConfigurationProvider<TConfig> : IConfigurationProvider<TConfig>
        where TConfig : class, new()
    {
        private readonly string configPath;
        private readonly YamlService yaml = new YamlService();
        private TConfig configuration;

        public ConfigurationProvider(string configPath)
        {
            this.configPath = configPath;
            this.BasePath = Path.GetDirectoryName(configPath);
        }

        public string BasePath { get; }

        public TConfig Configuration
        {
            get
            {
                if (this.configuration == null)
                {
                    this.configuration = this.CreateConfiguration() ?? new TConfig();
                }

                return this.configuration;
            }
        }

        public void Reload()
        {
            this.configuration = default;
        }

        T IConfigurationProvider.RelativeConfiguration<T>(string relativePath)
        {
            var path = Path.Combine(this.BasePath, relativePath);
            using (var input = File.OpenRead(path))
            {
                using (var sr = new StreamReader(input))
                {
                    var data = sr.ReadToEnd();

                    var dataObj = this.yaml.Deserialize<T>(data);
                    return dataObj;
                }
            }
        }

        public void Write(TConfig configuration)
        {
            throw new NotImplementedException();
        }

        private TConfig CreateConfiguration()
        {
            using (var input = File.OpenRead(this.configPath))
            {
                using (var sr = new StreamReader(input))
                {
                    var data = sr.ReadToEnd();

                    var dataObj = this.yaml.Deserialize<TConfig>(data);
                    return dataObj;
                }
            }
        }
    }
}