﻿namespace Base2art.WebApiRunner.Server.Configuration.Loader
{
    using System.Collections.Generic;
    using System.Linq;
    using Types;

    public static class ConfigurationProviderExtender
    {
        public static InstanceConfiguration[] JointModules<T>(this IConfigurationProvider<T> init)
            where T : ServerConfiguration
            => init?.Configuration?.Modules.GetValueOrDefault().ToArray();

        public static EndpointConfiguration[] JointEndpoints<T>(this IConfigurationProvider<T> init)
            where T : ServerConfiguration
            => init?.Configuration?.Endpoints
                   .GetValueOrDefault()
                   .JoinTo(init, init?.Configuration?.EndpointsConfigurationItems.GetValueOrDefault());

        public static TasksConfiguration[] JointTasks<T>(this IConfigurationProvider<T> init)
            where T : ServerConfiguration
            => init?.Configuration?.Tasks
                   .GetValueOrDefault()
                   .JoinTo(init, init?.Configuration?.TasksConfigurationItems.GetValueOrDefault());

        public static HealthChecksConfiguration[] JointHealthChecks<T>(this IConfigurationProvider<T> init)
            where T : ServerConfiguration
            => init?.Configuration?.HealthChecks
                   .GetValueOrDefault()
                   .JoinTo(init, init?.Configuration?.HealthChecksConfigurationItems.GetValueOrDefault());

        public static ApplicationConfiguration[] JointApplications<T>(this IConfigurationProvider<T> init)
            where T : ServerConfiguration
            => init?.Configuration?.Application
                   .GetValueOrDefault()
                   .JoinTo(init, init?.Configuration?.ApplicationConfigurationItems.GetValueOrDefault());

        public static InjectionConfiguration[] JointInjections<T>(this IConfigurationProvider<T> init)
            where T : ServerConfiguration
            => init?.Configuration?.Injection
                   .GetValueOrDefault()
                   .JoinTo(init, init?.Configuration?.InjectionConfigurationItems.GetValueOrDefault());

//        public static MiddlewareConfiguration[] JointMiddleware<T>(this IConfigurationProvider<T> init)
//            where T : ServerConfiguration
//            => init?.Configuration?.Middleware
//                   .GetValueOrDefault()
//                   .JoinTos(init, init?.Configuration?.MiddlewareConfigurationItems.GetValueOrDefault());

        public static AssemblyConfiguration[] JointAssemblies<T>(this IConfigurationProvider<T> init)
            where T : ServerConfiguration
            => init?.Configuration?.Assemblies
                   .GetValueOrDefault()
                   .JoinTo(init, init?.Configuration?.AssembliesConfigurationItems.GetValueOrDefault());

//        public static string[] JointAssemblySearchPaths<T>(this IConfigurationProvider<T> init)
//            where T : ServerConfiguration
//        {
//            return init?.Configuration?.AssemblySearchPaths.JoinTos(init, init?.Configuration?.AssemblySearchPathsConfigurationItems);
//        }

        public static CorsConfiguration[] JointCors<T>(this IConfigurationProvider<T> init)
            where T : ServerConfiguration
            => init?.Configuration?.Cors
                   .GetValueOrDefault().JoinTo(init, init?.Configuration?.CorsConfigurationItems.GetValueOrDefault());

        private static T[] JoinTo<T>(this T item, IConfigurationProvider initer, PathList paths)
        {
            if (item == null && paths == null)
            {
                return new T[0];
            }

            if (paths == null)
            {
                return new[] {item};
            }

            if (item == null)
            {
                return paths.Select(initer.RelativeConfiguration<T>).ToArray();
            }

            return new[] {item}.Union(paths.Select(initer.RelativeConfiguration<T>)).ToArray();
        }

        private static T[] JoinTos<T>(this IEnumerable<T> items, IConfigurationProvider initer, PathList paths)
        {
            if (items == null && paths == null)
            {
                return new T[0];
            }

            if (paths == null)
            {
                return items.ToArray();
            }

            if (items == null)
            {
                return paths.SelectMany(initer.RelativeConfiguration<List<T>>).Where(x => x != null).ToArray();
            }

            return items.Union(paths.SelectMany(initer.RelativeConfiguration<List<T>>)).Where(x => x != null).ToArray();
        }
    }
}