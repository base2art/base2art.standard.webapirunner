﻿namespace Base2art.WebApiRunner.Server.Configuration.Loader
{
    public interface IConfigurationSystem<TConfig> : IConfigurationProvider<TConfig>
    {
        void Write(TConfig configuration);
    }
}