namespace Base2art.WebApiRunner.Server.Configuration.Loader
{
    public interface IConfigSerializer
    {
        string Serialize<T>(T value);
    }
}