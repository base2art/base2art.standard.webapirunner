﻿namespace Base2art.WebApiRunner.Server.Configuration.Loader
{
    public interface IConfigurationProvider
    {
        T RelativeConfiguration<T>(string relativePath);
    }

    public interface IConfigurationProvider<out TConfig> : IConfigurationProvider
    {
        TConfig Configuration { get; }

//        void Write(TConfig configuration);

        void Reload();
    }
}