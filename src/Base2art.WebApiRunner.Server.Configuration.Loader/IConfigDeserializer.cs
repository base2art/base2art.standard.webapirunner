namespace Base2art.WebApiRunner.Server.Configuration.Loader
{
    public interface IConfigDeserializer
    {
        T Deserialize<T>(string input);
    }
}