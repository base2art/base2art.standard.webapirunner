﻿namespace Base2art.WebApiRunner.Server.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Microsoft.AspNetCore.Mvc.Internal;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class AspectsRegistrar : RegistrationBase
    {
        public override void RegisterFilters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration serverConfiguration)
        {
            base.RegisterFilters(builder, services, serverConfiguration);

            var aspectLookup = new Dictionary<string, List<ITypeInstanceConfiguration>>();

            foreach (var methodConfiguration in serverConfiguration.Endpoints ?? new List<IEndpointConfiguration>())
            {
                var id = methodConfiguration.Id();

                if (!aspectLookup.ContainsKey(id))
                {
                    aspectLookup[id] = new List<ITypeInstanceConfiguration>();
                }

                var list = aspectLookup[id];

//                foreach (var endpointsConfig1 in serverConfiguration.Endpoints ?? new List<IMethodConfiguration>())
//                {
//                    list.AddRange(endpointsConfig1.Aspects ?? new List<IAspectConfiguration>());
//                }

                list.AddRange(methodConfiguration.Aspects ?? new List<ITypeInstanceConfiguration>());
            }

            foreach (var methodConfiguration in serverConfiguration.Tasks ?? new List<ITaskConfiguration>())
            {
                var id = methodConfiguration.Id();

                if (!aspectLookup.ContainsKey(id))
                {
                    aspectLookup[id] = new List<ITypeInstanceConfiguration>();
                }

                var list = aspectLookup[id];

                list.AddRange(methodConfiguration.Aspects ?? new List<ITypeInstanceConfiguration>());
            }

            var hca = (serverConfiguration?.HealthChecks?.Aspects ?? new List<ICallableMethodConfiguration>()).ToList();

            foreach (var methodConfiguration in serverConfiguration?.HealthChecks?.Items ?? new List<IHealthCheckConfiguration>())
            {
                var id = methodConfiguration.Id();

                if (!aspectLookup.ContainsKey(id))
                {
                    aspectLookup[id] = hca;
                }
            }

            aspectLookup[$"{HttpMethod.Get}:healthchecks"] = hca;

            builder.AddMvcOptions(x => x.Filters.Add(new AspectFilter(
                                                                      services,
                                                                      serverConfiguration,
                                                                      aspectLookup)));
        }
    }
}