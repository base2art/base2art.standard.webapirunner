﻿namespace Base2art.WebApiRunner.Server.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Base2art.Aspects;
    using Base2art.Aspects.Web;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Controllers;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.AspNetCore.Mvc.Internal;
    using Microsoft.Extensions.DependencyInjection;
    using Reflection.Activation;
    using Web.App.Configuration;

    public class AspectFilter : IAsyncActionFilter, IOrderedFilter
    {
        private readonly IReadOnlyDictionary<string, List<ITypeInstanceConfiguration>> aspectLookup;
        private readonly IServerConfiguration configuration;
        private readonly IServiceProvider provider;

        public AspectFilter(
            IServiceProvider provider,
            IServerConfiguration configuration,
            IReadOnlyDictionary<string, List<ITypeInstanceConfiguration>> aspectLookup)
        {
            this.provider = provider;
            this.configuration = configuration;
            this.aspectLookup = aspectLookup;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var values = context.RouteData.Values;
            const string key = "5123D25E-BBA5-44D6-B306-EA915F3B7909";
            const string tagKey = "8E23DB9E-D37F-488B-AAED-54AA6D6CBBAE";

            var tag = "";

            if (values.ContainsKey(tagKey))
            {
                tag = values[tagKey] as string;
            }

            if (values.ContainsKey(key) && this.aspectLookup.TryGetValue((string) values[key], out var aspects))
            {
                ActionExecutedContext resultOuter = null;
                var descriptor = (ControllerActionDescriptor) context.ActionDescriptor;

                object Create(Type aspectType, IReadOnlyDictionary<string, object> parameters, IReadOnlyDictionary<string, object> properties)
                {
                    if (this.provider.GetService(aspectType) != null)
                    {
                        return this.provider.GetService(aspectType);
                    }

                    var lookup = this.provider?.GetService<IServiceLookup>();

                    return aspectType.CreateFrom(lookup, parameters, properties);
                }

                var apectPlainObjs = aspects.Select(aspect => Tuple.Create(aspect, Create(aspect.Type, aspect.Parameters, aspect.Properties)))
                                            .Select(x => x.Item2)
                                            .Where(x => x is IHttpAspect || x is IAspect)
                                            .ToArray();

                var parameterInfos = descriptor.MethodInfo.GetParameters();
                var contextActionArguments = context.ActionArguments;

                object createItem(ParameterInfo pi)
                {
                    if (pi.HasDefaultValue)
                    {
                        return pi.DefaultValue;
                    }

                    if (pi.ParameterType.IsValueType)
                    {
                        return Activator.CreateInstance(pi.ParameterType);
                    }

                    return null;
                }

                var parms = parameterInfos.Select(x => contextActionArguments.ContainsKey(x.Name)
                                                           ? contextActionArguments[x.Name]
                                                           : createItem(x));

                var arguments = Arguments.Create(parameterInfos, parms.ToArray());

                var item = new MethodInterceptionArgs
                           {
                               Arguments = arguments,
                               Instance = context.Controller,
                               Method = descriptor.MethodInfo,
                               ReturnValue = null
                           };

                Func<Task> backingAction = async () =>
                {
                    for (var i = 0; i < parameterInfos.Length; i++)
                    {
                        var pi = parameterInfos[i];
                        contextActionArguments[pi.Name] = arguments[i];
                    }

                    var result = await next();

                    if (result.Result is ObjectResult objResult)
                    {
                        item.ReturnValue = objResult.Value;
                    }
                    else
                    {
                        item.ReturnValue = result.Result;
                    }

                    resultOuter = result;
                };

                foreach (var aspect in apectPlainObjs)
                {
                    if (aspect is IHttpAspect webAspect)
                    {
                        var action1 = backingAction;
                        backingAction = async () =>
                        {
                            var action = action1;
                            await webAspect.OnInvoke(
                                                     item,
                                                     context.HttpContext.Request(),
                                                     context.HttpContext.Response(),
                                                     async () => { await action(); });
                        };
                    }
                    else if (aspect is IAspect aspectPlain)
                    {
                        var action1 = backingAction;
                        backingAction = async () =>
                        {
                            var action = action1;
                            await aspectPlain.OnInvoke(item, async () => { await action(); });
                        };
                    }
                    else
                    {
                        throw new InvalidCastException($"aspect of type (`{aspect?.GetType()}` is not an IHttpAspect or IAspect)");
                    }
                }

                await backingAction();

                if (resultOuter.Result is ObjectResult objResult1)
                {
                    objResult1.Value = item.ReturnValue;
                }
                else
                {
                    resultOuter.Result = (IActionResult) item.ReturnValue;
                }
            }
            else
            {
                await next();
            }
        }

        public int Order => 1000;

        public class MethodInterceptionArgs : IMethodInterceptionArgs
        {
            public Arguments Arguments { get; set; }
            public object ReturnValue { get; set; }
            public object Instance { get; set; }
            public MethodInfo Method { get; set; }
        }
    }
}