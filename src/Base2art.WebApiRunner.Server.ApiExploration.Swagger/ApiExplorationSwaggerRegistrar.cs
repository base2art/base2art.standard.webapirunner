﻿namespace Base2art.WebApiRunner.Server.ApiExploration.Swagger
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.Swagger;
    using Text;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class ApiExplorationSwaggerRegistrar : RegistrationBase
    {
        public override void RegisterApiExploration(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterApiExploration(builder, services, config);
            builder.Services.AddSwaggerGen(c =>
            {
                c.TagActionsBy(api =>
                {
                    if (api.RelativePath == null)
                    {
                        return new List<string> {"Unknown"};
                    }

                    var groupName = api.RelativePath.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries)
                                       .TakeWhile(x => !x.Contains("{"))
                                       .Join("/");

                    return new List<string> {groupName};
                });

                c.IgnoreObsoleteProperties();
                c.IgnoreObsoleteActions();
//                        c.UseFullTypeNameInSchemaIds();

                c.OperationFilter<AddDefaultResponse>(config.Application.ModelBindings);
                c.DescribeAllEnumsAsStrings();
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "API", Version = "v1"});
//                        c.SingleApiVersion("v1", this.ApiName);
            });
        }

        public override void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
            base.RegisterNativeMiddleware(app, config);
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });
        }
    }
}