﻿namespace Base2art.WebApiRunner.Server.ApiExploration.Swagger
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using Web.App.Configuration;

    public class AddDefaultResponse : IOperationFilter
    {
        private readonly IReadOnlyList<IModelBindingConfiguration> modelBindingConfigurations;

        public AddDefaultResponse(IReadOnlyList<IModelBindingConfiguration> modelBindingConfigurations)
            => this.modelBindingConfigurations = modelBindingConfigurations;

        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var apiDescription = context.ApiDescription;
            if (apiDescription.ParameterDescriptions.Count == 0)
            {
                return;
            }

            var comp = StringComparison.Ordinal;
            var parms = apiDescription.ParameterDescriptions
                                      .Where(x => x.Type.IsAbstract || x.Type.IsInterface)
                                      .ToArray();

            RemoveItems(operation, parms, x => x.Name, (x, name) => x.Name.StartsWith(name + ".", comp));

            var parmsSpecial = apiDescription.ParameterDescriptions
                                             .Where(x => x?.ModelMetadata?.BindingSource == BindingSource.Special)
                                             .ToArray();

            RemoveItems(
                        operation,
                        parmsSpecial,
                        x => x.Name,
                        (x, name) => string.Equals(x.Name, name, comp));

            var bound = new HashSet<Type>(this.modelBindingConfigurations
                                              .Where(x => x.BindingType == BindingType.Hidden)
                                              .Select(x => x.BoundType)
                                              .ToArray());

            var parmsBound = apiDescription.ParameterDescriptions
                                           .Where(x => bound.Contains(x.Type))
                                           .ToArray();

            RemoveItems(operation, parmsBound, x => x.Name, (x, name) => string.Equals(x.Name, name, comp));
        }

        private static void RemoveItems<T>(OpenApiOperation operation, T[] parms, Func<T, string> nameLookup, Func<OpenApiParameter, string, bool> compare)
        {
            foreach (var parm in parms)
            {
                var name = nameLookup(parm);

                var items = operation.Parameters.Where(x => compare(x, name)).ToArray();

                foreach (var item in items)
                {
                    operation.Parameters.Remove(item);
                }
            }
        }
    }
}