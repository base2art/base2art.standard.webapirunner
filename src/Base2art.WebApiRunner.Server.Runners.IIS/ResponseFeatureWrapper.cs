namespace Base2art.WebApiRunner.Server.Runners.IIS
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Features;

    internal class ResponseFeatureWrapper : IHttpResponseFeature
    {
        private readonly IHttpResponseFeature feature;

        public ResponseFeatureWrapper(IHttpResponseFeature feature) => this.feature = feature;

        public void OnStarting(Func<object, Task> callback, object state)
        {
            this.feature.OnStarting(callback, state);
        }

        public void OnCompleted(Func<object, Task> callback, object state)
        {
        }

        public int StatusCode
        {
            get => this.feature.StatusCode;
            set => this.feature.StatusCode = value;
        }

        public string ReasonPhrase
        {
            get => this.feature.ReasonPhrase;
            set => this.feature.ReasonPhrase = value;
        }

        public IHeaderDictionary Headers
        {
            get => this.feature.Headers;
            set => this.feature.Headers = value;
        }

        public Stream Body
        {
            get => this.feature.Body;
            set => this.feature.Body = value;
        }

        public bool HasStarted => this.feature.HasStarted;
    }
}