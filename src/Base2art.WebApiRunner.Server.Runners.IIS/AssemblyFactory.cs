namespace Base2art.WebApiRunner.Server.Runners.IIS
{
    using System.Collections.Generic;
    using System.Reflection;
    using Reflection.Discovery;

    internal class AssemblyFactory : IAssemblyFactory
    {
        public bool Supports(string rid) => false;
        public Assembly CreateFromPath(string path, bool forceRootLoadingLoader) => Assembly.LoadFile(path);

        public void RegisterUnmanaged(IReadOnlyList<IPackagedNativeDll> valueTupleUnmanaged)
        {
        }
    }
}