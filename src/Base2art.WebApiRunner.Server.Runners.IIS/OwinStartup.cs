using Base2art.WebApiRunner.Server.Runners.IIS;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(OwinStartup))]

namespace Base2art.WebApiRunner.Server.Runners.IIS
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Security;
    using Configuration;
    using Microsoft.Owin.Extensions;
    using Owin;
    using Reflection.Discovery;
    using Web.App.LifeCycle;

    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            // Paths in ASP.NET Core will not start with /api but with /

//            var config = new ServerConfiguration();

            var file = ConfigurationManager.AppSettings["configFile"];
            if (string.IsNullOrWhiteSpace(file))
            {
                file = "config/configuration.yaml";
            }

            try
            {
                Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            }
            catch (SecurityException)
            {
            }

            var localPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file);

            var (configPrimitive1, config1, registrations1) = ConsoleServerConfigurations.CreateConsoleServerConfiguration(
                                                                                                                           localPath,
                                                                                                                           FrameworkType.NetFx,
                                                                                                                           GetFrameworkVersions(),
                                                                                                                           new AssemblyFactory()
                                                                                                                          );

            var config = config1;
            IApplication application = new NullApplication();
            var registrations = registrations1;

            app.MapWhen(x => true, subApp => subApp.Use<CommandedAspNetCoreOwinMiddleware>(
                                                                                           app,
                                                                                           configPrimitive1,
                                                                                           config,
                                                                                           application,
                                                                                           registrations));
            app.UseStageMarker(PipelineStage.MapHandler);
        }

        private static string[] GetFrameworkVersions()
        {
            // <TargetFrameworks>net472;net48;netstandard1.3;netstandard2.0;netcoreapp2.1;netcoreapp2.2;netcoreapp3.0</TargetFrameworks>
            var netStandards = new[]
                               {
                                   "netstandard2.0",
                                   "netstandard1.6",
                                   "netstandard1.5",
                                   "netstandard1.4",
                                   "netstandard1.3",
                                   "netstandard1.2",
                                   "netstandard1.1",
                                   "netstandard"
                               };

            var net461 = new[] {"net461", "net46", "net452", "net451", "net45", "net"}.Concat(netStandards).ToArray();
            var net462 = new[] {"net462"}.Concat(net461).ToArray();
            var net47 = new[] {"net47"}.Concat(net462).ToArray();
            var net471 = new[] {"net471"}.Concat(net47).ToArray();
            var net472 = new[] {"net472"}.Concat(net471).ToArray();
            var net48 = new[] {"net48"}.Concat(net472).ToArray();
#if NET461
            return net461;
#elif NET462
            return net462;
#elif NET47
            return net47;
#elif NET471
            return net471;
#elif NET472
            return net472;
#elif NET48
            return net48;
#else
            return netStandards;
#endif
        }
    }
}