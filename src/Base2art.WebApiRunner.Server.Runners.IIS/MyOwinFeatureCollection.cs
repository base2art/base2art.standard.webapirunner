namespace Base2art.WebApiRunner.Server.Runners.IIS
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http.Features;
    using Microsoft.AspNetCore.Owin;

    public class MyOwinFeatureCollection : IFeatureCollection
    {
        private readonly OwinFeatureCollection owinFeatureCollection;

        public MyOwinFeatureCollection(OwinFeatureCollection owinFeatureCollection) => this.owinFeatureCollection = owinFeatureCollection;

        public IEnumerator<KeyValuePair<Type, object>> GetEnumerator()
        {
            using (var enumer = this.owinFeatureCollection.GetEnumerator())
            {
                while (enumer.MoveNext())
                {
                    // now empEnumerator.Current is the Employee instance without casting
                    var emp = enumer.Current;

                    if (emp.Key != typeof(IHttpResponseFeature))
                    {
                        yield return emp;
                    }

                    var key = emp.Key;
                    var value = emp.Value;
                    yield return new KeyValuePair<Type, object>(emp.Key, new ResponseFeatureWrapper((IHttpResponseFeature) emp.Value));
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public TFeature Get<TFeature>() => this.owinFeatureCollection.Get<TFeature>();

        public void Set<TFeature>(TFeature instance)
        {
            this.owinFeatureCollection.Set(instance);
        }

        public bool IsReadOnly => this.owinFeatureCollection.IsReadOnly;

        public int Revision => this.owinFeatureCollection.Revision;

        public object this[Type key]
        {
            get
            {
                var owinFeature = this.owinFeatureCollection[key];

                return key == typeof(IHttpResponseFeature)
                           ? new ResponseFeatureWrapper((IHttpResponseFeature) owinFeature)
                           : owinFeature;
            }
            set => this.owinFeatureCollection[key] = value;
        }
    }
}