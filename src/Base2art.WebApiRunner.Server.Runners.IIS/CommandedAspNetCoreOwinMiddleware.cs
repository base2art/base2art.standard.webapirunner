namespace Base2art.WebApiRunner.Server.Runners.IIS
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Commands;
    using Logging;
    using Microsoft.AspNetCore.Hosting.Server;
    using Microsoft.AspNetCore.Http.Features;
    using Microsoft.AspNetCore.Owin;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Owin;
    using Microsoft.Owin.BuilderProperties;
    using Owin;
    using Server.Configuration.CommandLine;
    using Web.App.Configuration;
    using Web.App.LifeCycle;

    public class CommandedAspNetCoreOwinMiddleware : OwinMiddleware, IServer
    {
        private readonly CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
        private readonly IReadOnlyList<ICommand> commands;
        private readonly IServerConfiguration config;
        private readonly Stack<ICommand> running = new Stack<ICommand>();
        private readonly Task startup;

        private Func<IOwinContext, Task> appFunc;

        public CommandedAspNetCoreOwinMiddleware(
            OwinMiddleware next,
            IAppBuilder app,
            ConsoleServerConfiguration configuration,
            IServerConfiguration config,
            IApplication application,
            IRegistrationHolder registrations)
            : base(next)
        {
            this.config = config;
            this.commands = new ICommand[]
                            {
                                new SetLogLevelCommand(),
                                new SetConsoleToFileCommand("iis"),
                                new ConsoleServerCommand(application, configuration, registrations, new FrameworkShim(), RunType.IIS_Legacy)
                                    .AddMiddlewareServicesRegistrations(x => x.AddSingleton<IServer>(this))
                            };

            var appProperties = new AppProperties(app.Properties);
            if (appProperties.OnAppDisposing != default)
            {
                appProperties.OnAppDisposing.Register(this.Dispose);
            }

            async Task StartMyApp()
            {
                foreach (var command in this.commands ?? new ICommand[0])
                {
                    this.running.Push(command);
                    var result = await command.Run(this.config);

                    if (result != 0)
                    {
                        return;
                    }
                }
            }

            this.startup = StartMyApp();
        }

        IFeatureCollection IServer.Features { get; } = new FeatureCollection();

        public Task StartAsync<TContext>(IHttpApplication<TContext> application, CancellationToken cancellationToken)
        {
            this.appFunc = async owinContext =>
            {
                var owinFeatureCollection = new MyOwinFeatureCollection(new OwinFeatureCollection(owinContext.Environment));

                var features = new FeatureCollection(owinFeatureCollection);
                var context = application.CreateContext(features);

                try
                {
                    features.Get<IHttpResponseFeature>().Headers["Content-Type"] = "";
                    await application.ProcessRequestAsync(context);
                    application.DisposeContext(context, null);
                }
                catch (Exception ex)
                {
                    application.DisposeContext(context, ex);
                    throw;
                }
            };

            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            this.cancelTokenSource.Cancel();

            while (this.running.TryPop(out var cmd))
            {
                try
                {
                    await cmd.Shutdown();
                }
                catch (Exception e)
                {
                    Logger.Log(LogLevel.Error, e);
                }
            }
        }

        public void Dispose()
        {
        }

        public override async Task Invoke(IOwinContext context)
        {
            if (this.appFunc == null)
            {
                await this.startup;
            }

            if (this.appFunc != null)
            {
                await this.appFunc.Invoke(context);
            }
        }
    }
}