namespace Base2art.WebApiRunner.Server.ValueTypes
{
    using System;
    using System.Collections.Generic;
    using Web.App.Configuration;
    using Web.ValueTypes;

    public class Module : IModule
    {
        public IReadOnlyList<IEndpointConfiguration> Endpoints => new IEndpointConfiguration[0];

        public IReadOnlyList<ITypeInstanceConfiguration> GlobalEndpointAspects => new ITypeInstanceConfiguration[0];

        public IReadOnlyList<ITaskConfiguration> Tasks => new ITaskConfiguration[0];

        public IReadOnlyList<ITypeInstanceConfiguration> GlobalTaskAspects => new ITypeInstanceConfiguration[0];

        public IHealthChecksConfiguration HealthChecks => null;

        public IApplicationConfiguration Application => new ApplicationConfiguration();

        public IReadOnlyList<IInjectionItemConfiguration> Injection => new IInjectionItemConfiguration[0];

        public IReadOnlyList<ICorsItemConfiguration> Cors => new ICorsItemConfiguration[0];

        private class ApplicationConfiguration : IApplicationConfiguration
        {
            public IReadOnlyList<ITypeInstanceConfiguration> Filters => new List<ITypeInstanceConfiguration>();

            public IReadOnlyList<IModelBindingConfiguration> ModelBindings => new List<IModelBindingConfiguration>
                                                                              {
                                                                                  new ModelBindingConfiguration()
                                                                              };

            public IReadOnlyList<ITypeInstanceConfiguration> InputFormatters => new List<ITypeInstanceConfiguration>();
            public IReadOnlyList<ITypeInstanceConfiguration> OutputFormatters => new List<ITypeInstanceConfiguration>();

            public IReadOnlyDictionary<string, string> MediaTypes => new Dictionary<string, string>();
            public IExceptionConfiguration Exceptions => new ExceptionConfiguration();

            public class ExceptionConfiguration : IExceptionConfiguration
            {
                public bool RegisterCommonHandlers { get; } = false;

                public bool AllowStackTraceInOutput { get; } = false;

                public IReadOnlyList<ITypeInstanceConfiguration> Loggers { get; } = new List<ITypeInstanceConfiguration>();
            }

            private class ModelBindingConfiguration : IModelBindingConfiguration
            {
                public Type BoundType { get; } = typeof(DomainName);
                public Type BinderType { get; } = typeof(DomainNameBinder);

                public IReadOnlyDictionary<string, object> BinderParameters => new Dictionary<string, object>();

                public IReadOnlyDictionary<string, object> BinderProperties { get; } = new Dictionary<string, object>();
                public BindingType BindingType { get; } = BindingType.Hidden;
            }
        }
    }
}