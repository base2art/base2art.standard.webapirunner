namespace Base2art.WebApiRunner.Server.ValueTypes
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Web.Bindings;
    using Web.ValueTypes;

    public class DomainNameBinder : IModelBinding<DomainName>
    {
        public Task<DomainName> BindModelAsync(
            Uri uri,
            IReadOnlyDictionary<string, string> cookies,
            IReadOnlyDictionary<string, string> headers)
            => Task.FromResult(DomainName.Create(uri.Host));

        async Task<object> IModelBinding.BindModelAsync(
            Uri uri,
            IReadOnlyDictionary<string, string> cookies,
            IReadOnlyDictionary<string, string> headers)
            => await this.BindModelAsync(uri, cookies, headers);
    }
}