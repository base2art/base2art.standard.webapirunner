﻿namespace Base2art.Web.ValueTypes
{
    public class DomainName
    {
        private DomainName(string value) => this.Value = value;
        public string Value { get; }

        public static DomainName Create(string value) => new DomainName(value);
    }
}