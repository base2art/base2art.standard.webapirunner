namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.Globalization;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Abstractions;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    public class UrlEncodingInputFormatter : InputFormatter
    {
        public UrlEncodingInputFormatter()
        {
            this.SupportedMediaTypes.Add(MediaTypeHeaderValues.ApplicationFormEncoded);
        }

        public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
        {
            var request = context.HttpContext.Request;
            var binder = context.HttpContext.RequestServices.GetService<IModelBinderFactory>();
            var logger = context.HttpContext.RequestServices.GetService<ILoggerFactory>();
            var options = context.HttpContext.RequestServices.GetService<IOptions<MvcOptions>>();

            var modelMetadata = context.Metadata;

            var factoryContext = new ModelBinderFactoryContext()
                                 {
                                     Metadata = modelMetadata,
                                     BindingInfo = new BindingInfo()
                                                   {
                                                       BinderModelName = modelMetadata.BinderModelName,
                                                       BinderType = modelMetadata.BinderType,
                                                       BindingSource = BindingSource.Form,
                                                       PropertyFilterProvider = modelMetadata.PropertyFilterProvider,
                                                   },

                                     CacheToken = modelMetadata,
                                 };

            IObjectModelValidator objectModelValidator = context.HttpContext.RequestServices.GetService<IObjectModelValidator>();

            var parameterBinder = new ParameterBinder(modelMetadata, binder, objectModelValidator, options, logger);

            var form = await request.ReadFormAsync();

            var provider = new FormValueProvider(BindingSource.Form, form, CultureInfo.CurrentCulture);
            var actionContext = new ControllerContext {HttpContext = context.HttpContext};

            var modelBinder = binder.CreateBinder(factoryContext);

            var modelMetadataParameterName = context.ModelName;

            try
            {
                var result = await parameterBinder.BindModelAsync(
                                                                  actionContext,
                                                                  modelBinder,
                                                                  provider,
                                                                  new ParameterDescriptor
                                                                  {
                                                                      BindingInfo = factoryContext.BindingInfo,
                                                                      Name = modelMetadataParameterName,
                                                                      ParameterType = modelMetadata.ModelType
                                                                  },
                                                                  modelMetadata,
                                                                  null);

                if (result.IsModelSet)
                {
                    return InputFormatterResult.Success(result.Model);
                }

                return InputFormatterResult.NoValue();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}