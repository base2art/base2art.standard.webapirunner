﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc.Formatters;

    public abstract class OutputFormatter<T> : TextOutputFormatter
        where T : class
    {
        protected OutputFormatter(string mimeType)
        {
            this.SupportedEncodings.Add(Encoding.UTF8);
            this.SupportedEncodings.Add(Encoding.Unicode);
            this.SupportedMediaTypes.Add(mimeType);
        }

        protected override bool CanWriteType(Type type) => !type.GetTypeInfo().IsValueType;

        public override Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            if (!(context.Object is T realValue))
            {
                return Task.FromResult(true);
            }

            var response = context.HttpContext.Response;
            return this.WriteValueToStream(
                                           realValue,
                                           context.ObjectType,
                                           context.WriterFactory(response.Body, selectedEncoding),
                                           selectedEncoding);
        }

        protected abstract Task WriteValueToStream(T realValue, Type declaredType, TextWriter writeStream, Encoding selectedEncoding);
    }
}