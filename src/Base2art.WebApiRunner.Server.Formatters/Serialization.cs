using Base2art.Serialization.CodeGeneration;

[assembly: IncludeJsonSerializer("Base2art.WebApiRunner.Server.Formatters.Json", MakePublic = false)]