﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Json;
    using Microsoft.AspNetCore.Mvc.Formatters;

    public abstract class TableOutputFormatterBase : TextOutputFormatter
    {
        private static readonly CamelCasingIndentedSimpleJsonSerializer Serializer = new CamelCasingIndentedSimpleJsonSerializer();

        public override Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            return Task.Factory.StartNew(() =>
            {
                var type = context.ObjectType;
                var elementType = this.GetElementType(type);

                var enumerable = ((IEnumerable) context.Object).OfType<object>().ToList();

                using (var writer = context.WriterFactory(context.HttpContext.Response.Body, selectedEncoding))
                {
                    var setOfKeys = this.GetKeys(elementType, enumerable);
                    this.WriteItems(setOfKeys, enumerable, writer);
                }
            });
        }

        protected abstract void WriteItems(string[] setOfKeys, List<object> enumerable, TextWriter writer);

        protected virtual string[] GetKeys(Type elementType, IEnumerable enumerable)
        {
            var propertyNames = new HashSet<string>();

            foreach (var item in enumerable)
            {
                var elementItemType = item.GetType();
                var properties = elementItemType.GetProperties();

                foreach (var property in properties.Select(x => Tuple.Create(x, x.GetMethod)).Where(x => x.Item2 != null))
                {
                    propertyNames.Add(property.Item1.Name);
                }
            }

            return propertyNames.ToArray();
        }

        protected override bool CanWriteType(Type type)
        {
            if (!base.CanWriteType(type))
            {
                return false;
            }

            var componentType = this.GetElementType(type);

            if (componentType == null)
            {
                return false;
            }

            return this.ArgTypeConforms(componentType);
        }

        private bool ArgTypeConforms(Type type)
        {
            if (type.IsGenericType)
            {
                var gtd = type.GetGenericTypeDefinition();
                if (typeof(KeyValuePair<,>) == gtd)
                {
                    return true;
                }
            }

            return !type.IsValueType && type != typeof(string);
        }

        private Type GetElementType(Type type)
        {
            if (type.IsArray)
            {
                return type.GetElementType();
            }

            var enumType = typeof(IEnumerable);
            var interfaces = type.GetInterfaces();
            if (type.IsInterface)
            {
                interfaces = interfaces.Union(new[] {type}).ToArray();
            }

            var @interface = interfaces.OrderByDescending(x => x.IsGenericType).FirstOrDefault(enumType.IsAssignableFrom);
            if (@interface == null)
            {
                return null;
            }

            if (!@interface.IsGenericType)
            {
                return null;
            }

            return @interface.GetGenericArguments()[0];
        }

        protected static class InternalMembers
        {
            internal static IJsonSerializer Serializer => TableOutputFormatterBase.Serializer;
        }
    }
}