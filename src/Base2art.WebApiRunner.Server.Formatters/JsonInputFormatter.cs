﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Json;
    using Microsoft.AspNetCore.Mvc.Formatters;

    /// <summary>
    ///     A <see cref="TextInputFormatter" /> for JSON content.
    /// </summary>
    public class JsonInputFormatter : TextInputFormatter //, IInputFormatterExceptionPolicy
    {
        /// <summary>
        ///     Initializes a new instance of <see cref="JsonInputFormatter" />.
        /// </summary>
        public JsonInputFormatter()
        {
            this.SupportedEncodings.Add(UTF8EncodingWithoutBOM);
            this.SupportedEncodings.Add(UTF16EncodingLittleEndian);

            this.SupportedMediaTypes.Add(MediaTypeHeaderValues.ApplicationJson);
            this.SupportedMediaTypes.Add(MediaTypeHeaderValues.TextJson);
            this.SupportedMediaTypes.Add(MediaTypeHeaderValues.ApplicationAnyJsonSyntax);
        }

        public override IReadOnlyList<string> GetSupportedContentTypes(string contentType, Type objectType)
        {
            var item = base.GetSupportedContentTypes(contentType, objectType);
            return item;
        }

        public override bool CanRead(InputFormatterContext context)
        {
            var result = base.CanRead(context);

            return result;
        }

        protected override bool CanReadType(Type type)
        {
            var result = base.CanReadType(type);
            return result;
        }

        /// <inheritdoc />
        public override async Task<InputFormatterResult> ReadRequestBodyAsync(
            InputFormatterContext context,
            Encoding encoding)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            var request = context.HttpContext.Request;

            using (var streamReader = context.ReaderFactory(request.Body, encoding))
            {
                var type = context.ModelType;
                var content = await streamReader.ReadToEndAsync();

                var model = new CamelCasingSimpleJsonSerializer().Deserialize(content, type);
                return InputFormatterResult.Success(model);
            }
        }
    }
}