﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;

    public class MarkdownOutputFormatter : TableOutputFormatterBase
    {
        private const string ReplacementString = "⎮";
//        private const string ReplacementString = "\\|";

        public MarkdownOutputFormatter()
        {
            this.SupportedEncodings.Add(Encoding.UTF8);
            this.SupportedEncodings.Add(Encoding.Unicode);
            this.SupportedMediaTypes.Add("text/markdown");
        }

        protected override void WriteItems(string[] setOfKeys, List<object> enumerable, TextWriter writer)
        {
            if (true)
            {
                var columnNames = setOfKeys.Select(column => column.Replace("|", ReplacementString)).ToArray();
                writer.WriteLine(string.Join(" | ", columnNames));
                writer.Flush();

                var columnUnderscores = setOfKeys.Select(column => "---").ToArray();
                writer.WriteLine(string.Join("|", columnUnderscores));
                writer.Flush();
            }

            foreach (var row in enumerable)
            {
                var fields = this.GetProperties(row, setOfKeys).Select(
                                                                       field =>
                                                                       {
                                                                           var data = field as string;
                                                                           if (field != null && data == null && field != DBNull.Value)
                                                                           {
                                                                               data = InternalMembers.Serializer.Serialize(field);
                                                                               if (data.StartsWith("\"", StringComparison.OrdinalIgnoreCase)
                                                                                   && data.EndsWith("\"", StringComparison.OrdinalIgnoreCase))
                                                                               {
                                                                                   data = data.Substring(1, data.Length - 2);
                                                                               }
                                                                           }

                                                                           if (data == null)
                                                                           {
                                                                               data = string.Empty;
                                                                           }

                                                                           return data.Replace("|", ReplacementString);
                                                                       }).ToArray();
                writer.WriteLine(string.Join(" | ", fields));
                writer.Flush();
            }
        }

        private object[] GetProperties(object row, string[] keys)
        {
            var type = row.GetType();
            var properties = type.GetProperties();

            return keys.Select(x => this.GetItem(row, properties.FirstOrDefault(y => y.Name == x))).ToArray();
        }

        private object GetItem(object row, PropertyInfo prop)
        {
            if (prop == null)
            {
                return string.Empty;
            }

            return prop.GetMethod.Invoke(row, null);
        }
    }
}