﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class FormatRegistrar : RegistrationBase
    {
        public override void RegisterFormatters(IMvcCoreBuilder builder, IServiceProvider provider, IServerConfiguration serverConfiguration)
        {
            base.RegisterFormatters(builder, provider, serverConfiguration);

            builder.AddMvcOptions(options =>
            {
                options.RespectBrowserAcceptHeader = true;
                
                // options.OutputFormatters.Insert(0, new JsonOutputFormatter());
                // options.OutputFormatters.Insert(1, new PrettyJsonOutputFormatter());
                
                options.OutputFormatters.Insert(0, new PrettyJsonOutputFormatter());
                options.OutputFormatters.Insert(1, new JsonOutputFormatter());
                
                options.OutputFormatters.Insert(2, new YamlTextFormatter());

                options.InputFormatters.Insert(0, new JsonInputFormatter());
                options.InputFormatters.Insert(1, new UrlEncodingInputFormatter());

                var lookup = provider?.GetService<ICreator>();

                if (lookup == null)
                {
                    return;
                }

                var itemInputs = serverConfiguration.Application.InputFormatters
                                                    .Select(formatter => lookup.Create(formatter.Type, formatter.Parameters, formatter.Properties))
                                                    .OfType<IInputFormatter>();
                foreach (var formatter in itemInputs)
                {
                    options.InputFormatters.Add(formatter);
                }

                var itemOutputs = serverConfiguration.Application.OutputFormatters
                                                     .Select(formatter => lookup.Create(formatter.Type, formatter.Parameters, formatter.Properties))
                                                     .OfType<IOutputFormatter>();

                foreach (var formatter in itemOutputs)
                {
                    options.OutputFormatters.Add(formatter);
                }
            });

            builder.AddXmlSerializerFormatters();

            builder.AddFormatterMappings(x =>
            {
                x.SetMediaTypeMappingForFormat("pretty-json", "application/pretty-json");
                x.SetMediaTypeMappingForFormat("json", "application/json");
//                x.SetMediaTypeMappingForFormat("plain-text", "text/plain");
                x.SetMediaTypeMappingForFormat("yaml", "text/yaml");
                x.SetMediaTypeMappingForFormat("xml", "text/xml");
            });
        }
    }
}