﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Json;

    public class PrettyJsonOutputFormatter : OutputFormatter<object>
    {
        public PrettyJsonOutputFormatter() : base("application/pretty-json")
        {
        }

        protected override async Task WriteValueToStream(object realValue, Type declaredType, TextWriter writeStream, Encoding selectedEncoding)
        {
            var str = new CamelCasingIndentedSimpleJsonSerializer().Serialize(realValue);

            await writeStream.WriteAsync(str);
            await writeStream.FlushAsync();
        }
    }
}