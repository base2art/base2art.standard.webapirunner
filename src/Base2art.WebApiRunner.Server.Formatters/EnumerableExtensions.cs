﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.Collections.Generic;

    internal static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action)
        {
            foreach (var i in ie)
            {
                action(i);
            }
        }
    }
}