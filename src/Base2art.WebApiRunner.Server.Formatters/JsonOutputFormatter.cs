﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Json;

    public class JsonOutputFormatter : OutputFormatter<object>
    {
        public JsonOutputFormatter() : base("application/json")
        {
        }

        protected override async Task WriteValueToStream(object realValue, Type declaredType, TextWriter writeStream, Encoding selectedEncoding)
        {
            var str = new CamelCasingSimpleJsonSerializer().Serialize(realValue);

            await writeStream.WriteAsync(str);
            await writeStream.FlushAsync();
        }
    }
}