﻿namespace Base2art.WebApiRunner.Server.Formatters
{
    using System;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using YamlDotNet.Serialization;
    using YamlDotNet.Serialization.NamingConventions;

    public class YamlTextFormatter : OutputFormatter<object>
    {
        public YamlTextFormatter() : base("text/yaml")
        {
        }

        protected override async Task WriteValueToStream(object realValue, Type declaredType, TextWriter writeStream, Encoding selectedEncoding)
        {
            this.Serialize(writeStream, realValue, declaredType);
            await writeStream.FlushAsync();
        }

        public void Serialize(TextWriter writeStream, object value, Type declaredType)
        {
            var deserializer = new SerializerBuilder()
                               .WithNamingConvention(new CamelCaseNamingConvention())
                               .Build();

            deserializer.Serialize(writeStream, value);
        }

        /*
        public string Serialize(object value, Type declaredType)
        {
            var deserializer = new SerializerBuilder()
                               .WithNamingConvention(new CamelCaseNamingConvention())
                               .Build();
            
            using (var ms = new MemoryStream())
            {
                using (var sw = new StringWriter())
                {
                    deserializer.Serialize(sw, value);
                    sw.Flush();
                    ms.Seek(0L, SeekOrigin.Begin);

                    using (var sr = new StreamReader(ms))
                    {
                        var result = sr.ReadToEnd();
                        return result;
                    }
                }
            }
        }
        */
    }
}