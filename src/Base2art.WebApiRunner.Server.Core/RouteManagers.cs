namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Net.Http;
    using System.Reflection;
    using System.Threading.Tasks;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public static class RouteManagers
    {
        public static void MapRoute<T>(
            this IRouteManager manager,
            HttpMethod method,
            string routeTemplate,
            Expression<Func<T, Task>> action,
            string tag,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties,
            RouteType routeType)
        {
            manager.MapRoute(method, routeTemplate, typeof(T), action.MethodOf(), tag, parameters, properties, routeType);
        }

        public static void MapRoute<T>(
            this IRouteManager manager,
            HttpMethod method,
            string routeTemplate,
            Expression<Func<T, Task>> action,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties,
            RouteType routeType)
        {
            manager.MapRoute(method, routeTemplate, typeof(T), action.MethodOf(), parameters, properties, routeType);
        }

        public static MethodInfo MethodOf<T>(this Expression<Action<T>> action)
        {
            var actionBody = ((MethodCallExpression) action.Body).Method;
            return actionBody;
        }

        public static MethodInfo MethodOf<T>(this Expression<Func<T, Task>> action)
        {
            var actionBody = ((MethodCallExpression) action.Body).Method;
            return actionBody;
        }

        public static string Id(this ISimpleRoute simpleRoute) => $"{simpleRoute.Method ?? HttpMethod.Get}:{simpleRoute.RouteTemplate}";

        public static string Id(this IEndpointConfiguration endpoint) => $"{endpoint.Verb ?? HttpVerb.Get}:{endpoint.Url}";

        public static string Id(this IHealthCheckConfiguration method) => $"{HttpMethod.Get}:healthchecks/{method.Name}";
    }
}