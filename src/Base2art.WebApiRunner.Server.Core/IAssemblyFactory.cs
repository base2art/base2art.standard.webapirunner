namespace Base2art.WebApiRunner.Server
{
    using System.Collections.Generic;
    using System.Reflection;
    using Reflection.Discovery;

    public interface IAssemblyFactory
    {
        Assembly CreateFromPath(string path, bool forceRootLoadingLoader);

        void RegisterUnmanaged(IReadOnlyList<IPackagedNativeDll> unmanaged);

        bool Supports(string rid);
    }
}