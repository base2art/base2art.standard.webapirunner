namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Extensions;
    using Microsoft.Extensions.Primitives;
    using Text;
    using Web.Http;
    using YourSameSiteMode = Microsoft.AspNetCore.Http.SameSiteMode;
    using YourCookieOptions = Microsoft.AspNetCore.Http.CookieOptions;
    using MySameSiteMode = Web.Http.SameSiteMode;
    using MyCookieOptions = Web.Http.CookieOptions;

    public static class HttpContextFactory
    {
        public static IHttpRequest Request(this HttpContext context) => new RequestWrapper(context.Request);

        public static IHttpResponse Response(this HttpContext context) => new ResponseWrapper(context.Response);

        private class RequestWrapper : IHttpRequest
        {
            private readonly HttpRequest httpContextRequest;

            public RequestWrapper(HttpRequest httpContextRequest) => this.httpContextRequest = httpContextRequest;
            public string Method => this.httpContextRequest.Method;
            public Uri Uri => new Uri(this.httpContextRequest.GetEncodedUrl());
            public IReadOnlyDictionary<string, string> Headers => this.httpContextRequest.Headers.ToDictionary(x => x.Key, x => x.Value.Join());
            public IReadOnlyDictionary<string, string> Cookies => this.httpContextRequest.Cookies.ToDictionary(x => x.Key, x => x.Value);
        }

        private class ResponseWrapper : IHttpResponse
        {
            private readonly HttpResponse response;

            public ResponseWrapper(HttpResponse response) => this.response = response;

            public int StatusCode
            {
                get => this.response.StatusCode;
                set => this.response.StatusCode = value;
            }

            public IDictionary<string, string> Headers => new HeaderWrapper(this.response.Headers);

            public Stream Body
            {
                get => this.response.Body;
                set => this.response.Body = value;
            }

            public long? ContentLength
            {
                get => this.response.ContentLength;
                set => this.response.ContentLength = value;
            }

            public string ContentType
            {
                get => this.response.ContentType;
                set => this.response.ContentType = value;
            }

            public IHttpResponseCookies Cookies => new CookieWrapper(this.response.Cookies);

            public bool HasStarted => this.response.HasStarted;

            public void Redirect(string location)
            {
                this.response.Redirect(location);
            }

            public void Redirect(string location, bool permanent)
            {
                this.response.Redirect(location, permanent);
            }
        }

        private class CookieWrapper : IHttpResponseCookies
        {
            private readonly IResponseCookies responseCookiesImplementation;

            public CookieWrapper(IResponseCookies responseCookies) => this.responseCookiesImplementation = responseCookies;

            public void Append(string key, string value)
            {
                this.responseCookiesImplementation.Append(key, value);
            }

            public void Append(string key, string value, MyCookieOptions options)
            {
                this.responseCookiesImplementation.Append(key, value, this.Map(options));
            }

            public void Delete(string key)
            {
                this.responseCookiesImplementation.Delete(key);
            }

            public void Delete(string key, MyCookieOptions options)
            {
                this.responseCookiesImplementation.Delete(key, this.Map(options));
            }

            private YourCookieOptions Map(MyCookieOptions options) => new YourCookieOptions
                                                                      {
                                                                          Domain = options.Domain,
                                                                          Expires = options.Expires,
                                                                          HttpOnly = options.HttpOnly,
                                                                          Path = options.Path,
                                                                          SameSite = this.Map(options.SameSite),
                                                                          Secure = options.Secure
                                                                      };

            private YourSameSiteMode Map(MySameSiteMode optionsSameSite)
            {
                switch (optionsSameSite)
                {
                    case MySameSiteMode.None:
                        return YourSameSiteMode.None;
                    case MySameSiteMode.Strict:
                        return YourSameSiteMode.Strict;
                    case MySameSiteMode.Lax:
                        return YourSameSiteMode.Lax;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private class HeaderWrapper : IDictionary<string, string>
        {
            private readonly IHeaderDictionary headers;

            public HeaderWrapper(IHeaderDictionary responseHeaders) => this.headers = responseHeaders;

            public void Add(KeyValuePair<string, string> item)
            {
                this.headers.Add(item.Key, item.Value);
            }

            public void Clear()
            {
                this.headers.Clear();
            }

            public bool Contains(KeyValuePair<string, string> item) =>
                this.headers.Contains(new KeyValuePair<string, StringValues>(item.Key, new[] {item.Value}));

            public void CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
            {
            }

            public bool Remove(KeyValuePair<string, string> item) => this.headers.Remove(item.Key);

            public int Count => this.headers.Count();

            public bool IsReadOnly => false;

            public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
            {
                foreach (var element in this.Keys)
                {
                    this.TryGetValue(element, out var value);
                    yield return new KeyValuePair<string, string>(element, value);
                }
            }

            IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

            public ICollection<string> Keys
            {
                get { return this.headers.Select(x => x.Key).ToList(); }
            }

            public bool TryGetValue(string key, out string value)
            {
                var ret = this.headers.TryGetValue(key, out var values);
                value = null;
                if (ret)
                {
                    value = values.Join();
                }

                return ret;
            }

            public string this[string key]
            {
                get => string.Join("", (IEnumerable<string>) this.headers[key]);
                set => this.headers[key] = value;
            }

            public ICollection<string> Values => this.headers.Values.Select(x => string.Join("", (IEnumerable<string>) x)).ToList();

            public void Add(string key, string value)
            {
                this.headers.Add(key, value);
            }

            public bool ContainsKey(string key) => this.headers.ContainsKey(key);

            public bool Remove(string key) => this.headers.Remove(key);
        }
    }
}