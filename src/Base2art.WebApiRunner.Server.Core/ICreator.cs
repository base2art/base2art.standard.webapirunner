﻿namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public interface ICreator
    {
        object Create(Type type, IReadOnlyDictionary<string, object> parameters, IReadOnlyDictionary<string, object> properties);
        object Create(TypeInfo type, IReadOnlyDictionary<string, object> parameters, IReadOnlyDictionary<string, object> properties);

        object CreateAndInvoke(
            Type type,
            MethodInfo methodInfo,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties);

        object CreateAndInvoke(
            TypeInfo type,
            MethodInfo methodInfo,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties);
    }
}