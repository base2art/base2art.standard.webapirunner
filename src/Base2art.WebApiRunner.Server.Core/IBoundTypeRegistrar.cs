namespace Base2art.WebApiRunner.Server
{
    using System;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    public interface IBoundTypeRegistrar
    {
        void AddType<T>(BindingSource source);
        void AddType(Type type, BindingSource source);
    }
}