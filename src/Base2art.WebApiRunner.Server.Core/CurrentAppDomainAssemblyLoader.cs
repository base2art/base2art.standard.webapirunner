﻿namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Reflection.Discovery;

    public class CurrentAppDomainAssemblyLoader : AssemblyLoaderBase
    {
        public CurrentAppDomainAssemblyLoader(IAssemblyFactory factory, string[] supportedFrameworkVersions) : base(factory, FrameworkType.NetCore,
                                                                                                                    supportedFrameworkVersions)
        {
        }

        protected override Assembly[] Create()
        {
            var asms = base.Create();

            var set = new HashSet<string>(asms.Select(x => x.GetName().Name));

            var loaded = new List<Assembly>();

            foreach (var path in this.SearchPaths())
            {
                if (Directory.Exists(path))
                {
                    var dir = new DirectoryInfo(path);
                    var files = dir.GetFiles()
                                   .Where(x => In(x.Extension, StringComparer.OrdinalIgnoreCase, ".exe", "exe", ".dll", ".dll"))
                                   .Select(x => Tuple.Create(Path.GetFileNameWithoutExtension(x.Name), x.FullName))
                                   .Where(x => !string.IsNullOrWhiteSpace(x.Item1));

                    foreach (var file in files)
                    {
                        if (!set.Contains(file.Item1))
                        {
                            set.Add(file.Item1);

                            loaded.Add(this.AssemblyLoadFile(file.Item2, false));
                        }
                    }
                }
            }

            return asms.Union(loaded).ToArray();
        }

        private static bool In<T>(T item, IComparer<T> comp, params T[] values)
            => (values ?? new T[0]).Any(x => comp.Compare(item, x) == 0);
    }
}