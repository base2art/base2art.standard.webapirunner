﻿namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Threading.Tasks;

    public static class TaskUnwrapper
    {
        public static Task<object> Get(Type returnTypeGenericTypeArgument, Task createAndInvoke)
        {
            var methodInfo = typeof(TaskUnwrapper).GetMethod(nameof(GetIntenal));
            methodInfo = methodInfo.MakeGenericMethod(returnTypeGenericTypeArgument);
            return (Task<object>) methodInfo.Invoke(null, new object[] {createAndInvoke});
        }

        public static async Task<object> GetIntenal<T>(Task<T> createAndInvoke) => await createAndInvoke;
    }
}