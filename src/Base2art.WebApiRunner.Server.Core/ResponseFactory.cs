﻿namespace Base2art.WebApiRunner.Server
{
    using System.Net;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    public static class ResponseFactory
    {
        public static ObjectResult CreateResponse<T>(this HttpRequest request, HttpStatusCode status, T resultObject) =>
            new CustomObjectResult(resultObject) {DeclaredType = typeof(T), StatusCode = (int) status};

        public class CustomObjectResult : ObjectResult
        {
            public CustomObjectResult(object resultObject) : base(resultObject)
            {
            }
        }
    }
}