﻿namespace Base2art.WebApiRunner.Server
{
    using System;

    public interface ITypeLoader
    {
        Type Load(string value);
    }
}