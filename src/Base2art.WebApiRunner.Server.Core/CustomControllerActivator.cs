﻿namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Diagnostics;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Controllers;

    /// <summary>
    ///     <see cref="T:Microsoft.AspNetCore.Mvc.Controllers.IControllerActivator" /> that uses type activation to create
    ///     controllers.
    /// </summary>
    public class CustomControllerActivator : IControllerActivator
    {
        private readonly ICreator creator;

        /// <summary>
        ///     Creates a new <see cref="T:Microsoft.AspNetCore.Mvc.Controllers.DefaultControllerActivator" />.
        /// </summary>
        /// <param name="creator">The Object Creator.</param>
        public CustomControllerActivator( ICreator creator)
        {
            this.creator = creator;
        }

        /// <inheritdoc />
        public virtual object Create(ControllerContext controllerContext)
        {
            if (controllerContext == null)
            {
                throw new ArgumentNullException(nameof(controllerContext));
            }

            if (controllerContext.ActionDescriptor == null)
            {
                throw new ArgumentNullException(nameof(controllerContext.ActionDescriptor));
            }

            var controllerTypeInfo = controllerContext.ActionDescriptor.ControllerTypeInfo;
            if (controllerTypeInfo == null)
            {
                throw new ArgumentNullException(nameof(controllerTypeInfo));
            }

            try
            {
                return this.creator.Create(controllerTypeInfo, null, null);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }

        /// <inheritdoc />
        public virtual void Release(ControllerContext context, object controller)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (controller == null)
            {
                throw new ArgumentNullException(nameof(controller));
            }

            if (!(controller is IDisposable disposable))
            {
                return;
            }

            disposable.Dispose();
        }
    }
}