﻿namespace Base2art.WebApiRunner.Server
{
    using System;
    using Microsoft.AspNetCore.Routing;
    using Web.Server.Registration;

    public interface IRouteManagerProvider
    {
        IRouteManager CreateRouteManager(Func<RouteBuilder> routerProvider);
    }
}