﻿namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Reflection;
    using ComponentModel.Composition;
    using Logging;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc.Controllers;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using Web.App.Configuration;
    using Web.App.LifeCycle;
    using Web.Server.Registration;
    using Web.ServerEnvironment;

    public abstract class StartupBase : IStartup
    {
        private readonly IApplication application;
        private readonly IWebServerSettings webServerSettings;
        private readonly IBoundTypeRegistrar registrar;
        private readonly IServerConfiguration configuration;

        private readonly IRouteManager routeManager;
        private RouteBuilder router;

        protected StartupBase(
            IServerConfiguration configuration,
            IApplication application,
            IRouteManagerProvider routeManagerProvider,
            IFrameworkShim shim,
            IWebServerSettings webServerSettings,
            IBoundTypeRegistrar registrar)
        {
            this.routeManager = routeManagerProvider.CreateRouteManager(this.GetCurrentRouteBuilder);
            this.configuration = configuration;
            this.application = application;
            this.webServerSettings = webServerSettings;
            this.registrar = registrar;
            this.Shim = shim;
        }

        protected abstract IRegistration[] Registrations { get; }

        protected IFrameworkShim Shim { get; }

        IServiceProvider IStartup.ConfigureServices(IServiceCollection services)
        {
            var registrations = this.Registrations;

            services.TryAddTransient<IControllerActivator, CustomControllerActivator>();

            var builder = services.AddMvcCore();

            builder.Services.AddSingleton(this.routeManager);

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assembly.GetName().Name.StartsWith("Base2art.", StringComparison.OrdinalIgnoreCase))
                {
                    builder.AddApplicationPart(assembly);
                }
            }

            var loader = ServiceLoader.CreateLoader();
            loader.Bind<IServerConfiguration>().To(this.configuration);
            loader.Bind<IBoundTypeRegistrar>().To(this.registrar);
            loader.Bind<IWebServerSettings>().To(this.webServerSettings);
            
            
            this.Call(registrations, x => x.RegisterSystemServices(builder, loader, this.configuration));
            this.Call(registrations, x => x.RegisterUserServices(builder, loader, this.configuration));
            this.Call(registrations, x => x.RegisterSystemServicesPostUserService(builder, loader, this.configuration));

            loader.Seal();

            var wrapper = new Wrapper(loader, services);

            this.Call(registrations, x => x.RegisterCors(builder, wrapper, this.configuration));
            this.Call(registrations, x => x.RegisterFormatters(builder, wrapper, this.configuration));
            this.Call(registrations, x => x.RegisterFilters(builder, wrapper, this.configuration));
            this.Call(registrations, x => x.RegisterBindings(builder, wrapper, this.configuration));
            this.Call(registrations, x => x.RegisterApiExploration(builder, wrapper, this.configuration));
            this.Call(registrations, x => x.RegisterMiddleware(builder, wrapper, this.configuration));

            wrapper.Seal();

            return wrapper;
        }

        void IStartup.Configure(IApplicationBuilder app)
        {
            this.ConfigurePhase3(app);
        }

        protected RouteBuilder GetCurrentRouteBuilder() => this.router;

        private void ConfigurePhase3(IApplicationBuilder app)
        {
            var registrations = this.Registrations;

            var lifetime = app.ApplicationServices.GetRequiredService<IApplicationLifetime>();
            lifetime.ApplicationStopped.Register(() => this.Call(registrations, x => x.Dispose()));

            this.RegisterMiddleware(app);

            this.Call(registrations, x => x.RegisterNativeMiddleware(app, this.configuration));

            var appApplicationServices = app.ApplicationServices;

            this.router = new RouteBuilder(app);

            this.router.DefaultHandler = this.Shim.GetDefaultRouteHandler(appApplicationServices);

            this.Call(registrations, x => x.RegisterRoutes(this.routeManager, appApplicationServices, this.configuration));
            app.UseRouter(this.router.Build());
        }

        protected virtual void RegisterMiddleware(IApplicationBuilder app)
        {
        }

        private void Call(IRegistration[] registrations, Action<IRegistration> action)
        {
            foreach (var registration in registrations)
            {
                action(registration);
            }
        }

        private class Wrapper : IServiceProvider
        {
            private readonly IServiceCollection backing;
            private readonly IServiceLoaderInjector loader;
            private ServiceProvider systemServices;

            public Wrapper(IServiceLoaderInjector loader, IServiceCollection backing)
            {
                this.loader = loader;
                this.backing = backing;
            }

            public object GetService(Type serviceType)
            {
                try
                {
                    return this.loader.Resolve(serviceType, false);
                }
                catch (BindingException)
                {
                    throw;
                }
                catch (Exception)
                {
                    if (this.systemServices != null)
                    {
                        try
                        {
                            var result1 = this.systemServices.GetService(serviceType);
                            return result1;
                        }
                        catch (Exception exception)
                        {
                            Logger.Log(LogLevel.Error, exception);
                        }
                    }

                    throw;
                }
            }

            public void Seal()
            {
                this.systemServices = this.backing.BuildServiceProvider();
            }
        }
    }
}