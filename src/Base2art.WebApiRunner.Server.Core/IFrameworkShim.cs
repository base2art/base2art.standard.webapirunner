namespace Base2art.WebApiRunner.Server
{
    using System;
    using Microsoft.AspNetCore.Mvc.ActionConstraints;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.AspNetCore.Routing;

    public interface IFrameworkShim
    {
        IRouter GetDefaultRouteHandler(IServiceProvider provider);
        IRouter CreateMegaRoute(IServiceProvider provider);
        IFilterMetadata CreateControllerActionFilter();
        IFilterMetadata CreateControllerResultFilter();

        IActionConstraintMetadata CreateHttpMethodActionConstraint(string[] methodNames);
    }
}