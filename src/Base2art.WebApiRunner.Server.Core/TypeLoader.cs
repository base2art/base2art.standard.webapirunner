﻿namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Web.App.Configuration;

    public class TypeLoader : ITypeLoader
    {
        private readonly IAssemblyLoader loader;

        public TypeLoader(IAssemblyLoader loader) => this.loader = loader;

        public Type Load(string value)
        {
            var list = new List<Assembly>();
            foreach (var assembly in this.loader.Assemblies())
            {
                list.Add(assembly);
            }

            return list.Select(asm => asm.GetType(value, false, true))
                       .FirstOrDefault(type => type != null);
        }
    }
}