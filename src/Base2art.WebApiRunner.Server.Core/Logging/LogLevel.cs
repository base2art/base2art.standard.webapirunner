namespace Base2art.WebApiRunner.Server.Logging
{
    public struct LogLevel
    {
        internal int Value { get; }

        public static LogLevel All => new LogLevel(00);
        public static LogLevel Debug => new LogLevel(10);
        public static LogLevel Info => new LogLevel(30);
        public static LogLevel Warning => new LogLevel(50);
        public static LogLevel Error => new LogLevel(70);
        public static LogLevel Fatal => new LogLevel(90);

        public static LogLevel[] KnownLevels => new[]
                                                {
                                                    All,
                                                    Debug,
                                                    Info,
                                                    Warning,
                                                    Error,
                                                    Fatal
                                                };

        public LogLevel(int level) => this.Value = level;
    }
}