namespace Base2art.WebApiRunner.Server.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Logger
    {
        public static LogLevel Level { get; set; } = LogLevel.Info;

        public static Action<string> Writer { get; set; } = null;

        public static void Log(LogLevel level, string line)
        {
            Log(level, () => line);
        }

        public static void Log(LogLevel level, Exception exception)
        {
            Log(level, () =>
            {
                var sb = new StringBuilder();
                sb.AppendLine("EXCEPTION");
                sb.AppendLine("---------");
                sb.AppendLine("    when(UTC):");

                var items = new List<(int Level, string Key, string Value)>();
                ExceptionLines(exception, 1, items);

                foreach (var item in items)
                {
                    sb.Append(' ', 4 * item.Level);
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }

                return sb.ToString();
            });
        }

        private static void ExceptionLines(Exception exception, int level, List<(int Level, string Key, string Value)> items)
        {
            if (exception == null)
            {
                return;
            }

            items.Add((level, "type", exception.GetType().ToString()));
            items.Add((level, "message", exception.Message));
            items.Add((level, "stacktrace", exception.StackTrace));

            ExceptionLines(exception.InnerException, level + 1, items);
        }

        public static void Log(LogLevel level, Func<string> line)
        {
            if (Level.Value > level.Value)
            {
                return;
            }

            var writer = Writer ?? Console.WriteLine;
            writer(line());
        }
    }
}