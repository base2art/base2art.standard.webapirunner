﻿namespace Base2art.WebApiRunner.Server.Registrations
{
    public interface IMediaTypeMap
    {
        string GetMimeType(string extension);
    }
}