﻿namespace Base2art.WebApiRunner.Server.Registrations
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using ComponentModel.Composition;

    public class RegistrarCreator : ICreator
    {
        private readonly IServiceLoaderInjector serviceLoaderInjector;

        public RegistrarCreator(IServiceLoaderInjector serviceLoaderInjector) => this.serviceLoaderInjector = serviceLoaderInjector;

        public object Create(
            Type type,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties)
        {
            object item;
            try
            {
                item = this.serviceLoaderInjector.Resolve(type, true);
            }
            catch (KeyNotFoundException e)
            {
                throw new BindingException($"Error Resolving type: `{type}`", e);
            }

            return item ?? type.CreateFrom(this.serviceLoaderInjector, parameters, properties);
        }
//            this.serviceLoaderInjector.Resolve(type, false);
//            ;

        public object Create(
            TypeInfo type,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties) => this.Create(type.AsType(), parameters, properties);
//            this.serviceLoaderInjector.Resolve(type, false);
//            type.AsType().CreateFrom(this.serviceLoaderInjector, parameters, properties);

        public object CreateAndInvoke(
            Type type,
            MethodInfo methodInfo,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties) => this.Invoke(this.Create(type, parameters, properties), methodInfo);

        public object CreateAndInvoke(
            TypeInfo type,
            MethodInfo methodInfo,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties) => this.Invoke(this.Create(type, parameters, properties), methodInfo);

        private object Invoke(object create, MethodInfo methodInfo) => methodInfo.Invoke(create, new object[0]);
    }
}