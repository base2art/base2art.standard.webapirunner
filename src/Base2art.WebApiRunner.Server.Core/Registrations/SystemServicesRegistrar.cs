﻿namespace Base2art.WebApiRunner.Server.Registrations
{
    using System.Collections.Generic;
    using System.Linq;
    using ComponentModel.Composition;
    using Microsoft.AspNetCore.Http.Features;
    using Microsoft.Extensions.DependencyInjection;
    using Web.App.Configuration;
    using Web.App.LifeCycle;
    using Web.Server.Registration;

    public class SystemServicesRegistrar : RegistrationBase
    {
        private readonly IApplication application;

        public SystemServicesRegistrar(IApplication application) => this.application = application;

        public override void RegisterSystemServices(IMvcCoreBuilder builder, IBindableServiceLoaderInjector collection, IServerConfiguration config)
        {
            base.RegisterSystemServices(builder, collection, config);
            builder.Services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue / 2;
                x.MultipartBodyLengthLimit = int.MaxValue / 2;
                x.MultipartHeadersLengthLimit = int.MaxValue / 2;
            });

            collection.Bind<IMediaTypeMap>()
                      .As(ServiceLoaderBindingType.Singleton)
                      .To(x => this.CreateMediaTypeMap(config));

            collection.Bind<IApplication>()
                      .As(ServiceLoaderBindingType.Singleton)
                      .To(x => this.application);

            builder.Services.AddTransient<ICreator>(provider => new RegistrarCreator(collection));

            collection.Bind<ICreator>()
                      .As(ServiceLoaderBindingType.Instance)
                      .To(x => new RegistrarCreator(x));
        }
//        override 
//        var serializer = new JsonSerializerWrapper(config.Formatters.JsonFormatter.SerializerSettings);
//        bindableResolver.RegisterInstance(this.ServerConfig, true);
//        bindableResolver.RegisterInstance(mediaTypeMap, true);
//        bindableResolver.RegisterInstance(new SystemCacheProvider(), true);
//        bindableResolver.RegisterInstance(serializer, true);
//        bindableResolver.RegisterInstance(new ObjectStringifier(), true);
//        bindableResolver.RegisterInstance(new HashService(), true);
//        bindableResolver.RegisterInstance(new CustomMethodInterceptAspectProvider(configuredAssembliesResolver, config), true);

        private IMediaTypeMap CreateMediaTypeMap(IServerConfiguration config)
        {
            var map = new MediaTypeMap();

            var empty = config.Application.MediaTypes ?? new Dictionary<string, string>();
            var mediaTypes = empty.Where(x => !string.IsNullOrWhiteSpace(x.Key))
                                  .Where(x => !string.IsNullOrWhiteSpace(x.Value));

            foreach (var mediaType in mediaTypes)
            {
                map.AddMapping(mediaType.Key, mediaType.Value);
            }

            return map;
        }
    }
}