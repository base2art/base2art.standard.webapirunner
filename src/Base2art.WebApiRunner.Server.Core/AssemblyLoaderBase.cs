﻿namespace Base2art.WebApiRunner.Server
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Logging;
    using Reflection.Discovery;
    using Web.App.Configuration;

    public abstract class AssemblyLoaderBase : IAssemblyLoader
    {
        private readonly Lazy<Assembly[]> assemblies;
        private readonly IAssemblyFactory factory;

        private readonly List<Assembly> loadedAssemblies = new List<Assembly>();

        private readonly Lazy<PackagedCodeLibrarySearcher> packageLookup;
        private readonly string[] supportedFrameworkVersions;
        private bool hasSet;

        protected AssemblyLoaderBase(IAssemblyFactory factory, FrameworkType frameworkType, string[] supportedFrameworkVersions)
        {
            this.factory = factory;
            this.supportedFrameworkVersions = supportedFrameworkVersions;
            this.FrameworkType = frameworkType;

            this.assemblies = new Lazy<Assembly[]>(() =>
            {
                AppDomain.CurrentDomain.AssemblyResolve += this.CurrentDomainOnAssemblyResolve;
                var items = this.Create();

                this.hasSet = true;
                return items;
            });
            this.packageLookup = new Lazy<PackagedCodeLibrarySearcher>(() => new PackagedCodeLibrarySearcher());
        }

        protected PackagedCodeLibrarySearcher PackageLookup => this.packageLookup.Value;

        protected FrameworkType FrameworkType { get; }

        public IEnumerable<Assembly> Assemblies() => this.assemblies.Value.Concat(this.loadedAssemblies);

        private Assembly CurrentDomainOnAssemblyResolve(object s, ResolveEventArgs args)
            => this.CreateAsm(args, this.SearchPaths(), this.SystemAssemblies());

        protected virtual string[] SearchPaths() => new[]
                                                    {
                                                        "./bin",
                                                        "./bin/app",
                                                        "./app-bin",
                                                        "./",
                                                        Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                                                    };

        protected virtual string[] SystemAssemblies() => new string[0];

        protected virtual Assembly[] Create() => AppDomain.CurrentDomain.GetAssemblies();

        private Assembly CreateAsm(ResolveEventArgs args, string[] paths, string[] systemAssemblies)
            => this.CreateAsmFromPath(args, paths, systemAssemblies);

        private Assembly CreateAsmFromPath(ResolveEventArgs args, string[] dllPaths, string[] systemAssemblies)
        {
            var name = args.Name;
            if (args.Name.Contains(','))
            {
                name = args.Name.Split(',')[0];
            }

            var asms = this.hasSet ? this.Assemblies() : this.loadedAssemblies;
            var matching = asms.FirstOrDefault(x => string.Equals(x.GetName().Name, name, StringComparison.OrdinalIgnoreCase));
            if (matching != null)
            {
                return matching;
            }

            var paths = dllPaths.Select(x => Path.Combine(x, name + ".dll"))
                                .Union(dllPaths.Select(x => Path.Combine(x, name + ".exe")))
                                .ToArray();

            foreach (var dllPath in paths)
            {
                if (!File.Exists(dllPath))
                {
                }
                else
                {
                    var dll = this.AssemblyLoadFile(dllPath,
                                                    systemAssemblies.Contains(Path.GetFileNameWithoutExtension(dllPath),
                                                                              StringComparer.OrdinalIgnoreCase));
                    if (this.hasSet)
                    {
                        this.loadedAssemblies.Add(dll);
                    }

                    return dll;
                }
            }

            
            if (this.AsmFromPath(name, false, this.supportedFrameworkVersions, systemAssemblies, out var assembly1))
            {
                Logger.Log(LogLevel.Debug, () => $"Loading From Resolve {{CurrentFramework SpecRid}} ... {assembly1.Location}");
                return assembly1;
            }

            if (this.AsmFromPath(name, true, this.supportedFrameworkVersions, systemAssemblies, out var assembly3))
            {
                Logger.Log(LogLevel.Debug, () => $"Loading From Resolve {{CurrentFramework AnyRid}} ... {assembly3.Location}");
                return assembly3;
            }

            Logger.Log(LogLevel.Debug, $"Loading DLL From Resolve `{name}` ...");
            foreach (var path in dllPaths)
            {
                Logger.Log(LogLevel.Debug, $"  Searching From Resolve `{path}` : NotFound");
            }

            return null;
        }

        private bool AsmFromPath(string name, bool allowAnyRid, string[] supportedFrameworkVersionLookup, string[] systemAssemblies,
                                 out Assembly assembly)
        {
            IEnumerable<IPackagedDll> dlls = this.FilterItems(this.GetDlls(name, null, supportedFrameworkVersionLookup))
                                                 .Where(x => allowAnyRid || !string.Equals(x.RuntimeIdentifier, "any",
                                                                                           StringComparison.OrdinalIgnoreCase))
                                                 .Where(x => string.Equals(x.Name, name, StringComparison.OrdinalIgnoreCase))
                                                 .GroupBy(x => x.Version)
                                                 .OrderByDescending(x => x.Key)
                                                 .FirstOrDefault();

            foreach (var packageDll in dlls ?? new List<IPackagedDll>())
            {
                try
                {
                    var dll = this.AssemblyLoadFile(packageDll.FullPath,
                                                    systemAssemblies.Contains(Path.GetFileNameWithoutExtension(packageDll.FullPath),
                                                                              StringComparer.OrdinalIgnoreCase));
                    if (this.hasSet)
                    {
                        this.loadedAssemblies.Add(dll);
                    }

                    assembly = dll;
                    return true;
                }
                catch (Exception)
                {
                }
            }

            assembly = null;
            return false;
        }

        protected (IReadOnlyList<IPackagedDll> Managed, IReadOnlyList<IPackagedNativeDll> Unmanaged) GetPackageLibraries(string name, string version)
        {
            var valueTuple = string.IsNullOrWhiteSpace(version)
                                 ? this.PackageLookup.GetLibraries(name)
                                 : this.PackageLookup.GetLibraries(name, version);
            return valueTuple;
        }

        private IEnumerable<IPackagedDll> GetDlls(
            string name,
            string version,
            string[] supportedFrameworkVersionLookup)
        {
            var dlls = this.GetPackageLibraries(name, version);

            foreach (var frameworkVersionName in supportedFrameworkVersionLookup)
            {
                var items = dlls.Managed
                                .Where(x => x.FrameworkVersion == frameworkVersionName)
                                .ToList();

                if (items.Count > 0)
                {
                    return items;
                }
            }

            return new List<IPackagedDll>();
        }

        private IEnumerable<IPackagedDll> FilterItems(IEnumerable<IPackagedDll> packagedDllsByVersion)
        {
            bool AreEqual(IPackagedDll z, IPackagedDll y) =>
                string.Equals(z.Name, y.Name, StringComparison.OrdinalIgnoreCase) && z.Version == y.Version;

            var dlls = new List<IPackagedDll>();
            if (packagedDllsByVersion != null)
            {
                foreach (var packagedDll in packagedDllsByVersion)
                {
                    if (!dlls.Any(dll => AreEqual(dll, packagedDll)))
                    {
                        dlls.Add(packagedDll);
                    }
                }
            }

            return dlls;
        }

        protected Assembly AssemblyLoadFile(string path, bool forceRootLoadingLoader)
            => this.factory.CreateFromPath(path, forceRootLoadingLoader);
    }
}