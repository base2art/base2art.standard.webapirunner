namespace Base2art.WebApiRunner.Server
{
    using System;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    public interface IBoundTypeLookup
    {
        bool Contains(Type modelType);
        BindingSource this[Type modelType] { get; }
    }
}