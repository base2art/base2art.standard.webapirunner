﻿namespace Base2art.WebApiRunner.Server
{
    using Web.App.Configuration;

    public interface IAspectLookup
    {
        ITypeInstanceConfiguration GetByName(string name);
    }
}