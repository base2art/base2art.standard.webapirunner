﻿namespace Base2art.WebApiRunner.Server.ApiExploration
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.AspNetCore.Mvc.Infrastructure;

    public class CustomApiDescriptionGroupCollectionProvider : IApiDescriptionGroupCollectionProvider
    {
        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;
        private readonly IApiDescriptionProvider[] _apiDescriptionProviders;
        private ApiDescriptionGroupCollection _apiDescriptionGroups;

        /// <summary>
        ///     Creates a new instance of
        ///     <see cref="T:Microsoft.AspNetCore.Mvc.ApiExplorer.ApiDescriptionGroupCollectionProvider" />.
        /// </summary>
        /// <param name="actionDescriptorCollectionProvider">
        ///     The <see cref="T:Microsoft.AspNetCore.Mvc.Infrastructure.IActionDescriptorCollectionProvider" />.
        /// </param>
        /// <param name="apiDescriptionProviders">
        ///     The <see cref="T:System.Collections.Generic.IEnumerable`1" />.
        /// </param>
        public CustomApiDescriptionGroupCollectionProvider(
            IActionDescriptorCollectionProvider actionDescriptorCollectionProvider,
            IEnumerable<IApiDescriptionProvider> apiDescriptionProviders)
        {
            this._actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
            this._apiDescriptionProviders = apiDescriptionProviders
                                            .OrderBy(item => item.Order)
                                            .ToArray();
        }

        /// <inheritdoc />
        public ApiDescriptionGroupCollection ApiDescriptionGroups
        {
            get
            {
                var actionDescriptors = this._actionDescriptorCollectionProvider.ActionDescriptors;
                if (this._apiDescriptionGroups == null || this._apiDescriptionGroups.Version != actionDescriptors.Version)
                {
                    this._apiDescriptionGroups = this.GetCollection(actionDescriptors);
                }

                return this._apiDescriptionGroups;
            }
        }

        private ApiDescriptionGroupCollection GetCollection(ActionDescriptorCollection actionDescriptors)
        {
            var context = new ApiDescriptionProviderContext(actionDescriptors.Items);
            foreach (var descriptionProvider in this._apiDescriptionProviders)
            {
                descriptionProvider.OnProvidersExecuting(context);
            }

            for (var index = this._apiDescriptionProviders.Length - 1; index >= 0; --index)
            {
                this._apiDescriptionProviders[index].OnProvidersExecuted(context);
            }

            var arr = context.Results.GroupBy(d => d.GroupName)
                             .Select(g => new ApiDescriptionGroup(g.Key, g.ToArray()))
                             .ToArray();

            return new ApiDescriptionGroupCollection(arr, actionDescriptors.Version);
        }
    }
}