﻿namespace Base2art.WebApiRunner.Server.ApiExploration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.AspNetCore.Mvc.Controllers;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.AspNetCore.Routing;
    using Microsoft.Extensions.Options;
    using Web.App.Configuration;

    /// <summary>
    ///     Implements a provider of <see cref="T:Microsoft.AspNetCore.Mvc.ApiExplorer.ApiDescription" /> for actions
    ///     represented
    ///     by <see cref="T:Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor" />.
    /// </summary>
    public class CustomApiDescriptionProvider : IApiDescriptionProvider
    {
        private static readonly string Template = "template";
        private readonly IServerConfiguration configuration;
        private readonly DefaultApiDescriptionProvider items;

        public CustomApiDescriptionProvider(
            IServerConfiguration configuration,
            IOptions<MvcOptions> optionsAccessor,
            IInlineConstraintResolver constraintResolver,
            IModelMetadataProvider modelMetadataProvider,
            IBoundTypeLookup lookup)
        {
            this.configuration = configuration;
            this.items = new DefaultApiDescriptionProvider(
                                                           optionsAccessor,
                                                           constraintResolver,
                                                           new Wrapper(modelMetadataProvider, this.configuration.Application.ModelBindings, lookup),
                                                           new TypeLookup(),
                                                           new OptionsWrapper<RouteOptions>(new RouteOptions
                                                                                            {
                                                                                                LowercaseUrls = true,
                                                                                                AppendTrailingSlash = true
                                                                                            }));
        }

        void IApiDescriptionProvider.OnProvidersExecuting(ApiDescriptionProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

//            var bindings = this.configuration.Application.ModelBindings;
            foreach (var actionDescriptor in context.Actions.OfType<ControllerActionDescriptor>())
            {
                if (actionDescriptor.AttributeRouteInfo == null || !actionDescriptor.AttributeRouteInfo.SuppressPathMatching)
                {
                    var routeValues = actionDescriptor.RouteValues;
                    if (routeValues.ContainsKey(Template))
                    {
                        actionDescriptor.AttributeRouteInfo = new AttributeRouteInfo
                                                              {
                                                                  Template = routeValues[Template],
                                                                  Name = Guid.NewGuid().ToString("N")
                                                              };
                    }
                }

//                var parameters = actionDescriptor.Parameters;
//                for (int i = parameters.Count - 1; i >= 0; i--)
//                {
//                    var parm = parameters[i];
//                    if (bindings.Any(x => x.BoundType == parm.ParameterType))
//                    {
//                        parameters.RemoveAt(i);
//                    }
//                }
            }

            this.items.OnProvidersExecuting(context);
        }

        void IApiDescriptionProvider.OnProvidersExecuted(ApiDescriptionProviderContext context)
        {
            this.items.OnProvidersExecuted(context);
        }

        int IApiDescriptionProvider.Order => this.items.Order;

        private class TypeLookup : IActionResultTypeMapper
        {
            public Type GetResultDataType(Type returnType) => returnType;

            public IActionResult Convert(object value, Type returnType) => new ObjectResult(value);
        }

        public class Wrapper : IModelMetadataProvider
        {
            private readonly IReadOnlyList<IModelBindingConfiguration> applicationModelBindings;
            private readonly IBoundTypeLookup boundTypeLookup;
            private readonly IModelMetadataProvider modelMetadataProvider;

            public Wrapper(
                IModelMetadataProvider modelMetadataProvider,
                IReadOnlyList<IModelBindingConfiguration> applicationModelBindings,
                IBoundTypeLookup boundTypeLookup)
            {
                this.modelMetadataProvider = modelMetadataProvider;
                this.applicationModelBindings = applicationModelBindings;
                this.boundTypeLookup = boundTypeLookup;
            }

            public ModelMetadata GetMetadataForType(Type modelType)
            {
                var bindings = this.applicationModelBindings;
                var matchedBinding = bindings.FirstOrDefault(x => x.BoundType == modelType);
                if (matchedBinding != null)
                {
                    if (matchedBinding.BindingType == BindingType.Hidden)
                    {
                        var metadataForType1 = this.modelMetadataProvider.GetMetadataForType(modelType) as DefaultModelMetadata;

                        return new CustomModelMetadata(metadataForType1, ModelMetadataIdentity.ForType(modelType));
                    }
                }

                if (this.boundTypeLookup?.Contains(modelType) == true)
                {
                    var metadataForType1 = this.modelMetadataProvider.GetMetadataForType(modelType) as DefaultModelMetadata;
                    if (metadataForType1 != null)
                    {
                        return new CustomModelMetadata(metadataForType1, ModelMetadataIdentity.ForType(modelType), this.boundTypeLookup[modelType]);
                    }
                }

                var metadataForType = this.modelMetadataProvider.GetMetadataForType(modelType);
                return metadataForType;
            }

            public IEnumerable<ModelMetadata> GetMetadataForProperties(Type modelType)
            {
                var metadataForProperties = this.modelMetadataProvider.GetMetadataForProperties(modelType);
                return metadataForProperties;
            }

            private class CustomModelMetadata : ModelMetadata
            {
                private readonly ModelMetadata baseValue;
                private readonly BindingSource overridingSource;

                public CustomModelMetadata(DefaultModelMetadata baseValue, ModelMetadataIdentity identity, BindingSource overridingSource = null)
                    : base(identity)
                {
                    this.baseValue = baseValue;
                    this.overridingSource = overridingSource;
                }

                public override IReadOnlyDictionary<object, object> AdditionalValues => this.baseValue.AdditionalValues;

                public override ModelPropertyCollection Properties => new ModelPropertyCollection(new ModelMetadata[0]);

                public override string BinderModelName => this.baseValue.BinderModelName;

                public override Type BinderType => this.baseValue.BinderType;

                public override BindingSource BindingSource => this.overridingSource ?? this.baseValue.BindingSource;

                public override bool ConvertEmptyStringToNull => this.baseValue.ConvertEmptyStringToNull;

                public override string DataTypeName => this.baseValue.DataTypeName;

                public override string Description => this.baseValue.Description;

                public override string DisplayFormatString => this.baseValue.DisplayFormatString;

                public override string DisplayName => this.baseValue.DisplayName;

                public override string EditFormatString => this.baseValue.EditFormatString;

                public override ModelMetadata ElementMetadata => this.baseValue.ElementMetadata;

                public override IEnumerable<KeyValuePair<EnumGroupAndName, string>> EnumGroupedDisplayNamesAndValues =>
                    this.baseValue.EnumGroupedDisplayNamesAndValues;

                public override IReadOnlyDictionary<string, string> EnumNamesAndValues => this.baseValue.EnumNamesAndValues;

                public override bool HasNonDefaultEditFormat => this.baseValue.HasNonDefaultEditFormat;

                public override bool HtmlEncode => this.baseValue.HtmlEncode;

                public override bool HideSurroundingHtml => this.baseValue.HideSurroundingHtml;

                public override bool IsBindingAllowed => this.baseValue.IsBindingAllowed;

                public override bool IsBindingRequired => this.baseValue.IsBindingRequired;

                public override bool IsEnum => this.baseValue.IsEnum;

                public override bool IsFlagsEnum => this.baseValue.IsFlagsEnum;

                public override bool IsReadOnly => this.baseValue.IsReadOnly;

                public override bool IsRequired => this.baseValue.IsRequired;

                public override ModelBindingMessageProvider ModelBindingMessageProvider => this.baseValue.ModelBindingMessageProvider;

                public override int Order => this.baseValue.Order;

                public override string Placeholder => this.baseValue.Placeholder;

                public override string NullDisplayText => this.baseValue.NullDisplayText;

                public override IPropertyFilterProvider PropertyFilterProvider => this.baseValue.PropertyFilterProvider;

                public override bool ShowForDisplay => this.baseValue.ShowForDisplay;

                public override bool ShowForEdit => this.baseValue.ShowForEdit;

                public override string SimpleDisplayProperty => this.baseValue.SimpleDisplayProperty;

                public override string TemplateHint => this.baseValue.TemplateHint;

                public override bool ValidateChildren => this.baseValue.ValidateChildren;

                public override IReadOnlyList<object> ValidatorMetadata => this.baseValue.ValidatorMetadata;

                public override Func<object, object> PropertyGetter => this.baseValue.PropertyGetter;

                public override Action<object, object> PropertySetter => this.baseValue.PropertySetter;
            }
        }
    }
}

/*
 *
        private readonly IList<IInputFormatter> _inputFormatters;
        private readonly IList<IOutputFormatter> _outputFormatters;
        private readonly IModelMetadataProvider _modelMetadataProvider;
        private readonly IInlineConstraintResolver _constraintResolver;

        /// <summary>
        /// Creates a new instance of <see cref="T:Microsoft.AspNetCore.Mvc.ApiExplorer.CustomApiDescriptionProvider" />.
        /// </summary>
        /// <param name="optionsAccessor">The accessor for <see cref="T:Microsoft.AspNetCore.Mvc.MvcOptions" />.</param>
        /// <param name="constraintResolver">The <see cref="T:Microsoft.AspNetCore.Routing.IInlineConstraintResolver" /> used for resolving inline
        /// constraints.</param>
        /// <param name="modelMetadataProvider">The <see cref="T:Microsoft.AspNetCore.Mvc.ModelBinding.IModelMetadataProvider" />.</param>
        public CustomApiDescriptionProvider(IOptions<MvcOptions> optionsAccessor, IInlineConstraintResolver constraintResolver,
                                            IModelMetadataProvider modelMetadataProvider)
        {
            this._inputFormatters = (IList<IInputFormatter>) optionsAccessor.Value.InputFormatters;
            this._outputFormatters = (IList<IOutputFormatter>) optionsAccessor.Value.OutputFormatters;
            this._constraintResolver = constraintResolver;
            this._modelMetadataProvider = modelMetadataProvider;
        }

        /// <inheritdoc />
        public int Order
        {
            get { return -1000; }
        }

        /// <inheritdoc />
        public void OnProvidersExecuting(ApiDescriptionProviderContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            foreach (ControllerActionDescriptor actionDescriptor in context.Actions.OfType<ControllerActionDescriptor>())
            {
                if (actionDescriptor.AttributeRouteInfo == null || !actionDescriptor.AttributeRouteInfo.SuppressPathMatching)
                {
                    ApiDescriptionActionData property = actionDescriptor.GetProperty<ApiDescriptionActionData>();
                    if (property != null)
                    {
                        foreach (string httpMethod in this.GetHttpMethods(actionDescriptor))
                            context.Results.Add(this.CreateApiDescription(actionDescriptor, httpMethod, property.GroupName));
                    }
                }
            }
        }

        public void OnProvidersExecuted(ApiDescriptionProviderContext context)
        {
        }

        private ApiDescription CreateApiDescription(ControllerActionDescriptor action, string httpMethod, string groupName)
        {
            RouteTemplate template = this.ParseTemplate(action);
            ApiDescription apiDescription = new ApiDescription()
                                            {
                                                ActionDescriptor = (ActionDescriptor) action,
                                                GroupName = groupName,
                                                HttpMethod = httpMethod,
                                                RelativePath = this.GetRelativePath(template)
                                            };
            List<TemplatePart> templatePartList1;
            if (template == null)
            {
                templatePartList1 = (List<TemplatePart>) null;
            }
            else
            {
                IList<TemplatePart> parameters = template.Parameters;
                templatePartList1 = parameters != null ? parameters.ToList<TemplatePart>() : (List<TemplatePart>) null;
            }

            if (templatePartList1 == null)
                templatePartList1 = new List<TemplatePart>();
            List<TemplatePart> templatePartList2 = templatePartList1;
            foreach (ApiParameterDescription parameter in (IEnumerable<ApiParameterDescription>)
                this.GetParameters(new CustomApiDescriptionProvider.ApiParameterContext(this._modelMetadataProvider, action,
                                                                                         (IReadOnlyList<TemplatePart>) templatePartList2)))
                apiDescription.ParameterDescriptions.Add(parameter);
            IApiRequestMetadataProvider[] metadataAttributes = this.GetRequestMetadataAttributes(action);
            foreach (ApiResponseType apiResponseType in (IEnumerable<ApiResponseType>)
                this.GetApiResponseTypes(this.GetResponseMetadataAttributes(action), this.GetRuntimeReturnType(this.GetDeclaredReturnType(action))))
                apiDescription.SupportedResponseTypes.Add(apiResponseType);
            foreach (ApiParameterDescription parameterDescription in
                apiDescription.ParameterDescriptions.Where<ApiParameterDescription>((Func<ApiParameterDescription, bool>)
                                                                                    (p => p.Source == BindingSource.Body)))
            {
                foreach (ApiRequestFormat requestFormat in (IEnumerable<ApiRequestFormat>) this.GetRequestFormats(metadataAttributes,
                                                                                                                  parameterDescription.Type))
                    apiDescription.SupportedRequestFormats.Add(requestFormat);
            }

            return apiDescription;
        }

        private IList<ApiParameterDescription> GetParameters(CustomApiDescriptionProvider.ApiParameterContext context)
        {
            if (context.ActionDescriptor.Parameters != null)
            {
                foreach (ParameterDescriptor parameter in (IEnumerable<ParameterDescriptor>) context.ActionDescriptor.Parameters)
                    new CustomApiDescriptionProvider.PseudoModelBindingVisitor(context, parameter).WalkParameter(CustomApiDescriptionProvider
                                                                                                                  .ApiParameterDescriptionContext
                                                                                                                  .GetContext(this._modelMetadataProvider.GetMetadataForType(parameter.ParameterType),
                                                                                                                              parameter.BindingInfo,
                                                                                                                              parameter.Name));
            }

            if (context.ActionDescriptor.BoundProperties != null)
            {
                foreach (ParameterDescriptor boundProperty in (IEnumerable<ParameterDescriptor>) context.ActionDescriptor.BoundProperties)
                    new CustomApiDescriptionProvider.PseudoModelBindingVisitor(context, boundProperty).WalkParameter(CustomApiDescriptionProvider
                                                                                                                      .ApiParameterDescriptionContext
                                                                                                                      .GetContext(context.MetadataProvider.GetMetadataForProperty(context.ActionDescriptor.ControllerTypeInfo.AsType(), boundProperty.Name),
                                                                                                                                  boundProperty
                                                                                                                                      .BindingInfo,
                                                                                                                                  boundProperty
                                                                                                                                      .Name));
            }

            for (int index = context.Results.Count - 1; index >= 0; --index)
            {
                if (!context.Results[index].Source.IsFromRequest)
                    context.Results.RemoveAt(index);
            }

            Dictionary<string, ApiParameterRouteInfo> dictionary =
                new Dictionary<string, ApiParameterRouteInfo>((IEqualityComparer<string>) StringComparer.OrdinalIgnoreCase);
            foreach (TemplatePart routeParameter in (IEnumerable<TemplatePart>) context.RouteParameters)
                dictionary.Add(routeParameter.Name, this.CreateRouteInfo(routeParameter));
            foreach (ApiParameterDescription result in (IEnumerable<ApiParameterDescription>) context.Results)
            {
                ApiParameterRouteInfo parameterRouteInfo;
                if ((result.Source == BindingSource.Path || result.Source == BindingSource.ModelBinding || result.Source == BindingSource.Custom)
                    && dictionary.TryGetValue(result.Name, out parameterRouteInfo))
                {
                    result.RouteInfo = parameterRouteInfo;
                    dictionary.Remove(result.Name);
                    if (result.Source == BindingSource.ModelBinding && !result.RouteInfo.IsOptional)
                        result.Source = BindingSource.Path;
                }
            }

            foreach (KeyValuePair<string, ApiParameterRouteInfo> keyValuePair in dictionary)
                context.Results.Add(new ApiParameterDescription()
                                    {
                                        Name = keyValuePair.Key,
                                        RouteInfo = keyValuePair.Value,
                                        Source = BindingSource.Path
                                    });
            return context.Results;
        }

        private ApiParameterRouteInfo CreateRouteInfo(TemplatePart routeParameter)
        {
            List<IRouteConstraint> routeConstraintList = new List<IRouteConstraint>();
            if (routeParameter.InlineConstraints != null)
            {
                foreach (InlineConstraint inlineConstraint in routeParameter.InlineConstraints)
                    routeConstraintList.Add(this._constraintResolver.ResolveConstraint(inlineConstraint.Constraint));
            }

            return new ApiParameterRouteInfo()
                   {
                       Constraints = (IEnumerable<IRouteConstraint>) routeConstraintList,
                       DefaultValue = routeParameter.DefaultValue,
                       IsOptional = routeParameter.IsOptional || routeParameter.DefaultValue != null
                   };
        }

        private IEnumerable<string> GetHttpMethods(ControllerActionDescriptor action)
        {
            if (action.ActionConstraints != null && action.ActionConstraints.Count > 0)
                return action.ActionConstraints.OfType<HttpMethodActionConstraint>()
                             .SelectMany<HttpMethodActionConstraint, string
                             >((Func<HttpMethodActionConstraint, IEnumerable<string>>) (c => c.HttpMethods));
            return (IEnumerable<string>) new string[1];
        }

        private RouteTemplate ParseTemplate(ControllerActionDescriptor action)
        {
            AttributeRouteInfo attributeRouteInfo = action.AttributeRouteInfo;
            switch (attributeRouteInfo != null ? attributeRouteInfo.Template : (string) null)
            {
                case null:
                    return (RouteTemplate) null;
                default:
                    return TemplateParser.Parse(action.AttributeRouteInfo.Template);
            }
        }

        private string GetRelativePath(RouteTemplate parsedTemplate)
        {
            if (parsedTemplate == null)
                return (string) null;
            List<string> stringList = new List<string>();
            foreach (TemplateSegment segment in (IEnumerable<TemplateSegment>) parsedTemplate.Segments)
            {
                string str = string.Empty;
                foreach (TemplatePart part in segment.Parts)
                {
                    if (part.IsLiteral)
                        str += part.Text;
                    else if (part.IsParameter)
                        str = str + "{" + part.Name + "}";
                }

                stringList.Add(str);
            }

            return string.Join("/", (IEnumerable<string>) stringList);
        }

        private IReadOnlyList<ApiRequestFormat> GetRequestFormats(IApiRequestMetadataProvider[] requestMetadataAttributes, Type type)
        {
            List<ApiRequestFormat> apiRequestFormatList = new List<ApiRequestFormat>();
            MediaTypeCollection contentTypes = new MediaTypeCollection();
            if (requestMetadataAttributes != null)
            {
                foreach (IApiRequestMetadataProvider metadataAttribute in requestMetadataAttributes)
                    metadataAttribute.SetContentTypes(contentTypes);
            }

            if (contentTypes.Count == 0)
                contentTypes.Add((string) null);
            foreach (string contentType in (Collection<string>) contentTypes)
            {
                foreach (IInputFormatter inputFormatter in (IEnumerable<IInputFormatter>) this._inputFormatters)
                {
                    IApiRequestFormatMetadataProvider metadataProvider = inputFormatter as IApiRequestFormatMetadataProvider;
                    if (metadataProvider != null)
                    {
                        IReadOnlyList<string> supportedContentTypes = metadataProvider.GetSupportedContentTypes(contentType, type);
                        if (supportedContentTypes != null)
                        {
                            foreach (string str in (IEnumerable<string>) supportedContentTypes)
                                apiRequestFormatList.Add(new ApiRequestFormat()
                                                         {
                                                             Formatter = inputFormatter,
                                                             MediaType = str
                                                         });
                        }
                    }
                }
            }

            return (IReadOnlyList<ApiRequestFormat>) apiRequestFormatList;
        }

        private IReadOnlyList<ApiResponseType> GetApiResponseTypes(IApiResponseMetadataProvider[] responseMetadataAttributes, Type type)
        {
            List<ApiResponseType> apiResponseTypeList = new List<ApiResponseType>();
            Dictionary<int, Type> dictionary = new Dictionary<int, Type>();
            MediaTypeCollection contentTypes = new MediaTypeCollection();
            if (responseMetadataAttributes != null)
            {
                foreach (IApiResponseMetadataProvider metadataAttribute in responseMetadataAttributes)
                {
                    metadataAttribute.SetContentTypes(contentTypes);
                    if (metadataAttribute.Type != (Type) null)
                        dictionary[metadataAttribute.StatusCode] = metadataAttribute.Type;
                }
            }

            if (dictionary.Count == 0 && type != (Type) null)
                dictionary[200] = type;
            if (contentTypes.Count == 0)
                contentTypes.Add((string) null);
            IEnumerable<IApiResponseTypeMetadataProvider> metadataProviders = this._outputFormatters.OfType<IApiResponseTypeMetadataProvider>();
            foreach (KeyValuePair<int, Type> keyValuePair in dictionary)
            {
                if (keyValuePair.Value == typeof(void))
                {
                    apiResponseTypeList.Add(new ApiResponseType()
                                            {
                                                StatusCode = keyValuePair.Key,
                                                Type = keyValuePair.Value
                                            });
                }
                else
                {
                    ApiResponseType apiResponseType = new ApiResponseType()
                                                      {
                                                          Type = keyValuePair.Value,
                                                          StatusCode = keyValuePair.Key,
                                                          ModelMetadata = this._modelMetadataProvider.GetMetadataForType(keyValuePair.Value)
                                                      };
                    foreach (string contentType in (Collection<string>) contentTypes)
                    {
                        foreach (IApiResponseTypeMetadataProvider metadataProvider in metadataProviders)
                        {
                            IReadOnlyList<string> supportedContentTypes = metadataProvider.GetSupportedContentTypes(contentType, keyValuePair.Value);
                            if (supportedContentTypes != null)
                            {
                                foreach (string str in (IEnumerable<string>) supportedContentTypes)
                                    apiResponseType.ApiResponseFormats.Add(new ApiResponseFormat()
                                                                           {
                                                                               Formatter = (IOutputFormatter) metadataProvider,
                                                                               MediaType = str
                                                                           });
                            }
                        }
                    }

                    apiResponseTypeList.Add(apiResponseType);
                }
            }

            return (IReadOnlyList<ApiResponseType>) apiResponseTypeList;
        }

        private Type GetDeclaredReturnType(ControllerActionDescriptor action)
        {
            Type returnType = action.MethodInfo.ReturnType;
            if (returnType == typeof(void) || returnType == typeof(Task))
                return typeof(void);
            Type type = CustomApiDescriptionProvider.GetTaskInnerTypeOrNull(returnType);
            if ((object) type == null)
                type = returnType;
            Type c = type;
            if (typeof(IActionResult).IsAssignableFrom(c))
                return (Type) null;
            return c;
        }

        private static Type GetTaskInnerTypeOrNull(Type type)
        {
            Type genericInterface = ClosedGenericMatcher.ExtractGenericInterface(type, typeof(Task<>));
            if ((object) genericInterface == null)
                return (Type) null;
            return genericInterface.GenericTypeArguments[0];
        }

        private Type GetRuntimeReturnType(Type declaredReturnType)
        {
            if (declaredReturnType == typeof(object))
                return (Type) null;
            return declaredReturnType;
        }

        private IApiRequestMetadataProvider[] GetRequestMetadataAttributes(ControllerActionDescriptor action)
        {
            if (action.FilterDescriptors == null)
                return (IApiRequestMetadataProvider[]) null;
            return action.FilterDescriptors.Select<FilterDescriptor, IFilterMetadata>((Func<FilterDescriptor, IFilterMetadata>) (fd => fd.Filter))
                         .OfType<IApiRequestMetadataProvider>().ToArray<IApiRequestMetadataProvider>();
        }

        private IApiResponseMetadataProvider[] GetResponseMetadataAttributes(ControllerActionDescriptor action)
        {
            if (action.FilterDescriptors == null)
                return (IApiResponseMetadataProvider[]) null;
            return action.FilterDescriptors.Select<FilterDescriptor, IFilterMetadata>((Func<FilterDescriptor, IFilterMetadata>) (fd => fd.Filter))
                         .OfType<IApiResponseMetadataProvider>().ToArray<IApiResponseMetadataProvider>();
        }

        private class ApiParameterContext
        {
            public ApiParameterContext(IModelMetadataProvider metadataProvider, ControllerActionDescriptor actionDescriptor,
                                       IReadOnlyList<TemplatePart> routeParameters)
            {
                this.MetadataProvider = metadataProvider;
                this.ActionDescriptor = actionDescriptor;
                this.RouteParameters = routeParameters;
                this.Results = (IList<ApiParameterDescription>) new List<ApiParameterDescription>();
            }

            public ControllerActionDescriptor ActionDescriptor { get; }

            public IModelMetadataProvider MetadataProvider { get; }

            public IList<ApiParameterDescription> Results { get; }

            public IReadOnlyList<TemplatePart> RouteParameters { get; }
        }

        private class ApiParameterDescriptionContext
        {
            public ModelMetadata ModelMetadata { get; set; }

            public string BinderModelName { get; set; }

            public BindingSource BindingSource { get; set; }

            public string PropertyName { get; set; }

            public static CustomApiDescriptionProvider.ApiParameterDescriptionContext GetContext(
                ModelMetadata metadata, Microsoft.AspNetCore.Mvc.ModelBinding.BindingInfo bindingInfo, string propertyName)
            {
                CustomApiDescriptionProvider.ApiParameterDescriptionContext descriptionContext =
                    new CustomApiDescriptionProvider.ApiParameterDescriptionContext();
                descriptionContext.ModelMetadata = metadata;
                descriptionContext.BinderModelName = (bindingInfo != null ? bindingInfo.BinderModelName : (string) null) ?? metadata.BinderModelName;
                BindingSource bindingSource = bindingInfo != null ? bindingInfo.BindingSource : (BindingSource) null;
                if ((object) bindingSource == null)
                    bindingSource = metadata.BindingSource;
                descriptionContext.BindingSource = bindingSource;
                descriptionContext.PropertyName = propertyName ?? metadata.PropertyName;
                return descriptionContext;
            }
        }

        private class PseudoModelBindingVisitor
        {
            public PseudoModelBindingVisitor(CustomApiDescriptionProvider.ApiParameterContext context, ParameterDescriptor parameter)
            {
                this.Context = context;
                this.Parameter = parameter;
                this.Visited =
                    new HashSet<CustomApiDescriptionProvider.PseudoModelBindingVisitor.PropertyKey>((IEqualityComparer<
                                                                                                         CustomApiDescriptionProvider.
                                                                                                         PseudoModelBindingVisitor.PropertyKey>)
                                                                                                     new CustomApiDescriptionProvider.
                                                                                                         PseudoModelBindingVisitor.
                                                                                                         PropertyKeyEqualityComparer());
            }

            public CustomApiDescriptionProvider.ApiParameterContext Context { get; }

            public ParameterDescriptor Parameter { get; }

            private HashSet<CustomApiDescriptionProvider.PseudoModelBindingVisitor.PropertyKey> Visited { get; }

            public void WalkParameter(CustomApiDescriptionProvider.ApiParameterDescriptionContext context)
            {
                BindingSource modelBinding = BindingSource.ModelBinding;
                this.Visit(context, modelBinding, string.Empty);
            }

            private void Visit(CustomApiDescriptionProvider.ApiParameterDescriptionContext bindingContext, BindingSource ambientSource,
                               string containerName)
            {
                BindingSource bindingSource = bindingContext.BindingSource;
                if (bindingSource != (BindingSource) null && bindingSource.IsGreedy)
                {
                    this.Context.Results.Add(this.CreateResult(bindingContext, bindingSource, containerName));
                }
                else
                {
                    ModelMetadata modelMetadata = bindingContext.ModelMetadata;
                    if (modelMetadata.IsEnumerableType || !modelMetadata.IsComplexType || modelMetadata.Properties.Count == 0)
                    {
                        IList<ApiParameterDescription> results = this.Context.Results;
                        CustomApiDescriptionProvider.ApiParameterDescriptionContext bindingContext1 = bindingContext;
                        BindingSource source = bindingSource;
                        if ((object) source == null)
                            source = ambientSource;
                        string containerName1 = containerName;
                        ApiParameterDescription result = this.CreateResult(bindingContext1, source, containerName1);
                        results.Add(result);
                    }
                    else
                    {
                        string str = containerName;
                        if (modelMetadata.ContainerType != (Type) null)
                            str = CustomApiDescriptionProvider.PseudoModelBindingVisitor.GetName(containerName, bindingContext);
                        for (int index = 0; index < modelMetadata.Properties.Count; ++index)
                        {
                            ModelMetadata property = modelMetadata.Properties[index];
                            CustomApiDescriptionProvider.PseudoModelBindingVisitor.PropertyKey propertyKey =
                                new CustomApiDescriptionProvider.PseudoModelBindingVisitor.PropertyKey(property, bindingSource);
                            CustomApiDescriptionProvider.ApiParameterDescriptionContext context =
                                CustomApiDescriptionProvider.ApiParameterDescriptionContext.GetContext(property,
                                                                                                        (Microsoft.AspNetCore.Mvc.ModelBinding.
                                                                                                            BindingInfo) null, (string) null);
                            if (this.Visited.Add(propertyKey))
                            {
                                CustomApiDescriptionProvider.ApiParameterDescriptionContext bindingContext1 = context;
                                BindingSource ambientSource1 = bindingSource;
                                if ((object) ambientSource1 == null)
                                    ambientSource1 = ambientSource;
                                string containerName1 = str;
                                this.Visit(bindingContext1, ambientSource1, containerName1);
                            }
                            else
                            {
                                IList<ApiParameterDescription> results = this.Context.Results;
                                CustomApiDescriptionProvider.ApiParameterDescriptionContext bindingContext1 = context;
                                BindingSource source = bindingSource;
                                if ((object) source == null)
                                    source = ambientSource;
                                string containerName1 = str;
                                ApiParameterDescription result = this.CreateResult(bindingContext1, source, containerName1);
                                results.Add(result);
                            }
                        }
                    }
                }
            }

            private ApiParameterDescription CreateResult(CustomApiDescriptionProvider.ApiParameterDescriptionContext bindingContext,
                                                         BindingSource source, string containerName)
            {
                return new ApiParameterDescription()
                       {
                           ModelMetadata = bindingContext.ModelMetadata,
                           Name = CustomApiDescriptionProvider.PseudoModelBindingVisitor.GetName(containerName, bindingContext),
                           Source = source,
                           Type = bindingContext.ModelMetadata.ModelType,
                           ParameterDescriptor = this.Parameter
                       };
            }

            private static string GetName(string containerName, CustomApiDescriptionProvider.ApiParameterDescriptionContext metadata)
            {
                if (!string.IsNullOrEmpty(metadata.BinderModelName))
                    return metadata.BinderModelName;
                return ModelNames.CreatePropertyModelName(containerName, metadata.PropertyName);
            }

            private struct PropertyKey
            {
                public readonly Type ContainerType;
                public readonly string PropertyName;
                public readonly BindingSource Source;

                public PropertyKey(ModelMetadata metadata, BindingSource source)
                {
                    this.ContainerType = metadata.ContainerType;
                    this.PropertyName = metadata.PropertyName;
                    this.Source = source;
                }
            }

            private class PropertyKeyEqualityComparer : IEqualityComparer<CustomApiDescriptionProvider.PseudoModelBindingVisitor.PropertyKey>
            {
                public bool Equals(CustomApiDescriptionProvider.PseudoModelBindingVisitor.PropertyKey x,
                                   CustomApiDescriptionProvider.PseudoModelBindingVisitor.PropertyKey y)
                {
                    if (x.ContainerType == y.ContainerType && x.PropertyName == y.PropertyName)
                        return x.Source == y.Source;
                    return false;
                }

                public int GetHashCode(CustomApiDescriptionProvider.PseudoModelBindingVisitor.PropertyKey obj)
                {
                    HashCodeCombiner hashCodeCombiner = HashCodeCombiner.Start();
                    hashCodeCombiner.Add((object) obj.ContainerType);
                    hashCodeCombiner.Add(obj.PropertyName);
                    hashCodeCombiner.Add((object) obj.Source);
                    return hashCodeCombiner.CombinedHash;
                }
            }
        }
 * 
 */