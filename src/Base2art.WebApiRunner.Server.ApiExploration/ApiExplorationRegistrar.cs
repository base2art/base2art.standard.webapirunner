﻿namespace Base2art.WebApiRunner.Server.ApiExploration
{
    using System;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class ApiExplorationRegistrar : RegistrationBase
    {
        public override void RegisterApiExploration(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterApiExploration(builder, services, config);

            builder.Services.TryAddSingleton<IApiDescriptionGroupCollectionProvider, CustomApiDescriptionGroupCollectionProvider>();
            builder.Services.TryAddEnumerable(ServiceDescriptor.Transient<IApiDescriptionProvider, CustomApiDescriptionProvider>());
        }
    }
}