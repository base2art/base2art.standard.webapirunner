if (Test-Path ".version")
{
    exit 0
}


$version = "0.0.0.0"
$minversion = [System.Version]::Parse("0.1.4.9")
$suffix = ""


if (-not(Test-Path /data/nuget-repository/))
{
    mkdir -Path "/data/nuget-repository/" -Force
}

$ToNatural = { [regex]::Replace($_, '\d+', { $args[0].Value.PadLeft(5) }) }

$items = Get-ChildItem -Path "/data/nuget-repository" -filter "Base2art.WebApiRunner.Server.Core.*.nupkg" |
        where{ -not($_ -match ".symbols.nupkg") } |
        Sort-Object $ToNatural -Descending

if ($items)
{
    $first = $items[0]
    $version = [System.Version]::Parse($first.Name.Replace("Base2art.WebApiRunner.Server.Core.", "").Replace(".nupkg", ""))

    $major = $version.Major
    $minor = $version.Minor
    $build = $version.Build
    $revision = $version.Revision

    if ($major -eq -1)
    {
        $major = 0
    }
    if ($minor -eq -1)
    {
        $minor = 0
    }
    if ($build -eq -1)
    {
        $build = 0
    }
    if ($revision -eq -1)
    {
        $revision = 0
    }

    $version = "$( $major ).$( $minor ).$( $build ).$( $revision + 1 )"
}

$derivedVersion = [System.Version]::Parse($version)

if( $minversion -gt $derivedVersion) {
    $version = $minversion.ToString()
}

Set-Content -Path ".version" -Value "$($version)$($suffix)"
