echo "Publishing ..."

$projName = "Base2art.WebApiRunner.Server.Runners.CommandLineInterface"

$publishProjs = @(
@($projName, "netstandard2.0"),
@($projName, "netcoreapp2.1"),
@($projName, "net472")
)



function processDir($base, $joiners, $rel, $output)
{
    $joined = $joiners -join '/'
    $relJoined = $rel -join "/"
    $output.Add(@( $relJoined, $joined ))

    $items = Get-ChildItem -Path $base | ?{ $_.PSIsContainer }

    foreach ($item in $items)
    {
        $joinerCopy = @()
        $relCopy = @()

        $joinerCopy = $joinerCopy + $joiners
        $relCopy = $relCopy + $rel

        $joinerCopy += $item.Name
        $relCopy += $item.Name


        processDir $item $joinerCopy $relCopy $output
    }

    #return $output
}


foreach ($publishProj in $publishProjs)
{
    $name = $publishProj[0]
    $frame = $publishProj[1]
    dotnet publish --configuration Release "src\$name\$name.csproj" -f "$( $frame )"

    if (-not$?)
    {
        exit $LASTEXITCODE
    }
}


$path = Get-Item "src\$( $projName )\bin\Release\net472\publish"
$output = New-Object System.Collections.Generic.List[System.Object]
processDir $path @("net472") @("bin\Release\net472\publish") $output


$path = Get-Item "src\$( $projName )\bin\Release\netcoreapp2.1\publish"
processDir $path @("netcoreapp2.1") @("bin\Release\netcoreapp2.1\publish") $output

#echo "-------------"



$csProjPath = "src\$( $projName )\$( $projName ).csproj"
$newCsProjPath = "src\$( $projName )\$($projName.Replace(".Server.Runners.", ".") ).csproj"
$doc = New-Object System.Xml.XmlDocument
$doc.Load($csProjPath)

#$ns = New-Object System.Xml.XmlNamespaceManager($doc.NameTable)
#$ns.AddNamespace("ns", $doc.DocumentElement.NamespaceURI)

$node = $doc.SelectSingleNode("//ItemGroup[@Label='build-system']")


function appendToParent($parent, $include, $packgePath)
{
    $child = $doc.CreateElement("Content")
    $child.SetAttribute("Include", $include)
    $child.SetAttribute("Pack", "true")
    $child.SetAttribute("PackagePath", $packgePath)

    $swallow = $parent.AppendChild($child)
}

foreach ($item in $output)
{
    appendToParent $node "$( $item[0] )\*.config" $item[1]
    appendToParent $node "$( $item[0] )\*.dll" $item[1]
    appendToParent $node "$( $item[0] )\*.exe" $item[1]
    appendToParent $node "$( $item[0] )\*.pdb" $item[1]
    appendToParent $node "$( $item[0] )\*.json" $item[1]
}

$nodes = $doc.SelectNodes("//ProjectReference")

foreach ($node_ in $nodes)
{
    $node_.SetAttribute("PrivateAssets", "true")
}

$sdk = $doc.DocumentElement.GetAttribute("Sdk")
$doc.DocumentElement.SetAttribute("Sdk",$sdk.Replace(".Web", ""))

$cwd = Resolve-Path .
$newCsProjPath = Join-Path $cwd $newCsProjPath
$doc.Save($newCsProjPath)


