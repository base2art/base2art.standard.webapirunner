
## must copy locally to ~/data for this to work

cd nuget-repository/current

$pkgs = Get-ChildItem -Path "." -filter "Base2art.WebApiRunner.*.nupkg"
echo $pkgs

$key = Get-Content -Raw "~/.config/base2art.nuget-config"

$key = $key.Trim()

ForEach ($pkg in $pkgs)
{
  dotnet nuget push $pkg.name -k "$($key)" --source "https://api.nuget.org/v3/index.json"
}


cd ../..
