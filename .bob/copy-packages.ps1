if (-not(Test-Path ~/data/nuget-repository/))
{
    New-Item -ItemType Directory -Force  -Path ~/data/nuget-repository/
}


if (Test-Path /data/nuget-repository/current)
{
    Remove-Item -Path ~/data/nuget-repository/current -Force -Recurse
}

if (-not(Test-Path /data/nuget-repository/current))
{
    New-Item -ItemType Directory  -Path ~/data/nuget-repository/current -Force
}

$folders = Get-ChildItem -Path "src" 

foreach ($folder in $folders)
{
    $projPath = "src/$( $folder.Name )/bin/Release"

    if (Test-Path $projPath)
    {
        $childs = Get-ChildItem -Path $projPath -Filter "*.nupkg"


        foreach ($child in $childs)
        {
            Copy-Item -Path $child.FullName -Destination ~/data/nuget-repository
        }

        foreach ($child in $childs)
        {
            Copy-Item -Path $child.FullName -Destination ~/data/nuget-repository/current
        }
    }
}


#
#$folders = Get-ChildItem -Path "src/addins/stable" 
#
#foreach ($folder in $folders)
#{
#    $projPath = "src/addins/stable/$( $folder.Name )/bin/Release"
#
#
#    if (Test-Path $projPath)
#    {
#        $childs = Get-ChildItem -Path $projPath -Filter "*.nupkg"
#
#
#        foreach ($child in $childs)
#        {
#            Copy-Item -Path $child.FullName -Destination "/data/nuget-repository"
#        }
#
#        foreach ($child in $childs)
#        {
#            Copy-Item -Path $child.FullName -Destination "/data/nuget-repository/current"
#        }
#
#    }
#}