


#### How do I return Different Http Response codes when writing an application?

In your service you can throw the following exceptions resulting in the the following errors:


| Exception                                                      | Resulting http status code|
| -------------------------------------------------------------- | --------------------------|
| Base2art.WebApiRunner.App.Api.NotAuthenticatedException        | 401                       |
| System.UnauthorizedAccessException                             | 401                       |
| System.Authentication.AuthenticationException                  | 401                       |
| Base2art.WebApiRunner.App.Api.NotAuthorizedToResourceException | 403                       |
| System.AccessViolationException                                | 403                       |
| Base2art.WebApiRunner.App.Api.ConflictException                | 409                       |
| System.InvalidOperationException                               | 409                       |
| Base2art.WebApiRunner.App.Api.UnprocessableEntityException     | 422                       |
| System.ArgumentException                                       | 422                       |
| System.FormatException                                         | 422                       |
| System.IO.InvalidDataException                                 | 422                       |
| System.NotImplementedException                                 | 501                       |
| All Other Uncaught Exceptions (by default)                     | 500                       |

