
#### What is the difference between admin and public

#### How do I configure alternate mime-types ?

#### How do I configure additional filters?

##### Default Filters

#### How do I maniplate the output response?

#### How do I configure additional formats?

##### Default Formats

#### How do I configure additional formatters?

##### Default Formatters 


#### How do I pass a dictionary-like object to my injected instance.

Example (Config):

```
    - requestedType: Base2art.WebApiRunner.Security.IRoleLookup
      fulfillingType: Base2art.WebApiRunner.Security.ConfigurableRoleLookup
      parameters:
        userLookup:
          "test@base2art.com":
            - "user"
            - "everyone"
        
        
```


Example (Code):

```
namespace Base2art.WebApiRunner.Security
{
  using System.Linq;
  
  public class ConfigurableRoleLookup
  {
    public ConfigurableRoleLookup(Dictionary<object, object> userLookup)
    {
      // Unfortunately auto conversion is not currently happening.
      object listOfRolesObject = userLookup["test@base2art.com"];
      string[] listOfRoles = ((List<object>)listOfRolesObject).Select(x=>x.ToString()).ToArray();
    }
  }
}

```

