namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System;
    using System.DirectoryServices.AccountManagement;

    public class ActiveDirectoryLookupController
    {
        public string List()
        {
            var domain = Environment.UserDomainName;
            var user = Environment.UserName;

            try
            {
                var context = new PrincipalContext(ContextType.Domain);
                var identity = UserPrincipal.FindByIdentity(context, user);
                return identity.Sid.ToString();
            }
            catch (Exception)
            {
                var context = new PrincipalContext(ContextType.Machine);
                var identity = UserPrincipal.FindByIdentity(context, "Administrator");
                return identity.Sid.ToString();
            }

            //new[] {"Item1", "Item2"};
        }
    }
}