﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System.Collections.Generic;
    using System.Linq;
    using Repositories;
    using Web.Http;

    public class CustomControllerWithInterfaceParameters
    {
        private readonly IListProvider provider;

        public CustomControllerWithInterfaceParameters(IListProvider provider) => this.provider = provider;

        public List<string> GetNames(IHttpRequest request) => this.provider.GetItems().Concat(new[] {request.Uri.AbsolutePath}).ToList();
    }
}