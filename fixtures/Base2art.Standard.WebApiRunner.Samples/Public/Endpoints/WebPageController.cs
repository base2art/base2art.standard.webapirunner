﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Web.Pages;

    public class WebPageController
    {
        public WebPage GetWebPage() => WebPage.Create(new Test1());

        public WebPage GetScript() => WebPage.Create(new Test1(), "application/javascript");

        public class Test1 : IWebPage
        {
            async Task<string> IWebPage.ExecuteAsync()
            {
                using (var sw = new StringWriter())
                {
                    await sw.WriteLineAsync();

                    await sw.WriteLineAsync(DateTime.UtcNow.Date.ToString("yyyy-MM-dd"));
                    await sw.WriteLineAsync();
                    await sw.FlushAsync();
                    return sw.GetStringBuilder().ToString();
                }
            }
        }
    }
}