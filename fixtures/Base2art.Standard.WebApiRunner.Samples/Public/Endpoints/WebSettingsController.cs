namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System.Collections;
    using System.Collections.Generic;
    using Web.ServerEnvironment;

    public class WebSettingsController
    {
        private readonly IWebServerSettings settings;

        public WebSettingsController(IWebServerSettings settings)
        {
            this.settings = settings;
        }

        public Dictionary<string, string> GetSettings()
            => new Dictionary<string, string>
               {
                   {nameof(IWebServerSettings.ContentRoot), this.settings?.ContentRoot},
                   {nameof(IWebServerSettings.CurrentDirectory), this.settings?.CurrentDirectory},
                   {nameof(IWebServerSettings.WebRoot), this.settings?.WebRoot},
               };
    }
}