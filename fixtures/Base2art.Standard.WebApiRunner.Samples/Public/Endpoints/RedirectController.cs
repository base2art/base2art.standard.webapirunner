﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System.Threading.Tasks;
    using Resources;

    public class RedirectController
    {
        public Task<Person> Get(string id) => Task.FromResult(new Person {Name = id});
    }
}