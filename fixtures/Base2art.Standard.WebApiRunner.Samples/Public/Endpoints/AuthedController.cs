namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System;
    using System.Security.Claims;

    public class AuthedController
    {
        public string[] List(ClaimsPrincipal principal) => new[]
                                                           {
                                                               principal.Identity.IsAuthenticated.ToString(),
                                                               Environment.UserDomainName,
                                                               Environment.UserName,
                                                               Environment.MachineName
                                                           };
    }
}