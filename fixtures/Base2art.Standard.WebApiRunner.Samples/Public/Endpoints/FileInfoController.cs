﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;

    public class FileInfoController
    {
        public FileInfo GetFileInfo(string path)
            => new FileInfo(path.Replace("_", "/"));

        public async Task<string> UploadFileInfo(IFormFile file)
            => file.Name + ":" + file.ContentType + ":" + await this.GetContent(file);

        private async Task<string> GetContent(IFormFile file)
        {
            using (var openReadStream = file.OpenReadStream())
            {
                using (var sr = new StreamReader(openReadStream))
                {
                    return await sr.ReadToEndAsync();
                }
            }
        }
    }
}