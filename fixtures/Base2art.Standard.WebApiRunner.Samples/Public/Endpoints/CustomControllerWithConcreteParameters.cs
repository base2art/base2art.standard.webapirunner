﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using Repositories;

    public class CustomControllerWithConcreteParameters : CustomControllerWithParametersBase<ListProvider>
    {
        public CustomControllerWithConcreteParameters(ListProvider provider) : base(provider, x => x.GetItems().ToArray())
        {
        }
    }
}