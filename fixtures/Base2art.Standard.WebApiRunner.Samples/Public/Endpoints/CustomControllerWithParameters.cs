﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System.Linq;

    public class CustomControllerWithParameters
    {
        private readonly string suffix;

        public CustomControllerWithParameters(string suffix) => this.suffix = suffix;

        public string[] List() => new[] {"Item1", "Item2"}.Select(x => x + this.suffix).ToArray();
    }
}