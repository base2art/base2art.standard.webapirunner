﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Security.Claims;
    using Resources;
    using Web.Http;

    public class CustomController
    {
        private readonly string[] listItems;

        public CustomController(string[] listItems) => this.listItems = listItems;

        public string[] List() => this.listItems.Concat(new[] {"Item1", "Item2"}).ToArray();

        public string[] GetParameterOptional(int i = 2) => Enumerable.Range(0, i).Select(x => "Item" + x).ToArray();

        public string[] GetParameter(int i) => Enumerable.Range(0, i).Select(x => "Item" + x).ToArray();

        public string[] GetParameterNullable(int? i) => Enumerable.Range(0, i.GetValueOrDefault()).Select(x => "Item" + x).ToArray();

        public string[] GetParameterNullableOptional(int? i = 2) => Enumerable.Range(0, i.GetValueOrDefault(3)).Select(x => "Item" + x).ToArray();

        public string GetPath(IHttpRequest request, int? id = null) => id + request.Uri.AbsolutePath;

        public string[] GetFiltered(PaginationData paginationData, ClaimsPrincipal principal)
        {
            if (principal?.Identity == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            var start = paginationData.PageIndex * paginationData.PageSize;

            var items = Enumerable.Range(0, 1000).Select(x => "Item" + x).Skip(start).Take(paginationData.PageSize).ToArray();
            return items;
        }

        public string[] Augment(string[] input)
        {
            return input.Select((x, i) => x + "-" + i).ToArray();
        }

        public string[] AugmentString(string value)
        {
            return new[] {value, DateTime.Today.ToString("yyyy-MM-dd")};
        }

        public string[] AugmentInt(int value)
        {
            return new[] {value.ToString(), DateTime.Today.ToString("yyyy-MM-dd")};
        }

        public Person AugmentPerson(Person value) => new Person {Name = $"{value?.Name}-{DateTime.Today:yyyy-MM-dd}"};

        public Person[] AugmentPersons(Person[] value)
        {
            return value.Select(x => new Person {Name = $"{x?.Name}-{DateTime.Today:yyyy-MM-dd}"}).ToArray();
        }

        public void Exception(string exceptionType, string additionalData = null)
        {
            var asms = new[] {"Base2art.Web.Exceptions", "netstandard"};

            var additionalDataParts = (additionalData ?? string.Empty).Split(',')
                                                                      .Where(x => !string.IsNullOrWhiteSpace(x))
                                                                      .ToArray();

            Type type = null;

            foreach (var asm in asms)
            {
                type = Type.GetType(exceptionType + ", " + asm, false, true);
                if (type != null)
                {
                    break;
                }
            }

            if (type == null)
            {
                return;
            }

            if (additionalDataParts.Length == 0)
            {
                var exception1 = Activator.CreateInstance(type);

                throw (Exception) exception1;
            }

            var firstMatchingCtor = type.GetConstructors().FirstOrDefault(x => x.GetParameters().Length == additionalDataParts.Length);

            var parms = firstMatchingCtor.GetParameters()
                                         .Select<ParameterInfo, object>((x, i) =>
                                         {
                                             var curr = additionalDataParts[i];

                                             if (x.ParameterType == typeof(string))
                                             {
                                                 return curr;
                                             }

                                             if (x.ParameterType == typeof(bool))
                                             {
                                                 return bool.Parse(curr);
                                             }

                                             return curr;
                                         })
                                         .ToArray();

            var exception = Activator.CreateInstance(type, parms);

            throw (Exception) exception;
        }
    }
}