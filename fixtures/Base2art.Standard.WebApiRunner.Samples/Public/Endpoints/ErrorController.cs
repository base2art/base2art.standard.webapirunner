﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System;

    public class ErrorController
    {
        public string[] List(string exceptionType)
        {
            var asm = typeof(InvalidCastException).Assembly;
            var type = asm.GetType(exceptionType);
            if (type == null)
            {
                return new[] {"asd"};
            }

            throw (Exception) Activator.CreateInstance(type);
        }
    }
}