﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using Web.ValueTypes;

    public class ValueTypeController
    {
        public string GetDomainName(DomainName d, int id) => d.Value + "-" + id;
    }
}