﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class CustomControllerWithParametersBase<T>
    {
        private readonly Func<T, string[]> lookup;
        private readonly T provider;

        public CustomControllerWithParametersBase(T provider, Func<T, string[]> lookup)
        {
            this.provider = provider;
            this.lookup = lookup;
        }

        public List<string> GetNames() => this.lookup(this.provider).ToList();
    }
}