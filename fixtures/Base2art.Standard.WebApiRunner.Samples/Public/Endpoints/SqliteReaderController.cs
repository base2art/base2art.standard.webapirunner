namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Microsoft.Data.Sqlite;

    public class SqliteReaderController
    {
        public string[] List()
        {
            var sql = "select Name from User ORDER BY Name DESC";
            var builder = new SqliteConnectionStringBuilder();

            var cd = Environment.CurrentDirectory;
            var curr = new DirectoryInfo(cd);

            while (!curr.GetDirectories().Select(x => x.Name).Contains(".bob"))
            {
                curr = curr.Parent;
            }

            builder.DataSource = Path.Combine(
                                              curr.FullName,
                                              "fixtures",
                                              "Base2art.Standard.WebApiRunner.Samples",
                                              "wwwroot",
                                              "sample-db.db");

            var dbConnection = new SqliteConnection(builder.ConnectionString);
            dbConnection.Open();
            var command = new SqliteCommand(sql, dbConnection);
            var reader = command.ExecuteReader();
            var items = new List<string>();
            while (reader.Read())
            {
                items.Add(reader["Name"]?.ToString() ?? string.Empty);
            }

            return items.ToArray();
        }
    }
}