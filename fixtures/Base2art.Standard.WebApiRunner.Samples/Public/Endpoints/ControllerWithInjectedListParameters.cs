namespace Base2art.Standard.WebApiRunner.Samples.Public.Endpoints
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ControllerWithInjectedListParameters
    {
        private readonly List<int> items;

        public ControllerWithInjectedListParameters(List<int> items) => this.items = items;

        public Task<IEnumerable<int>> Get() => Task.FromResult<IEnumerable<int>>(this.items);
    }
}