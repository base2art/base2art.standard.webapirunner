﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Tasks
{
    using System.Threading.Tasks;

    public class CustomTask
    {
        public Task<string> ExecuteAsync() => Task.FromResult("151E1BA8-2C56-4405-909A-48A213BD99CE");
    }
}