﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Resources;

    public class FileWritingTask
    {
        public Task<string> ExecuteAsync()
        {
            var outputPath = Path.Combine(Path.GetTempPath(), $"{nameof(FileWritingTask)}.{nameof(this.ExecuteAsync)}.log");
            File.WriteAllText(outputPath, DateTime.UtcNow.ToString(CultureInfo.InvariantCulture));
            return Task.FromResult("151E1BA8-2C56-4405-909A-48A213BD99CE");
        }

        public Task<string> ExecuteAsync(IDictionary<string, int> data) => this.Actual(data, MethodBase.GetCurrentMethod());

        public Task<string> ExecuteAsync(Person data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var value = data.Name;

            var hashCode = typeof(Person).FullName.GetHashCode();

            var outputPath = Path.Combine(Path.GetTempPath(), $"{nameof(FileWritingTask)}.{nameof(this.ExecuteAsync)}-{hashCode}.log");
            File.WriteAllText(outputPath, value);
            return Task.FromResult("151E1BA8-2C56-4405-909A-48A213BD99CE");
        }

        //////////////

        public Task<string> ExecuteAsync(Dictionary<string, object> data) => this.Actual(data, MethodBase.GetCurrentMethod());

        public Task<string> ExecuteAsync(IDictionary<string, object> data) => this.Actual(data, MethodBase.GetCurrentMethod());

        public Task<string> ExecuteAsync(IReadOnlyDictionary<string, object> data) => this.Actual(data, MethodBase.GetCurrentMethod());

        //////////////

        public Task<string> ExecuteAsync(Dictionary<string, string> data) => this.Actual(data, MethodBase.GetCurrentMethod());

        public Task<string> ExecuteAsync(IDictionary<string, string> data) => this.Actual(data, MethodBase.GetCurrentMethod());

        public Task<string> ExecuteAsync(IReadOnlyDictionary<string, string> data) => this.Actual(data, MethodBase.GetCurrentMethod());

        private Task<string> Actual<TValue>(IEnumerable<KeyValuePair<string, TValue>> data, MethodBase method)
//        where T : IEnumerable<KeyValuePair<string, TValue>>
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var value = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture);
            var firstOrDefault = data.Where(x => x.Key == "Name").ToArray();
            if (firstOrDefault.Length == 1)
            {
                value = $"{firstOrDefault[0].Value}";
            }

            var hashCode = method.GetParameters()[0].ParameterType.FullName.GetHashCode();

            var outputPath = Path.Combine(Path.GetTempPath(), $"{nameof(FileWritingTask)}.{nameof(this.ExecuteAsync)}-{hashCode}.log");
            File.WriteAllText(outputPath, value);
            return Task.FromResult("151E1BA8-2C56-4405-909A-48A213BD99CE");
        }
    }
}