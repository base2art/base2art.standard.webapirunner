namespace Base2art.Standard.WebApiRunner.Samples.Public.Tasks
{
    using System.Threading.Tasks;
    using MessageQueue;
    using Web.Messages;

    public class RestartServerTask
    {
        private readonly IMessageQueue queue;

        public RestartServerTask(IMessageQueue queue) => this.queue = queue;

        public Task ExecuteAsync() => this.queue.Push(new RecycleAppMessage());
    }
}