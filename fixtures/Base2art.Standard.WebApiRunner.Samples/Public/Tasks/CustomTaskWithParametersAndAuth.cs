namespace Base2art.Standard.WebApiRunner.Samples.Public.Tasks
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public class CustomTaskWithParametersAndAuth
    {
        public Task<Dictionary<string, string>> ExecuteAsync(IDictionary<string, string> values, ClaimsPrincipal user)
            => Task.FromResult(values.ToDictionary(x => x.Key, x => x.Value));
    }
}