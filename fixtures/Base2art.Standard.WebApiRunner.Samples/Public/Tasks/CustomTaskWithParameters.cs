﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Tasks
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CustomTaskWithParameters
    {
        public Task<Dictionary<string, string>> ExecuteAsync(IDictionary<string, string> values)
            => Task.FromResult(values.ToDictionary(x => x.Key, x => x.Value));
    }
}