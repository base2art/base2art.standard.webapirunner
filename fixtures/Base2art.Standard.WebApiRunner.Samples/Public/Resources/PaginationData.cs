﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.Resources
{
    public class PaginationData
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}