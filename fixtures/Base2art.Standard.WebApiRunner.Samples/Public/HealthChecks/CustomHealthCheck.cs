﻿namespace Base2art.Standard.WebApiRunner.Samples.Public.HealthChecks
{
    using System;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.Server.Routing.Models;

    public class CustomHealthCheck
    {
        public HealthCheckResultItem ExecuteWithReturnHealthCheck()
        {
            Console.Write("7EF6CAF4-CA7A-4A90-897A-DC3655BF2A18");
            return new HealthCheckResultItem {Data = "6EF6CAF4-CA7A-4A90-897A-DC3655BF2A18"};
        }

        public HealthCheckResultItem ExecuteHealthyWithReturnHealthCheck()
        {
            Console.Write("7EF6CAF4-CA7A-4A90-897A-DC3655BF2A19");
            return new HealthCheckResultItem {IsHealthy = true, Message = "", Data = "6EF6CAF4-CA7A-4A90-897A-DC3655BF2A19"};
        }

        public Task<HealthCheckResultItem> ExecuteAsyncWithReturnHealthCheck()
        {
            Console.Write("0EA91652-462B-4BF7-912D-57AF3C8B3F45");
            return Task.FromResult(new HealthCheckResultItem {Data = "1EA91652-462B-4BF7-912D-57AF3C8B3F45"});
        }

        public string ExecuteWithReturnString()
        {
            Console.Write("52710F05-9902-4D2B-8EBD-38979B5F2B97");
            return "151E1BA8-2C56-4405-909A-48A213BD99CE";
        }

        public Task<string> ExecuteAsyncWithReturnString()
        {
            Console.Write("6248EFD2-0306-4FF5-BE28-0C528131FA6B");
            return Task.FromResult("151E1BA8-2C56-4405-909A-48A213BD99CF");
        }

        public Task ExecuteAsync()
        {
            Console.Write("F4AEAEAC-5B1E-48BC-BC4C-07BB4F31DAE9");
            return Task.CompletedTask;
        }

        public void Execute()
        {
            Console.Write("E4937DE0-8B37-45AF-9B5E-82C71D8403B2");
        }
    }
}