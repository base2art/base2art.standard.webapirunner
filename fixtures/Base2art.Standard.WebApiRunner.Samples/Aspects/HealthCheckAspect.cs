﻿namespace Base2art.Standard.WebApiRunner.Samples.Aspects
{
    using System;
    using System.Reflection;
    using System.Threading.Tasks;
    using Base2art.Aspects;

    public abstract class HealthCheckAspect<T> : IAspect
    {
        Task IAspect.OnInvoke(IMethodInterceptionArgs args, Func<Task> next)
        {
            if (args.Method.ReturnType == typeof(T) || args.Method.ReturnType == typeof(Task<>).MakeGenericType(typeof(T)))
            {
                return this.OnInvoke(new MethodInterceptionArgs<T>(args), async () =>
                {
                    await next();
                    return (T) args.ReturnValue;
                });
            }

            return next();
        }

        public IAspect AspectIt() => this;

        protected abstract Task OnInvoke(IMethodInterceptionArgs<T> args, Func<Task<T>> next);
    }

    internal class MethodInterceptionArgs<T> : IMethodInterceptionArgs<T>
    {
        private readonly IMethodInterceptionArgs args;

        public MethodInterceptionArgs(IMethodInterceptionArgs args) => this.args = args;

        public Arguments Arguments => this.args.Arguments;

        public T ReturnValue
        {
            get => (T) this.args.ReturnValue;
            set => this.args.ReturnValue = value;
        }

        object IMethodInterceptionArgs.ReturnValue
        {
            get => this.args.ReturnValue;
            set => this.args.ReturnValue = value;
        }

        public object Instance => this.args.Instance;

        public MethodInfo Method => this.args.Method;
    }
}