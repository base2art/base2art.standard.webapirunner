﻿namespace Base2art.Standard.WebApiRunner.Samples.Aspects
{
    using System;
    using System.Reflection;
    using System.Threading.Tasks;

    internal static class AspectManagers
    {
        public static Type TaskOfType<T1>() => typeof(Task<T1>);

        public static bool IsAssignableTo(this Type type, Type methodReturnType) =>
            type == methodReturnType || methodReturnType.GetTypeInfo().IsAssignableFrom(type.GetTypeInfo());
    }
}