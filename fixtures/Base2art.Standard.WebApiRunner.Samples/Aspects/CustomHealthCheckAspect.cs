﻿namespace Base2art.Standard.WebApiRunner.Samples.Aspects
{
    using System;
    using System.Threading.Tasks;
    using Base2art.Aspects;
    using Base2art.WebApiRunner.Server.Routing.Models;

    public class CustomHealthCheckAspect : TypedAspect<HealthCheckResult>
    {
        public CustomHealthCheckAspect()
        {
            Console.WriteLine("Here");
        }

        protected override async Task OnInvoke(IMethodInterceptionArgs<HealthCheckResult> args, Func<Task<HealthCheckResult>> next)
        {
            await next();

            var result = args.ReturnValue;
            result.Items[0].Value.Data = new {Next = "here"};
        }
    }
}