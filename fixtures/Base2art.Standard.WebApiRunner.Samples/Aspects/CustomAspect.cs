﻿namespace Base2art.Standard.WebApiRunner.Samples.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.Aspects;

    public class CustomAspect : TypedAspect<string[]>
    {
        protected override async Task OnInvoke(IMethodInterceptionArgs<string[]> args, Func<Task<string[]>> next)
        {
            await next();

            var result = args.ReturnValue;

            var strs = new List<string>(result);
            strs.Insert(0, "Item0");
            strs.Add("Item3");
            args.ReturnValue = strs.ToArray();
        }
    }
}