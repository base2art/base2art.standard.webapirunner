namespace Base2art.Standard.WebApiRunner.Samples
{
    [System.CodeDom.Compiler.GeneratedCode("SjY", "1.0.0.0")]
    public static class Router
    {
        public static CustomControllerBuilder CustomController
        {
            get
            {
                return new CustomControllerBuilder();
            }
        }

        public static MessageHandlerServiceBuilder MessageHandlerService
        {
            get
            {
                return new MessageHandlerServiceBuilder();
            }
        }

        public static HealthCheckControllerBuilder HealthCheckController
        {
            get
            {
                return new HealthCheckControllerBuilder();
            }
        }

        public class CustomControllerBuilder
        {
            public CustomController_ListBuilder List
            {
                get
                {
                    return new CustomController_ListBuilder();
                }
            }

            public CustomController_GetFilteredBuilder GetFiltered
            {
                get
                {
                    return new CustomController_GetFilteredBuilder();
                }
            }

            public CustomController_GetParameterOptionalBuilder GetParameterOptional
            {
                get
                {
                    return new CustomController_GetParameterOptionalBuilder();
                }
            }

            public CustomController_GetParameterBuilder GetParameter
            {
                get
                {
                    return new CustomController_GetParameterBuilder();
                }
            }

            public CustomController_GetParameterNullableBuilder GetParameterNullable
            {
                get
                {
                    return new CustomController_GetParameterNullableBuilder();
                }
            }

            public CustomController_GetParameterNullableOptionalBuilder GetParameterNullableOptional
            {
                get
                {
                    return new CustomController_GetParameterNullableOptionalBuilder();
                }
            }

            public CustomController_AugmentPersonBuilder AugmentPerson
            {
                get
                {
                    return new CustomController_AugmentPersonBuilder();
                }
            }

            public CustomController_AugmentPersonsBuilder AugmentPersons
            {
                get
                {
                    return new CustomController_AugmentPersonsBuilder();
                }
            }

            public CustomController_ExceptionBuilder Exception
            {
                get
                {
                    return new CustomController_ExceptionBuilder();
                }
            }
        }

        public class MessageHandlerServiceBuilder
        {
            public MessageHandlerService_ProcessItemBuilder ProcessItem
            {
                get
                {
                    return new MessageHandlerService_ProcessItemBuilder();
                }
            }
        }

        public class HealthCheckControllerBuilder
        {
            public HealthCheckController_ExecuteAsyncBuilder ExecuteAsync
            {
                get
                {
                    return new HealthCheckController_ExecuteAsyncBuilder();
                }
            }
        }

        public class CustomController_ListBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo()
            {
                var url = "/api/test";
                return (System.Net.Http.HttpMethod.Get, url);
            }

            public string Link()
            {
                var url = "/api/test";
                return url;
            }
        }

        public class CustomController_GetFilteredBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo(Base2art.Standard.WebApiRunner.Samples.Public.Resources.PaginationData paginationData)
            {
                var url = "/api/test-paged";
                url = ReplaceInUrlStruct(url, "paginationData.PageIndex", paginationData?.PageIndex);
                url = ReplaceInUrlStruct(url, "paginationData.PageSize", paginationData?.PageSize);
                return (System.Net.Http.HttpMethod.Get, url);
            }

            public string Link(Base2art.Standard.WebApiRunner.Samples.Public.Resources.PaginationData paginationData)
            {
                var url = "/api/test-paged";
                url = ReplaceInUrlStruct(url, "paginationData.PageIndex", paginationData?.PageIndex);
                url = ReplaceInUrlStruct(url, "paginationData.PageSize", paginationData?.PageSize);
                return url;
            }
        }

        public class CustomController_GetParameterOptionalBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo(int i = 2)
            {
                var url = "/api/test-parameter-optional";
                if (i != 2)
                {
                    url = ReplaceInUrlStruct(url, "i", i);
                }

                return (System.Net.Http.HttpMethod.Get, url);
            }

            public string Link(int i = 2)
            {
                var url = "/api/test-parameter-optional";
                if (i != 2)
                {
                    url = ReplaceInUrlStruct(url, "i", i);
                }

                return url;
            }
        }

        public class CustomController_GetParameterBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo(int i)
            {
                var url = "/api/test-parameter";
                url = ReplaceInUrlStruct(url, "i", i);
                return (System.Net.Http.HttpMethod.Get, url);
            }

            public string Link(int i)
            {
                var url = "/api/test-parameter";
                url = ReplaceInUrlStruct(url, "i", i);
                return url;
            }
        }

        public class CustomController_GetParameterNullableBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo(System.Nullable<int> i)
            {
                var url = "/api/test-parameter-nullable";
                url = ReplaceInUrlStruct(url, "i", i);
                return (System.Net.Http.HttpMethod.Get, url);
            }

            public string Link(System.Nullable<int> i)
            {
                var url = "/api/test-parameter-nullable";
                url = ReplaceInUrlStruct(url, "i", i);
                return url;
            }
        }

        public class CustomController_GetParameterNullableOptionalBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo(System.Nullable<int> i = 2)
            {
                var url = "/api/test-parameter-nullable-optional";
                if (i != 2)
                {
                    url = ReplaceInUrlStruct(url, "i", i);
                }

                return (System.Net.Http.HttpMethod.Get, url);
            }

            public string Link(System.Nullable<int> i = 2)
            {
                var url = "/api/test-parameter-nullable-optional";
                if (i != 2)
                {
                    url = ReplaceInUrlStruct(url, "i", i);
                }

                return url;
            }
        }

        public class CustomController_AugmentPersonBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo()
            {
                var url = "/api/test-person";
                return (System.Net.Http.HttpMethod.Put, url);
            }

            public string Link()
            {
                var url = "/api/test-person";
                return url;
            }
        }

        public class CustomController_AugmentPersonsBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo()
            {
                var url = "/api/test-person";
                return (new System.Net.Http.HttpMethod("FAKE"), url);
            }

            public string Link()
            {
                var url = "/api/test-person";
                return url;
            }
        }

        public class CustomController_ExceptionBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo(string exceptionType, string additionalData = null)
            {
                var url = "/api/exception/{exceptionType}";
                url = ReplaceInUrlString(url, "exceptionType", exceptionType);
                if (additionalData != null)
                {
                    url = ReplaceInUrlString(url, "additionalData", additionalData);
                }

                return (System.Net.Http.HttpMethod.Get, url);
            }

            public string Link(string exceptionType, string additionalData = null)
            {
                var url = "/api/exception/{exceptionType}";
                url = ReplaceInUrlString(url, "exceptionType", exceptionType);
                if (additionalData != null)
                {
                    url = ReplaceInUrlString(url, "additionalData", additionalData);
                }

                return url;
            }
        }

        public class MessageHandlerService_ProcessItemBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo()
            {
                var url = "/tasks/queue/process-item";
                return (System.Net.Http.HttpMethod.Post, url);
            }

            public string Link()
            {
                var url = "/tasks/queue/process-item";
                return url;
            }
        }

        public class HealthCheckController_ExecuteAsyncBuilder
        {
            public (System.Net.Http.HttpMethod method, string route) RouteInfo(string internalTagC4F47CA0A9D44F739E3A0BB182CF27D7 = "")
            {
                var url = "/healthchecks";
                if (internalTagC4F47CA0A9D44F739E3A0BB182CF27D7 != "")
                {
                    url = ReplaceInUrlString(url, "internalTagC4F47CA0A9D44F739E3A0BB182CF27D7", internalTagC4F47CA0A9D44F739E3A0BB182CF27D7);
                }

                return (System.Net.Http.HttpMethod.Get, url);
            }

            public string Link(string internalTagC4F47CA0A9D44F739E3A0BB182CF27D7 = "")
            {
                var url = "/healthchecks";
                if (internalTagC4F47CA0A9D44F739E3A0BB182CF27D7 != "")
                {
                    url = ReplaceInUrlString(url, "internalTagC4F47CA0A9D44F739E3A0BB182CF27D7", internalTagC4F47CA0A9D44F739E3A0BB182CF27D7);
                }

                return url;
            }
        }

        private static string ReplaceInUrl(string url, string var1, string val1)
        {
            var realVal = val1 ?? string.Empty;
            if (url.Contains($"{{{var1}}}"))
            {
                return url.Replace($"{{{var1}}}", System.Uri.EscapeUriString(realVal));
            }

            var kvp = $"{System.Uri.EscapeUriString(var1)}={System.Uri.EscapeUriString(realVal)}";
            var joiner = url.Contains("?") ? "&" : "?";
            return $"{url}{joiner}{kvp}";
        }

        private static string ReplaceInUrlString(string url, string var1, string val1)
        {
            return ReplaceInUrl(url, var1, val1);
        }

        private static string ReplaceInUrlStruct(string url, string var1, System.ValueType val1)
        {
            return ReplaceInUrl(url, var1, val1.ToString());
        }

        private static string ReplaceInUrlEnum(string url, string var1, System.Enum val1)
        {
            return ReplaceInUrl(url, var1, val1.ToString("G"));
        }

        private static string ReplaceInUrlObject(string url, string var1, object val1)
        {
            return ReplaceInUrl(url, var1, val1.ToString());
        }
    }
}