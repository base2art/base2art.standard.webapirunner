﻿namespace Base2art.Standard.WebApiRunner.Samples.Repositories
{
    using System.Collections.Generic;

    public interface IListProvider
    {
        List<string> GetItems();
    }
}