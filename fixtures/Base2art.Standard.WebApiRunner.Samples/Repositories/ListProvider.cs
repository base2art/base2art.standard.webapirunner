﻿namespace Base2art.Standard.WebApiRunner.Samples.Repositories
{
    using System.Collections.Generic;

    public class ListProvider : IListProvider
    {
        public List<string> GetItems() => new List<string> {"sdf", "123"};
    }
}