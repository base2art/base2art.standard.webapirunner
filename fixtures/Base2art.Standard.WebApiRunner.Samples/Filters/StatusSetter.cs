﻿namespace Base2art.Standard.WebApiRunner.Samples.Filters
{
    using System;
    using System.Net;
    using Public.Resources;
    using Web.Filters.Specialized;

    public class StatusSetter : SetStatusCodeFilter<Person>
    {
        protected override void SetStatusCode(Person content, Action<HttpStatusCode> setAction)
        {
            setAction((HttpStatusCode) (400 + content.Name.Length));
        }
    }
}