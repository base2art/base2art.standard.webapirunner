﻿namespace Base2art.Standard.WebApiRunner.Samples.Filters
{
    using System;
    using System.Net;
    using Public.Resources;
    using Web.Filters.Specialized;

    public class RedirectSetter : RedirectFilter<Person>
    {
        protected override void SetRedirect(Person content, Action<HttpStatusCode, string> setRedirect)
        {
            setRedirect(HttpStatusCode.MovedPermanently, "/person/" + content.Name.Replace(" ", "-"));
        }
    }
}