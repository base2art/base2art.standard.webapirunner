﻿namespace Base2art.Standard.WebApiRunner.Samples.Filters
{
    using System.Collections.Generic;
    using System.Net;
    using Public.Resources;
    using Web.Filters.Specialized;

    public class CookieSetter : SetCookieFilter<Person>
    {
        protected override void SetCookies(Person content, IList<Cookie> cookies)
        {
            cookies.Add(new Cookie("name", content.Name));
        }
    }
}