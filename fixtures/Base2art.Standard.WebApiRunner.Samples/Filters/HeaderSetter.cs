﻿namespace Base2art.Standard.WebApiRunner.Samples.Filters
{
    using System.Collections.Generic;
    using Public.Resources;
    using Web.Filters.Specialized;

    public class HeaderSetter : SetHeadersFilter<Person>
    {
        protected override void SetHeaders(Person content, ICollection<KeyValuePair<string, string>> headers)
        {
            headers.Add(new KeyValuePair<string, string>("Authorization", "Bearer " + content.Name));
        }
    }
}