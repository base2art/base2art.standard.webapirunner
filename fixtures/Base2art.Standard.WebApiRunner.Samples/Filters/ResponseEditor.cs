﻿namespace Base2art.Standard.WebApiRunner.Samples.Filters
{
    using Public.Resources;
    using Web.Filters;
    using Web.Http;

    public class ResponseEditor : IResponseFilter<Person>
    {
        public void Filter(Person content, IHttpRequest request, IHttpResponse response)
        {
            response.Cookies.Append("name", content.Name);
            response.Redirect("/" + content.Name);
            response.StatusCode = 400 + content.Name.Length;
            response.Headers.Add("nameHeader", content.Name);
            response.Cookies.Append(".Session", "CADA2260359B4EF2BFD7527EBE938D56");
        }

        void IResponseFilter.Filter(object content, IHttpRequest request, IHttpResponse response)
            => this.Filter((Person) content, request, response);
    }
}