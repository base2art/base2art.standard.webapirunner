﻿namespace Base2art.Standard.WebApiRunner.SampleRegistration
{
    using System;
    using Microsoft.AspNetCore.Builder;
    using Web.App.Configuration;
    using Web.Server.Registration;

    public class FixtureRegistration : RegistrationBase
    {
        public FixtureRegistration()
        {
            Console.WriteLine("Middleware");
        }

        public override void RegisterNativeMiddleware(IApplicationBuilder app, IServerConfiguration config)
        {
            base.RegisterNativeMiddleware(app, config);
            Console.WriteLine("Middleware");
        }
    }
}