param ([string]$toolsPath)

if ([System.String]::IsNullOrWhitespace($toolsPath))
{
  $base = Resolve-Path "~"
  $toolsPath = "$base/.nuget/packages/base2art.webapirunner.server.runners.commandlineinterface/0.0.5-rc05/tools"
}

. "$($toolsPath)/Deployment.ps1"

$deploymentProcessor = New-Webapi-Deployment

$deploymentProcessor.bin('src/SampleConsumingSolution1/bin/Debug/netstandard2.0/')

# PAckaaging Example
#$deploymentProcessor.package("Newtonsoft.Json", "12.0.1")

# config example
#$deploymentProcessor.config("input-file.yaml", "environment", "destinationName.yaml")
#$deploymentProcessor.config("conf-processor.yaml", "Prod-Processor", "configuration.yaml")
$deploymentProcessor.config("conf/conf.yaml", "Prod", "configuration.yaml")

echo $deploymentProcessor.deploy()


Exit 0

